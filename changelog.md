==================================
MUDANÇAS NA VERSÃO 1.2.8 - Responsavel:Marlon
Implantação Portal ADM
Implementção primeiro acesso portal administrativo.
==================================
MUDANÇAS NA VERSÃO 1.2.7 - Responsavel:Marlon
Melhorias feature Solicitações de Portadores PT/EN
==================================
MUDANÇAS NA VERSÃO 1.2.6 - Responsavel:Rodolfo
Implantação capturatokenRe
Implementção primeiro acesso gestor estrangeiro.
==================================
MUDANÇAS NA VERSÃO 1.2.5 - Responsavel:Rodolfo
Correção erro encoding.
==================================
MUDANÇAS NA VERSÃO 1.2.4 - Responsavel:Rodolfo
Correção erro SLF4J.
==================================
MUDANÇAS NA VERSÃO 1.2.3 - Responsavel:Rodolfo
Correção de conflitos
==================================
MUDANÇAS NA VERSÃO 1.2.2 - Responsavel:Rodolfo
Melhorias no projeto.
Correção de bugs.
==================================
MUDANÇAS NA VERSÃO 1.2.1 - Responsavel:Rodolfo
Melhorias no projeto.
Correção de bugs.
==================================
MUDANÇAS NA VERSÃO 1.2.0 - Responsavel:Diogo
Melhorias no projeto.
==================================
MUDANÇAS NA VERSÃO 1.1.9 - Responsavel:Rodolfo
Melhorias no projeto.
Implementação metodo digitar letra.
==================================
MUDANÇAS NA VERSÃO 1.1.8 - Responsavel:Rodolfo
Melhorias no projeto.
==================================
MUDANÇAS NA VERSÃO 1.1.7 - Responsavel:Rodolfo
Melhorias no projeto
Alteração GitIgnore
Alteração nos runners do projeto adicionado (snippets,monochrome,stepNotifications,useFileNameCompatibleName)
==================================
MUDANÇAS NA VERSÃO 1.1.6 - Responsavel:Rodolfo
Criação Pages consulta de Limite.
==================================
MUDANÇAS NA VERSÃO 1.1.5 - Responsavel:Rodolfo
Correção vulnerabilidade Junit.
==================================
MUDANÇAS NA VERSÃO 1.1.4 - Responsavel:Rodolfo
Implementação Opera WebDriver.
Melhoria na estrutura do projeto remoção de warnings.
==================================
MUDANÇAS NA VERSÃO 1.1.3 - Responsavel:Rodolfo
Implementação login estrangeiro.
Melhoria na estrutura do projeto.
==================================
MUDANÇAS NA VERSÃO 1.1.2 - Responsavel:Diogo Rangel
Correção Script Consulta de Limites
==================================
MUDANÇAS NA VERSÃO 1.1.1 - Responsavel:Rodolfo.
Implementação FIXRE.
==================================
MUDANÇAS NA VERSÃO 1.1.0 - Responsavel:Rodolfo.
Implementação RE.
==================================
MUDANÇAS NA VERSÃO 1.0.16 - Responsavel:Gabriel.
Implementação feature 3193.
===================================
MUDANÇAS NA VERSÃO 1.0.15 - Responsavel:Rodolfo.
Implementação feature 3193.
===================================
MUDANÇAS NA VERSÃO 1.0.14 - Responsavel:Rodolfo.
Implementação feature 3185.
Correção POM.
===================================
MUDANÇAS NA VERSÃO 1.0.13 - Responsavel:Rodolfo.
Implementação feature 3185.
Correção conflitos.
===================================
MUDANÇAS NA VERSÃO 1.0.12 - Responsavel:Rodolfo.
Implementação feature 3182.
===================================
MUDANÇAS NA VERSÃO 1.0.11 - Responsavel:Rodolfo.
Correção caminho ALM.
===================================
MUDANÇAS NA VERSÃO 1.0.10 - Responsavel:Rodolfo.
Correção SMS Token.
===================================
MUDANÇAS NA VERSÃO 1.0.9 - Responsavel:Rodolfo.
Implementação feature Senha Cartão.
===================================
MUDANÇAS NA VERSÃO 1.0.8 - Responsavel:Rodolfo, Maycon, Marlon.
Correção features faturas e extratos.
Implementação Parcelamento de Fatura.
===================================
MUDANÇAS NA VERSÃO 1.0.7 - Responsavel:Rodolfo, Maycon, Marlon.
Correção features faturas e extratos.
===================================
MUDANÇAS NA VERSÃO 1.0.6 - Responsavel:Rodolfo, Maycon, Marlon.
Correção features LGPD e extratos.
===================================
MUDANÇAS NA VERSÃO 1.0.5  - Responsavel:Rodrigo.
Implementação feature LGPD.
===================================
MUDANÇAS NA VERSÃO 1.0.4 - Responsavel:Maycon, Marlon.
Fix Implementação RELATORIO
===================================
MUDANÇAS NA VERSÃO 1.0.3 - Responsavel:Maycon, Marlon.
Implementação Faturas. 
===================================
MUDANÇAS NA VERSÃO 1.0.2 - Responsavel:Rodolfo.
Fix Implementação Edge.
===================================
MUDANÇAS NA VERSÃO 1.0.1 - Responsavel:Rodolfo.
Fix CoreAutomação.
===================================
MUDANÇAS NA VERSÃO 1.0.0  - Responsavel:Rodolfo.
Portal PJ Automação.
Implementação Changelog.
Implementação Edge.
==============================================================================================================================================================================