package br.com.bradesco.pupj.pagesweb;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import br.com.bradesco.pupj.utilitarios.UtilsWeb;

public class Limites_ConsultaDeLimitePages extends UtilsWeb {

	@SuppressWarnings("unused")

	private WebDriver driverWeb;

	public Limites_ConsultaDeLimitePages(WebDriver driverWeb) {
		this.driverWeb = driverWeb;
		PageFactory.initElements(driverWeb, this);
	}

	// Campo nome do cart�o
	@FindBy(xpath = "//span[contains(.,'EMPRESARIAL PLATINUM VISA')]")
	public WebElement msgNomeCartao;
	// Campo valor de limite do cart�o
	@FindBy(xpath = "//span[contains(.,'Limite total R$ 30.000,00')]")
	public WebElement msgValorDeLimite;
	// Campo valor de limite do cart�o Representante Estrangeiro
	@FindBy(xpath = "//span[contains(.,'R$ 50.000,00')]")
	public WebElement msgValorDeLimiteestrangeiro;
	// Campo valor de limite total do cart�o hibrido
	@FindBy(xpath = "//span[contains(.,'Limite total R$ 5.000,00')]")
	public WebElement msgValorDeLimitehibrido;
	// Campo valor de limite total do cart�o hibrido cfi
	@FindBy(xpath = "//span[contains(.,'Limite total R$ 20.000,00')]")
	public WebElement msgValorDeLimitehibridocfi;
	// Campo valor de limite total do cart�o Centralizado
	@FindBy(xpath = "//span[contains(.,'Limite total R$ 15.000,00')]")
	public WebElement msgValorDeLimiteCentralizado;
	// Campo valor de limite total do cart�o Centralizado CFI
	@FindBy(xpath = "//span[contains(.,'Limite total R$ 20.000,00')]")
	public WebElement msgValorDeLimiteCentralizadoCFI;
	// Campo valor de limite disponivel do cart�o hibrido
	@FindBy(xpath = "//span[contains(.,'R$ 4.993,33')]")
	public WebElement msgValorDeLimitedisponivelhibrido;
	// Campo valor de limite disponivel do cart�o hibrido cfi
	@FindBy(xpath = "//span[contains(.,'R$ 20.000,00')]")
	public WebElement msgValorDeLimitedisponivelhibridocfi;
	// Campo valor de limite disponivel do cart�o Centralizado
	@FindBy(xpath = "//span[contains(.,'R$ 15.000,00')]")
	public WebElement msgValorDeLimitedisponivelCentralizado;
	// Campo valor de limite disponivel do cart�o Centralizado CFI
	@FindBy(xpath = "//span[contains(.,'R$ 1.000,00')]")
	public WebElement msgValorDeLimitedisponivelCentralizadoCFI;
	// Campo valor de limite do cart�o negativo
	@FindBy(xpath = "//span[contains(.,'R$ -2.000,00')]")
	public WebElement msgValorDeLimiteNegativo;
	// Status do cart�o bloqueio
	@FindBy(css = ".jsx-3843811591 > .jsx-466331245")
	public WebElement statusCartao;
	// Campo valor de limite do cart�o negativo
	@FindBy(xpath = "//label/div")
	public WebElement btnBloqueioTemporario;

	// Campo valor de limite do cart�o negativo
	@FindBy(xpath = "//span[contains(.,'Cart�o bloqueado.')]")
	public WebElement msgBloqueioTemporario;
	// Campo valor de limite do cart�o negativo
	@FindBy(xpath = "//span[contains(.,'Cart�o desbloqueado.')]")
	public WebElement msgDesbloqueioTemporario;

	public void clicarBtnBloqueioTemporario() {
		btnBloqueioTemporario.click();
	}
}