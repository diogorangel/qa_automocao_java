package br.com.bradesco.pupj.runners;

import org.junit.runner.RunWith;

import br.com.bradesco.automacaocartoes.core.ExtendedCucumberRunner;
import br.com.bradesco.pupj.utilitarios.SetUpTearDown;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberOptions.SnippetType;

@RunWith(ExtendedCucumberRunner.class)
@CucumberOptions(

		plugin = { "pretty", 
				"html:target/cucumber-report", 
				"junit:target/cucumber-report/junitResult.xml",
				"json:target/cucumber-report/jsonResult.json" },
		stepNotifications = true, 
		useFileNameCompatibleName = true, 
		monochrome = true,
		snippets = SnippetType.CAMELCASE,
		strict = true, 		
		features = {"src/test/resources/features/EN/000_CARTOESPJ-3172_Login_EN.feature",
					"src/test/resources/features/EN/004_CARTOESPJ-3186_ConsultaDeLimites_EN.feature",
					"src/test/resources/features/PT/017_CARTOESPJ-4873_OpenFinance_TrazerDados_PT.feature",
					"src/test/resources/features/EN/017_CARTOESPJ-4873_OpenFinance_TrazerDados_EN.feature"},
		glue = {"br.com.bradesco.pupj.steps", "br.com.bradesco.pupj.utilitarios" }, 
		
		//tags = {"@004_CARTOESPJ-3186_ConsultarLimitesENG" }
		//tags = {"@ConsultarLimite_GestorVerificarConsultaLimiteEmpresaRepresentanteEstrangeiroEN"}
		//tags = {"@ConsultarLimite_GestorVerificarConsultaLimiteComoRepresentanteEstrangeirodoPortadorHibridoEN"}
		//tags = {"@ConsultarLimite_GestorVerificarConsultaLimiteComoRepresentanteEstrangeirodoPortadorHibridoCFIEN"}
		//tags = {"@ConsultarLimite_GestorVerificarConsultaLimiteComoRepresentanteEstrangeirodoPortadorCentralizadoEN"}
		//tags = {"@ConsultarLimite_GestorVerificarConsultaLimiteComoRepresentanteEstrangeirodoPortadorCentralizadoCFIEN"}
		tags ={"@ConsultarLimite_RepresentanteEstrangeiroVerificaLimitesDetalhesdoPortadoremIngles"}
)

public class ConsultarLimitesEngTest extends SetUpTearDown {

}