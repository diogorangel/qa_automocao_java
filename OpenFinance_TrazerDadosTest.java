package br.com.bradesco.pupj.runners;

import br.com.bradesco.automacaocartoes.core.ExtendedCucumberRunner;
import br.com.bradesco.pupj.utilitarios.SetUpTearDown;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberOptions.SnippetType;
import org.junit.runner.RunWith;

@RunWith(ExtendedCucumberRunner.class)
@CucumberOptions(
		plugin = { "pretty", 
				"html:target/cucumber-report", 
				"junit:target/cucumber-report/junitResult.xml",
				"json:target/cucumber-report/jsonResult.json" },
		snippets = SnippetType.CAMELCASE, 	
		monochrome = true,
		stepNotifications = true, 
		useFileNameCompatibleName = true,
		features = {"src/test/resources/features/PT/000_CARTOESPJ-3172_Login_PT.feature",
					"src/test/resources/features/PT/017_CARTOESPJ-4873_OpenFinance_TrazerDados_PT.feature",},
		glue = {"br.com.bradesco.pupj.steps", "br.com.bradesco.pupj.utilitarios" }, 

		tags = {"@CARTOESPJ-4873FecharPopupTrazerDadosOpenFinanceComoGestor"}		
				
	    
)

public class OpenFinance_TrazerDadosTest extends SetUpTearDown {

}