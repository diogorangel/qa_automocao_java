#encoding: Cp1252
####################################################
########## CENARIOS LOGIN PORTAL PJ ################
####################################################
@000_CARTOESPJ-4873TrazerDadosOpenFinanceInglês
Feature: CARTOESPJ-3338 Open Finance- Consentimento da Recepção de Dados

  @CARTOESPJ-4873FecharPopupTrazerDadosOpenFinanceemIngles
  Scenario Outline: CARTOESPJ-4873 - Fechar Pop Up Trazer Dados do Open Finance em Ingles Como Gestor
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login em ingles <cpf> <senha>
    Then Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
    And Verifico o Logout com sucesso

    Examples: 
      | cpf          						 | senha      			| ambiente | rede      |
      | "06218670635" | "Br@12345" | "TI"    			 | "empresa" |