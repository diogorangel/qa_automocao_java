package br.com.bradesco.pupj.pagesweb;

import br.com.bradesco.pupj.utilitarios.UtilsWeb;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FeatureToogle_Page extends UtilsWeb {

	@SuppressWarnings("unused")
	private WebDriver driverWeb;

	public FeatureToogle_Page(WebDriver driverWeb) {
		this.driverWeb = driverWeb;
		PageFactory.initElements(driverWeb, this);
	}
	
	//lbl Forma de Pagamento
	@FindBy(xpath = "//h1[contains(.,'Forma de pagamento')]")
	public WebElement lblFormadePagamento;
	//lbl Forma de Pagamento em Ingles
	@FindBy(xpath = "//h1[contains(.,'Payment method')]")
	public WebElement lblFormadePagamentoEn;
	//lbl  Escolha Empresa 
	@FindBy(xpath = "//h2[contains(.,'Choose a company')]")
	public WebElement lblEscolhaEmpresa;
	//lbl Escolha Empresa em Ingles
	@FindBy(xpath = "//21[contains(.,'Escolha uma empresa')]")
	public WebElement lblEscolhaEmpresaEn;
	// Campo Login
    @FindBy(xpath = "//input[@id='formulario_login']")
    public WebElement txtLogin;
    // Campo Senha
    @FindBy(xpath = "//input[@id='formulario_senha']")
    public WebElement txtSenha;
    // Bot�o Acessar
    @FindBy(xpath = "//input[@id='formulario_efetuarLogin']")
    public WebElement btnAcessar;
    // Menu 'Plataforma PJ'
    @FindBy(xpath = "//div[@id='formulario_j_id_16']/ul/li[1]")
    public WebElement menuPlataformaPJ;
    // Submenu 'Gerenciar Funcionalidades'
    @FindBy(xpath = "//div[@id='formulario_j_id_1d']/div[2]/a")
    public WebElement menuGerenciarFuncionalidades;
    // Campo de busca 'Funcionalidade'
    @FindBy(xpath = "//input[@id='formulario_j_id_1h_nomeFuncionalidade']")
    public WebElement txtNomeFuncionalidade;
    // Campo de busca 'Mensagem'
    @FindBy(xpath = "//input[@id='formulario_j_id_1h_mensagemFuncionalidade']")
    public WebElement txtMensagemFuncionalidade;
    // Combo box 'Perfil'
    @FindBy(xpath = "//div[@id='formulario_j_id_1h_codigo_TipoAcesso']")
    public WebElement cbPerfil;
    // Combo box com as op��es do 'Perfil'
    @FindBy(xpath = "//ul[@id='formulario_j_id_1h_codigo_TipoAcesso_items']")
    public WebElement menuPerfil;
    // Op��o 'Todos' do combo box 'Perfil'
    @FindBy(xpath = "//li[@id='formulario_j_id_1h_codigo_TipoAcesso_0']")
    public WebElement optTodosCbPerfil;
    // Op��o 'Gestor' do combo box 'Perfil'
    @FindBy(xpath = "//li[@id='formulario_j_id_1h_codigo_TipoAcesso_1']")
    public WebElement optGestorCbPerfil;
    // Op��o 'Portador' do combo box 'Perfil'
    @FindBy(xpath = "//li[@id='formulario_j_id_1h_codigo_TipoAcesso_2']")
    public WebElement optPortadorCbPerfil;
    // Op��o 'Gestor Portador' do combo box 'Perfil'
    @FindBy(xpath = "//li[@id='formulario_j_id_1h_codigo_TipoAcesso_3']")
    public WebElement optGestorPortadorCbPerfil;
    // Op��o 'Gestor Estrangeiro' do combo box 'Perfil'
    @FindBy(xpath = "//li[@id='formulario_j_id_1h_codigo_TipoAcesso_4']")
    public WebElement optGestorEstrangeiroCbPerfil;
    // Op��o 'Sem Perfil' do combo box 'Perfil'
    @FindBy(xpath = "//li[@id='formulario_j_id_1h_codigo_TipoAcesso_5']")
    public WebElement optSemPerfilCbPerfil;
    // Combo box 'Plataforma'
    @FindBy(xpath = "//div[@id='formulario_j_id_1h_codigoTipoPlataforma']")
    public WebElement cbPlataforma;
    // Combo box com as op��es da 'Plataforma'
    @FindBy(xpath = "//ul[@id='formulario_j_id_1h_codigoTipoPlataforma_items']")
    public WebElement menuPlataforma;
    // Op��o 'Todos' do combo box 'Plataforma'
    @FindBy(xpath = "//li[@id='formulario_j_id_1h_codigoTipoPlataforma_0']")
    public WebElement optTodosCbPlataforma;
    // Op��o 'Web' do combo box 'Plataforma'
    @FindBy(xpath = "//li[@id='formulario_j_id_1h_codigoTipoPlataforma_1']")
    public WebElement optWebCbPlataforma;
    // Op��o 'Mobile' do combo box 'Plataforma'
    @FindBy(xpath = "//li[@id='formulario_j_id_1h_codigoTipoPlataforma_2']")
    public WebElement optMobileCbPlataforma;
    // Combo box 'Status' da tela de Pesquisa
    @FindBy(xpath = "//div[@id='formulario_j_id_1h_codigoStatus']")
    public WebElement cbStatusPesquisa;
    // Bot�o Pesquisar Funcionalidade
    @FindBy(xpath = "//button[@id='formulario_j_id_1h_btnConsultar']")
    public WebElement btnPesquisar;
    // Tabela de resultado das funcionalidades
    @FindBy(xpath = "//tbody[@id='formulario_j_id_2n_resultadoConsulta_data']")
    public WebElement tblResultadoConsulta;
    // Bot�o de editar da tabela de resultados de funcionalidade
    @FindBy(xpath = "//tbody[@id='formulario_j_id_2n_resultadoConsulta_data']/tr[1]/td/button")
    public WebElement btnEditarFuncionalidade1Consulta;
 // Bot�o de editar da tabela de resultados de funcionalidade bloqueio
    @FindBy(xpath = "//tbody[@id='formulario_j_id_2n_resultadoConsulta_data']/tr[3]/td/button")
    public WebElement btnEditarFuncionalidade3Consulta;
    // Label 'Manuten��o de Funcionalidades'
    @FindBy(xpath = "//span[@id='formulario_lbl_manutencao']")
    public WebElement lblManutencaoFuncionalidade;
    // Combo box 'Status' da tela de Manute��o de Funcionalidades
    @FindBy(xpath = "//label[@id='formulario_crudForm_statusFUNC_label']")
    public WebElement cbStatusManutencao;
    // Combo box com as op��es do 'Status' da tela Manuten��o de Funcionalidades
    @FindBy(xpath = "//ul[@id='formulario_crudForm_statusFUNC_items']")
    public WebElement menuStatus;
    // Op��o 'Ativo' do combo box 'Status'
    @FindBy(xpath = "//li[@id='formulario_crudForm_statusFUNC_0']")
    public WebElement optAtivoCbStatus;
    // Op��o 'Inativo' do combo box 'Status'
    @FindBy(xpath = "//li[@id='formulario_crudForm_statusFUNC_1']")
    public WebElement optInativoCbStatus;
    // Op��o 'Ocultar menu' do combo box 'Status'
    @FindBy(xpath = "//li[@id='formulario_crudForm_statusFUNC_2']")
    public WebElement optOcultarMenuCbStatus;
    // Bot�o 'Salvar' da tela Manuten��o de Funcionalidades
    @FindBy(xpath = "//button[@id='formulario_crudForm_j_id_2g']")
    public WebElement btnSalvar;
    // Se��o de Mensagens da Tela Manuten��o de Funcionalidades
    @FindBy(xpath = "//div[@id='formulario_messages']")
    public WebElement frmMensagens;
    //Mensagem de Indisponibilidade de qualquer servi�o de uma Feature Toogle
    @FindBy(xpath ="//span[contains(.,'Servi�o indispon�vel')]")
    public WebElement lblServicoIndisponivel;
    //Mensagem de Indisponibilidade de qualquer servi�o de uma Feature Toogle
    @FindBy(xpath ="//p[contains(.,'Servi�o Indispon�vel Temporariamente.')]")
    public WebElement msgServicoIndisponivel;
    //Tela Consulta de Limites Feature Toogle Ativa
    @FindBy(xpath ="//h1[contains(.,'Consulta de limites')]")
    public WebElement lblConsultaLimites;
 // Campo valor de limite Total do Cart�o Feature Toogle Ativa
 	@FindBy(xpath = "//span[normalize-space()='Limite total R$ 12.000,00']")
 	public WebElement lblLimiteTotal;
 	// Campo verifica��o N�mero do Cart�o Portador Feature Toogle Ativa
 	@FindBy(xpath = "//span[normalize-space()='Final 1186']")
 	public WebElement dataCartaoPortador;
 	// Campo verifica��o Nome do Cart�o Portador Feature Toogle Ativa
 	@FindBy(xpath = "//span[normalize-space()='AMEX CORPORATE GOLD']")
 	public WebElement dataNomedocartaoPortador;
 	// Campo verifica��o Titulo Status Portador Feature Toogle Ativa
 	@FindBy(xpath = "//div[@data-test='DataItem_Label' and contains(., 'Status')]")
 	public WebElement lblStatusPortador;
 	// Campo verifica��o Data Status Feature Toogle Ativa
 	@FindBy(xpath = "//div[@data-test='DataItem_Data' and contains(., 'Ativo')]")
 	public WebElement dataStatusPortador;
 	// Campo verifica��o label bloqueio tempor�rio Feature Toogle Ativa
 	@FindBy(xpath = "//div[@data-test='DataItem_Label' and contains(., 'Bloqueio tempor�rio')]")
 	public WebElement bloqueiotemporarioportador;
 	// Campo verifica��o label melhor dia de compra Feature Toogle Ativa
 	@FindBy(xpath = "//div[@data-test='DataItem_Label' and contains(., 'Melhor dia de compra')]")
 	public WebElement lbldiadecompra;
 	// Campo verifica��o Data melhor dia de compra Feature Toogle Ativa
 	@FindBy(xpath = "//div[@data-test='DataItem_Data' and contains(., 'Dia 29')]")
 	public WebElement datadiadecompra;
 	// Campo verifica��o lbl Limite Disponivel Feature Toogle Ativa
 	@FindBy(xpath = "//span[normalize-space()='Limite dispon�vel']")
 	public WebElement lblLimiteDisponivel;
 	// Campo verifica��o data Limite Disponivel Feature Toogle Ativa
 	@FindBy(xpath = "//span[normalize-space()='R$ 11.795,00']")
 	public WebElement dataLimiteDisponivel;
 	// Campo verifica��o lbl Limite Disponivel Feature Toogle Ativa
 	@FindBy(xpath = "//span[normalize-space()='Limite utilizado']")
 	public WebElement lblLimiteUtilizado;
 	// Campo verifica��o data Limite Utilizado Feature Toogle Ativa
 	@FindBy(xpath = "//span[normalize-space()='R$ 205,00']")
 	public WebElement dataLimiteUtilizado;
 	// Campo verifica��o lbl Limite dispon�vel Feature Toogle Ativa
 	@FindBy(xpath = "//span[normalize-space()='Limite dispon�vel']")
 	public WebElement lblLimitedisponivel;
 	// Campo verifica��o lbl Compra nacional Feature Toogle Ativa
 	@FindBy(xpath = "//span[normalize-space()='Compra nacional']")
 	public WebElement lblCompranacional;
 	// Campo verifica��o data Compra nacional Feature Toogle Ativa
 	@FindBy(xpath = "//span[normalize-space()='R$ 3.875,00']")
 	public WebElement dataCompranacional;
 	// Campo valor de limite Total do Cart�o nacional Feature Toogle Ativa
 	@FindBy(xpath = "//span[normalize-space()='Limite total R$ 4.080,00']")
 	public WebElement dataLimiteTotalnacional;
 	// Campo verifica��o lbl saque nacional Feature Toogle Ativa
 	@FindBy(xpath = "//span[normalize-space()='Saque nacional']")
 	public WebElement lblSaquenacional;
 	// Campo verifica��o data Saque nacional Feature Toogle Ativa
 	@FindBy(xpath = "//span[normalize-space()='R$ 1.346,40']")
 	public WebElement dataSaquenacional;
 	// Campo valor de data Saque Nacional do Cart�o nacional Feature Toogle Ativa
 	@FindBy(xpath = "//span[normalize-space()='Limite total R$ 1.346,00']")
 	public WebElement dataLimiteTotalNacionalPortador;
 	// Campo verifica��o lbl Compra no exterior Feature Toogle Ativa
 	@FindBy(xpath = "//span[normalize-space()='Compra no exterior']")
 	public WebElement lblCompraexterior;
 	// Campo verifica��o data Compra no exterior Feature Toogle Ativa
 	@FindBy(xpath = "//span[normalize-space()='US$ 1.619,63 (R$ 7.920,00)']")
 	public WebElement dataCompraexterior;
 	// Campo valor de data Compra Exterior Feature Toogle Ativa
 	@FindBy(xpath = "//span[normalize-space()='Limite total US$ 1.619,63(R$ 7.920,00)']")
 	public WebElement dataCompraexteriornacional;
 	// Campo verifica��o lbl saque no exterior Feature Toogle Ativa
 	@FindBy(xpath = "//span[normalize-space()='Saque no exterior']")
 	public WebElement lblSaqueexterior;
 	// Campo verifica��o data Saque no exterior Feature Toogle Ativa
 	@FindBy(xpath = "//span[normalize-space()='US$ 0,00 (R$ 0,00)']")
 	public WebElement dataSaqueexterior;
 	// Campo valor de data Limite no Exterior Feature Toogle Ativa
 	@FindBy(xpath = "//span[normalize-space()='Limite total US$ 1.085,07(R$ 5.306,00)']")
 	public WebElement datalimitetotalexterior;
 	//Lbl Solicita��es Validar Feature Toogle Consultar Limites Inativa
 	@FindBy(xpath = "//span[contains(.,'Solicita��es')]")
 	public WebElement lblSolicitacoes;
 	
    public void clicarBtnSalvar() {
        btnSalvar.click();
    }

    public void clicarOptAtivoCbStatus() {
        optAtivoCbStatus.click();
    }

    public void clicarOptInativoCbStats() {
        optInativoCbStatus.click();
    }

     public void clicarOptOcultarMenuCbStatus() {
         optOcultarMenuCbStatus.click();
     }

    public void clicarCbStatusManutencao() {
        cbStatusManutencao.click();
    }

    public void clicarEditarFuncionalidade1Consulta() {
        btnEditarFuncionalidade1Consulta.click();
    }
    public void clicarEditarFuncionalidade3Consulta() {
        btnEditarFuncionalidade3Consulta.click();
    }

    public void clicarPesquisar() {
        btnPesquisar.click();
    }

    public void clicarComboPerfil() {
        cbPerfil.click();
    }

    public void clicarOptTodosCbPerfil() {
        optTodosCbPerfil.click();
    }

    public void clicarOptGestorCbPerfil() {
        optGestorCbPerfil.click();
    }

    public void clicarOptPortadorCbPerfil() {
        optPortadorCbPerfil.click();
    }

    public void clicarOptGestorPortadorCbPerfil() {
        optGestorPortadorCbPerfil.click();
    }

    public void clicarOptGestorEstrangeiroCbPerfil() {
        optGestorEstrangeiroCbPerfil.click();
    }

    public void clicarOptSemPerfilCbPerfil() {
        optSemPerfilCbPerfil.click();
    }

    public void clicarComboPlataforma() {
        cbPlataforma.click();
    }

    public void clicarOptTodosCbPlataforma() {
        optTodosCbPlataforma.click();
    }

    public void clicarOptWebCbPlataforma() {
        optWebCbPlataforma.click();
    }

    public void clicarOptMobileCbPlataforma() {
        optMobileCbPlataforma.click();
    }

    public void informarFuncionalidade(String funcionalidade) {
        txtNomeFuncionalidade.sendKeys(funcionalidade);
    }

    public void informarMensagem(String mensagem) {
        txtMensagemFuncionalidade.sendKeys(mensagem);
    }

    public void clicarMenuGerenciarFuncionalidades() {
        menuGerenciarFuncionalidades.click();
    }

    public void clicarMenuPlataformaPJ() {
        menuPlataformaPJ.click();
    }

    public void clicarAcessar() {
        btnAcessar.click();
    }

    public void informarLogin(String login) {
        txtLogin.sendKeys(login);
    }

    public void informarSenha(String senha) {
        txtSenha.sendKeys(senha);
    }

    public void focarBotaoAcessar() {
        txtSenha.sendKeys("");
    }
}