package br.com.bradesco.pupj.steps;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import br.com.bradesco.automacaocartoes.core.DriverFactory;
import br.com.bradesco.pupj.pagesweb.CabecalhoRodape_CabecalhoPage;
import br.com.bradesco.pupj.pagesweb.CabecalhoRodape_RodapePage;
import br.com.bradesco.pupj.pagesweb.EsqueciMinhaSenha_esqueci_minha_senhaPage;
import br.com.bradesco.pupj.pagesweb.Limites_ConsultaDeLimitePages;
import br.com.bradesco.pupj.pagesweb.Limites_ConsultaDeLimite_DetalhesPages;
import br.com.bradesco.pupj.pagesweb.Limites_ConsultaDeLimite_EmpresasPage;
import br.com.bradesco.pupj.pagesweb.Limites_ConsultaDeLimite_PortadoresPage;
import br.com.bradesco.pupj.pagesweb.Limites_ConsultaDeLimite_ProdutosPage;
import br.com.bradesco.pupj.pagesweb.Login_LoginPage;
import br.com.bradesco.pupj.pagesweb.OpenFinance_TrazerDados_Page;
import br.com.bradesco.pupj.utilitarios.UtilsWeb;
import cucumber.deps.com.thoughtworks.xstream.InitializationException;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;

@SuppressWarnings({ "unused" })
public class OpenFinance_TrazerDadosSteps extends UtilsWeb {

	public static WebDriver driverWeb = DriverFactory.getDriverWeb();
	
	Login_LoginPage login = new Login_LoginPage(driverWeb);
	CabecalhoRodape_CabecalhoPage cabecalhoLogado = new CabecalhoRodape_CabecalhoPage(driverWeb);
	CabecalhoRodape_RodapePage rodape = new CabecalhoRodape_RodapePage(driverWeb);
	Limites_ConsultaDeLimitePages limitePortador = new Limites_ConsultaDeLimitePages(driverWeb);
	Limites_ConsultaDeLimite_DetalhesPages limiteGestor = new Limites_ConsultaDeLimite_DetalhesPages(driverWeb);
	Limites_ConsultaDeLimite_PortadoresPage pageLimitePortador = new Limites_ConsultaDeLimite_PortadoresPage(driverWeb);
	Limites_ConsultaDeLimite_ProdutosPage pageLimiteProduto = new Limites_ConsultaDeLimite_ProdutosPage(driverWeb);
	Limites_ConsultaDeLimite_EmpresasPage pageLimite_EmpresasPage = new Limites_ConsultaDeLimite_EmpresasPage(driverWeb);
	OpenFinance_TrazerDados_Page  pageOpenFinance_TrazerDados= new OpenFinance_TrazerDados_Page(driverWeb);
	
	@Then("Fecho o Pop-up Trazer Dados Open Finance como Gestor")
	public void fecho_popup_trazerdados_openfinance() throws InterruptedException   {
		tirarScreenshot("Tela com o Pop-up (Apresenta��o) Trazer Dados Open Finance");
		espera(10);
		esperaElemento(pageOpenFinance_TrazerDados.fecharpopupOpenFinance, 60);
		Assert.assertTrue(elementoExiste(pageOpenFinance_TrazerDados.fecharpopupOpenFinance));
		pageOpenFinance_TrazerDados.fecharpopupTrazerDados();
		tirarScreenshot("Tela Inicial Portal PJ P�s Fechar Pop-Up Trazer Dados Open Finance");
		espera(5);
	}


}