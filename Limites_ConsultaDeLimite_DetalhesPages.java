package br.com.bradesco.pupj.pagesweb;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import br.com.bradesco.pupj.utilitarios.UtilsWeb;

public class Limites_ConsultaDeLimite_DetalhesPages extends UtilsWeb {

	@SuppressWarnings("unused")

	private WebDriver driverWeb;

	public Limites_ConsultaDeLimite_DetalhesPages(WebDriver driverWeb) {
		this.driverWeb = driverWeb;
		PageFactory.initElements(driverWeb, this);
	}
	// Titulo pagina
	@FindBy(xpath = "//h1[contains(.,'Consulta de Limite')]")
	public WebElement msgTitulo;
	// Titulo pagina
	@FindBy(xpath = "//h1[contains(.,'Limit Consultation')]")
	public WebElement msgTitleEn;
	// Campo valor de limite do cart�o
	@FindBy(xpath = "//span[contains(.,'Limite total R$ 30.000,00')]")
	public WebElement msgValorDeLimite;
	// Campo valor de limite do cart�o Representante Estrangeiro
	@FindBy(xpath = "//div[@data-test='DataItem_Data' and contains(., 'R$')]")
	public WebElement lblValorDeLimiteempresa;
	// Campo cnpj NOME
	@FindBy(xpath = "//input[@id='nomePortador']")
	public WebElement txtCnpjEmpresa;
	// bOT�O PESQUISAR
	@FindBy(css = ".rcg-row:nth-child(19)")
	public WebElement btnPesquisar;
	// bOT�O PESQUISAR CNPJ
	@FindBy(css = ".jsx-2302210378 > svg")
	public WebElement btnPesquisarCnpj;
	// Campo limite da empresa
	@FindBy(xpath = "//span[contains(.,'Limite da empresa')]")
	public WebElement lblLimiteEmpresa;
	// Campo limite da empresa
	@FindBy(xpath = "//span[contains(.,'R$ 320.000,00')]")
	public WebElement lblValorLimiteEmpresa;
	// Filtro produto
	@FindBy(css = ".jsx-3327037891 > svg")
	public WebElement listaFiltroProduto;
	// Filtro produto Elo
	@FindBy(xpath = "//a[contains(text(),'Elo')]")
	public WebElement listaFiltroProdutoElo;
	// Filtro produto Visa
	@FindBy(xpath = "//a[contains(text(),'Visa')]")
	public WebElement listaFiltroProdutoVisa;
	// Filtro produto Mastercard
	@FindBy(xpath = "//a[contains(text(),'Mastercard')]")
	public WebElement listaFiltroProdutoMastercard;
	// Filtro produto Amex
	@FindBy(xpath = "//a[contains(text(),'Amex')]")
	public WebElement listaFiltroProdutoAmex;
	// Nome do Cart�o elo Valida��o
	@FindBy(xpath = "//h1[contains(.,'CARTAO AEREO ELO MAIS')]")
	public WebElement lblElo;
	// Nome do Cart�o Visa Valida��o
	@FindBy(xpath = "//h1[contains(.,'EMPRESARIAL PLATINUM VISA')]")
	public WebElement lblVisa;
	// Nome do Cart�o Amex Valida��o
	@FindBy(xpath = "//h1[contains(.,'AMEX CORPORATE')]")
	public WebElement lblAmex;
	// Nome do Cart�o Mastercard Valida��o
	@FindBy(xpath = "//h1[contains(.,'CARTAO PASSAGEM BRADESCO')]")
	public WebElement lblMastercard;
	// limite negativo Gestor
	@FindBy(xpath = "//p[contains(.,'Total Dispon�vel: R$ -2.000,00')]")
	public WebElement lblLimiteNegativoGestor;
	// Filtro Portadores 
	@FindBy(xpath = "//b[contains(.,'Portadores')]")
	public WebElement lblPortadores;
	// Filtro Portadores Ingl�s
	@FindBy(xpath = "//b[contains(.,'Carholders')]")
	public WebElement lblPortadoresIngles;
	// Filtro Empresas Portugu�s
	@FindBy(xpath = "//b[contains(.,'Empresas')]")
	public WebElement lblEmpresas;
	// Filtro Empresas Ingl�s
	@FindBy(xpath = "//b[contains(.,'Companies')]")
	public WebElement lblEmpresasIngles;
	// Filtro Produtos Portugu�s
	@FindBy(xpath = "//b[contains(.,'Produtos')]")
	public WebElement lblProdutos;
	// Filtro Produtos Ingl�s
	@FindBy(xpath = "//b[contains(.,'Products')]")
	public WebElement lblProdutosIngles;
	// Clicar em Portadores
	@FindBy(xpath = "//a[@href=/consulta/portadores/]")
	public WebElement hrefPortadores;
	// Clicar em Produtos
	@FindBy(xpath = "//a[@href=/consulta/produtos/]")
	public WebElement hrefProdutos;
	// Clicar em Empresas
	@FindBy(xpath = "//a[@href=/consulta/empresas/]")
	public WebElement hrefEmpresas;
	// txt compid
	@FindBy(xpath = "//input[@id='nomePortador']")
	public WebElement txtCompid;
	// txt txtProduto
	@FindBy(xpath = "//input[@id='nomePortador']")
	public WebElement txtProduto;
	// lbl visa
	@FindBy(xpath = "//span[contains(.,'EMPRESARIAL PLATINUM VISA')]")
	public WebElement lblTipoDeCartao;
	// lbl compid
	@FindBy(xpath = "//span[contains(.,'606943')]")
	public WebElement lblcompID;
	// lbl pesquisa
	@FindBy(css = ".jsx-2302210378 > svg")
	public WebElement btnPesquisa;
	// txt CNPJ
	@FindBy(xpath = "//input[@id='nomePortador']")
	public WebElement txtCNPJ;
	// txt CNPJ
	@FindBy(xpath = "//span[contains(.,'MASSA MELHORIA EXTRATO')]")
	public WebElement lblNomeEmpresa;
	// lbl CNPJ
	@FindBy(xpath = "//span[contains(.,'78.139.464/0001-70')]")
	public WebElement lblCnpj;
	// lbl erro CNPJ
	@FindBy(xpath = "//p[contains(.,'O CNPJ informado n�o � v�lido')]")
	public WebElement lblerroCnpj;
	// lbl erro empresa
	@FindBy(xpath = "//span[contains(.,'Empresa n�o encontrada')]")
	public WebElement lblerroempresa;
	// lbl erro empresa
	@FindBy(xpath = "//p[contains(.,'Confirme se o nome da empresa foi digitado corretamente.')]")
	public WebElement lblSuberroempresa;
	// lbl erro produto
	@FindBy(xpath = "//span[contains(.,'Produto n�o encontrado')]")
	public WebElement lblErroProduto;
	// lbl erro produto
	@FindBy(xpath = "//p[contains(.,'Confirme se o nome ou Comp ID do produto foi digitado corretamente.')]")
	public WebElement lblSubErroProduto;
	// lbl erro CPF
	@FindBy(xpath = "//p[contains(.,'O CPF informado n�o � v�lido')]")
	public WebElement lblErroCPF;
	// lbl erro PORTADOR
	@FindBy(xpath = "//span[contains(.,'Portador n�o encontrado')]")
	public WebElement lblErroPortador;
	// lbl erro mensagem PORTADOR atual
	@FindBy(xpath = "//p[contains(.,'Confirme se o nome ou CPF do portador foi digitado corretamente.')]")
	public WebElement lblSubErroPortador;
	////tag �ltima atualiza��o em escrita
	@FindBy(xpath ="//time[contains(.,'�ltima atualiza��o em: 21/03/2022 �s 16h37')]")
	public WebElement lbldatapage;
	// btn voltar
	@FindBy(css = ".secondary")
	public WebElement btnvoltar;
	
	// lbl erro mensagem antiga
	//@FindBy(xpath = "//p[contains(.,'Confirme se o nome ou CPF do portador foi digitado corretamente ou verifique os filtros da pesquisa.')]")
	//public WebElement lblSubErroPortador;
	
	//tag �ltima atualiza��o em: 
	//@FindBy(xpath ="//time/b")
	//public WebElement datapage;


	public void clicarTitulo() {
		msgTitulo.click();
	}
	
	public void clicarPortadores() {
		hrefPortadores.click();
	}
	
	public void clicarProdutos() {
		hrefProdutos.click();
	}
	
	public void clicarEmpresas() {
		hrefEmpresas.click();
	}

	public void clickTitle() {
		msgTitleEn.click();
	}
	
	public void clicarMenuPortadores() {
		lblPortadores.click();
	}
	
	public void clicarMenuPortadoresIngles() {
		lblPortadoresIngles.click();
	}
	
	public void clicarMenuProdutosIngles() {
		lblProdutosIngles.click();
	}
	
	public void clicarMenuProdutos() {
		lblProdutos.click();
	}
	
	public void clicarMenuEmpresas() {
		lblEmpresas.click();
	}
	
	public void clicarMenuEmpresasIngles() {
		lblEmpresasIngles.click();
	}

	public void informarTxtCpfNome(String nomeEmpresa) {
		txtCnpjEmpresa.sendKeys(nomeEmpresa);
	}

	public void clicarBtnPesquisar() {
		btnPesquisar.click();
	}

	public void clicarBtnPesquisarCnpj() {
		btnPesquisarCnpj.click();
	}

	public void clicarListaFiltroProduto() {
		listaFiltroProduto.click();
	}

	public void clicarListaFiltroProdutoElo() {
		listaFiltroProdutoElo.click();
	}

	public void clicarListaFiltroProdutoVisa() {
		listaFiltroProdutoVisa.click();
	}

	public void clicarListaFiltroProdutoAmex() {
		listaFiltroProdutoAmex.click();
	}

	public void clicarListaFiltroProdutoMastercard() {
		listaFiltroProdutoMastercard.click();
	}

	public void clicarMenuPortadoresLimites() {
		lblPortadores.click();
	}

	public void digitarCompid(String compID) {
		txtCompid.sendKeys(compID);
	}

	public void digitarProdutoInexistente(String produtos) {
		txtProduto.sendKeys(produtos);
	}

	public void digitarNomeProduto(String nomeProduto) {
		txtProduto.sendKeys(nomeProduto);
	}

	public void clicarBtnPesquisarCompID() {
		btnPesquisa.click();
	}

	public void clicarBtnPesquisarProduto() {
		btnPesquisa.click();
	}

	public void digitarCNPJ(String cnpj) {
		txtCNPJ.sendKeys(cnpj);
	}

	public void clicarBtnPesquisarcnpj() {
		btnPesquisa.click();
	}

	public void clicarBtnPesquisarcnpjEnter() {
		driverWeb.findElement(By.id("nomePortador")).sendKeys(Keys.ENTER);
	}

	public void clicarBtnPesquisarProdutoEnter() {
		driverWeb.findElement(By.id("nomePortador")).sendKeys(Keys.ENTER);
	}

	public void clicarBtnPesquisarPortadorEnter() {
		driverWeb.findElement(By.id("nomePortador")).sendKeys(Keys.ENTER);
	}

	public void clicarBtnvoltar() {
		btnvoltar.click();
	}

}