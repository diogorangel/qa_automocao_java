#encoding: iso-8859-1
################################################################
################# CENARIOS DE FEATURE TOGGLE ###################
################################################################

@016_FeatureToggle_PT
Feature: 016_FeatureToggle_PT

  #Aviso Viagem ativado
  @FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleAtivadoNacional
  Scenario Outline: CTWR 08-09 Validar funcionalidade Aviso Viagem com Feature Toggle ativado gestor nacional
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Aviso Viagem

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "aviso_viagem" | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleAtivadoRE
  Scenario Outline: CTWR 08-09 Validar funcionalidade Aviso Viagem com Feature Toggle ativado gestor estrangeiro
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Aviso Viagem

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "aviso_viagem" | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleAtivadoPortador
  Scenario Outline: CTWR 08-09 Validar funcionalidade Aviso Viagem com Feature Toggle ativado portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Aviso Viagem

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "aviso_viagem" | "portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleAtivadoGP
  Scenario Outline: CTWR 08-09 Validar funcionalidade Aviso Viagem com Feature Toggle ativado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Aviso Viagem

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "aviso_viagem" | "gestor portador" | "web"      |

  #Aviso Viagem inativado
  @FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleInativadoNacional
  Scenario Outline: CTWR 08-11 Validar funcionalidade Aviso Viagem com Feature Toggle inativado gestor nacional
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Aviso Viagem
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "aviso_viagem" | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleInativadoRE
  Scenario Outline: CTWR 08-11 Validar funcionalidade Aviso Viagem com Feature Toggle inativado gestor estrangeiro
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Aviso Viagem
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "aviso_viagem" | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleInativadoPortador
  Scenario Outline: CTWR 08-11 Validar funcionalidade Aviso Viagem com Feature Toggle inativado portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Aviso Viagem
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "aviso_viagem" | "portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleInativadoGP
  Scenario Outline: CTWR 08-11 Validar funcionalidade Aviso Viagem com Feature Toggle inativado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Aviso Viagem
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "aviso_viagem" | "gestor portador" | "web"      |

  #Aviso Viagem ocultado
  @FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleOcultadoNacional
  Scenario Outline: CTWR 08-10 Validar funcionalidade Aviso Viagem com Feature Toggle ocultado gestor nacional
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Aviso Viagem
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "aviso_viagem" | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleOcultadoRE
  Scenario Outline: CTWR 08-10 Validar funcionalidade Aviso Viagem com Feature Toggle ocultado gestor estrangeiro
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Aviso Viagem
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "aviso_viagem" | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleOcultadoPortador
  Scenario Outline: CTWR 08-10 Validar funcionalidade Aviso Viagem com Feature Toggle ocultado portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Aviso Viagem
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "aviso_viagem" | "portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleOcultadoGP
  Scenario Outline: CTWR 08-10 Validar funcionalidade Aviso Viagem com Feature Toggle ocultado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Aviso Viagem
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "aviso_viagem" | "gestor portador" | "web"      |


  #Boletos ativado
  @FeatureToggle_ValidarFuncionalidadeBoletosComFeatureToggleAtivadoNacional
  Scenario Outline: CTWR 08-18 Validar funcionalidade Boletos com Feature Toggle ativado gestor nacional
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Boletos

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "boleto"       | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeBoletosComFeatureToggleAtivadoRE
  Scenario Outline: CTWR 08-18 Validar funcionalidade Boletos com Feature Toggle ativado gestor estrangeiro
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Boletos

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "boleto"       | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeBoletosComFeatureToggleAtivadoGP
  Scenario Outline: CTWR 08-18 Validar funcionalidade Boletos com Feature Toggle ativado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Boletos

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "boleto"       | "gestor portador" | "web"      |

  #Boletos inativado
  @FeatureToggle_ValidarFuncionalidadeBoletosComFeatureToggleInativadoNacional
  Scenario Outline: CTWR 08-19 Validar funcionalidade Boletos com Feature Toggle inativado gestor nacional
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Boletos
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "boleto"       | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeBoletosComFeatureToggleInativadoRE
  Scenario Outline: CTWR 08-19 Validar funcionalidade Boletos com Feature Toggle inativado gestor estrangeiro
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Boletos
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "boleto"       | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeBoletosComFeatureToggleInativadoGP
  Scenario Outline: CTWR 08-19 Validar funcionalidade Boletos com Feature Toggle inativado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Boletos
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "boleto"       | "gestor portador" | "web"      |

  #Boletos ocultado
  @FeatureToggle_ValidarFuncionalidadeBoletosComFeatureToggleOcultadoNacional
  Scenario Outline: CTWR 08-20 Validar funcionalidade Boletos com Feature Toggle ocultado gestor nacional
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Boletos
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "boleto"       | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeBoletosComFeatureToggleOcultadoRE
  Scenario Outline: CTWR 08-20 Validar funcionalidade Boletos com Feature Toggle ocultado gestor estrangeiro
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Boletos
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "boleto"       | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeBoletosComFeatureToggleOcultadoGP
  Scenario Outline: CTWR 08-20 Validar funcionalidade Boletos com Feature Toggle ocultado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Boletos
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "boleto"       | "gestor portador" | "web"      |


  #LGPD ativado
  @FeatureToggle_ValidarFuncionalidadeLGPDComFeatureToggleAtivadoNacional
  Scenario Outline: CTWR 08-26 Validar funcionalidade LGPD com Feature Toggle ativado gestor nacional
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de LGPD

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "lgpd"         | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeLGPDComFeatureToggleAtivadoPortador
  Scenario Outline: CTWR 08-26 Validar funcionalidade LGPD com Feature Toggle ativado portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de LGPD

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "lgpd"         | "portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeLGPDComFeatureToggleAtivadoGP
  Scenario Outline: CTWR 08-26 Validar funcionalidade LGPD com Feature Toggle ativado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de LGPD

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "lgpd"         | "gestor portador" | "web"      |

  #LGPD inativado
  @FeatureToggle_ValidarFuncionalidadeLGPDComFeatureToggleInativadoNacional
  Scenario Outline: CTWR 08-27 Validar funcionalidade LGPD com Feature Toggle inativado gestor nacional
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de LGPD
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "lgpd"         | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeLGPDComFeatureToggleInativadoPortador
  Scenario Outline: CTWR 08-27 Validar funcionalidade LGPD com Feature Toggle inativado portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de LGPD
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "lgpd"         | "portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeLGPDComFeatureToggleInativadoGP
  Scenario Outline: CTWR 08-27 Validar funcionalidade LGPD com Feature Toggle inativado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de LGPD
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "lgpd"         | "gestor portador" | "web"      |

  #LGPD ocultado
  @FeatureToggle_ValidarFuncionalidadeLGPDComFeatureToggleOcultadoNacional
  Scenario Outline: CTWR 08-28 Validar funcionalidade LGPD com Feature Toggle ocultado gestor nacional
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de LGPD
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "lgpd"         | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeLGPDComFeatureToggleOcultadoPortador
  Scenario Outline: CTWR 08-28 Validar funcionalidade LGPD com Feature Toggle ocultado portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de LGPD
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "lgpd"         | "portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeLGPDComFeatureToggleOcultadoGP
  Scenario Outline: CTWR 08-28 Validar funcionalidade LGPD com Feature Toggle ocultado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de LGPD
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "lgpd"         | "gestor portador" | "web"      |


  #Taxa de Cambio ativado
  @FeatureToggle_ValidarFuncionalidadeTaxaDeCambioComFeatureToggleAtivadoNacional
  Scenario Outline: CTWR 08-50 Validar funcionalidade Taxa de Cambio com Feature Toggle ativado gestor nacional
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Taxa de Cambio

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade         | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consulta_taxa_cambio" | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeTaxaDeCambioComFeatureToggleOcultadoRE
  Scenario Outline: CTWR 08-50 Validar funcionalidade Taxa de Cambio com Feature Toggle ativado gestor estrangeiro
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Taxa de Cambio

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade         | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "consulta_taxa_cambio" | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeTaxaDeCambioComFeatureToggleAtivadoPortador
  Scenario Outline: CTWR 08-50 Validar funcionalidade Taxa de Cambio com Feature Toggle ativado portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Taxa de Cambio

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade         | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "consulta_taxa_cambio" | "portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeTaxaDeCambioComFeatureToggleAtivadoGP
  Scenario Outline: CTWR 08-50 Validar funcionalidade Taxa de Cambio com Feature Toggle ativado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Taxa de Cambio

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade         | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "consulta_taxa_cambio" | "gestor portador" | "web"      |

  #Taxa de Cambio inativado
  @FeatureToggle_ValidarFuncionalidadeTaxaDeCambioComFeatureToggleInativadoNacional
  Scenario Outline: CTWR 08-51 Validar funcionalidade Taxa de Cambio com Feature Toggle inativado gestor nacional
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Taxa de Cambio
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade         | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consulta_taxa_cambio" | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeTaxaDeCambioComFeatureToggleInativadoRE
  Scenario Outline: CTWR 08-51 Validar funcionalidade Taxa de Cambio com Feature Toggle inativado gestor estrangeiro
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Taxa de Cambio
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade         | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "consulta_taxa_cambio" | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeTaxaDeCambioComFeatureToggleInativadoPortador
  Scenario Outline: CTWR 08-51 Validar funcionalidade Taxa de Cambio com Feature Toggle inativado portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Taxa de Cambio
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade         | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "consulta_taxa_cambio" | "portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeTaxaDeCambioComFeatureToggleInativadoGP
  Scenario Outline: CTWR 08-51 Validar funcionalidade Taxa de Cambio com Feature Toggle inativado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Taxa de Cambio
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade         | perfil            | plataforma |
      | "admin"  | "admin"  | "25358287078"  | "Br@12345" | "TH"     | "empresa" | "consulta_taxa_cambio" | "gestor portador" | "web"      |

  #Taxa de Cambio ocultado
  @FeatureToggle_ValidarFuncionalidadeTaxaDeCambioComFeatureToggleOcultadoNacional
  Scenario Outline: CTWR 08-52 Validar funcionalidade Taxa de Cambio com Feature Toggle ocultado gestor nacional
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Taxa de Cambio
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade         | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consulta_taxa_cambio" | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeTaxaDeCambioComFeatureToggleOcultadoRE
  Scenario Outline: CTWR 08-52 Validar funcionalidade Taxa de Cambio com Feature Toggle ocultado gestor estrangeiro
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Taxa de Cambio
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade         | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "consulta_taxa_cambio" | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeTaxaDeCambioComFeatureToggleOcultadoPortador
  Scenario Outline: CTWR 08-52 Validar funcionalidade Taxa de Cambio com Feature Toggle ocultado portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Taxa de Cambio
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade         | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "consulta_taxa_cambio" | "portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeTaxaDeCambioComFeatureToggleOcultadoGP
  Scenario Outline: CTWR 08-52 Validar funcionalidade Taxa de Cambio com Feature Toggle ocultado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Taxa de Cambio
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade         | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "consulta_taxa_cambio" | "gestor portador" | "web"      |


  #Parcelamento de fatura ativado
  @FeatureToggle_ValidarFuncionalidadeParcelamentoDeFaturaComFeatureToggleAtivadoNacional
  Scenario Outline: CTWR 08-29 Validar funcionalidade Parcelamento de Fatura com Feature Toggle ativado gestor nacional
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And Acesso o cabecalho de Parcelamento de Fatura

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade        | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "parcelamento_fatura" | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeParcelamentoDeFaturaComFeatureToggleAtivadoRE
  Scenario Outline: CTWR 08-29 Validar funcionalidade Parcelamento de Fatura com Feature Toggle ativado gestor estrangeiro
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And Acesso o cabecalho de Parcelamento de Fatura

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade        | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "parcelamento_fatura" | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeParcelamentoDeFaturaComFeatureToggleAtivadoGP
  Scenario Outline: CTWR 08-29 Validar funcionalidade Parcelamento de Fatura com Feature Toggle ativado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And Acesso o cabecalho de Parcelamento de Fatura

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade        | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "parcelamento_fatura" | "gestor portador" | "web"      |

  #Parcelamento de fatura inativado
  @FeatureToggle_ValidarFuncionalidadeParcelamentoDeFaturaComFeatureToggleInativadoNacional
  Scenario Outline: CTWR 08-30 Validar funcionalidade Parcelamento de Fatura com Feature Toggle inativado gestor nacional
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And Acesso o cabecalho de Parcelamento de Fatura
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade        | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "parcelamento_fatura" | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeParcelamentoDeFaturaComFeatureToggleInativadoRE
  Scenario Outline: CTWR 08-30 Validar funcionalidade Parcelamento de Fatura com Feature Toggle inativado gestor estrangeiro
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And Acesso o cabecalho de Parcelamento de Fatura
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade        | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "parcelamento_fatura" | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeParcelamentoDeFaturaComFeatureToggleInativadoGP
  Scenario Outline: CTWR 08-30 Validar funcionalidade Parcelamento de Fatura com Feature Toggle inativado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And Acesso o cabecalho de Parcelamento de Fatura
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade        | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "parcelamento_fatura" | "gestor portador" | "web"      |

  #Parcelamento de fatura ocultado
  @FeatureToggle_ValidarFuncionalidadeParcelamentoDeFaturaComFeatureToggleOcultadoNacional
  Scenario Outline: CTWR 08-31 Validar funcionalidade Parcelamento de Fatura com Feature Toggle ocultado gestor nacional
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And Acesso o cabecalho de Parcelamento de Fatura
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade        | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "parcelamento_fatura" | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeParcelamentoDeFaturaComFeatureToggleOcultadoRE
  Scenario Outline: CTWR 08-31 Validar funcionalidade Parcelamento de Fatura com Feature Toggle ocultado gestor estrangeiro
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And Acesso o cabecalho de Parcelamento de Fatura
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade        | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "parcelamento_fatura" | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeParcelamentoDeFaturaComFeatureToggleOcultadoGP
  Scenario Outline: CTWR 08-31 Validar funcionalidade Parcelamento de Fatura com Feature Toggle ocultado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And Acesso o cabecalho de Parcelamento de Fatura
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade        | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "parcelamento_fatura" | "gestor portador" | "web"      |


  #Consultas ativado
  @FeatureToggle_ValidarBotaoConsultasComFeatureToggleAtivadoNacional
  Scenario Outline: CTWR 08-32 Validar botao Consultas com Feature Toggle ativado gestor nacional
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Consultas

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consultas"    | "gestor" | "web"      |

  @FeatureToggle_ValidarBotaoConsultasComFeatureToggleAtivadoRE
  Scenario Outline: CTWR 08-32 Validar botao Consultas com Feature Toggle ativado gestor estrangeiro
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Consultas

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "consultas"    | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarBotaoConsultasComFeatureToggleAtivadoPortador
  Scenario Outline: CTWR 08-32 Validar botao Consultas com Feature Toggle ativado portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Consultas

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "consultas"    | "portador" | "web"      |

  @FeatureToggle_ValidarBotaoConsultasComFeatureToggleAtivadoGP
  Scenario Outline: CTWR 08-32 Validar botao Consultas com Feature Toggle ativado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Consultas

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "consultas"    | "gestor portador" | "web"      |

  #Consultas inativado
  @FeatureToggle_ValidarBotaoConsultasComFeatureToggleInativadoNacional
  Scenario Outline: CTWR 08-33 Validar botao Consultas com Feature Toggle inativado gestor nacional
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Consultas
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consultas"    | "gestor" | "web"      |

  @FeatureToggle_ValidarBotaoConsultasComFeatureToggleInativadoRE
  Scenario Outline: CTWR 08-33 Validar botao Consultas com Feature Toggle inativado gestor estrangeiro
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Consultas
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "consultas"    | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarBotaoConsultasComFeatureToggleInativadoPortador
  Scenario Outline: CTWR 08-33 Validar botao Consultas com Feature Toggle inativado portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Consultas
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "consultas"    | "portador" | "web"      |

  @FeatureToggle_ValidarBotaoConsultasComFeatureToggleInativadoGP
  Scenario Outline: CTWR 08-33 Validar botao Consultas com Feature Toggle inativado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Consultas
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "consultas"    | "gestor portador" | "web"      |

  #Consultas ocultado
  @FeatureToggle_ValidarBotaoConsultasComFeatureToggleOcultadoNacional
  Scenario Outline: CTWR 08-34 Validar botao Consultas com Feature Toggle ocultado gestor nacional
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Consultas
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consultas"    | "gestor" | "web"      |

  @FeatureToggle_ValidarBotaoConsultasComFeatureToggleOcultadoRE
  Scenario Outline: CTWR 08-34 Validar botao Consultas com Feature Toggle ocultado gestor estrangeiro
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Consultas
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "consultas"    | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarBotaoConsultasComFeatureToggleOcultadoPortador
  Scenario Outline: CTWR 08-34 Validar botao Consultas com Feature Toggle ocultado portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Consultas
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "consultas"    | "portador" | "web"      |

  @FeatureToggle_ValidarBotaoConsultasComFeatureToggleOcultadoGP
  Scenario Outline: CTWR 08-34 Validar botao Consultas com Feature Toggle ocultado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Consultas
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "consultas"    | "gestor portador" | "web"      |


  #Solicitacoes Gestor ativado
  @FeatureToggle_ValidarBotaoSolicitacoesGestorComFeatureToggleAtivadoNacional
  Scenario Outline: CTWR 08-35 Validar botao Solicitacoes Gestor com Feature Toggle ativado gestor nacional
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Solicitacoes Gestor

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade       | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "solicitacao_gestor" | "gestor" | "web"      |

  @FeatureToggle_ValidarBotaoSolicitacoesGestorComFeatureToggleAtivadoRE
  Scenario Outline: CTWR 08-35 Validar botao Solicitacoes Gestor com Feature Toggle ativado gestor estrangeiro
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Solicitacoes Gestor

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade       | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "solicitacao_gestor" | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarBotaoSolicitacoesGestorComFeatureToggleAtivadoGP
  Scenario Outline: CTWR 08-35 Validar botao Solicitacoes Gestor com Feature Toggle ativado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Solicitacoes Gestor

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade       | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "solicitacao_gestor" | "gestor portador" | "web"      |

  #Solicitacoes Gestor inativado
  @FeatureToggle_ValidarBotaoSolicitacoesGestorComFeatureToggleInativadoNacional
  Scenario Outline: CTWR 08-36 Validar botao Solicitacoes Gestor com Feature Toggle inativado gestor nacional
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Solicitacoes Gestor
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade       | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "solicitacao_gestor" | "gestor" | "web"      |

  @FeatureToggle_ValidarBotaoSolicitacoesGestorComFeatureToggleInativadoRE
  Scenario Outline: CTWR 08-36 Validar botao Solicitacoes Gestor com Feature Toggle inativado gestor estrangeiro
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Solicitacoes Gestor
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade       | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "solicitacao_gestor" | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarBotaoSolicitacoesGestorComFeatureToggleInativadoGP
  Scenario Outline: CTWR 08-36 Validar botao Solicitacoes Gestor com Feature Toggle inativado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Solicitacoes Gestor
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade       | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "solicitacao_gestor" | "gestor portador" | "web"      |

  #Solicitacoes Gestor ocultado
  @FeatureToggle_ValidarBotaoSolicitacoesGestorComFeatureToggleOcultadoNacional
  Scenario Outline: CTWR 08-37 Validar botao Solicitacoes Gestor com Feature Toggle ocultado gestor nacional
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Solicitacoes Gestor
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade       | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "solicitacao_gestor" | "gestor" | "web"      |

  @FeatureToggle_ValidarBotaoSolicitacoesGestorComFeatureToggleOcultadoRE
  Scenario Outline: CTWR 08-37 Validar botao Solicitacoes Gestor com Feature Toggle ocultado gestor estrangeiro
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Solicitacoes Gestor
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade       | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "solicitacao_gestor" | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarBotaoSolicitacoesGestorComFeatureToggleOcultadoGP
  Scenario Outline: CTWR 08-37 Validar botao Solicitacoes Gestor com Feature Toggle ocultado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Solicitacoes Gestor
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade       | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "solicitacao_gestor" | "gestor portador" | "web"      |


  #Fatura ativado
  @FeatureToggle_ValidarFuncionalidadeFaturaComFeatureToggleAtivadoNacional
  Scenario Outline: CTWR 08-59 Validar funcionalidade Fatura com Feature Toggle ativado gestor nacional
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Faturas

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "fatura"       | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeFaturaComFeatureToggleAtivadoRE
  Scenario Outline: CTWR 08-59 Validar funcionalidade Fatura com Feature Toggle ativado gestor estrangeiro
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Faturas

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "fatura"       | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeFaturaComFeatureToggleAtivadoGP
  Scenario Outline: CTWR 08-59 Validar funcionalidade Fatura com Feature Toggle ativado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Faturas

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "fatura"       | "gestor portador" | "web"      |

  #Fatura inativado
  @FeatureToggle_ValidarFuncionalidadeFaturaComFeatureToggleInativadoNacional
  Scenario Outline: CTWR 08-60 Validar funcionalidade Fatura com Feature Toggle inativado gestor nacional
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Faturas
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "fatura"       | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeFaturaComFeatureToggleInativadoRE
  Scenario Outline: CTWR 08-60 Validar funcionalidade Fatura com Feature Toggle inativado gestor estrangeiro
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Faturas
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "fatura"       | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeFaturaComFeatureToggleInativadoGP
  Scenario Outline: CTWR 08-60 Validar funcionalidade Fatura com Feature Toggle inativado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Faturas
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "fatura"       | "gestor portador" | "web"      |

  #Fatura ocultado
  @FeatureToggle_ValidarFuncionalidadeFaturaComFeatureToggleOcultadoNacional
  Scenario Outline: CTWR 08-61 Validar funcionalidade Fatura com Feature Toggle ocultado gestor nacional
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Faturas
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "fatura"       | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeFaturaComFeatureToggleOcultadoRE
  Scenario Outline: CTWR 08-61 Validar funcionalidade Fatura com Feature Toggle ocultado gestor estrangeiro
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Faturas
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "fatura"       | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeFaturaComFeatureToggleOcultadoGP
  Scenario Outline: CTWR 08-61 Validar funcionalidade Fatura com Feature Toggle ocultado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o cabecalho de Faturas
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "fatura"       | "gestor portador" | "web"      |

  #Consulta de Limites Portador
  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesPortadorComFeatureToggleInativa
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites Portador com Feature Toggle Inativa
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Portador que a tela Consulta de Limites esta Inativo
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade     | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesPortadorComFeatureToggleOculto
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites Portador com Feature Toggle Oculto
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Portador que a tela Consulta de Limites esta Inativo
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade     | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesPortadorComFeatureToggleAtiva
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites Portador com Feature Toggle Ativa
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Portador que a tela Consulta de Limites esta Ativa
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade     | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorComFeatureToggleInativa
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites Gestor com Feature Toggle Inativa
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Fecho o Pop-up Trazer Dados Open Finance como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor que a tela Consulta de Limites esta Inativo
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade     | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorComFeatureToggleOculto
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites Gestor com Feature Toggle Oculto
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Fecho o Pop-up Trazer Dados Open Finance como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor que a tela Consulta de Limites esta Oculto
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade     | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorComFeatureToggleAtiva
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites Gestor com Feature Toggle Ativa
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Fecho o Pop-up Trazer Dados Open Finance como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor que a tela Consulta de Limites esta Ativa
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade     | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorEstrangeiroComFeatureToggleInativa
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites Gestor Estrangeiro com Feature Toggle Inativa
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Fecho o Pop-up Trazer Dados Open Finance como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor Estrangeiro que a tela Consulta de Limites esta Inativo
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TU"     | "empresa" | "consulta_limites" | "Gestor Estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorEstrangeiroComFeatureToggleOculto
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites Gestor Estrangeiro com Feature Toggle Oculto
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Fecho o Pop-up Trazer Dados Open Finance como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor Estrangeiro que a tela Consulta de Limites esta Oculto
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Gestor Estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorEstrangeiroComFeatureToggleAtiva
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites Gestor Estrangeiro com Feature Toggle Ativa
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Fecho o Pop-up Trazer Dados Open Finance como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor Estrangeiro que a tela Consulta de Limites esta Ativa
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Gestor Estrangeiro" | "web"      |


  #Solicitacoes ativado
  @FeatureToggle_ValidarBotaoSolicitacoesComFeatureToggleAtivadoRE
  Scenario Outline: CTWR 08-74 Validar botao Solicitacoes com Feature Toggle ativado gestor estrangeiro
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Solicitacoes

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "solicitacao"  | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarBotaoSolicitacoesComFeatureToggleAtivadoPortador
  Scenario Outline: CTWR 08-74 Validar botao Solicitacoes com Feature Toggle ativado portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Solicitacoes

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "solicitacao"  | "portador" | "web"      |

  @FeatureToggle_ValidarBotaoSolicitacoesComFeatureToggleAtivadoGP
  Scenario Outline: CTWR 08-74 Validar botao Solicitacoes com Feature Toggle ativado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Solicitacoes

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "solicitacao"  | "gestor portador" | "web"      |

  #Solicitacoes inativado
  @FeatureToggle_ValidarBotaoSolicitacoesComFeatureToggleInativadoRE
  Scenario Outline: CTWR 08-75 Validar botao Solicitacoes com Feature Toggle inativado gestor estrangeiro
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Solicitacoes
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "solicitacao"  | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarBotaoSolicitacoesComFeatureToggleInativadoPortador
  Scenario Outline: CTWR 08-75 Validar botao Solicitacoes com Feature Toggle inativado portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Solicitacoes
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "solicitacao"  | "portador" | "web"      |

  @FeatureToggle_ValidarBotaoSolicitacoesComFeatureToggleInativadoGP
  Scenario Outline: CTWR 08-75 Validar botao Solicitacoes com Feature Toggle inativado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Solicitacoes
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "solicitacao"  | "gestor portador" | "web"      |

  #Solicitacoes ocultado
  @FeatureToggle_ValidarBotaoSolicitacoesComFeatureToggleOcultadoRE
  Scenario Outline: CTWR 08-76 Validar botao Solicitacoes com Feature Toggle ocultado gestor estrangeiro
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Solicitacoes
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "solicitacao"  | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarBotaoSolicitacoesComFeatureToggleOcultadoPortador
  Scenario Outline: CTWR 08-76 Validar botao Solicitacoes com Feature Toggle ocultado portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Solicitacoes
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "solicitacao"  | "portador" | "web"      |

  @FeatureToggle_ValidarBotaoSolicitacoesComFeatureToggleOcultadoGP
  Scenario Outline: CTWR 08-76 Validar botao Solicitacoes com Feature Toggle ocultado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Solicitacoes
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "solicitacao"  | "gestor portador" | "web"      |


  #Boleto ativado
  @FeatureToggle_ValidarBotaoBoletoComFeatureToggleAtivadoPortador
  Scenario Outline: CTWR 08-77 Validar botao Boleto com Feature Toggle ativado portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Boleto

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade    | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "boleto_portador" | "portador" | "web"      |

  #Boleto inativado
  @FeatureToggle_ValidarBotaoBoletoComFeatureToggleInativadoPortador
  Scenario Outline: CTWR 08-78 Validar botao Boleto com Feature Toggle inativado portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Boleto
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade    | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "boleto_portador" | "portador" | "web"      |

  #Boleto ocultado
  @FeatureToggle_ValidarBotaoBoletoComFeatureToggleOcultadoPortador
  Scenario Outline: CTWR 08-79 Validar botao Boleto com Feature Toggle ocultado portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o botao de Boleto
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade    | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "boleto_portador" | "portador" | "web"      |


  #Extrato ativado
  @FeatureToggle_ValidarFuncionalidadeExtratoComFeatureToggleAtivadoNacional
  Scenario Outline: CTWR 08-62 Validar funcionalidade Extrato com Feature Toggle ativado gestor nacional
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o funcionalidade de Extrato

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "extrato"    | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeExtratoComFeatureToggleAtivadoRE
  Scenario Outline: CTWR 08-62 Validar funcionalidade Extrato com Feature Toggle ativado gestor estrangeiro
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o funcionalidade de Extrato

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "extrato"    | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeExtratoComFeatureToggleAtivadoPortador
  Scenario Outline: CTWR 08-62 Validar funcionalidade Extrato com Feature Toggle ativado portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o funcionalidade de Extrato

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "extrato"    | "portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeExtratoComFeatureToggleAtivadoGP
  Scenario Outline: CTWR 08-62 Validar funcionalidade Extrato com Feature Toggle ativado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o funcionalidade de Extrato

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "extrato"    | "gestor portador" | "web"      |

  #Extrato inativado
  @FeatureToggle_ValidarFuncionalidadeExtratoComFeatureToggleInativadoNacional
  Scenario Outline: CTWR 08-63 Validar funcionalidade Extrato com Feature Toggle inativado gestor nacional
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o funcionalidade de Extrato
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "extrato"    | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeExtratoComFeatureToggleInativadoRE
  Scenario Outline: CTWR 08-63 Validar funcionalidade Extrato com Feature Toggle inativado gestor estrangeiro
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o funcionalidade de Extrato
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "extrato"    | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeExtratoComFeatureToggleInativadoPortador
  Scenario Outline: CTWR 08-63 Validar funcionalidade Extrato com Feature Toggle inativado portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o funcionalidade de Extrato
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "extrato"    | "portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeExtratoComFeatureToggleInativadoGP
  Scenario Outline: CTWR 08-63 Validar funcionalidade Extrato com Feature Toggle inativado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o funcionalidade de Extrato
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "extrato"    | "gestor portador" | "web"      |

  #Extrato ocultado
  @FeatureToggle_ValidarFuncionalidadeExtratoComFeatureToggleOcultadoNacional
  Scenario Outline: CTWR 08-64 Validar funcionalidade Extrato com Feature Toggle ocultado gestor nacional
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o funcionalidade de Extrato
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "extrato"    | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeExtratoComFeatureToggleOcultadoRE
  Scenario Outline: CTWR 08-64 Validar funcionalidade Extrato com Feature Toggle ocultado gestor estrangeiro
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o funcionalidade de Extrato
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "extrato"    | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeExtratoComFeatureToggleOcultadoPortador
  Scenario Outline: CTWR 08-64 Validar funcionalidade Extrato com Feature Toggle ocultado portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o funcionalidade de Extrato
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "extrato"    | "portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeExtratoComFeatureToggleOcultadoGP
  Scenario Outline: CTWR 08-64 Validar funcionalidade Extrato com Feature Toggle ocultado gestor portador
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    And acesso o funcionalidade de Extrato
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | "06218670635"  | "Br@12345" | "TH"     | "empresa" | "extrato"    | "gestor portador" | "web"      |
      
      
     @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorPortadorComFeatureToggleInativa
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites Gestor Estrangeiro com Feature Toggle Inativa
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Fecho o Pop-up Trazer Dados Open Finance como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor Portador que a tela Consulta de Limites esta Inativo
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "06218670635" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorPortadorComFeatureToggleOculto
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites Gestor Portador com Feature Toggle Oculto
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Fecho o Pop-up Trazer Dados Open Finance como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor Portador que a tela Consulta de Limites esta Oculto
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "06218670635" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorPortadorComFeatureToggleAtiva
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites Gestor Portador com Feature Toggle Ativa
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Fecho o Pop-up Trazer Dados Open Finance como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor Portador que a tela Consulta de Limites esta Ativa
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "06218670635" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Gestor" | "web"      |    
      
     @FeatureToggle_ValidarAbaProdutosemConsultadeLimitescomoGestorestaInativa
 		 Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites aba Produtos Inativa como Gestor Feature Toggle Inativa
	    Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login <cpf> <senha>
	    And Fecho o Pop-up Trazer Dados Open Finance como Gestor
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor que a aba Produtos em Consulta de Limites esta Inativa
	    And Verifico o Logout com sucesso
	    And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade    								 | perfil       | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consulta_limite_prod" | "Gestor" | "web"      |    
      
      @FeatureToggle_ValidarAbaPortadoresemConsultadeLimitescomoGestorestaInativa
 			 Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites aba Portador  Inativa como Gestor Feature Toggle Inativa
		    Given Acessar a pagina do Portal ADM <ambiente>
		    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
		    Then Acesso a opcao Gerenciar Funcionalidades
		    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
		    And Acessar a pagina do Portal PJ <ambiente> <rede>
		    And Efetuo o login <cpf> <senha>
		    And Fecho o Pop-up Trazer Dados Open Finance como Gestor
		    And Verifico o login efetuado com sucesso
		    Then Verifico como Gestor que a aba Portadores em Consulta de Limites esta Inativa
		    And Verifico o Logout com sucesso
		    And Acessar a pagina do Portal ADM <ambiente>
		    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
		    And Acesso a opcao Gerenciar Funcionalidades
		    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     									| perfil               | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consulta_limite_port" | "Gestor"				  | "web"      |    
		
		@FeatureToggle_ValidarAbaProdutosemConsultadeLimitescomoGestorestaOculta
 		 Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites aba Produtos Oculta como Gestor Feature Toggle Oculta
	    Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login <cpf> <senha>
	    And Fecho o Pop-up Trazer Dados Open Finance como Gestor
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor que a aba Produtos em Consulta de Limites esta Oculta
	    And Verifico o Logout com sucesso
	    And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade    								 | perfil       | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consulta_limite_prod" | "Gestor" | "web"      |    
      
      @FeatureToggle_ValidarAbaPortadoresemConsultadeLimitescomoGestorestaOculta
 			 Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites aba Portador Oculta como Gestor Feature Toggle Oculta
		    Given Acessar a pagina do Portal ADM <ambiente>
		    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
		    Then Acesso a opcao Gerenciar Funcionalidades
		    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
		    And Acessar a pagina do Portal PJ <ambiente> <rede>
		    And Efetuo o login <cpf> <senha>
		    And Fecho o Pop-up Trazer Dados Open Finance como Gestor
		    And Verifico o login efetuado com sucesso
		    Then Verifico como Gestor que a aba Portadores em Consulta de Limites esta Oculta
		    And Verifico o Logout com sucesso
		    And Acessar a pagina do Portal ADM <ambiente>
		    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
		    And Acesso a opcao Gerenciar Funcionalidades
		    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     									| perfil               | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consulta_limite_port" | "Gestor"				  | "web"      |   
      
      
      @FeatureToggle_ValidarAbaProdutosemConsultadeLimitescomoGestorPortadorestaInativa
 		 Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites aba Produtos Inativa como Gestor Portador Feature Toggle Inativa
	    Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login <cpf> <senha>
	    And Fecho o Pop-up Trazer Dados Open Finance como Gestor
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor Portador que a aba Produtos em Consulta de Limites esta Inativa
	    And Verifico o Logout com sucesso
	    And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade    								 | perfil       | plataforma |
      | "admin"  | "admin"  | "06218670635" | "Br@12345" | "TI"     | "empresa" | "consulta_limite_prod" | "Gestor" | "web"      |    
      
      @FeatureToggle_ValidarAbaPortadoresemConsultadeLimitescomoGestorPortadorestaInativa
 			 Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites aba Portador Inativa como Gestor Portador Feature Toggle Inativa
		    Given Acessar a pagina do Portal ADM <ambiente>
		    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
		    Then Acesso a opcao Gerenciar Funcionalidades
		    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
		    And Acessar a pagina do Portal PJ <ambiente> <rede>
		    And Efetuo o login <cpf> <senha>
		    #And Fecho o Pop-up Trazer Dados Open Finance como Gestor
		    And Verifico o login efetuado com sucesso
		    Then Verifico como Gestor Portador que a aba Portadores em Consulta de Limites esta Inativa
		    And Verifico o Logout com sucesso
		    And Acessar a pagina do Portal ADM <ambiente>
		    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
		    And Acesso a opcao Gerenciar Funcionalidades
		    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     									| perfil               | plataforma |
      | "admin"  | "admin"  | "06218670635" | "Br@12345" | "TI"     | "empresa" | "consulta_limite_port" | "Gestor"				  | "web"      |    
		
		@FeatureToggle_ValidarAbaProdutosemConsultadeLimitescomoGestorPortadorestaOculta
 		 Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites aba Produtos Oculta como Gestor Portador Feature Toggle Oculta
	    Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login <cpf> <senha>
	    And Fecho o Pop-up Trazer Dados Open Finance como Gestor
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor Portador que a aba Produtos em Consulta de Limites esta Oculta
	    And Verifico o Logout com sucesso
	    And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade    								 | perfil       | plataforma |
      | "admin"  | "admin"  | "06218670635" | "Br@12345" | "TI"     | "empresa" | "consulta_limite_prod" | "Gestor" | "web"      |    
      
      @FeatureToggle_ValidarAbaPortadoresemConsultadeLimitescomoGestorPortadorestaOculta
 			 Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites aba Portador Oculta como Gestor Portador Feature Toggle Oculta
		    Given Acessar a pagina do Portal ADM <ambiente>
		    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
		    Then Acesso a opcao Gerenciar Funcionalidades
		    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
		    And Acessar a pagina do Portal PJ <ambiente> <rede>
		    And Efetuo o login <cpf> <senha>
		   # And Fecho o Pop-up Trazer Dados Open Finance como Gestor
		    And Verifico o login efetuado com sucesso
		    Then Verifico como Gestor Portador que a aba Portadores em Consulta de Limites esta Oculta
		    And Verifico o Logout com sucesso
		    And Acessar a pagina do Portal ADM <ambiente>
		    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
		    And Acesso a opcao Gerenciar Funcionalidades
		    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     									| perfil               | plataforma |
      | "admin"  | "admin"  | "06218670635" | "Br@12345" | "TI"     | "empresa" | "consulta_limite_port" | "Gestor"				  | "web"      |    
      
      
    @FeatureToggle_ValidarAbaProdutosemConsultadeLimitescomoGestorEstrangeiroestaInativa
 		 Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites aba Produtos Inativa como Gestor Estrangeiro Feature Toggle Inativa
	    Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login <cpf> <senha>
	    And Fecho o Pop-up Trazer Dados Open Finance como Gestor
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor Estrangeiro que a aba Produtos em Consulta de Limites esta Inativa
	    And Verifico o Logout com sucesso
	    And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                											   | senha      | ambiente | rede      | funcionalidade    								 | perfil       | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     | "empresa" | "consulta_limite_prod" | "Gestor" | "web"      |    
      
      @FeatureToggle_ValidarAbaPortadoresemConsultadeLimitescomoGestorEstrangeiroestaInativa
 			 Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites aba Portador Inativa como Gestor Estrangeiro Feature Toggle Inativa
		    Given Acessar a pagina do Portal ADM <ambiente>
		    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
		    Then Acesso a opcao Gerenciar Funcionalidades
		    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
		    And Acessar a pagina do Portal PJ <ambiente> <rede>
		    And Efetuo o login <cpf> <senha>
		    And Fecho o Pop-up Trazer Dados Open Finance como Gestor
		    And Verifico o login efetuado com sucesso
		    Then Verifico como Gestor Estrangeiro que a aba Portadores em Consulta de Limites esta Inativa
		    And Verifico o Logout com sucesso
		    And Acessar a pagina do Portal ADM <ambiente>
		    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
		    And Acesso a opcao Gerenciar Funcionalidades
		    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                  										 | senha      | ambiente | rede      | funcionalidade     									| perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     | "empresa" | "consulta_limite_port" | "Gestor"				  | "web"      |    
		
		@FeatureToggle_ValidarAbaProdutosemConsultadeLimitescomoGestorEstrangeiroestaOculta
 		 Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites aba Produtos Oculta como Gestor Estrangeiro Feature Toggle Oculta
	    Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login <cpf> <senha>
	    And Fecho o Pop-up Trazer Dados Open Finance como Gestor
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor Estrangeiro que a aba Produtos em Consulta de Limites esta Oculta
	    And Verifico o Logout com sucesso
	    And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                  										 | senha      | ambiente | rede      | funcionalidade    								 | perfil       | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     | "empresa" | "consulta_limite_prod" | "Gestor" | "web"      |    
      
      @FeatureToggle_ValidarAbaPortadoresemConsultadeLimitescomoGestorEstrangeiroestaOculta
 			 Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites aba Portador Oculta como Gestor Estrangeiro Feature Toggle Oculta
		    Given Acessar a pagina do Portal ADM <ambiente>
		    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
		    Then Acesso a opcao Gerenciar Funcionalidades
		    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
		    And Acessar a pagina do Portal PJ <ambiente> <rede>
		    And Efetuo o login <cpf> <senha>
		    And Fecho o Pop-up Trazer Dados Open Finance como Gestor
		    And Verifico o login efetuado com sucesso
		    Then Verifico como Gestor Estrangeiro que a aba Portadores em Consulta de Limites esta Oculta
		    And Verifico o Logout com sucesso
		    And Acessar a pagina do Portal ADM <ambiente>
		    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
		    And Acesso a opcao Gerenciar Funcionalidades
		    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                  										 | senha      | ambiente | rede      | funcionalidade     									| perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     | "empresa" | "consulta_limite_port" | "Gestor"				  | "web"      | 
      
      @FeatureToggle_ValidarFuncionalidadeRelatoriosGestorComFeatureToggleInativo
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Relatorios Gestor com Feature Toggle Inativa
	    Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login <cpf> <senha>
	    #And Fecho o Pop-up Trazer Dados Open Finance como Gestor
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor que a tela Relat�rios esta Inativo
	    And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "relatorios_fixos" | "Gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeRelatoriosGestorComFeatureToggleOculto
  Scenario Outline: CTWR 08-31 Validar funcionalidade Relatrios Gestor com Feature Toggle Oculto
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    #And Fecho o Pop-up Trazer Dados Open Finance como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor que a tela Relat�rios esta Oculto
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "relatorios_fixos" | "Gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeRelatoriosGestorComFeatureToggleAtiva
  Scenario Outline: CTWR 08-31 Validar funcionalidade Relatrios Gestor com Feature Toggle Ativa
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    #And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor que a tela Relat�rios esta Ativa
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "relatorios_fixos" | "Gestor" | "web"      | 
      
      
    @FeatureToggle_ValidarFuncionalidadeRelatoriosGestorPortadorComFeatureToggleInativo
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Relatorios Gestor Portador com Feature Toggle Inativa
	    Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login <cpf> <senha>
	    #And Fecho o Pop-up Trazer Dados Open Finance como Gestor
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor Portador que a tela Relat�rios esta Inativo
	    And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "06218670635" | "Br@12345" | "TI"     | "empresa" | "relatorios_fixos" | "Gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeRelatoriosGestorPortadorComFeatureToggleOculto
  Scenario Outline: CTWR 08-31 Validar funcionalidade Relatrios Gestor Portador com Feature Toggle Oculto
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    #And Fecho o Pop-up Trazer Dados Open Finance como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor Portador que a tela Relat�rios esta Oculto
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "06218670635" | "Br@12345" | "TI"     | "empresa" | "relatorios_fixos" | "Gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeRelatoriosGestorPortadorComFeatureToggleAtiva
  Scenario Outline: CTWR 08-31 Validar funcionalidade Relatrios Gestor Portador com Feature Toggle Ativa
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    #And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor Portador que a tela Relat�rios esta Ativa
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "06218670635" | "Br@12345" | "TI"     | "empresa" | "relatorios_fixos" | "Gestor" | "web"      | 
      
      
      @FeatureToggle_ValidarFuncionalidadeRelatoriosGestorEstrangeiroComFeatureToggleInativo
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Relatorios Gestor Estrangeiro com Feature Toggle Inativa
	    Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login <cpf> <senha>
	    #And Fecho o Pop-up Trazer Dados Open Finance como Gestor
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor Estrangeiro que a tela Relat�rios esta Inativo
	    And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     | "empresa" | "relatorios_fixos" | "Gestor Estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeRelatoriosGestorEstrangeiroComFeatureToggleOculto
  Scenario Outline: CTWR 08-31 Validar funcionalidade Relatrios Gestor Estrangeiro com Feature Toggle Oculto
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    #And Fecho o Pop-up Trazer Dados Open Finance como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor Estrangeiro que a tela Relat�rios esta Oculto
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      		| funcionalidade    											 | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     | "empresa" | "relatorios_fixos" | "Gestor Estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeRelatoriosGestorEstrangeiroComFeatureToggleAtiva
  Scenario Outline: CTWR 08-31 Validar funcionalidade Relatrios Gestor Estrangeiro com Feature Toggle Ativa
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    #And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor Estrangeiro que a tela Relat�rios esta Ativa
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     | "empresa" | "relatorios_fixos" | "Gestor Estrangeiro" | "web"      | 
      
  @FeatureToggle_PortadorVerificaFuncionalidadeRelatoriosComFeatureToggleAtiva
  Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Portador com Feature Toggle Ativa
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Portador que a tela Relat�rios esta Ativa
    And Verifico o Logout com sucesso
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "45291270446" | "Br@12345" | "TI"     				| "empresa"										  | "relatorios_fixos" | "Portador"			 | "web"      | 
      
      
   @FeatureToggle_PortadorVerificaFuncionalidadeRelatoriosComFeatureToggleInativo
  Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Portador com Feature Toggle Inativo
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Portador que a tela Relat�rios esta Inativo
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "45291270446" | "Br@12345" | "TI"     				| "empresa"										  | "relatorios_fixos" | "Portador"			 | "web"      | 
      
  @FeatureToggle_PortadorVerificaFuncionalidadeRelatoriosComFeatureToggleOculto
  Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Portador com Feature Toggle Oculto
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Portador que a tela Relat�rios esta Oculto
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "45291270446" | "Br@12345" | "TI"     				| "empresa"										  | "relatorios_fixos" | "Portador"			 | "web"      | 
       
       
 @FeatureToggle_PortadorVerificaFuncionalidadeBloqueioComFeatureToggleAtivo
  Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Portador com Feature Toggle Ativo
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade Bloqueio <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Portador que a tela de Bloqueio esta Ativo
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "45291270446" | "Br@12345" | "TI"     				| "empresa"										  | "bloqueio" | "Portador"			 | "web"      | 
       
    @FeatureToggle_PortadorVerificaFuncionalidadeBloqueioComFeatureToggleInativo
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Portador com Feature Toggle Inativo
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade Bloqueio <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login <cpf> <senha>
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Portador que a tela de Bloqueio esta Inativo
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "45291270446" | "Br@12345" | "TI"     				| "empresa"										  | "bloqueio"						 | "Portador"			 | "web"      |     
       
   @FeatureToggle_PortadorVerificaFuncionalidadeBloqueioComFeatureToggleOculto
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Portador com Feature Toggle Oculto
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Ocultar funcionalidade Bloqueio <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login <cpf> <senha>
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Portador que a tela de Bloqueio esta Oculto
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "45291270446" | "Br@12345" | "TI"     				| "empresa"										  | "bloqueio" | "Portador"			 | "web"      |     
       
   @FeatureToggle_GestorVerificaFuncionalidadeBloqueioComFeatureToggleAtivo
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Gestor com Feature Toggle Ativo
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade Bloqueio <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login <cpf> <senha>
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor que a tela de Bloqueio esta Ativo
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "25358287078" | "Br@12345" | "TI"     				| "empresa"										  | "bloqueio" | "Gestor"			 | "web"      | 
       
    @FeatureToggle_GestorVerificaFuncionalidadeBloqueioComFeatureToggleInativo
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Gestor com Feature Toggle Inativo
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade Bloqueio <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login <cpf> <senha>
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor que a tela de Bloqueio esta Inativo
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "25358287078" | "Br@12345" | "TI"     				| "empresa"										  | "bloqueio"						 | "Gestor"			 | "web"      |     
       
   @FeatureToggle_GestorVerificaFuncionalidadeBloqueioComFeatureToggleOculto
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Gestor com Feature Toggle Oculto
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Ocultar funcionalidade Bloqueio <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login <cpf> <senha>
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor que a tela de Bloqueio esta Oculto
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "25358287078" | "Br@12345" | "TI"     				| "empresa"										  | "bloqueio" | "Gestor"			 | "web"      |     
       
       
    @FeatureToggle_GestorPortadorVerificaFuncionalidadeBloqueioComFeatureToggleAtivo
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Gestor Portador com Feature Toggle Ativo
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade Bloqueio <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login <cpf> <senha>
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor que a tela de Bloqueio esta Ativo
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "06218670635" | "Br@12345" | "TI"     				| "empresa"										  | "bloqueio" | "Gestor"			 | "web"      | 
       
    @FeatureToggle_GestorPortadorVerificaFuncionalidadeBloqueioComFeatureToggleInativo
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Gestor Portador com Feature Toggle Inativo
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade Bloqueio <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login <cpf> <senha>
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor que a tela de Bloqueio esta Inativo
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "06218670635" | "Br@12345" | "TI"     				| "empresa"										  | "bloqueio"						 | "Gestor"			 | "web"      |     
       
   @FeatureToggle_GestorPortadorVerificaFuncionalidadeBloqueioComFeatureToggleOculto
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Gestor Portador com Feature Toggle Oculto
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Ocultar funcionalidade Bloqueio <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login <cpf> <senha>
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor que a tela de Bloqueio esta Oculto
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "06218670635" | "Br@12345" | "TI"     				| "empresa"										  | "bloqueio" | "Gestor"			 | "web"      |     
       
       
    @FeatureToggle_GestorEstrangeiroVerificaFuncionalidadeBloqueioComFeatureToggleAtivo
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Gestor Estrangeiro com Feature Toggle Ativo
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade Bloqueio <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login <cpf> <senha>
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor que a tela de Bloqueio esta Ativo
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     				| "empresa"										  | "bloqueio" | "Gestor"			 | "web"      | 
       
    @FeatureToggle_GestorEstrangeiroVerificaFuncionalidadeBloqueioComFeatureToggleInativo
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Gestor Estrangeiro com Feature Toggle Inativo
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade Bloqueio <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login <cpf> <senha>
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor que a tela de Bloqueio esta Inativo
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     				| "empresa"										  | "bloqueio"						 | "Gestor"			 | "web"      |     
       
   @FeatureToggle_GestorEstrangeiroVerificaFuncionalidadeBloqueioComFeatureToggleOculto
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Gestor Estrangeiro com Feature Toggle Oculto
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Ocultar funcionalidade Bloqueio <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login <cpf> <senha>
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor que a tela de Bloqueio esta Oculto
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     				| "empresa"										  | "bloqueio" | "Gestor"			 | "web"      | 
       
       
  @FeatureToggle_PortadorVerificaFuncionalidadeServicosComFeatureToggleAtiva
  Scenario Outline: CTWR 08-31 Validar funcionalidade Servicos como Portador com Feature Toggle Ativa
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Portador que a aba Servicos esta Ativa
    And Verifico o Logout com sucesso
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "45291270446" | "Br@12345" | "TI"     				| "empresa"										  | "servicos" | "Portador"			 | "web"      | 
      
      
   @FeatureToggle_PortadorVerificaFuncionalidadeServicosComFeatureToggleInativo
  Scenario Outline: CTWR 08-31 Validar funcionalidade Servi�os como Portador com Feature Toggle Inativo
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Portador que a aba Servicos esta Inativo
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "45291270446" | "Br@12345" | "TI"     				| "empresa"										  | "servicos" | "Portador"			 | "web"      | 
      
  @FeatureToggle_PortadorVerificaFuncionalidadeServicosComFeatureToggleOculto
  Scenario Outline: CTWR 08-31 Validar funcionalidade Servi�os como Portador com Feature Toggle Oculto
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Portador que a tela Servi�os esta Oculto
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "45291270446" | "Br@12345" | "TI"     				| "empresa"										  | "servicos" | "Portador"			 | "web"      |  
       
    
    @FeatureToggle_GestorVerificaFuncionalidadeServicosComFeatureToggleAtiva
 	 Scenario Outline: CTWR 08-31 Validar funcionalidade Servicos como Gestor com Feature Toggle Ativa
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor que a aba Servicos esta Ativa
    And Verifico o Logout com sucesso
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "25358287078" | "Br@12345" | "TI"     				| "empresa"										  | "servicos" | "Gestor"			 | "web"      | 
      
      
   @FeatureToggle_GestorVerificaFuncionalidadeServicosComFeatureToggleInativo
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Servi�os como Gestor com Feature Toggle Inativo
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login <cpf> <senha>
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor que a aba de Servicos esta Inativo
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "25358287078" | "Br@12345" | "TI"     				| "empresa"										  | "servicos" | "Gestor"			 | "web"      | 
      
  @FeatureToggle_GestorVerificaFuncionalidadeServicosComFeatureToggleOculto
  Scenario Outline: CTWR 08-31 Validar funcionalidade Servi�os como Gestor com Feature Toggle Oculto
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor que a aba de Servicos esta Oculto
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "25358287078" | "Br@12345" | "TI"     				| "empresa"										  | "servicos" | "Gestor"			 | "web"      | 
       
  @FeatureToggle_GestorPortadorVerificaFuncionalidadeServicosComFeatureToggleAtiva
 	 Scenario Outline: CTWR 08-31 Validar funcionalidade Servicos como Gestor Portador com Feature Toggle Ativa
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor que a aba de Servicos esta Ativa
    And Verifico o Logout com sucesso
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "06218670635" | "Br@12345" | "TI"     				| "empresa"										  | "servicos" | "Gestor"			 | "web"      | 
      
      
   @FeatureToggle_GestorPortadorVerificaFuncionalidadeServicosComFeatureToggleInativo
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Servi�os como Gestor Portador com Feature Toggle Inativo
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login <cpf> <senha>
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor que a aba de Servicos esta Inativo
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "06218670635" | "Br@12345" | "TI"     				| "empresa"										  | "servicos" | "Gestor"			 | "web"      | 
      
  @FeatureToggle_GestorPortadorVerificaFuncionalidadeServicosComFeatureToggleOculto
  Scenario Outline: CTWR 08-31 Validar funcionalidade Servi�os como Gestor Portador com Feature Toggle Oculto
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor que a aba de Servicos esta Oculto
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "06218670635" | "Br@12345" | "TI"     				| "empresa"										  | "servicos" | "Gestor"			 | "web"      |   
       
  @FeatureToggle_GestorEstrangeiroVerificaFuncionalidadeServicosComFeatureToggleAtiva
 	 Scenario Outline: CTWR 08-31 Validar funcionalidade Servicos como Gestor Estrangeiro com Feature Toggle Ativa
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor que a aba de Servicos esta Ativa
    And Verifico o Logout com sucesso
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     				| "empresa"										  | "servicos"							 | "Gestor Estrangeiro"			 | "web"      | 
      
      
   @FeatureToggle_GestorEstrangeiroVerificaFuncionalidadeServicosComFeatureToggleInativo
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Servi�os como Gestor Estrangeiro com Feature Toggle Inativo
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login <cpf> <senha>
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor que a aba de Servicos esta Inativo
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     				| "empresa"										  | "servicos" | "Gestor Estrangeiro"			 | "web"      | 
      
  @FeatureToggle_GestorEstrangeiroPortadorVerificaFuncionalidadeServicosComFeatureToggleOculto
  Scenario Outline: CTWR 08-31 Validar funcionalidade Servi�os como Gestor Portador com Feature Toggle Oculto Estrangeiro
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor que a aba de Servicos esta Oculto
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     				| "empresa"										  | "servicos"							 | "Gestor Estrangeiro"			 | "web"      |
       
   @FeatureToggle_PortadorVerificaFuncionalidadeFormadePagamentoComFeatureToggleAtiva
  Scenario Outline: CTWR 08-31 Validar funcionalidade Forma de Pagamento como Portador com Feature Toggle Ativa
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Portador que a funcionalidade Forma de Pagamento esta Ativa
    And Verifico o Logout com sucesso
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "45291270446" | "Br@12345" | "TI"     				| "empresa"										  | "consulta_forma_pgto" | "Portador"			 | "web"      | 
      
      
   @FeatureToggle_PortadorVerificaFuncionalidadeFormadePagamentoComFeatureToggleInativo
  Scenario Outline: CTWR 08-31 Validar funcionalidade Forma de Pagamento como Portador com Feature Toggle Inativo
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Portador que a funcionalidade Forma de Pagamento esta Inativo
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "45291270446" | "Br@12345" | "TI"     				| "empresa"										  | "consulta_forma_pgto" | "Portador"			 | "web"      | 
      
  @FeatureToggle_PortadorVerificaFuncionalidadeFormadePagamentoComFeatureToggleOculto
  Scenario Outline: CTWR 08-31 Validar funcionalidade Forma de Pagamento como Portador com Feature Toggle Oculto
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Portador que a funcionalidade Forma de Pagamento esta Oculto
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "45291270446" | "Br@12345" | "TI"     				| "empresa"										  | "consulta_forma_pgto" | "Portador"			 | "web"      |  
       
    
    @FeatureToggle_GestorVerificaFuncionalidadeFormadePagamentoComFeatureToggleAtiva
 	 Scenario Outline: CTWR 08-31 Validar funcionalidade Forma de Pagamento como Gestor com Feature Toggle Ativa
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor que a funcionalidade Forma de Pagamento esta Ativo
    And Verifico o Logout com sucesso
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "25358287078" | "Br@12345" | "TI"     				| "empresa"										  | "consulta_forma_pgto" | "Gestor"			 | "web"      | 
      
      
   @FeatureToggle_GestorVerificaFuncionalidadeFormadePagamentoComFeatureToggleInativo
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Forma de Pagamento como Gestor com Feature Toggle Inativo
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login <cpf> <senha>
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor que a funcionalidade Forma de Pagamento esta Inativo
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "25358287078" | "Br@12345" | "TI"     				| "empresa"										  | "consulta_forma_pgto" | "Gestor"			 | "web"      | 
      
  @FeatureToggle_GestorVerificaFuncionalidadeFormadePagamentoComFeatureToggleOculto
  Scenario Outline: CTWR 08-31 Validar funcionalidade Forma de Pagamento como Gestor com Feature Toggle Oculto
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor que a funcionalidade Forma de Pagamento esta Oculto
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "25358287078" | "Br@12345" | "TI"     				| "empresa"										  | "consulta_forma_pgto" | "Gestor"			 | "web"      | 
       
  @FeatureToggle_GestorPortadorVerificaFuncionalidadeFormadePagamentoComFeatureToggleAtiva
 	 Scenario Outline: CTWR 08-31 Validar funcionalidade Forma de Pagamento como Gestor Portador com Feature Toggle Ativa
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor que a funcionalidade Forma de Pagamento esta Ativo
    And Verifico o Logout com sucesso
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "06218670635" | "Br@12345" | "TI"     				| "empresa"										  | "consulta_forma_pgto" | "Gestor"			 | "web"      | 
      
      
   @FeatureToggle_GestorPortadorVerificaFuncionalidadeFormadePagamentoComFeatureToggleInativo
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Forma de Pagamento como Gestor Portador com Feature Toggle Inativo
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login <cpf> <senha>
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor que a funcionalidade Forma de Pagamento esta Inativo
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "06218670635" | "Br@12345" | "TI"     				| "empresa"										  | "consulta_forma_pgto" | "Gestor"			 | "web"      | 
      
  @FeatureToggle_GestorPortadorVerificaFuncionalidadeFormadePagamentoComFeatureToggleOculto
  Scenario Outline: CTWR 08-31 Validar funcionalidade Forma de Pagamento como Gestor Portador com Feature Toggle Oculto
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor que a funcionalidade Forma de Pagamento esta Oculto
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "06218670635" | "Br@12345" | "TI"     				| "empresa"										  | "consulta_forma_pgto" | "Gestor"			 | "web"      |   
       
  @FeatureToggle_GestorEstrangeiroVerificaFuncionalidadeFormadePagamentoComFeatureToggleAtiva
 	 Scenario Outline: CTWR 08-31 Validar funcionalidade Forma de Pagamento como Gestor Estrangeiro com Feature Toggle Ativa
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor que a funcionalidade Forma de Pagamento esta Ativo
    And Verifico o Logout com sucesso
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     				| "empresa"										  | "consulta_forma_pgto"							 | "Gestor Estrangeiro"			 | "web"      | 
      
      
   @FeatureToggle_GestorEstrangeiroVerificaFuncionalidadeFormadePagamentoComFeatureToggleInativo
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Forma de Pagamento como Gestor Estrangeiro com Feature Toggle Inativo
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login <cpf> <senha>
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor que a funcionalidade Forma de Pagamento esta Inativo
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     				| "empresa"										  | "consulta_forma_pgto" | "Gestor Estrangeiro"			 | "web"      | 
      
  @FeatureToggle_GestorEstrangeiroPortadorVerificaFuncionalidadeFormadePagamentoComFeatureToggleOculto
  Scenario Outline: CTWR 08-31 Validar funcionalidade Forma de Pagamento como Gestor Portador com Feature Toggle Oculto Estrangeiro
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor que a funcionalidade Forma de Pagamento esta Oculto
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     				| "empresa"										  | "consulta_forma_pgto"							 | "Gestor Estrangeiro"			 | "web"      |                       
       