package br.com.bradesco.pupj.steps;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import br.com.bradesco.automacaocartoes.core.DriverFactory;
import br.com.bradesco.pupj.pagesweb.CabecalhoRodape_CabecalhoPage;
import br.com.bradesco.pupj.pagesweb.CabecalhoRodape_RodapePage;
import br.com.bradesco.pupj.pagesweb.EsqueciMinhaSenha_esqueci_minha_senhaPage;
import br.com.bradesco.pupj.pagesweb.Limites_ConsultaDeLimitePages;
import br.com.bradesco.pupj.pagesweb.Limites_ConsultaDeLimite_DetalhesPages;
import br.com.bradesco.pupj.pagesweb.Limites_ConsultaDeLimite_EmpresasPage;
import br.com.bradesco.pupj.pagesweb.Limites_ConsultaDeLimite_PortadoresPage;
import br.com.bradesco.pupj.pagesweb.Limites_ConsultaDeLimite_ProdutosPage;
import br.com.bradesco.pupj.pagesweb.Login_LoginPage;
import br.com.bradesco.pupj.utilitarios.UtilsWeb;
import cucumber.deps.com.thoughtworks.xstream.InitializationException;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;

@SuppressWarnings({ "unused" })
public class ConsultarLimitesSteps extends UtilsWeb {

	public static WebDriver driverWeb = DriverFactory.getDriverWeb();
	
	Login_LoginPage login = new Login_LoginPage(driverWeb);
	CabecalhoRodape_CabecalhoPage cabecalhoLogado = new CabecalhoRodape_CabecalhoPage(driverWeb);
	CabecalhoRodape_RodapePage rodape = new CabecalhoRodape_RodapePage(driverWeb);
	Limites_ConsultaDeLimitePages limitePortador = new Limites_ConsultaDeLimitePages(driverWeb);
	Limites_ConsultaDeLimite_DetalhesPages limiteGestor = new Limites_ConsultaDeLimite_DetalhesPages(driverWeb);
	Limites_ConsultaDeLimite_PortadoresPage pageLimitePortador = new Limites_ConsultaDeLimite_PortadoresPage(driverWeb);
	Limites_ConsultaDeLimite_ProdutosPage pageLimiteProduto = new Limites_ConsultaDeLimite_ProdutosPage(driverWeb);
	Limites_ConsultaDeLimite_EmpresasPage pageLimite_EmpresasPage = new Limites_ConsultaDeLimite_EmpresasPage(driverWeb);
	
	@Then("Verifico Consulta De Limite Positivo Portador")
	public void verifico_Consulta_De_Limite_Positivo_Portador() {
		esperaElemento(limitePortador.msgValorDeLimite, 60);
		Assert.assertTrue(elementoExiste(limitePortador.msgValorDeLimite));
		Assert.assertEquals("Limite total R$ 30.000,00", limitePortador.msgValorDeLimite.getText());
		tirarScreenshot("Tela final limite Portador");
	}

	@Then("Verifico Consulta De Limite Negativo Portador")
	public void verifico_Consulta_De_Limite_Negativo_Portador() {
		esperaElemento(limitePortador.msgValorDeLimiteNegativo, 60);
		Assert.assertTrue(elementoExiste(limitePortador.msgValorDeLimiteNegativo));
		Assert.assertEquals("R$ -2.000,00", limitePortador.msgValorDeLimiteNegativo.getText());
		tirarScreenshot("Tela final limite Portador");
	}
	
	@Then("Verifico Consulta De Limite total Portador Hibrido")
	public void verifico_Consulta_De_Limite__Portador_Hibrido() {
		esperaElemento(limitePortador.msgValorDeLimitehibrido, 60);
		Assert.assertTrue(elementoExiste(limitePortador.msgValorDeLimitehibrido));
		Assert.assertEquals("Limite total R$ 5.000,00", limitePortador.msgValorDeLimitehibrido.getText());
		tirarScreenshot("Tela final limite Portador");
	}
	
	@Then("Verifico Consulta De Limite total Portador Hibrido CFI")
	public void verifico_Consulta_De_Limite__Portador_Hibrido_CFI() {
		esperaElemento(limitePortador.msgValorDeLimitehibridocfi, 60);
		Assert.assertTrue(elementoExiste(limitePortador.msgValorDeLimitehibridocfi));
		Assert.assertEquals("Limite total R$ 20.000,00", limitePortador.msgValorDeLimitehibridocfi.getText());
		tirarScreenshot("Tela final limite Portador");
	}
	
	@Then("Verifico Consulta De Limite total disponivel Portador Centralizado")
	public void verifico_Consulta_De_Limite__Portador_Centralizado() {
		esperaElemento(limitePortador.msgValorDeLimiteCentralizado, 60);
		Assert.assertTrue(elementoExiste(limitePortador.msgValorDeLimiteCentralizado));
		Assert.assertEquals("Limite total R$ 15.000,00", limitePortador.msgValorDeLimiteCentralizado.getText());
		tirarScreenshot("Tela final limite Portador");
	}
	
	@Then("Verifico Consulta De Limite total Portador Centralizado CFI")
	public void verifico_Consulta_De_Limite__Portador_Centralizado_CFI() {
		esperaElemento(limitePortador.msgValorDeLimiteCentralizadoCFI, 60);
		Assert.assertTrue(elementoExiste(limitePortador.msgValorDeLimiteCentralizadoCFI));
		Assert.assertEquals("Limite total R$ 20.000,00", limitePortador.msgValorDeLimiteCentralizadoCFI.getText());
		tirarScreenshot("Tela final limite Portador");
	}
	@Then("Verifico Consulta De Limite disponivel Portador Hibrido")
	public void verifico_Consulta_De_Limite__Portador_Disponivel_Hibrido() {
		esperaElemento(limitePortador.msgValorDeLimitedisponivelhibrido, 60);
		Assert.assertTrue(elementoExiste(limitePortador.msgValorDeLimitedisponivelhibrido));
		Assert.assertEquals("R$ 4.993,33", limitePortador.msgValorDeLimitedisponivelhibrido.getText());
		tirarScreenshot("Tela final limite Portador");
	}
	
	@Then("Verifico Consulta De Limite disponivel Portador Hibrido CFI")
	public void verifico_Consulta_De_Limite__Portador_Disponivel_Hibrido_CFI() {
		esperaElemento(limitePortador.msgValorDeLimitedisponivelhibridocfi, 60);
		Assert.assertTrue(elementoExiste(limitePortador.msgValorDeLimitedisponivelhibridocfi));
		Assert.assertEquals("R$ 20.000,00", limitePortador.msgValorDeLimitedisponivelhibridocfi.getText());
		tirarScreenshot("Tela final limite Portador");
	}
	
	@Then("Verifico Consulta De Limite disponivel Portador Centralizado")
	public void verifico_Consulta_De_Limite__Portador_Disponivel_Centralizado() {
		esperaElemento(limitePortador.msgValorDeLimitedisponivelCentralizado, 60);
		Assert.assertTrue(elementoExiste(limitePortador.msgValorDeLimitedisponivelCentralizado));
		Assert.assertEquals("R$ 15.000,00", limitePortador.msgValorDeLimitedisponivelCentralizado.getText());
		tirarScreenshot("Tela final limite Portador");
	}
	
	@Then("Verifico Consulta De Limite Portador disponivel Centralizado CFI")
	public void verifico_Consulta_De_Limite__Portador_Disponivel_Centralizado_CFI() {
		esperaElemento(limitePortador.msgValorDeLimitedisponivelCentralizadoCFI, 60);
		Assert.assertTrue(elementoExiste(limitePortador.msgValorDeLimitedisponivelCentralizadoCFI));
		Assert.assertEquals("R$ 1.000,00", limitePortador.msgValorDeLimitedisponivelCentralizadoCFI.getText());
		tirarScreenshot("Tela final limite Portador");
	}
	@Then("Verifico Consulta De Limite Positivo")
	public void verifico_Consulta_De_Limite_Positivo() throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(pageLimitePortador.lblPortadores, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblPortadores));
		pageLimitePortador.clicarMenuPortadores();
		tirarScreenshot("Tela consulta de limites");
		esperaElemento(pageLimitePortador.lblEscolhaPortador, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblEscolhaPortador));
		tirarScreenshot("Menu escolha de Portador");
		pageLimitePortador.clicarEscolhaPortador();
		esperaElemento(pageLimitePortador.lblValorTotalPostivivo, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblValorTotalPostivivo));
		Assert.assertEquals("Total Dispon�vel: R$ 12.000,00", pageLimitePortador.lblValorTotalPostivivo.getText());
		tirarScreenshot("Tela final limite Portador");
	}
	
	@Then("Verifico Consulta De Limite Negativo")
	public void verifico_Consulta_De_Limite_Negativo() throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(pageLimitePortador.lblPortadores, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblPortadores));
		pageLimitePortador.clicarMenuPortadores();
		tirarScreenshot("Tela consulta de limites");
		esperaElemento(pageLimitePortador.lblEscolhaPortador, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblEscolhaPortador));
		tirarScreenshot("Menu escolha de Portador");
		pageLimitePortador.clicarEscolhaPortador();
		esperaElemento(limiteGestor.lblLimiteNegativoGestor, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.lblLimiteNegativoGestor));
		Assert.assertEquals("Total Dispon�vel: R$ -2.000,00", limiteGestor.lblLimiteNegativoGestor.getText());
		tirarScreenshot("Tela final limite Portador");
	}
	
	@Then("Verifico Consulta De Limite Disponivel Portador Centralizado {string}") 
	public void verifico_Consulta_De_Limite_Disponivel_Portador_Centralizado(String nomeCpfPortadorCentralizado) throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(limiteGestor.msgTitulo, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
		limiteGestor.clicarTitulo();
		esperaElemento(pageLimitePortador.lblPortadores, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblPortadores));
		pageLimitePortador.clicarMenuPortadores();
		tirarScreenshot("Tela consulta de limites aba Portadores");
		esperaElemento(pageLimitePortador.txtCpfNomeCentralizado, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.txtCpfNomeCentralizado));
		esperaElemento(pageLimitePortador.lblBuscaPortador, 60);
		digitarLetra(pageLimitePortador.txtPortador,nomeCpfPortadorCentralizado.toString());
		espera(5);
		tirarScreenshot("Menu escolha de Portador");
		pageLimitePortador.clicarEscolhaPortador();
		esperaElemento(pageLimitePortador.lblValorDisponivelCentralizado, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblValorDisponivelCentralizado));
		Assert.assertEquals("Total Dispon�vel: R$ -194.400,31", pageLimitePortador.lblValorDisponivelCentralizado.getText());
		tirarScreenshot("Tela final do limite Disponivel do Portador Centralizado");
	}
	
	@Then("Verifico Consulta De Limite Disponivel Portador Hibrido {string}") 
	public void verifico_Consulta_De_Limite_Disponivel_Portador_Hibrido(String nomePortadorHibrido) throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(limiteGestor.msgTitulo, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
		limiteGestor.clicarTitulo();
		esperaElemento(pageLimitePortador.lblPortadores, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblPortadores));
		pageLimitePortador.clicarMenuPortadores();
		tirarScreenshot("Tela consulta de limites");
		esperaElemento(pageLimitePortador.txtCpfNomeHibrido, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.txtCpfNomeHibrido));
		esperaElemento(pageLimitePortador.lblBuscaPortador, 60);
		digitarLetra(pageLimitePortador.txtPortador,nomePortadorHibrido.toString());
		espera(5);
		tirarScreenshot("Consulta de limite - Pesquisa portador por CPF");
		esperaElemento(pageLimitePortador.lblEscolhaPortador, 80);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblEscolhaPortador));
		tirarScreenshot("Menu escolha de Portador");
		pageLimitePortador.clicarEscolhaPortador();
		esperaElemento(pageLimitePortador.lblValorDisponivelHibrido, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblValorDisponivelHibrido));
		Assert.assertEquals("Total Dispon�vel: R$ 12.000,00", pageLimitePortador.lblValorDisponivelHibrido.getText());
		tirarScreenshot("Tela final do limite Disponivel do Portador Hibrido");
	}
	
	@Then("Verifico Consulta De Limite Disponivel Portador Hibrido CFI {string}") 
	public void verifico_Consulta_De_Limite_Disponivel_Portador_Hibrido_CFI(String nomeCpfPortadorHibridoCFI) throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(limiteGestor.msgTitulo, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
		limiteGestor.clicarTitulo();
		esperaElemento(pageLimitePortador.lblPortadores, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblPortadores));
		pageLimitePortador.clicarMenuPortadores();
		tirarScreenshot("Tela consulta de limites");
		esperaElemento(pageLimitePortador.txtCpfNomeHibridoCFI, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.txtCpfNomeHibridoCFI));
		esperaElemento(pageLimitePortador.lblBuscaPortador, 60);
		digitarLetra(pageLimitePortador.txtPortador,nomeCpfPortadorHibridoCFI.toString());
		espera(5);
		tirarScreenshot("Consulta de limite - Pesquisa portador por CPF");
		esperaElemento(pageLimitePortador.lblEscolhaPortador, 80);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblEscolhaPortador));
		tirarScreenshot("Menu escolha de Portador");
		pageLimitePortador.clicarEscolhaPortador();
		esperaElemento(pageLimitePortador.lblValorDisponivelHibridoCFI, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblValorDisponivelHibridoCFI));
		Assert.assertEquals("Total Dispon�vel: R$ 20.000,00", pageLimitePortador.lblValorDisponivelHibridoCFI.getText());
		tirarScreenshot("Tela final do limite Disponivel do Portador Hibrido CFI");
	}
	
	@Then("Verifico Consulta De Limite Disponivel Portador Centralizado CFI {string}") 
	public void verifico_Consulta_De_Limite_Disponivel_Portador_Centralizado_CFI(String nomeCpfPortadorCentralizadoCFI) throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(limiteGestor.msgTitulo, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
		limiteGestor.clicarTitulo();
		esperaElemento(pageLimitePortador.lblPortadores, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblPortadores));
		pageLimitePortador.clicarMenuPortadores();
		tirarScreenshot("Tela consulta de limites");
		esperaElemento(pageLimitePortador.txtCpfNomeCentralizadoCFI, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.txtCpfNomeCentralizadoCFI));
		digitarLetra(pageLimitePortador.txtPortador,nomeCpfPortadorCentralizadoCFI.toString());
		espera(5);
		tirarScreenshot("Consulta de limite - Pesquisa portador por CPF");
		esperaElemento(pageLimitePortador.lblEscolhaPortador, 80);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblEscolhaPortador));
		tirarScreenshot("Menu escolha de Portador");
		pageLimitePortador.clicarEscolhaPortador();
		esperaElemento(pageLimitePortador.lblValorDisponivelCentralizadoCFI, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblValorDisponivelCentralizadoCFI));
		Assert.assertEquals("Total Dispon�vel: R$ -138.407,20", pageLimitePortador.lblValorDisponivelCentralizadoCFI.getText());
		tirarScreenshot("Tela final do limite Disponivel do Portador Centralizado");
	}
	
	@Then("Verifico Consulta De Limite Total Portador Centralizado {string}") 
	public void verifico_Consulta_De_Limite_Total_Portador_Centralizado(String nomeCpfPortadorCentralizado) throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(limiteGestor.msgTitulo, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
		limiteGestor.clicarTitulo();
		esperaElemento(pageLimitePortador.lblPortadores, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblPortadores));
		pageLimitePortador.clicarMenuPortadores();
		tirarScreenshot("Tela consulta de limites");
		esperaElemento(pageLimitePortador.txtCpfNomeCentralizadoCFI, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.txtCpfNomeCentralizadoCFI));
		digitarLetra(pageLimitePortador.txtPortador,nomeCpfPortadorCentralizado.toString());
		espera(5);
		tirarScreenshot("Consulta de limite - Pesquisa portador por CPF");
		esperaElemento(pageLimitePortador.lblEscolhaPortador, 80);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblEscolhaPortador));
		tirarScreenshot("Menu escolha de Portador");
		pageLimitePortador.clicarEscolhaPortador();
		esperaElemento(pageLimitePortador.lblValorTotalCentralizado, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblValorTotalCentralizado));
		esperaElemento(pageLimitePortador.lblDescricaoValorTotal, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblDescricaoValorTotal));
		Assert.assertEquals("Limite Total", pageLimitePortador.lblDescricaoValorTotal.getText());
		Assert.assertEquals("R$ 2.000,00", pageLimitePortador.lblValorTotalCentralizado.getText());
		tirarScreenshot("Tela final do limite Total do Portador Centralizado");
	}
	
	@Then("Verifico Consulta De Limite Total Portador Hibrido {string}") 
	public void verifico_Consulta_De_Limite_Total_Portador_Hibrido(String nomePortadorHibrido) throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(limiteGestor.msgTitulo, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
		limiteGestor.clicarTitulo();
		esperaElemento(pageLimitePortador.lblPortadores, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblPortadores));
		pageLimitePortador.clicarMenuPortadores();
		tirarScreenshot("Tela consulta de limites");
		esperaElemento(pageLimitePortador.txtCpfNomeHibrido, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.txtCpfNomeHibrido));
		esperaElemento(pageLimitePortador.lblBuscaPortador, 60);
		digitarLetra(pageLimitePortador.txtPortador,nomePortadorHibrido.toString());
		espera(5);
		tirarScreenshot("Consulta de limite - Pesquisa portador por CPF");
		esperaElemento(pageLimitePortador.lblEscolhaPortador, 80);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblEscolhaPortador));
		tirarScreenshot("Menu escolha de Portador");
		pageLimitePortador.clicarEscolhaPortador();
		esperaElemento(pageLimitePortador.lblValorTotalCentralizadoCFI, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblValorTotalCentralizadoCFI));
		esperaElemento(pageLimitePortador.lblDescricaoValorTotal, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblDescricaoValorTotal));
		Assert.assertEquals("Limite Total", pageLimitePortador.lblDescricaoValorTotal.getText());
		Assert.assertEquals("R$ 12.000,00", pageLimitePortador.lblValorTotalCentralizadoCFI.getText());
		tirarScreenshot("Tela final do limite Total do Portador Hibrido");
	}
	
	@Then("Verifico Consulta De Limite Total Portador Hibrido CFI {string}") 
	public void verifico_Consulta_De_Limite_Total_Portador_Hibrido_CFI(String nomeCpfPortadorHibridoCFI) throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(limiteGestor.msgTitulo, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
		limiteGestor.clicarTitulo();
		esperaElemento(pageLimitePortador.lblPortadores, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblPortadores));
		pageLimitePortador.clicarMenuPortadores();
		tirarScreenshot("Tela consulta de limites");
		esperaElemento(pageLimitePortador.txtCpfNomeHibridoCFI, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.txtCpfNomeHibridoCFI));
		esperaElemento(pageLimitePortador.lblBuscaPortador, 60);
		digitarLetra(pageLimitePortador.txtPortador,nomeCpfPortadorHibridoCFI.toString());
		espera(5);
		tirarScreenshot("Consulta de limite - Pesquisa portador por CPF");
		esperaElemento(pageLimitePortador.lblEscolhaPortador, 80);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblEscolhaPortador));
		tirarScreenshot("Menu escolha de Portador");
		pageLimitePortador.clicarEscolhaPortador();
		esperaElemento(pageLimitePortador.lblValorTotalHibrido, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblValorTotalHibrido));
		esperaElemento(pageLimitePortador.lblDescricaoValorTotal, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblDescricaoValorTotal));
		Assert.assertEquals("Limite Total", pageLimitePortador.lblDescricaoValorTotal.getText());
		Assert.assertEquals("R$ 20.000,00", pageLimitePortador.lblValorTotalHibrido.getText());
		tirarScreenshot("Tela final do limite Total do Portador Hibrido CFI");
	}
	
	@Then("Verifico Consulta De Limite Total Portador Centralizado CFI {string}") 
	public void verifico_Consulta_De_Limite_Total_Portador_Centralizado_CFI(String nomeCpfPortadorCentralizadoCFI) throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(limiteGestor.msgTitulo, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
		limiteGestor.clicarTitulo();
		esperaElemento(pageLimitePortador.lblPortadores, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblPortadores));
		pageLimitePortador.clicarMenuPortadores();
		tirarScreenshot("Tela consulta de limites");
		esperaElemento(pageLimitePortador.txtCpfNomeCentralizadoCFI, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.txtCpfNomeCentralizadoCFI));
		digitarLetra(pageLimitePortador.txtPortador,nomeCpfPortadorCentralizadoCFI.toString());
		espera(5);
		tirarScreenshot("Consulta de limite - Pesquisa portador por CPF");
		esperaElemento(pageLimitePortador.lblEscolhaPortador, 80);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblEscolhaPortador));
		tirarScreenshot("Menu escolha de Portador");
		pageLimitePortador.clicarEscolhaPortador();
		esperaElemento(pageLimitePortador.lblValorTotalCentralizadoCFI, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblValorTotalCentralizadoCFI));
		esperaElemento(pageLimitePortador.lblDescricaoValorTotal, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblDescricaoValorTotal));
		Assert.assertEquals("Limite Total", pageLimitePortador.lblDescricaoValorTotal.getText());
		Assert.assertEquals("R$ 5.000,00", pageLimitePortador.lblValorTotalCentralizadoCFI.getText());
		tirarScreenshot("Tela final do limite Total do Portador Centralizado CFI");
	}
	
	@Then("Verifico Limites do portador") 
	public void verifico_limites_do_portador() throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(limiteGestor.msgTitulo, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
		limiteGestor.clicarTitulo();
		esperaElemento(pageLimitePortador.lblPortadores, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblPortadores));
		pageLimitePortador.clicarMenuPortadores();
		tirarScreenshot("Tela consulta de limites");
		esperaElemento(pageLimitePortador.lblEscolhaPortador, 80);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblEscolhaPortador));
		scrollAteOElemento(pageLimitePortador.lblEscolhaPortador);
		espera(5);
		tirarScreenshot("Menu escolha do Portador");
		pageLimitePortador.clicarEscolhaPortador();
		scrollToTop();
		esperaElemento(pageLimitePortador.lblDescricaoValorTotal, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblDescricaoValorTotal));
		esperaElemento(pageLimitePortador.lbllimitesPortador, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lbllimitesPortador));
		esperaElemento(pageLimitePortador.lblValorDisponivelPortador, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblValorDisponivelPortador));
		Assert.assertEquals("Limite Total", pageLimitePortador.lblDescricaoValorTotal.getText());
		scrollAteOElemento(pageLimitePortador.lblDescricaoValorTotal);
		Assert.assertEquals("R$ 50.000,00", pageLimitePortador.lbllimitesPortador.getText());
		scrollAteOElemento(pageLimitePortador.lbllimitesPortador);
		Assert.assertEquals("Total Dispon�vel: R$ 49.204,36", pageLimitePortador.lblValorDisponivelPortador.getText());
		scrollAteOElemento(pageLimitePortador.lblValorDisponivelPortador);
		tirarScreenshot("Tela final com a exibi��o dos Limites do Portador Selecionado");
	}
	
	@Then("Verifico a busca de portadores por CPF {string}")
	public void verifico_a_busca_de_portadores_por_CPF(String nomeCpfPortador) throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(pageLimitePortador.lblPortadores, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblPortadores));
		pageLimitePortador.clicarMenuPortadores();
		tirarScreenshot("Tela consulta de limites");
		esperaElemento(pageLimitePortador.lblEscolhaPortador, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblEscolhaPortador));
		tirarScreenshot("Menu escolha de Portador");
		esperaElemento(pageLimitePortador.txtCpfNome, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.txtCpfNome));
		limiteGestor.informarTxtCpfNome(nomeCpfPortador);
		tirarScreenshot("Consulta de limite - Pesquisa portador por nome");
		limiteGestor.clicarBtnPesquisar();
		espera(10);
		esperaElemento(pageLimitePortador.lblValorTotalVisa, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblValorTotalVisa));
		Assert.assertEquals("Total Dispon�vel: R$ 30.000,00", pageLimitePortador.lblValorTotalVisa.getText());
		tirarScreenshot("Tela final limite Portador");
	}
	
	@Then("Verifico a busca de portadores por Nome {string}")
	public void verifico_a_busca_de_portadores_por_Nome(String nomeCpfPortador) throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(pageLimitePortador.lblPortadores, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblPortadores));
		pageLimitePortador.clicarMenuPortadores();
		tirarScreenshot("Tela consulta de limites");
		esperaElemento(pageLimitePortador.lblEscolhaPortador, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblEscolhaPortador));
		tirarScreenshot("Menu escolha de Portador");
		esperaElemento(pageLimitePortador.txtCpfNome, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.txtCpfNome));
		//limiteGestor.informarTxtCpfNome(nomeCpfPortador);
		digitarLetra(pageLimitePortador.txtPortador,nomeCpfPortador.toString());
		espera(10);
		tirarScreenshot("Consulta de limite - Pesquisa portador por nome");
		limiteGestor.clicarBtnPesquisar();
		esperaElemento(pageLimitePortador.lblValorTotalVisa, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblValorTotalVisa));
		Assert.assertEquals("Total Dispon�vel: R$ 30.000,00", pageLimitePortador.lblValorTotalVisa.getText());
		tirarScreenshot("Tela final limite Portador");
	}

	@Then("Verifico a busca de portadores por Nome do Produto {string}")
	public void verifico_a_busca_de_portadores_por_Nome_do_Produto(String nomeProduto) throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(pageLimitePortador.lblPortadores, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblPortadores));
		pageLimitePortador.clicarMenuPortadores();
		tirarScreenshot("Tela consulta de limites");
		esperaElemento(limiteGestor.lblProdutos, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.lblProdutos));
		limiteGestor.clicarMenuPortadoresLimites();
		espera(15);
		tirarScreenshot("Tela consulta de Produtos por compid");
		esperaElemento(limiteGestor.txtCompid, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.txtCompid));
		//limiteGestor.digitarNomeProduto(nomeProduto);
		digitarLetra(pageLimiteProduto.txtProduto,nomeProduto.toString());
		limiteGestor.clicarBtnPesquisarProduto();
		espera(15);
		esperaElemento(limiteGestor.lblTipoDeCartao, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.lblTipoDeCartao));
		Assert.assertEquals("EMPRESARIAL PLATINUM VISA", limiteGestor.lblTipoDeCartao.getText());
		esperaElemento(limiteGestor.lblcompID, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.lblcompID));
		Assert.assertEquals("606943", limiteGestor.lblcompID.getText());
		tirarScreenshot("Tela consulta de Produtos por Produto - Resultado de busca");
	}
	

	@Then("Verifico a busca de portadores por Nome da Empresa {string}")
	public void verifico_a_busca_de_portadores_por_Nome_da_Empresa(String nomeEmpresa) throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(limiteGestor.txtCnpjEmpresa, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.txtCnpjEmpresa));
		//limiteGestor.informarTxtCnpjEmpresa(nomeEmpresa);
		digitarLetra(pageLimite_EmpresasPage.msgNomeEmpresa,nomeEmpresa.toString());
		limiteGestor.clicarBtnPesquisarCnpj();
		espera(10);
		tirarScreenshot("Consulta de limite - Pesquisa empresa por nome");
		esperaElemento(limiteGestor.lblLimiteEmpresa, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.lblLimiteEmpresa));
		Assert.assertEquals("Limite da empresa", limiteGestor.lblLimiteEmpresa.getText());
		esperaElemento(limiteGestor.lblValorLimiteEmpresa, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.lblValorLimiteEmpresa));
		Assert.assertEquals("R$ 320.000,00", limiteGestor.lblValorLimiteEmpresa.getText());
	}

	@Then("Verifico o Filtro Por Bandeira Elo")
	public void verifico_o_Filtro_Por_Bandeira() throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(pageLimitePortador.lblPortadores, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblPortadores));
		pageLimitePortador.clicarMenuPortadores();
		tirarScreenshot("Tela consulta de limites");
		esperaElemento(limiteGestor.listaFiltroProduto, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.listaFiltroProduto));
		limiteGestor.clicarListaFiltroProduto();
		tirarScreenshot("Tela consulta de limites - Escolha bandeira");
		esperaElemento(limiteGestor.listaFiltroProdutoElo, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.listaFiltroProdutoElo));
		limiteGestor.clicarListaFiltroProdutoElo();
		tirarScreenshot("Consulta de limite - Pesquisa portador por bandeira");
		esperaElemento(pageLimitePortador.lblEscolhaPortadorElo, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblEscolhaPortadorElo));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblEscolhaPortadorElo));
		tirarScreenshot("Menu escolha de Portador");
		pageLimitePortador.clicarEscolhaPortador();
		esperaElemento(limiteGestor.lblElo, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.lblElo));
		Assert.assertEquals("CARTAO AEREO ELO MAIS", limiteGestor.lblElo.getText());
		tirarScreenshot("Tela final limite Portador");
	}
	
	@Then("Verifico o Filtro Por Bandeira Visa")
	public void verifico_o_Filtro_Por_Bandeira_Visa() throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(pageLimitePortador.lblPortadores, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblPortadores));
		pageLimitePortador.clicarMenuPortadores();
		tirarScreenshot("Tela consulta de limites");
		esperaElemento(limiteGestor.listaFiltroProduto, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.listaFiltroProduto));
		limiteGestor.clicarListaFiltroProduto();
		tirarScreenshot("Tela consulta de limites - Escolha bandeira");
		esperaElemento(limiteGestor.listaFiltroProdutoVisa, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.listaFiltroProdutoVisa));
		limiteGestor.clicarListaFiltroProdutoVisa();
		tirarScreenshot("Consulta de limite - Pesquisa portador por bandeira");
		esperaElemento(pageLimitePortador.lblEscolhaPortador, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblEscolhaPortador));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblEscolhaPortador));
		tirarScreenshot("Menu escolha de Portador");
		pageLimitePortador.clicarEscolhaPortador();
		esperaElemento(limiteGestor.lblVisa, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.lblVisa));
		Assert.assertEquals("EMPRESARIAL PLATINUM VISA", limiteGestor.lblVisa.getText());
		tirarScreenshot("Tela final limite Portador");
	}
	@Then("Verifico o Filtro Por Bandeira Amex")
	public void verifico_o_Filtro_Por_Bandeira_Amex() throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(pageLimitePortador.lblPortadores, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblPortadores));
		pageLimitePortador.clicarMenuPortadores();
		tirarScreenshot("Tela consulta de limites");
		esperaElemento(limiteGestor.listaFiltroProduto, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.listaFiltroProduto));
		limiteGestor.clicarListaFiltroProduto();
		tirarScreenshot("Tela consulta de limites - Escolha bandeira");
		esperaElemento(limiteGestor.listaFiltroProdutoAmex, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.listaFiltroProdutoAmex));
		limiteGestor.clicarListaFiltroProdutoAmex();
		tirarScreenshot("Consulta de limite - Pesquisa portador por bandeira");
		esperaElemento(pageLimitePortador.lblEscolhaPortadorAmex, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblEscolhaPortadorAmex));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblEscolhaPortadorAmex));
		tirarScreenshot("Menu escolha de Portador");
		pageLimitePortador.clicarEscolhaPortadorAmex();
		esperaElemento(limiteGestor.lblAmex, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.lblAmex));
		Assert.assertEquals("AMEX CORPORATE", limiteGestor.lblAmex.getText());
		tirarScreenshot("Tela final limite Portador");
	}
	
	@Then("Verifico o Filtro Por Bandeira Mastercard")
	public void verifico_o_Filtro_Por_Bandeira_MasterCard() throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(pageLimitePortador.lblPortadores, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblPortadores));
		pageLimitePortador.clicarMenuPortadores();
		tirarScreenshot("Tela consulta de limites");
		esperaElemento(limiteGestor.listaFiltroProduto, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.listaFiltroProduto));
		limiteGestor.clicarListaFiltroProduto();
		tirarScreenshot("Tela consulta de limites - Escolha bandeira");
		esperaElemento(limiteGestor.listaFiltroProdutoAmex, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.listaFiltroProdutoMastercard));
		limiteGestor.clicarListaFiltroProdutoMastercard();
		tirarScreenshot("Consulta de limite - Pesquisa portador por bandeira");
		esperaElemento(pageLimitePortador.lblEscolhaPortadorMastercard, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblEscolhaPortadorMastercard));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblEscolhaPortadorMastercard));
		tirarScreenshot("Menu escolha de Portador");
		pageLimitePortador.clicarEscolhaPortadorMastercard();
		esperaElemento(limiteGestor.lblMastercard, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.lblMastercard));
		Assert.assertEquals("CARTAO PASSAGEM BRADESCO", limiteGestor.lblMastercard.getText());
		tirarScreenshot("Tela final limite Portador");
	}
	
	@Then("Verifico a busca produtos por Comp ID {string}")
	public void verifico_a_busca_produtos_por_Comp_ID(String compID) throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(pageLimitePortador.lblPortadores, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblPortadores));
		pageLimitePortador.clicarMenuPortadores();
		tirarScreenshot("Tela consulta de limites");
		esperaElemento(limiteGestor.lblProdutos, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.lblProdutos));
		limiteGestor.clicarMenuPortadoresLimites();
		espera(15);
		tirarScreenshot("Tela consulta de Produtos por compid");
		esperaElemento(limiteGestor.txtCompid, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.txtCompid));
		//limiteGestor.digitarCompid(compID);
		digitarLetra(pageLimiteProduto.txtCompid,compID.toString());
		limiteGestor.clicarBtnPesquisarCompID();
		espera(15);
		esperaElemento(limiteGestor.lblTipoDeCartao, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.lblTipoDeCartao));
		Assert.assertEquals("EMPRESARIAL PLATINUM VISA", limiteGestor.lblTipoDeCartao.getText());
		esperaElemento(limiteGestor.lblcompID, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.lblcompID));
		Assert.assertEquals("606943", limiteGestor.lblcompID.getText());
		tirarScreenshot("Tela consulta de Produtos por compid - Resultado de busca");
	}
	
	@Then("Verifico a busca Empresas por CNPJ {string}")
	public void verifico_a_busca_Empresas_por_CNPJ(String cnpj) throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(limiteGestor.lblEmpresas, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresas));
		limiteGestor.clicarTitulo();
		espera(15);
		tirarScreenshot("Tela consulta de Empresas por CNPJ");
		esperaElemento(limiteGestor.txtCNPJ, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.txtCNPJ));
		//limiteGestor.digitarCNPJ(cnpj);
		digitarLetra(pageLimite_EmpresasPage.txtCnpj,cnpj.toString());
		limiteGestor.clicarBtnPesquisarcnpj();
		espera(15);
		esperaElemento(limiteGestor.lblNomeEmpresa, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.lblNomeEmpresa));
		Assert.assertEquals("MASSA MELHORIA EXTRATO", limiteGestor.lblNomeEmpresa.getText());
		esperaElemento(limiteGestor.lblCnpj, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.lblCnpj	));
		Assert.assertEquals("78.139.464/0001-70", limiteGestor.lblCnpj.getText());
		tirarScreenshot("Tela consulta de Empresas por CNPJ - Resultado de busca");
	}

	@Then("Verifico empresas com dados inexistentes {string}")
	public void verifico_empresas_com_dados_inexistentes(String cnpj) throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(limiteGestor.lblEmpresas, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresas));
		limiteGestor.clicarTitulo();
		espera(15);
		tirarScreenshot("Tela consulta de Empresas por CNPJ");
		esperaElemento(limiteGestor.txtCNPJ, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.txtCNPJ));
		//limiteGestor.digitarCNPJ(cnpj);
		digitarLetra(pageLimite_EmpresasPage.txtCnpj,cnpj.toString());
		limiteGestor.clicarBtnPesquisarcnpjEnter();
		//esperaElemento(limiteGestor.lblerroCnpj, 60);
		//Assert.assertTrue(elementoExiste(limiteGestor.lblerroCnpj));
		//Assert.assertEquals("O CNPJ informado n�o � v�lido", limiteGestor.lblerroCnpj.getText());
		esperaElemento(limiteGestor.lblerroempresa, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.lblerroempresa));
		Assert.assertEquals("Empresa n�o encontrada", limiteGestor.lblerroempresa.getText());
		Assert.assertEquals("Confirme se o nome da empresa foi digitado corretamente.", limiteGestor.lblSuberroempresa.getText());
		tirarScreenshot("Tela consulta de Empresas por CNPJ - Resultado de busca");
	}


	@Then("Verifico produtos com dados inexistentes {string}")
	public void verifico_produtos_com_dados_inexistentes(String produtos) throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(pageLimitePortador.lblPortadores, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblPortadores));
		pageLimitePortador.clicarMenuPortadores();
		tirarScreenshot("Tela consulta de limites");
		esperaElemento(limiteGestor.lblPortadores, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.lblPortadores));
		limiteGestor.clicarMenuPortadoresLimites();
		espera(15);
		tirarScreenshot("Tela consulta de Produtos por compid");
		esperaElemento(limiteGestor.txtCompid, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.txtCompid));
		//limiteGestor.digitarProdutoInexistente(produto);
		digitarLetra(pageLimiteProduto.txtProduto,produtos.toString());
		limiteGestor.clicarBtnPesquisarProdutoEnter();
		esperaElemento(limiteGestor.lblErroProduto, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.lblErroProduto));
		Assert.assertEquals("Produto n�o encontrado", limiteGestor.lblErroProduto.getText());
		Assert.assertEquals("Confirme se o nome ou Comp ID do produto foi digitado corretamente.", limiteGestor.lblSubErroProduto.getText());
		tirarScreenshot("Tela consulta de Produtos por Produto - Resultado de busca");	
	}

	@Then("Verifico portadores com dados inexistentes {string}")
	public void verifico_portadores_com_dados_inexistentes(String cpfBusca) throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(pageLimitePortador.lblPortadores, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblPortadores));
		pageLimitePortador.clicarMenuPortadores();
		tirarScreenshot("Tela consulta de portadores");
		esperaElemento(pageLimitePortador.txtCpfNome, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.txtCpfNome));
		//pageLimitePortador.digitarCpfInexistente(cpfBusca);
		digitarLetra(pageLimitePortador.txtCpfNome,cpfBusca.toString());
		limiteGestor.clicarBtnPesquisarPortadorEnter();
		//esperaElemento(limiteGestor.lblErroCPF, 60);
		//Assert.assertTrue(elementoExiste(limiteGestor.lblErroCPF));
		//Assert.assertEquals("O CPF informado n�o � v�lido", limiteGestor.lblErroCPF.getText());
		esperaElemento(limiteGestor.lblErroPortador, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.lblErroPortador));
		esperaElemento(limiteGestor.lblSubErroPortador, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.lblSubErroPortador));
		Assert.assertEquals("Portador n�o encontrado", limiteGestor.lblErroPortador.getText());
		//Assert.assertEquals("Confirme se o nome ou CPF do portador foi digitado corretamente ou verifique os filtros da pesquisa.", limiteGestor.lblSubErroPortador.getText());
		Assert.assertEquals("Confirme se o nome ou CPF do portador foi digitado corretamente.", limiteGestor.lblSubErroPortador.getText());
		tirarScreenshot("Tela consulta de portadores por CPF - Resultado de busca");	
	}

	@Then("Verifico limites e valido os  detalhes do Portador")
	public void verifico_o_consulta_e_validar_detalhes_do_Portador() throws InterruptedException  {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(pageLimitePortador.lblPortadores, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblPortadores));
		pageLimitePortador.clicarMenuPortadores();
		tirarScreenshot("Tela consulta de limites");
		esperaElemento(pageLimitePortador.lblEscolhaPortador, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblEscolhaPortador));
		tirarScreenshot("Menu escolha de Portador");
		pageLimitePortador.clicarEscolhaPortador();
		esperaElemento(pageLimitePortador.lblValorTotal, 60);
		esperaElemento(pageLimitePortador.lblNomedoportador, 60);
		esperaElemento(pageLimitePortador.dataNomedoportador, 60);
		esperaElemento(pageLimitePortador.lblNomedaempresa, 60);
		esperaElemento(pageLimitePortador.dataNomedaempresa, 60);
		esperaElemento(pageLimitePortador.dataCNPJ, 60);
		esperaElemento(pageLimitePortador.lblNCartao, 60);
		esperaElemento(pageLimitePortador.dataNCartao, 60);
		esperaElemento(pageLimitePortador.lblContato, 60);
		esperaElemento(pageLimitePortador.dataContato, 60);
		esperaElemento(pageLimitePortador.dataNomedocartao, 60);
		esperaElemento(pageLimitePortador.dataCompid, 60);
		esperaElemento(pageLimitePortador.lblStatus, 60);
		esperaElemento(pageLimitePortador.dataStatus, 60);
		esperaElemento(pageLimitePortador.lblVencimento, 60);
		esperaElemento(pageLimitePortador.dataVencimento, 60);
		esperaElemento(pageLimitePortador.lbldisponivelcompra, 60);
		esperaElemento(pageLimitePortador.dataValorUtilizado, 60);
		esperaElemento(pageLimitePortador.lblValorUtilizado, 60);
		esperaElemento(pageLimitePortador.dataTotalDisponivel, 60);
		esperaElemento(pageLimitePortador.lblLimiteCompras, 60);
		esperaElemento(pageLimitePortador.dataLimiteCompras, 60);
		esperaElemento(pageLimitePortador.lblSaqueNacional, 60);
		esperaElemento(pageLimitePortador.dataValorUtilizadonacional, 60);
		esperaElemento(pageLimitePortador.lblValorUtilizadonacional, 60);
		esperaElemento(pageLimitePortador.dataTotalDisponivelnacional, 60);
		esperaElemento(pageLimitePortador.lblLimiteTotalNacional, 60);
		esperaElemento(pageLimitePortador.dataLimiteTotalNacional, 60);
		esperaElemento(pageLimitePortador.lblSaqueExterior, 60);
		esperaElemento(pageLimitePortador.dataValorUtilizadoexterior, 60);
		esperaElemento(pageLimitePortador.lblValorUtilizadoexterior, 60);
		esperaElemento(pageLimitePortador.dataVUtilizado, 60);
		esperaElemento(pageLimitePortador.dataTotalDisponivelexterior, 60);
		esperaElemento(pageLimitePortador.lblLimiteTotalexterior, 60);
		esperaElemento(pageLimitePortador.dataLimiteTotalExterior, 60);
		esperaElemento(pageLimitePortador.dataLCompras, 60);
		esperaElemento(pageLimitePortador.lblSaqueInternacional, 60);
		esperaElemento(pageLimitePortador.dataValorUtilizadoInternacional, 60);
		esperaElemento(pageLimitePortador.lblValorUtilizadoInternacional, 60);
		esperaElemento(pageLimitePortador.dataVInternacional, 60);
		esperaElemento(pageLimitePortador.dataTotalDisponivelInternacional, 60);
		esperaElemento(pageLimitePortador.lblLimiteTotalinternacional, 60);
		esperaElemento(pageLimitePortador.dataLimiteTotalInternacional, 60);
		esperaElemento(pageLimitePortador.dataLComprasInternacional, 60);
		scrollAteOElemento(pageLimitePortador.lblValorTotal);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblValorTotal));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblNomedoportador));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataNomedoportador));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblNomedaempresa));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataNomedaempresa));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataCNPJ));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblNCartao));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataNCartao));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblContato));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataContato));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataNomedocartao));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataCompid));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblStatus));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataStatus));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblVencimento));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataVencimento));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lbldisponivelcompra));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataValorUtilizado));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblValorUtilizado));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataTotalDisponivel));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblLimiteCompras));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataLimiteCompras));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblSaqueNacional));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataValorUtilizadonacional));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblValorUtilizadonacional));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataTotalDisponivelnacional));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblLimiteTotalNacional));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataLimiteTotalNacional));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblSaqueExterior));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataValorUtilizadoexterior));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblValorUtilizadoexterior));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataVUtilizado));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataTotalDisponivelexterior));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblLimiteTotalexterior));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataLimiteTotalExterior));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataLCompras));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblSaqueInternacional));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataValorUtilizadoInternacional));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblValorUtilizadoInternacional));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataVInternacional));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataTotalDisponivelInternacional));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblLimiteTotalinternacional));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataLimiteTotalInternacional));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataLComprasInternacional));
		espera(15);
		Assert.assertEquals("Nome do portador", pageLimitePortador.lblNomedoportador.getText());
		Assert.assertEquals("NOME PORTADOR TESTE", pageLimitePortador.dataNomedoportador.getText());
		Assert.assertEquals("Empresa", pageLimitePortador.lblNomedaempresa.getText());
		Assert.assertEquals("MASSA MELHORIA EXTRATO", pageLimitePortador.dataNomedaempresa.getText());
		Assert.assertEquals("19.527.168/0001-88", pageLimitePortador.dataCNPJ.getText());
		Assert.assertEquals("N�mero do cart�o", pageLimitePortador.lblNCartao.getText());
		Assert.assertEquals("**** **** **** 7057", pageLimitePortador.dataNCartao.getText());
		Assert.assertEquals("Contato", pageLimitePortador.lblContato.getText());
		Assert.assertEquals("(011) 9 6633-4466", pageLimitePortador.dataContato.getText());
		Assert.assertEquals("EMPRESARIAL", pageLimitePortador.dataNomedocartao.getText());
		Assert.assertEquals("614170", pageLimitePortador.dataCompid.getText());
		Assert.assertEquals("Status", pageLimitePortador.lblStatus.getText());
		Assert.assertEquals("Cancelado", pageLimitePortador.dataStatus.getText());
		Assert.assertEquals("Vencimento", pageLimitePortador.lblVencimento.getText());
		Assert.assertEquals("Dia 5", pageLimitePortador.dataVencimento.getText());
		Assert.assertEquals("Dispon�vel para compra", pageLimitePortador.lbldisponivelcompra.getText());
		Assert.assertEquals("R$ 0,00", pageLimitePortador.dataValorUtilizado.getText());
		Assert.assertEquals("Valor Utilizado", pageLimitePortador.lblValorUtilizado.getText());
		Assert.assertEquals("Total Dispon�vel: R$ 28.000,00", pageLimitePortador.dataTotalDisponivel.getText());
		Assert.assertEquals("Limite Compras", pageLimitePortador.lblLimiteCompras.getText());
		Assert.assertEquals("R$ 28.000,00", pageLimitePortador.dataLimiteCompras.getText());
		Assert.assertEquals("Dispon�vel para saque nacional", pageLimitePortador.lblSaqueNacional.getText());
		Assert.assertEquals("R$ 2.000,00", pageLimitePortador.dataValorUtilizadonacional.getText());
		Assert.assertEquals("Valor Utilizado", pageLimitePortador.lblValorUtilizadonacional.getText());
		Assert.assertEquals("Total Dispon�vel: R$ 0,00", pageLimitePortador.dataTotalDisponivelnacional.getText());
		Assert.assertEquals("Limite Total", pageLimitePortador.lblLimiteTotalNacional.getText());
		Assert.assertEquals("R$ 2.000,00", pageLimitePortador.dataLimiteTotalNacional.getText());
		Assert.assertEquals("Dispon�vel para compra no exterior", pageLimitePortador.lblSaqueExterior.getText());
		Assert.assertEquals("Valor Utilizado", pageLimitePortador.lblValorUtilizadoexterior.getText());
		Assert.assertEquals("(R$ 0,00)", pageLimitePortador.dataVUtilizado.getText());
		Assert.assertEquals("Total Dispon�vel: R$ 12.000,00", pageLimitePortador.dataTotalDisponivelexterior.getText());
		Assert.assertEquals("Limite Compras", pageLimitePortador.lblLimiteTotalexterior.getText());
		Assert.assertEquals("US$ 2.380,95", pageLimitePortador.dataLimiteTotalExterior.getText());
		Assert.assertEquals("(R$ 12.000,00)", pageLimitePortador.dataLCompras.getText());
		Assert.assertEquals("Dispon�vel para saque internacional", pageLimitePortador.lblSaqueInternacional.getText());
		Assert.assertEquals("US$ 2.119,05", pageLimitePortador.dataValorUtilizadoInternacional.getText());
		Assert.assertEquals("Valor Utilizado", pageLimitePortador.lblValorUtilizadoInternacional.getText());
		Assert.assertEquals("(R$ 0,00)", pageLimitePortador.dataVInternacional.getText());
		Assert.assertEquals("Total Dispon�vel: R$ 10.680,00", pageLimitePortador.dataTotalDisponivelInternacional.getText());
		Assert.assertEquals("Limite Total", pageLimitePortador.lblLimiteTotalinternacional.getText());
		Assert.assertEquals("US$ 2.119,05", pageLimitePortador.dataLimiteTotalInternacional.getText());
		Assert.assertEquals("(R$ 10.680,00)", pageLimitePortador.dataLComprasInternacional.getText());
		Assert.assertEquals("Total Dispon�vel: R$ 40.000,00", pageLimitePortador.lblValorTotal.getText());
		scrollToTop();
		tirarScreenshot("Tela  Verifico limites e validar os  detalhes do Portador");
	}
	
	@Then("Verifico detalhes dos meus limites como Portador")
	public void verifico_detalhes_dos_meus_limites() throws InterruptedException  {
		esperaElemento(pageLimitePortador.tituladaTela, 60);
		esperaElemento(pageLimitePortador.lblLimiteTotal, 60);
		esperaElemento(pageLimitePortador.dataCartaoPortador, 60);
		esperaElemento(pageLimitePortador.dataNomedocartaoPortador, 60);
		esperaElemento(pageLimitePortador.lblStatusPortador, 60);
		esperaElemento(pageLimitePortador.dataStatusPortador, 60);
		esperaElemento(pageLimitePortador.bloqueiotemporarioportador, 60);
		esperaElemento(pageLimitePortador.lbldiadecompra, 60);
		esperaElemento(pageLimitePortador.datadiadecompra, 60);
		esperaElemento(pageLimitePortador.lblLimiteDisponivel, 60);
		esperaElemento(pageLimitePortador.lblLimiteUtilizado, 60);
		esperaElemento(pageLimitePortador.dataLimiteUtilizado, 60);
		esperaElemento(pageLimitePortador.lblCompranacional, 60);
		esperaElemento(pageLimitePortador.dataCompranacional, 60);
		esperaElemento(pageLimitePortador.dataLimiteTotalnacional, 60);
		esperaElemento(pageLimitePortador.lblSaquenacional, 60);
		esperaElemento(pageLimitePortador.dataSaquenacional, 60);
		esperaElemento(pageLimitePortador.dataLimiteTotalNacionalPortador, 60);
		esperaElemento(pageLimitePortador.lblCompraexterior, 60);
		esperaElemento(pageLimitePortador.dataCompraexterior, 60);
		esperaElemento(pageLimitePortador.dataCompraexteriornacional, 60);
		esperaElemento(pageLimitePortador.lblSaqueexterior, 60);
		esperaElemento(pageLimitePortador.dataSaqueexterior, 60);
		esperaElemento(pageLimitePortador.datalimitetotalexterior, 60);
		scrollAteOElemento(pageLimitePortador.tituladaTela);
		Assert.assertEquals("Consulta de limites", pageLimitePortador.tituladaTela.getText());
		Assert.assertEquals("Limite total R$ 0,00", pageLimitePortador.lblLimiteTotal.getText());
		Assert.assertEquals("Final 5934", pageLimitePortador.dataCartaoPortador.getText());
		Assert.assertEquals("AMEX CORPORATE", pageLimitePortador.dataNomedocartaoPortador.getText());
		Assert.assertEquals("Status", pageLimitePortador.lblStatusPortador.getText());
		Assert.assertEquals("Ativo", pageLimitePortador.dataStatusPortador.getText());
		Assert.assertEquals("Bloqueio tempor�rio", pageLimitePortador.bloqueiotemporarioportador.getText());
		Assert.assertEquals("Melhor dia de compra", pageLimitePortador.lbldiadecompra.getText());
		Assert.assertEquals("Dia 22", pageLimitePortador.datadiadecompra.getText());
		Assert.assertEquals("Limite dispon�vel", pageLimitePortador.lblLimiteDisponivel.getText());
		Assert.assertEquals("R$ 0,00", pageLimitePortador.dataLimiteDisponivel.getText());
		Assert.assertEquals("Limite utilizado", pageLimitePortador.lblLimiteUtilizado.getText());
		Assert.assertEquals("R$ 0,00", pageLimitePortador.dataLimiteUtilizado.getText());
		Assert.assertEquals("Compra nacional", pageLimitePortador.lblCompranacional.getText());
		Assert.assertEquals("R$ 0,00", pageLimitePortador.dataCompranacional.getText());
		Assert.assertEquals("Limite total R$ 0,00", pageLimitePortador.dataLimiteTotalnacional.getText());
		Assert.assertEquals("Saque nacional", pageLimitePortador.lblSaquenacional.getText());
		Assert.assertEquals("R$ 0,00", pageLimitePortador.dataSaquenacional.getText());
		Assert.assertEquals("Limite total R$ 0,00", pageLimitePortador.dataLimiteTotalNacionalPortador.getText());
		Assert.assertEquals("Compra no exterior", pageLimitePortador.lblCompraexterior.getText());
		Assert.assertEquals("US$ 0,00 (R$ 0,00)", pageLimitePortador.dataCompraexterior.getText());
		Assert.assertEquals("Limite total US$ 0,00(R$ 0,00)", pageLimitePortador.dataCompraexteriornacional.getText());
		Assert.assertEquals("Saque no exterior", pageLimitePortador.lblSaqueexterior.getText());
		Assert.assertEquals("US$ 0,00 (R$ 0,00)", pageLimitePortador.dataSaqueexterior.getText());
		Assert.assertEquals("Limite total US$ 0,00(R$ 0,00)", pageLimitePortador.datalimitetotalexterior.getText());
		scrollToTop();
		tirarScreenshot("Tela  Verifico detalhes dos meus limites como Portador");
	}
	
	@Then("Verifico a data e horario correto da ultima atualizacao como Gestor da tela Consulta de Limites")
	public void verifico_data_horario_correto_ultima_atualizacao() throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(limiteGestor.msgTitulo, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
		limiteGestor.clicarTitulo();
		esperaElemento(limiteGestor.lbldatapage, 60);
		//esperaElemento(limiteGestor.datapage, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.lbldatapage));
		//Assert.assertTrue(elementoExiste(limiteGestor.datapage));
		scrollAteOElemento(limiteGestor.lbldatapage);
		Assert.assertEquals("�ltima atualiza��o em: 21/03/2022 �s 16h37", limiteGestor.lbldatapage.getText());
		//Assert.assertEquals("21/03/2022 �s 16h37", limiteGestor.datapage.getText());
		espera(5);
		scrollToTop();
		tirarScreenshot("Tela Consulta de Limite com o hor�rio da �ltima atualiza��o");
	}
	
	@Then("Verifico a data e horario correto da ultima atualizacao do Portador como Gestor")
	public void verifico_data_horario_correto_ultima_atualizacao_do_Portador() throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(limiteGestor.msgTitulo, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
		limiteGestor.clicarTitulo();
		esperaElemento(limiteGestor.lbldatapage, 60);
		//esperaElemento(limiteGestor.datapage, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.lbldatapage));
		//Assert.assertTrue(elementoExiste(limiteGestor.datapage));
		scrollAteOElemento(limiteGestor.lbldatapage);
		Assert.assertEquals("�ltima atualiza��o em: 21/03/2022 �s 16h37", limiteGestor.lbldatapage.getText());
		//Assert.assertEquals("21/03/2022 �s 16h37", limiteGestor.datapage.getText());
		espera(5);
		scrollToTop();
		tirarScreenshot("Tela Consulta de Limite com o hor�rio da �ltima atualiza��o");
		esperaElemento(pageLimitePortador.lblEscolhaPortador, 60);
		scrollAteOElemento(pageLimitePortador.lblEscolhaPortador);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblEscolhaPortador));
		pageLimitePortador.clicarEscolhaPortador();
		esperaElemento(limiteGestor.lbldatapage, 60);
		scrollAteOElemento(limiteGestor.lbldatapage);
		Assert.assertTrue(elementoExiste(limiteGestor.lbldatapage));
		espera(15);
		Assert.assertEquals("�ltima atualiza��o em: 21/03/2022 �s 16h37", limiteGestor.lbldatapage.getText());
		scrollToTop();
		tirarScreenshot("Tela final limite Portador");
	}

	@Then("Valido o bot�o Voltar")
	public void valido_o_bot�o_Voltar() {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(limiteGestor.msgTitulo, 60);
		esperaElemento(pageLimitePortador.lblPortadores, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblPortadores));
		pageLimitePortador.clicarMenuPortadores();
		tirarScreenshot("Tela consulta de limites");
		esperaElemento(pageLimitePortador.lblEscolhaPortador, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblEscolhaPortador));
		tirarScreenshot("Menu escolha de Portador");
		pageLimitePortador.clicarEscolhaPortador();
		esperaElemento(pageLimitePortador.lblValorTotal, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblValorTotal));
		Assert.assertEquals("Total Dispon�vel: R$ 40.000,00", pageLimitePortador.lblValorTotal.getText());
		tirarScreenshot("Tela detalhes limite Portador");
		esperaElemento(limiteGestor.btnvoltar, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.btnvoltar));
		scrollAteOElemento(limiteGestor.btnvoltar);
		tirarScreenshot("Tela detalhes limite Portador voltar");
		limiteGestor.clicarBtnvoltar();
		esperaElemento(pageLimitePortador.lblBuscaPortador, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblBuscaPortador));
		Assert.assertEquals("Buscar por um portador", pageLimitePortador.lblBuscaPortador.getText());
		scrollAteOElemento(pageLimitePortador.lblBuscaPortador);
		tirarScreenshot("Tela final limite Portador");
	}
	
	@Then("Verifico Limite Total do Representante Estrangeiro")
	public void verifico_Limite_Total_Representante_Estrangeiro() throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(limiteGestor.msgTitulo, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
		limiteGestor.clicarTitulo();
		tirarScreenshot("Tela consulta de limites");
		esperaElemento(limiteGestor.lblValorDeLimiteempresa, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.lblValorDeLimiteempresa));
		Assert.assertEquals("R$ 50.000,00", limiteGestor.lblValorDeLimiteempresa.getText());
		tirarScreenshot("Tela final limite Representante Estrangeiro");
	}
	
	@Then("Verifico Total Disponivel como Representante Estrangeiro de Portador Hibrido")
	public void verifico_Consulta_De_Limite_Representante_Estrangeiro_Portador_Hibrido() throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(limiteGestor.msgTitulo, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
		limiteGestor.clicarTitulo();
		tirarScreenshot("Tela consulta de limites");
		esperaElemento(pageLimitePortador.lblEscolhaPortador, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblEscolhaPortador));
		pageLimitePortador.clicarEscolhaPortador();
		tirarScreenshot("Menu escolha de Portador");
		esperaElemento(pageLimitePortador.lblValorDisponivelPortadorHibrido, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblValorDisponivelPortadorHibrido));
		Assert.assertEquals("Total Dispon�vel: R$ 20.000,00", pageLimitePortador.lblValorDisponivelPortadorHibrido.getText());
		tirarScreenshot("Tela final Total Disponivel  do Portador Hibrido do Representante Estrangeiro");
	}

	@Then("Verifico Total Disponivel como Representante Estrangeiro de Portador Hibrido CFI {string}")
	public void verifico_Consulta_De_Limite_Representante_Estrangeiro_Portador_HibridoCFI (String REportadorHibridoCFI) throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(limiteGestor.msgTitulo, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
		limiteGestor.clicarTitulo();
		tirarScreenshot("Tela consulta de limites");
		espera(5);
		esperaElemento(pageLimitePortador.lblPortadores, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblPortadores));
		pageLimitePortador.clicarMenuPortadores();
		tirarScreenshot("Tela consulta de limites aba Portadores");
		esperaElemento(pageLimitePortador. txtCpfNomeHibridoCFI, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador. txtCpfNomeHibridoCFI));
		esperaElemento(pageLimitePortador.lblBuscaPortador, 60);
		digitarLetra(pageLimitePortador.txtPortador,REportadorHibridoCFI.toString());
		espera(5);
		scrollAteOElemento(pageLimitePortador.lblEscolhaPortadorHibridoCFI);
		tirarScreenshot("Menu escolha de Portador Digitado");
		pageLimitePortador.clicarEscolhaPortadorHibridoCFIRE();
		esperaElemento(pageLimitePortador.lblValorDisponivelPortadorHibridoCFI, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblValorDisponivelPortadorHibridoCFI));
		Assert.assertEquals("Total Dispon�vel: R$ 19.999,00", pageLimitePortador.lblValorDisponivelPortadorHibridoCFI.getText());
		tirarScreenshot("Tela final limite total do Portador Hibrido CFI do Representante Estrangeiro");
	}
	
	
	@Then("Verifico Total Disponivel como Representante Estrangeiro do Portador Centralizado {string}")
	public void verifico_Consulta_De_Limite_Representante_Estrangeiro_Portador_Centralizado(String escolherPortadorCentralizado)  throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(limiteGestor.msgTitulo, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
		limiteGestor.clicarTitulo();
		tirarScreenshot("Tela consulta de limites");
		esperaElemento(pageLimitePortador.lblEscolhaPortadorCentralizado, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblEscolhaPortadorCentralizado));
		scrollAteOElemento(pageLimitePortador.lblEscolhaPortadorCentralizado);
		tirarScreenshot("Menu escolha de Portador Centralizado");
		scrollToTop();
		esperaElemento(pageLimitePortador.txtCpfNomeCentralizado, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.txtCpfNomeCentralizado));
		esperaElemento(pageLimitePortador.lblBuscaPortador, 60);
		digitarLetra(pageLimitePortador.txtPortador,escolherPortadorCentralizado.toString());
		scrollAteOElemento(pageLimitePortador.lblBuscaPortador);
		espera(10);
		pageLimitePortador.clicarEscolhaPortadorCentralizado();
		scrollToTop();
		espera(5);
		esperaElemento(pageLimitePortador.lblValorDisponivelPortadorCentralizado, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblValorDisponivelPortadorCentralizado));
		scrollAteOElemento(pageLimitePortador.lblValorDisponivelPortadorCentralizado);
		espera(10);
		Assert.assertEquals("Total Dispon�vel: R$ 10.000,00", pageLimitePortador.lblValorDisponivelPortadorCentralizado.getText());
		scrollToTop();
		tirarScreenshot("Tela final limite total do Portador Centralizado do Representante Estrangeiro");
	}

	@Then("Verifico Total Disponivel como Representante Estrangeiro de Portador Centralizado CFI {string}")
	public void verifico_Consulta_De_Limite_Representante_Estrangeiro_Portador_CentralizadoCFI(String escolherPortadorCentralizadoCFI ) throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(limiteGestor.msgTitulo, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
		limiteGestor.clicarTitulo();
		tirarScreenshot("Tela consulta de limites");
		espera(5);
		esperaElemento(pageLimitePortador.lblPortadores, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblPortadores));
		pageLimitePortador.clicarMenuPortadores();
		tirarScreenshot("Tela consulta de limites aba Portadores");
		esperaElemento(pageLimitePortador. txtCpfNomeCentralizadoCFI, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador. txtCpfNomeCentralizadoCFI));
		esperaElemento(pageLimitePortador.lblBuscaPortador, 60);
		digitarLetra(pageLimitePortador.txtPortador,escolherPortadorCentralizadoCFI.toString());
		espera(5);
		scrollAteOElemento(pageLimitePortador.lblEscolhaPortadorCentralizadoCFI);
		tirarScreenshot("Menu escolha de Portador Centralizado CFI Digitado");
		pageLimitePortador.clicarEscolhaPortadorCentralizadoCFI();
		esperaElemento(pageLimitePortador.lblValorDisponivelPortadorCentralizadoCFI, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblValorDisponivelPortadorCentralizadoCFI));
		Assert.assertEquals("Total Dispon�vel: R$ 20.000,00", pageLimitePortador.lblValorDisponivelPortadorCentralizadoCFI.getText());
		tirarScreenshot("Tela final limite total do Portador Centralizado CFI do Representante Estrangeiro");
	}
	
	@Then("Verifico limites e valido os  detalhes do Portador como Representante Estrangeiro {string}")
	public void verifico_o_consulta_e_validar_detalhes_do_Portador(String escolherPortadorCentralizado) throws InterruptedException  {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
		tirarScreenshot("Menu consultas");
		esperaElemento(limiteGestor.msgTitulo, 60);
		Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
		limiteGestor.clicarTitulo();
		tirarScreenshot("Tela consulta de limites em Portugu�s");
		espera(3);
		esperaElemento(pageLimitePortador.lblPortadores, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblPortadores));
		pageLimitePortador.clicarMenuPortadores();
		tirarScreenshot("Tela consulta de limites aba Portadores");
		Assert.assertTrue(elementoExiste(pageLimitePortador. txtCpfNomeCentralizado));
		esperaElemento(pageLimitePortador.lblBuscaPortador, 60);
		digitarLetra(pageLimitePortador.txtPortador,escolherPortadorCentralizado.toString());
		espera(5);
		scrollAteOElemento(pageLimitePortador.lblEscolhaPortadorCentralizado);
		tirarScreenshot("Menu escolha de Portador Centralizado Digitado");
		esperaElemento(pageLimitePortador.lblEscolhaPortador, 60);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblEscolhaPortador));
		tirarScreenshot("Menu escolha de Portador");
		pageLimitePortador.clicarEscolhaPortador();
		espera(2);
		esperaElemento(pageLimitePortador.lblconsutalimiteRE, 60);
		esperaElemento(pageLimitePortador.lbldetalhesportadorRE, 60);
		esperaElemento(pageLimitePortador.dataNomedocartaoRE, 60);
		esperaElemento(pageLimitePortador.dataCompidRE, 60);
		esperaElemento(pageLimitePortador.lblStatusRE, 60);
		esperaElemento(pageLimitePortador.dataStatusRE, 60);
		esperaElemento(pageLimitePortador.lblVencimentoRE, 60);
		esperaElemento(pageLimitePortador.dataVencimentoRE, 60);
		esperaElemento(pageLimitePortador.lblNomedoportadorRE, 60);
		esperaElemento(pageLimitePortador.dataNomedoportadorRE, 60);
		esperaElemento(pageLimitePortador.lblNomedaempresaRE, 60);
		esperaElemento(pageLimitePortador.dataNomedaempresaRE, 60);
		esperaElemento(pageLimitePortador.dataCNPJRE, 60);
		esperaElemento(pageLimitePortador.lblNCartaoRE, 60);
		esperaElemento(pageLimitePortador.dataNCartaoRE, 60);
		esperaElemento(pageLimitePortador.lblContatoRE, 60);
		esperaElemento(pageLimitePortador.dataContatoRE, 60);
		esperaElemento(pageLimitePortador.lbldisponivelcompraRE, 60);
		esperaElemento(pageLimitePortador.dataValorUtilizadoRE, 60);
		esperaElemento(pageLimitePortador.lblValorUtilizadoRE, 60);
		esperaElemento(pageLimitePortador.dataTotalDisponivelRE, 60);
		esperaElemento(pageLimitePortador.lblLimiteComprasRE, 60);
		esperaElemento(pageLimitePortador.dataLimiteComprasRE, 60);
		esperaElemento(pageLimitePortador.lblSaqueNacionalRE, 60);
		esperaElemento(pageLimitePortador.dataValorUtilizadonacionalRE, 60);
		esperaElemento(pageLimitePortador.lblValorUtilizadonacionalRE, 60);
		esperaElemento(pageLimitePortador.dataTotalDisponivelnacionalRE, 60);
		esperaElemento(pageLimitePortador.lblLimiteTotalNacionalRE, 60);
		esperaElemento(pageLimitePortador.dataLimiteTotalNacionalRE, 60);
		esperaElemento(pageLimitePortador.lblSaqueExteriorRE, 60);
		esperaElemento(pageLimitePortador.dataValorUtilizadoexteriorRE, 60);
		esperaElemento(pageLimitePortador.lblValorUtilizadoexteriorRE, 60);
		esperaElemento(pageLimitePortador.dataVUtilizadoRE, 60);
		esperaElemento(pageLimitePortador.dataTotalDisponivelexteriorRE, 60);
		esperaElemento(pageLimitePortador.lblLimiteTotalexteriorRE, 60);
		esperaElemento(pageLimitePortador.dataLimiteTotalExteriorRE, 60);
		esperaElemento(pageLimitePortador.dataLComprasRE, 60);
		esperaElemento(pageLimitePortador.lblSaqueInternacionalRE, 60);
		esperaElemento(pageLimitePortador.dataValorUtilizadoInternacionalRE, 60);
		esperaElemento(pageLimitePortador.lblValorUtilizadoInternacionalRE, 60);
		esperaElemento(pageLimitePortador.dataVInternacionalRE, 60);
		esperaElemento(pageLimitePortador.dataTotalDisponivelInternacionalRE, 60);
		esperaElemento(pageLimitePortador.lblLimiteTotalinternacionalRE, 60);
		esperaElemento(pageLimitePortador.dataLimiteTotalInternacionalRE, 60);
		esperaElemento(pageLimitePortador.dataLComprasInternacionalRE, 60);
		esperaElemento(pageLimitePortador.lbltotaldisponivelRE, 60);
		esperaElemento(pageLimitePortador.datatotalutilizadoRE, 60);
		esperaElemento(pageLimitePortador.lbllimiteTotalRE, 60);
		esperaElemento(pageLimitePortador.datalimiteutilizadoRE, 60);
		esperaElemento(pageLimitePortador.lblvalorutilizadoRE, 60);
		scrollAteOElemento(pageLimitePortador.lbltotaldisponivelRE);
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblconsutalimiteRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lbldetalhesportadorRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataNomedocartaoRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataCompidRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblStatusRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataStatusRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblVencimentoRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataVencimentoRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblNomedoportadorRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataNomedoportadorRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblNomedaempresaRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataNomedaempresaRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataCNPJRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblNCartaoRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataNCartaoRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblContatoRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataContatoRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lbldisponivelcompraRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataValorUtilizadoRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblValorUtilizadoRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataTotalDisponivelRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblLimiteComprasRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataLimiteComprasRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblSaqueNacionalRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataValorUtilizadonacionalRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblValorUtilizadonacionalRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataTotalDisponivelnacionalRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblLimiteTotalNacionalRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataLimiteTotalNacionalRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblSaqueExteriorRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataValorUtilizadoexteriorRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblValorUtilizadoexteriorRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataVUtilizadoRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataTotalDisponivelexteriorRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblLimiteTotalexteriorRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataLimiteTotalExteriorRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataLComprasRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblSaqueInternacionalRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataValorUtilizadoInternacionalRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblValorUtilizadoInternacionalRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataVInternacionalRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataTotalDisponivelInternacionalRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblLimiteTotalinternacionalRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataLimiteTotalInternacionalRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.dataLComprasInternacionalRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lbltotaldisponivelRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.datatotalutilizadoRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lbllimiteTotalRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.datalimiteutilizadoRE));
		Assert.assertTrue(elementoExiste(pageLimitePortador.lblvalorutilizadoRE));
		espera(15);
		Assert.assertEquals("Consulta de Limite", pageLimitePortador.lblconsutalimiteRE.getText());
		Assert.assertEquals("Detalhes do Portador", pageLimitePortador.lbldetalhesportadorRE.getText());
		Assert.assertEquals("AMEX CONTA EBTA", pageLimitePortador.dataNomedocartaoRE.getText());
		Assert.assertEquals("605313", pageLimitePortador.dataCompidRE.getText());
		Assert.assertEquals("Status", pageLimitePortador.lblStatusRE.getText());
		Assert.assertEquals("Bloqueado\nNovo Cart�o", pageLimitePortador.dataStatusRE.getText());
		Assert.assertEquals("Vencimento", pageLimitePortador.lblVencimentoRE.getText());
		Assert.assertEquals("Dia 25", pageLimitePortador.dataVencimentoRE.getText());
		Assert.assertEquals("Nome do portador", pageLimitePortador.lblNomedoportadorRE.getText());
		Assert.assertEquals("PORT ARQ DOIS", pageLimitePortador.dataNomedoportadorRE.getText());
		Assert.assertEquals("Empresa", pageLimitePortador.lblNomedaempresaRE.getText());
		Assert.assertEquals("ARQUIVO DOIS", pageLimitePortador.dataNomedaempresaRE.getText());
		Assert.assertEquals("75.808.590/0001-82", pageLimitePortador.dataCNPJRE.getText());
		Assert.assertEquals("N�mero do cart�o", pageLimitePortador.lblNCartaoRE.getText());
		Assert.assertEquals("**** **** **** 9341", pageLimitePortador.dataNCartaoRE.getText());
		Assert.assertEquals("Contato", pageLimitePortador.lblContatoRE.getText());
		Assert.assertEquals("(011) 9 6633-4466", pageLimitePortador.dataContatoRE.getText());
		Assert.assertEquals("Dispon�vel para compra", pageLimitePortador.lbldisponivelcompraRE.getText());
		Assert.assertEquals("R$ 0,00", pageLimitePortador.dataValorUtilizadoRE.getText());
		Assert.assertEquals("Valor Utilizado", pageLimitePortador.lblValorUtilizadoRE.getText());
		Assert.assertEquals("Total Dispon�vel: R$ 0,00", pageLimitePortador.dataTotalDisponivelRE.getText());
		Assert.assertEquals("Limite Compras", pageLimitePortador.lblLimiteComprasRE.getText());
		Assert.assertEquals("R$ 0,00", pageLimitePortador.dataLimiteComprasRE.getText());
		Assert.assertEquals("Dispon�vel para saque nacional", pageLimitePortador.lblSaqueNacionalRE.getText());
		Assert.assertEquals("R$ 0,00", pageLimitePortador.dataValorUtilizadonacionalRE.getText());
		Assert.assertEquals("Valor Utilizado", pageLimitePortador.lblValorUtilizadonacionalRE.getText());
		Assert.assertEquals("Total Dispon�vel: R$ 0,00", pageLimitePortador.dataTotalDisponivelnacionalRE.getText());
		Assert.assertEquals("Limite Total", pageLimitePortador.lblLimiteTotalNacionalRE.getText());
		Assert.assertEquals("R$ 0,00", pageLimitePortador.dataLimiteTotalNacionalRE.getText());
		Assert.assertEquals("Dispon�vel para compra no exterior", pageLimitePortador.lblSaqueExteriorRE.getText());
		Assert.assertEquals("US$ 0,00", pageLimitePortador.dataValorUtilizadoexteriorRE.getText());
		Assert.assertEquals("Valor Utilizado", pageLimitePortador.lblValorUtilizadoexteriorRE.getText());
		Assert.assertEquals("(R$ 0,00)", pageLimitePortador.dataVUtilizadoRE.getText());
		Assert.assertEquals("Total Dispon�vel: R$ 0,00", pageLimitePortador.dataTotalDisponivelexteriorRE.getText());
		Assert.assertEquals("Limite Compras", pageLimitePortador.lblLimiteTotalexteriorRE.getText());
		Assert.assertEquals("US$ 0,00", pageLimitePortador.dataLimiteTotalExteriorRE.getText());
		Assert.assertEquals("(R$ 0,00)", pageLimitePortador.dataLComprasRE.getText());
		Assert.assertEquals("Dispon�vel para saque internacional", pageLimitePortador.lblSaqueInternacionalRE.getText());
		Assert.assertEquals("US$ 0,00", pageLimitePortador.dataValorUtilizadoInternacionalRE.getText());
		Assert.assertEquals("Valor Utilizado", pageLimitePortador.lblValorUtilizadoInternacionalRE.getText());
		Assert.assertEquals("(R$ 0,00)", pageLimitePortador.dataVInternacionalRE.getText());
		Assert.assertEquals("Total Dispon�vel: R$ 0,00", pageLimitePortador.dataTotalDisponivelInternacionalRE.getText());
		Assert.assertEquals("Limite Total", pageLimitePortador.lblLimiteTotalinternacionalRE.getText());
		Assert.assertEquals("US$ 0,00", pageLimitePortador.dataLimiteTotalInternacionalRE.getText());
		Assert.assertEquals("(R$ 0,00)", pageLimitePortador.dataLComprasInternacionalRE.getText());
		Assert.assertEquals("Total Dispon�vel: R$ 0,00", pageLimitePortador.lbltotaldisponivelRE.getText());
		Assert.assertEquals("R$ 0,00", pageLimitePortador.datatotalutilizadoRE.getText());
		Assert.assertEquals("Limite Total", pageLimitePortador.lbllimiteTotalRE.getText());
		Assert.assertEquals("R$ 0,00", pageLimitePortador.datalimiteutilizadoRE.getText());
		Assert.assertEquals("Valor Utilizado", pageLimitePortador.lblvalorutilizadoRE.getText());
		espera(2);		
		scrollToTop();
		tirarScreenshot("Tela  Verifico limites e validar os  detalhes do Portador");
	}

	@Then("Valido pagina��o Empresas")
	public void valido_pagina��o_Empresas() {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
	}

	@Then("Valido pagina��o Produtos")
	public void valido_pagina��o_Produtos() {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
	}

	@Then("Valido pagina��o Portadores")
	public void valido_pagina��o_Portadores() {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		cabecalhoLogado.clicarMenuConsultas();
	}

	@Then("Efetuo bloqueio temporario")
	public void efetuo_bloqueio_tempor�rio() {
		tirarScreenshot("Tela Consulta de Limites - Cart�o Desbloqueado");
		Assert.assertTrue(elementoExiste(limitePortador.btnBloqueioTemporario));
		limitePortador.clicarBtnBloqueioTemporario();
		esperaElemento(limitePortador.msgBloqueioTemporario, 60);
		tirarScreenshot("Tela Consulta de Limites - Cart�o Bloqueado");
		Assert.assertEquals("Cart�o bloqueado.",limitePortador.msgBloqueioTemporario.getText());
	}

	@Then("Efetuo desbloqueio temporario")
	public void efetuo_desbloqueio_tempor�rio() {
		tirarScreenshot("Tela Consulta de Limites - Cart�o Bloqueado");
		Assert.assertTrue(elementoExiste(limitePortador.btnBloqueioTemporario));
		limitePortador.clicarBtnBloqueioTemporario();
		esperaElemento(limitePortador.msgDesbloqueioTemporario, 60);
		tirarScreenshot("Tela Consulta de Limites - Cart�o Desbloqueado");
		Assert.assertEquals("Cart�o desbloqueado.",limitePortador.msgDesbloqueioTemporario.getText());

	}

	@Then("efetuo desbloqueio tempor�rio com bloqueio na central")
	public void efetuo_desbloqueio_tempor�rio_com_bloqueio_na_central() {
			//TO-DO Aguardando Massa
	}

}