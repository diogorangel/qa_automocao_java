#encoding: Cp1252
@004_CARTOESPJ-3186_ConsultarLimitesENG
Feature: CARTOESPJ-3186 Consultar Limites portal PJ ENG


  @ConsultarLimite_GestorVerificarConsultaLimiteEmpresaRepresentanteEstrangeiroEN
  Scenario Outline: CARTOESPJ - 3186 - Gestor Estrangeiro - Verificar Consulta Limite Empresa Representante Estrangeiro en Ingl�s
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
   	When Efetuo o login em ingles <email> <senha>
   	Then Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
    Then Verifico o login efetuado com sucesso no idioma escolhido
    And Verifico Limite Representante Estrangeiro em Ingles
    And Verifico o Logout com sucesso

    Examples:
      | email           				 | senha      | ambiente | rede      |
      | "PRIMACESSWEB@GMAIL.COM" |"Br@123456"	| "TH"     | "empresa" |
      
   
   @ConsultarLimite_GestorVerificarConsultaLimiteComoRepresentanteEstrangeirodoPortadorHibridoEN
  	Scenario Outline: CARTOESPJ - 3240 - Gestor Estrangeiro - Verificar Limite Como Representante Estrangeiro de um Portador Hibrido em Ingl�s
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
   	When Efetuo o login em ingles <email> <senha>
   	Then Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
    Then Verifico o login efetuado com sucesso no idioma escolhido
    And Verifico Total Disponivel como Representante Estrangeiro de Portador Hibrido em Ingles
    And Verifico o Logout com sucesso

    Examples:
      | email           				 															 | senha     			  | ambiente | rede      |
      | "PRIMACESSWEB@GMAIL.COM" |"Br@123456"	| "TH"     				| "empresa" |
      
   @ConsultarLimite_GestorVerificarConsultaLimiteComoRepresentanteEstrangeirodoPortadorHibridoCFIEN
  	Scenario Outline: CARTOESPJ - 3240 - Gestor Estrangeiro - Verificar Limite Como Representante Estrangeiro de um Portador Hibrido CFI em Ingl�s
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
   	When Efetuo o login em ingles <email> <senha>
   	Then Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
    Then Verifico o login efetuado com sucesso no idioma escolhido
    And Verifico Total Disponivel como Representante Estrangeiro de Portador Hibrido CFI em Ingles <escolherPortadorHibridoCFI>
    And Verifico o Logout com sucesso

    Examples:
      | email           				 															 | senha             | ambiente 	| rede            | escolherPortadorHibridoCFI|
      | "TESTEQA68@GMAIL.COM"					 |"Br@12345"		| "TH"            | "empresa" | "26690132609"												 |
      
   @ConsultarLimite_GestorVerificarConsultaLimiteComoRepresentanteEstrangeirodoPortadorCentralizadoEN
  	Scenario Outline: CARTOESPJ - 3186 - Gestor Estrangeiro - Verificar Limite Como Representante Estrangeiro de um Portador Centralizado em Ingl�s
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
   	When Efetuo o login em ingles <email> <senha>
   	Then Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
    Then Verifico o login efetuado com sucesso no idioma escolhido
    And Verifico Limite Total como Representante Estrangeiro de Portador Centralizado em Ingles <escolherPortadorCentralizado>
    And Verifico o Logout com sucesso

    Examples:
      | email           														  | senha      			| ambiente | rede     			 | escolherPortadorCentralizado|
      | "TESTEQA68@GMAIL.COM" |"Br@12345"	  | "TH"     			| "empresa" | 		"93742250000"                        |
      
    @ConsultarLimite_GestorVerificarConsultaLimiteComoRepresentanteEstrangeirodoPortadorCentralizadoCFIEN
  	Scenario Outline: CARTOESPJ - 3186 - Gestor Estrangeiro - Verificar Limite Como Representante Estrangeiro de um Portador Centralizado CFI em Ingl�s
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
   	When Efetuo o login em ingles <email> <senha>
   	Then Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
    Then Verifico o login efetuado com sucesso no idioma escolhido
    And Verifico Limite Total como Representante Estrangeiro de Portador Centralizado CFI em Ingles <escolherPortadorCentralizadoCFI>
    And Verifico o Logout com sucesso

    Examples:
      | email           				 																| senha            | ambiente |       rede      | escolherPortadorCentralizadoCFI|
      | "TESTEQA68@GMAIL.COM"  					|"Br@12345" 	|    "TH"       | "empresa" |               "26690131475"   							  |
      
 @ConsultarLimite_RepresentanteEstrangeiroVerificaLimitesDetalhesdoPortadoremIngles
  	Scenario Outline: CARTOESPJ - 3186 - Gestor Estrangeiro - Verificar Limites e detalhes Como Representante Estrangeiro de um Portador em Ingl�s
 		 Given Acessar a pagina do Portal PJ <ambiente> <rede>
     When Efetuo o login em ingles <email> <senha>
     Then Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
     Then Verifico o login efetuado com sucesso no idioma escolhido
     And Verifico limites e valido os  detalhes do Portador como Representante Estrangeiro em Ingles <escolherPortadorCentralizado>
     And Verifico o Logout com sucesso
    
     Examples:
       | email           														  | senha      			| ambiente | rede     			 | escolherPortadorCentralizado|
       | "TESTEQA68@GMAIL.COM" |"Br@12345"	  | "TH"     			| "empresa" | 		"93742250000"                        |    