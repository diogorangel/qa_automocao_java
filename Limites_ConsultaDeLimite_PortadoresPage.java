package br.com.bradesco.pupj.pagesweb;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import br.com.bradesco.pupj.utilitarios.UtilsWeb;

public class Limites_ConsultaDeLimite_PortadoresPage extends UtilsWeb {

	@SuppressWarnings("unused")

	private WebDriver driverWeb;

	public Limites_ConsultaDeLimite_PortadoresPage(WebDriver driverWeb) {
		this.driverWeb = driverWeb;
		PageFactory.initElements(driverWeb, this);
	}

	// Campo verificação Nome do Cartão
	@FindBy(xpath = "//h1[contains(.,'EMPRESARIAL')]")
	public WebElement dataNomedocartao;
	// Campo verificação CompID
	@FindBy(xpath = "//p[contains(.,'614170')]")
	public WebElement dataCompid;
	// Campo verificação Titulo Status
	@FindBy(xpath = "//div[@data-test='DataItem_Label' and contains(., 'Status')]")
	public WebElement lblStatus;
	// Campo verificação Data Status
	@FindBy(xpath = "//div[@data-test='DataItem_Data' and contains(., 'Cancelado')]")
	public WebElement dataStatus;
	// Campo verificação Titulo Vencimento
	@FindBy(xpath = "//div[@data-test='DataItem_Label' and contains(., 'Vencimento')]")
	public WebElement lblVencimento;
	// Campo verificação Data Vencimento
	@FindBy(xpath = "//div[@data-test='DataItem_Data' and contains(., 'Dia 5')]")
	public WebElement dataVencimento;
	// Campo verificação Titulo Nome do Portador
	@FindBy(xpath = "//div[@data-test='DataItem_Label' and contains(., 'Nome do portador')]")
	public WebElement lblNomedoportador;
	// Campo verificação Nome do Portador
	@FindBy(xpath = "//div[@data-test='DataItem_Data' and contains(., 'NOME PORTADOR TESTE')]")
	public WebElement dataNomedoportador;
	// Campo verificação Titulo Empresa
	@FindBy(xpath = "//div[@data-test='DataItem_Label' and contains(., 'Empresa')]")
	public WebElement lblNomedaempresa;
	// Campo verificação Nome da Empresa
	@FindBy(xpath = "//div[@data-test='DataItem_Data' and contains(., 'MASSA MELHORIA EXTRATO')]")
	public WebElement dataNomedaempresa;
	// Campo verificação CNPJ
	@FindBy(xpath = "//div[@data-test='DataItem_Info' and contains(., '19.527.168/0001-88')]")
	public WebElement dataCNPJ;
	// Campo verificação Título Número do Cartão
	@FindBy(xpath = "//div[@data-test='DataItem_Label' and contains(., 'Número do cartão')]")
	public WebElement lblNCartao;
	// Campo verificação Número do Cartão
	@FindBy(xpath = "//div[@data-test='DataItem_Data' and contains(., '**** **** **** 7057')]")
	public WebElement dataNCartao;
	// Campo verificação Título Contato
	@FindBy(xpath = "//div[@data-test='DataItem_Label' and contains(., 'Contato')]")
	public WebElement lblContato;
	// Campo verificação Data Contato
	@FindBy(xpath = "//div[@data-test='DataItem_Data' and contains(., '(011)  9 6633-4466')]")
	public WebElement dataContato;
	// Gestor titulo Disponivel para compra
	@FindBy(xpath = "//h1[contains(.,'Disponível para compra')]")
	public WebElement lbldisponivelcompra;
	// Gestor Verifica valor utilizado
	@FindBy(xpath = "//span[normalize-space()='R$ 0,00']")
	public WebElement dataValorUtilizado;
	// Gestor lbl valor utilizado
	@FindBy(xpath = "//span[contains(.,'Valor Utilizado')]")
	public WebElement lblValorUtilizado;
	// Gestor lbl Total disponível
	@FindBy(xpath = "//p[contains(.,'Total Disponível: R$ 28.000,00')]")
	public WebElement dataTotalDisponivel;
	// Gestor lbl Limite Compras
	@FindBy(xpath = "//span[contains(.,'Limite Compras')]")
	public WebElement lblLimiteCompras;
	// Gestor data Limite Compras
	@FindBy(xpath = "//span[contains(.,'R$ 28.000,00')]")
	public WebElement dataLimiteCompras;
	// Gestor lbl Saque Nacional
	@FindBy(xpath = "//h1[contains(.,'Disponível para saque nacional')]")
	public WebElement lblSaqueNacional;
	// Gestor Verifica valor utilizado nacional
	@FindBy(xpath = "//span[contains(.,'R$ 2.000,00')]")
	public WebElement dataValorUtilizadonacional;
	// Gestor lbl valor utilizado saque nacional
	@FindBy(xpath = "//span[contains(.,'Valor Utilizado')]")
	public WebElement lblValorUtilizadonacional;
	// Gestor lbl Total disponível nacional
	@FindBy(xpath = "//p[contains(.,'Total Disponível: R$ 0,00')]")
	public WebElement dataTotalDisponivelnacional;
	// Gestor lbl Limite Total nacional
	@FindBy(xpath = "//span[contains(.,'Limite Total')]")
	public WebElement lblLimiteTotalNacional;
	// Gestor data Limite Compras total nacional
	@FindBy(xpath = "//span[contains(.,'R$ 2.000,00')]")
	public WebElement dataLimiteTotalNacional;
	// Gestor lbl Saque exterior
	@FindBy(xpath = "//h1[contains(.,'Disponível para compra no exterior')]")
	public WebElement lblSaqueExterior;
	// Gestor Verifica valor utilizado exterior
	@FindBy(xpath = "//p[contains(.,'US$ 2.380,95')]")
	public WebElement dataValorUtilizadoexterior;
	// Gestor lbl valor utilizado saque exterior
	@FindBy(xpath = "//span[contains(.,'Valor Utilizado')]")
	public WebElement lblValorUtilizadoexterior;
	// Gestor Valor utilizado
	@FindBy(xpath = "//span[normalize-space()='(R$ 0,00)']")
	public WebElement dataVUtilizado;
	// Gestor lbl Total disponível exterior
	@FindBy(xpath = "//p[contains(.,'Total Disponível: R$ 12.000,00')]")
	public WebElement dataTotalDisponivelexterior;
	// Gestor lbl Limite Compras exterior
	@FindBy(xpath = "//span[2][contains(.,'Limite Compras')]")
	public WebElement lblLimiteTotalexterior;
	// Gestor data Limite Compras total exterior
	@FindBy(xpath = "//div[normalize-space()='US$ 2.380,95']")
	public WebElement dataLimiteTotalExterior;
	// Gestor Valor utilizado Compras exterior
	@FindBy(xpath = "//span[normalize-space()='(R$ 12.000,00)']")
	public WebElement dataLCompras;
	// Gestor lbl Saque internacional
	@FindBy(xpath = "//h1[contains(.,'Disponível para saque internacional')]")
	public WebElement lblSaqueInternacional;
	// Gestor Verifica valor utilizado internacional
	@FindBy(xpath = "//div[normalize-space()='US$ 2.119,05']")
	public WebElement dataValorUtilizadoInternacional;
	// Gestor lbl valor utilizado saque internacional
	@FindBy(xpath = "//span[normalize-space()='Valor Utilizado']")
	public WebElement lblValorUtilizadoInternacional;
	// Gestor Valor utilizado internacional
	@FindBy(xpath = "//span[normalize-space()='(R$ 0,00)']")
	public WebElement dataVInternacional;
	// Gestor lbl Total disponível internacional
	@FindBy(xpath = "//p[contains(.,'Total Disponível: R$ 10.680,00')]")
	public WebElement dataTotalDisponivelInternacional;
	// Gestor lbl Limite Total internacional
	@FindBy(xpath = "//span[normalize-space()='Limite Total']")
	public WebElement lblLimiteTotalinternacional;
	// Gestor data Limite Compras total internacional
	@FindBy(xpath = "//div[normalize-space()='US$ 2.119,05']")
	public WebElement dataLimiteTotalInternacional;
	// Gestor Valor utilizado Compras Internacional
	@FindBy(xpath = "//span[normalize-space()='(R$ 10.680,00)']")
	public WebElement dataLComprasInternacional;
	// Gestor Estrangeiro verifica Campo valor de limite do cartão Hibrido
	@FindBy(xpath = "//p[contains(.,'Total Disponível: R$ 20.000,00')]")
	public WebElement lblValorDisponivelPortadorHibrido;
	// Gestor Estrangeiro verifica Campo valor de Total Disponivel do cartão Hibrido
	// CFI
	@FindBy(xpath = "//p[contains(.,'Total Disponível: R$ 19.999,00')]")
	public WebElement lblValorDisponivelPortadorHibridoCFI;
	// Gestor Estrangeiro verifica Campo valor de Total Disponivel do cartão
	// Centralizado CFI
	@FindBy(xpath = "//p[contains(.,'Total Disponível: R$ 20.000,00')]")
	public WebElement lblValorDisponivelPortadorCentralizadoCFI;
	// Gestor Estrangeiro verifica Campo valor de limite em Inglês Portador
	// Centralizado CFI
	@FindBy(xpath = "//p[contains(.,'Total available: R$ 20.000,00')]")
	public WebElement lblValorDisponivelPortadorCentralizadoCFIIngles;
	// Gestor Estrangeiro verifica Campo valor de limite em Inglês Portador Hibrido
	@FindBy(xpath = "//p[contains(.,'Total available: R$ 20.000,00')]")
	public WebElement lblValorDisponivelPortadorHibridoIngles;
	// Gestor Estrangeiro verifica Campo valor de limite em Inglês Portador Hibrido
	// CFI
	@FindBy(xpath = "//p[contains(.,'Total available: R$ 19.999,00')]")
	public WebElement lblValorDisponivelPortadorHibridoCFIIngles;
	// Gestor Estrangeiro verifica Campo valor de limite do Portador Centralizado em
	// Inglês
	@FindBy(xpath = "//p[contains(.,'Total available: R$ 0,00')]")
	public WebElement lblValorDisponivelPortadorCentralizadoIngles;
	// Gestor Estrangeiro verifica Campo valor de limite do Portador Centralizado
	@FindBy(xpath = "//p[contains(.,'Total Disponível: R$ 10.000,00')]")
	public WebElement lblValorDisponivelPortadorCentralizado;
	// Gestor verifica Campo valor de limite do cartão Centralizado
	@FindBy(xpath = "//p[contains(.,'Total Disponível: R$ -194.400,31')]")
	public WebElement lblValorDisponivelCentralizado;
	// Gestor verifica Campo valor de limite Total Descrição
	@FindBy(xpath = "//span[normalize-space()='Limite Total']")
	public WebElement lblDescricaoValorTotal;
	// Gestor verifica Campo valor de limite Total do cartão Centralizado
	@FindBy(xpath = "//span[normalize-space()='R$ 2.000,00']")
	public WebElement lblValorTotalCentralizado;
	// Gestor verifica Campo valor de limite Total do cartão Hibrido
	@FindBy(xpath = "//span[normalize-space()='R$ 15.000,00']")
	public WebElement lblValorTotalHibrido;
	// Gestor verifica Campo valor de limite Total do cartão Centralizado CFI
	@FindBy(xpath = "//span[normalize-space()='R$ 12.000,00']")
	public WebElement lblValorTotalCentralizadoCFI;
	// Gestor verifica Campo valor de limite Total do cartão Centralizado Hibrido
	// CFI
	@FindBy(xpath = "//span[normalize-space()='R$ 20.000,00']")
	public WebElement lblValorTotalHibridoCFI;
	// Gestor verifica limites do portador
	@FindBy(xpath = "//span[normalize-space()='R$ 50.000,00']")
	public WebElement lbllimitesPortador;
	// Gestor verifica Campo valor de limite do cartão Hibrido
	@FindBy(xpath = "//p[contains(.,'Total Disponível: R$ 12.000,00')]")
	public WebElement lblValorDisponivelHibrido;
	// Gestor verifica Campo valor de limite do cartão Hibrido CFI
	@FindBy(xpath = "//p[contains(.,'Total Disponível: R$ 20.000,00')]")
	public WebElement lblValorDisponivelHibridoCFI;
	// Gestor verifica Campo valor de limite do cartão Centralizado CFI
	@FindBy(xpath = "//p[contains(.,'Total Disponível: R$ -138.407,20')]")
	public WebElement lblValorDisponivelCentralizadoCFI;
	// Gestor verifica valor total disponivel do portador
	@FindBy(xpath = "//p[contains(.,'Total Disponível: R$ 49.204,36')]")
	public WebElement lblValorDisponivelPortador;
	// Campo valor de limite do cartão Positivo
	@FindBy(xpath = "//p[contains(.,'Total Disponível: R$ 12.000,00')]")
	public WebElement lblValorTotalPostivivo;
	// Campo valor de limite do cartão
	@FindBy(xpath = "//p[contains(.,'Total Disponível: R$ 30.000,00')]")
	public WebElement lblValorTotalVisa;
	// Campo valor de limite do cartão
	@FindBy(xpath = "//span[contains(.,'R$ -2.000,00')]")
	public WebElement lblValorTotalNegativo;
	// Campo CPF NOME Portador
	@FindBy(xpath = "//input[@id='nomePortador']")
	public WebElement txtCpfNome;
	// Campo CPF NOME Portador Centralizado
	@FindBy(xpath = "//input[@id='nomePortador']")
	public WebElement txtCpfNomeCentralizado;
	// Campo CPF NOME Portador Hibrido
	@FindBy(xpath = "//input[@id='nomePortador']")
	public WebElement txtCpfNomeHibrido;
	// Campo CPF NOME Portador Centralizado CFI
	@FindBy(xpath = "//input[@id='nomePortador']")
	public WebElement txtCpfNomeCentralizadoCFI;
	// Campo CPF NOME Portador Hibrido CFI
	@FindBy(xpath = "//input[@id='nomePortador']")
	public WebElement txtCpfNomeHibridoCFI;
	// Campo CPF NOME Portador Centralizado
	@FindBy(xpath = "//input[@id='nomePortador']")
	public WebElement txtCPFCentralizado;
	// Campo nome do Portador - Local para inserir o nome
	@FindBy(id = "nomePortador")
	public WebElement txtPortador;
	// lbl titulo campo Portador Português
	@FindBy(xpath = "//label[contains(.,'Buscar por um portador')]")
	public WebElement lblBuscaPortador;
	// titulo campo Portador Inglês
	@FindBy(xpath = "//label[contains(.,'Search for a cardholder')]")
	public WebElement lblBuscaPortadorIngles;
	// Filtro Portadores Português
	@FindBy(xpath = "//b[contains(.,'Portadores')]")
	public WebElement lblPortadores;
	// Filtro Portadores Inglês
	@FindBy(xpath = "//b[contains(.,'Carholders')]")
	public WebElement lblPortadoresIngles;
	// Campo escolher portador diversos (incluido o hibrido consulta pelo
	// representante estrageiro)
	@FindBy(xpath = "//div[2]/div/div/div/div[3]")
	public WebElement lblEscolhaPortador;
	// Campo escolher portador como representante estrangeiro portador hibrido CFI
	@FindBy(xpath = "//div[2]/div/div/div/div[3]")
	public WebElement lblEscolhaPortadorHibridoCFI;
	// Campo escolher portador como representante estrangeiro portador Centralizado
	@FindBy(xpath = "//div[2]/div/div/div/div[3]")
	public WebElement lblEscolhaPortadorCentralizado;
	// Campo escolher portador como representante estrangeiro portador Centralizado
	// CFI
	@FindBy(xpath = "//div[2]/div/div/div/div[3]")
	public WebElement lblEscolhaPortadorCentralizadoCFI;
	// Campo escolher portador Amex
	@FindBy(xpath = "//div[2]/div/div/div/div[10]")
	public WebElement lblEscolhaPortadorAmex;
	// Campo escolher portador MasterCard
	@FindBy(xpath = "//div[2]/div/div/div/div[10]")
	public WebElement lblEscolhaPortadorMastercard;
	// Campo escolher portador Elo
	@FindBy(xpath = "//div[2]/div/div/div/div[7]")
	public WebElement lblEscolhaPortadorElo;
	// Campo valor de limite Total do Cartão
	@FindBy(xpath = "//p[contains(.,'Total Disponível: R$ 40.000,00')]")
	public WebElement lblValorTotal;
	// Campo verificação Titulo da tela
	@FindBy(xpath = "//h1[contains(.,'Consulta de limites')]")
	public WebElement tituladaTela;
	// Campo valor de limite Total do Cartão
	@FindBy(xpath = "//span[normalize-space()='Limite total R$ 0,00']")
	public WebElement lblLimiteTotal;
	// Campo verificação Número do Cartão Portador
	@FindBy(xpath = "//span[normalize-space()='Final 5934']")
	public WebElement dataCartaoPortador;
	// Campo verificação Nome do Cartão Portador
	@FindBy(xpath = "//span[normalize-space()='AMEX CORPORATE']")
	public WebElement dataNomedocartaoPortador;
	// Campo verificação Titulo Status Portador
	@FindBy(xpath = "//div[@data-test='DataItem_Label' and contains(., 'Status')]")
	public WebElement lblStatusPortador;
	// Campo verificação Data Status
	@FindBy(xpath = "//div[@data-test='DataItem_Data' and contains(., 'Ativo')]")
	public WebElement dataStatusPortador;
	// Campo verificação label bloqueio temporário
	@FindBy(xpath = "//div[@data-test='DataItem_Label' and contains(., 'Bloqueio temporário')]")
	public WebElement bloqueiotemporarioportador;
	// Campo verificação label melhor dia de compra
	@FindBy(xpath = "//div[@data-test='DataItem_Label' and contains(., 'Melhor dia de compra')]")
	public WebElement lbldiadecompra;
	// Campo verificação Data melhor dia de compra
	@FindBy(xpath = "//div[@data-test='DataItem_Data' and contains(., 'Dia 22')]")
	public WebElement datadiadecompra;
	// Campo verificação lbl Limite Disponivel
	@FindBy(xpath = "//span[normalize-space()='Limite disponível']")
	public WebElement lblLimiteDisponivel;
	// Campo verificação data Limite Disponivel
	@FindBy(xpath = "//span[normalize-space()='R$ 0,00']")
	public WebElement dataLimiteDisponivel;
	// Campo verificação lbl Limite Disponivel
	@FindBy(xpath = "//span[normalize-space()='Limite utilizado']")
	public WebElement lblLimiteUtilizado;
	// Campo verificação data Limite Utilizado
	@FindBy(xpath = "//span[normalize-space()='R$ 0,00']")
	public WebElement dataLimiteUtilizado;
	// Campo verificação lbl Compra nacional
	@FindBy(xpath = "//span[normalize-space()='Compra nacional']")
	public WebElement lblCompranacional;
	// Campo verificação data Compra nacional
	@FindBy(xpath = "//span[normalize-space()='R$ 0,00']")
	public WebElement dataCompranacional;
	// Campo valor de limite Total do Cartão nacional
	@FindBy(xpath = "//span[normalize-space()='Limite total R$ 0,00']")
	public WebElement dataLimiteTotalnacional;
	// Campo verificação lbl saque nacional
	@FindBy(xpath = "//span[normalize-space()='Saque nacional']")
	public WebElement lblSaquenacional;
	// Campo verificação data Saque nacional
	@FindBy(xpath = "//span[normalize-space()='R$ 0,00']")
	public WebElement dataSaquenacional;
	// Campo valor de data Saque Nacional do Cartão nacional
	@FindBy(xpath = "//span[normalize-space()='Limite total R$ 0,00']")
	public WebElement dataLimiteTotalNacionalPortador;
	// Campo verificação lbl Compra no exterior
	@FindBy(xpath = "//span[normalize-space()='Compra no exterior']")
	public WebElement lblCompraexterior;
	// Campo verificação data Compra no exterior
	@FindBy(xpath = "//span[normalize-space()='US$ 0,00 (R$ 0,00)']")
	public WebElement dataCompraexterior;
	// Campo valor de data Compra Exterior
	@FindBy(xpath = "//span[normalize-space()='Limite total US$ 0,00(R$ 0,00)']")
	public WebElement dataCompraexteriornacional;
	// Campo verificação lbl saque no exterior
	@FindBy(xpath = "//span[normalize-space()='Saque no exterior']")
	public WebElement lblSaqueexterior;
	// Campo verificação data Saque no exterior
	@FindBy(xpath = "//span[normalize-space()='US$ 0,00 (R$ 0,00)']")
	public WebElement dataSaqueexterior;
	// Campo valor de data Limite no Exterior
	@FindBy(xpath = "//span[normalize-space()='Limite total US$ 0,00(R$ 0,00)']")
	public WebElement datalimitetotalexterior;
	// Campo verificação Nome Consulta de Limites Representante Estrangeiro
	@FindBy(xpath = "//h1[contains(.,'Consulta de Limite')]")
	public WebElement lblconsutalimiteRE;
	// Campo verificação Subtitulo Detalhes do Portador Representante Estrangeiro
	@FindBy(xpath = "//h1[contains(.,'Detalhes do Portador')]")
	public WebElement lbldetalhesportadorRE;
	// Campo verificação Nome do Cartão Representante Estrangeiro
	@FindBy(xpath = "//h1[contains(.,'AMEX CONTA EBTA')]")
	public WebElement dataNomedocartaoRE;
	// Campo verificação CompID Representante Estrangeiro
	@FindBy(xpath = "//p[contains(.,'605313')]")
	public WebElement dataCompidRE;
	// Campo verificação Titulo Status Representante Estrangeiro
	@FindBy(xpath = "//div[@data-test='DataItem_Label' and contains(., 'Status')]")
	public WebElement lblStatusRE;
	// Campo verificação Data Status Representante Estrangeiro Bloqueado
	@FindBy(xpath = "//div[@data-test='DataItem_Data' and contains(., 'Bloqueado' )]")
	public WebElement dataStatusRE;
	// Campo verificação Titulo Vencimento Representante Estrangeiro
	@FindBy(xpath = "//div[@data-test='DataItem_Label' and contains(., 'Vencimento')]")
	public WebElement lblVencimentoRE;
	// Campo verificação Data Vencimento Representante Estrangeiro
	@FindBy(xpath = "//div[@data-test='DataItem_Data' and contains(., 'Dia 25')]")
	public WebElement dataVencimentoRE;
	// Campo verificação Titulo Nome do Portador Representante Estrangeiro
	@FindBy(xpath = "//div[@data-test='DataItem_Label' and contains(., 'Nome do portador')]")
	public WebElement lblNomedoportadorRE;
	// Campo verificação Nome do Portador Representante Estrangeiro
	@FindBy(xpath = "//div[@data-test='DataItem_Data' and contains(., 'PORT ARQ DOIS')]")
	public WebElement dataNomedoportadorRE;
	// Campo verificação Titulo Empresa Representante Estrangeiro
	@FindBy(xpath = "//div[@data-test='DataItem_Label' and contains(., 'Empresa')]")
	public WebElement lblNomedaempresaRE;
	// Campo verificação Nome da Empresa Representante Estrangeiro
	@FindBy(xpath = "//div[@data-test='DataItem_Data' and contains(., 'ARQUIVO DOIS')]")
	public WebElement dataNomedaempresaRE;
	// Campo verificação CNPJ Representante Estrangeiro
	@FindBy(xpath = "//div[@data-test='DataItem_Info' and contains(., '75.808.590/0001-82')]")
	public WebElement dataCNPJRE;
	// Campo verificação Título Número do Cartão Representante Estrangeiro
	@FindBy(xpath = "//div[@data-test='DataItem_Label' and contains(., 'Número do cartão')]")
	public WebElement lblNCartaoRE;
	// Campo verificação Número do Cartão Representante Estrangeiro
	@FindBy(xpath = "//div[@data-test='DataItem_Data' and contains(., '**** **** **** 9341')]")
	public WebElement dataNCartaoRE;
	// Campo verificação Título Contato Representante Estrangeiro
	@FindBy(xpath = "//div[@data-test='DataItem_Label' and contains(., 'Contato')]")
	public WebElement lblContatoRE;
	// Campo verificação Data Contato Representante Estrangeiro
	@FindBy(xpath = "//div[@data-test='DataItem_Data' and contains(., '(011)  9 6633-4466')]")
	public WebElement dataContatoRE;
	// Gestor titulo Disponivel para compra Representante Estrangeiro
	@FindBy(xpath = "//h1[contains(.,'Disponível para compra')]")
	public WebElement lbldisponivelcompraRE;
	// Gestor Verifica valor utilizado Representante Estrangeiro
	@FindBy(xpath = "//span[normalize-space()='R$ 0,00']")
	public WebElement dataValorUtilizadoRE;
	// Gestor lbl valor utilizado Representante Estrangeiro
	@FindBy(xpath = "//span[contains(.,'Valor Utilizado')]")
	public WebElement lblValorUtilizadoRE;
	// Gestor lbl Total disponível Representante Estrangeiro
	@FindBy(xpath = "//p[contains(.,'Total Disponível: R$ 0,00')]")
	public WebElement dataTotalDisponivelRE;
	// Gestor lbl Limite Compras Representante Estrangeiro
	@FindBy(xpath = "//span[contains(.,'Limite Compras')]")
	public WebElement lblLimiteComprasRE;
	// Gestor data Limite Compras Representante Estrangeiro
	@FindBy(xpath = "//span[contains(.,'R$ 0,00')]")
	public WebElement dataLimiteComprasRE;
	// Gestor lbl Saque Nacional Representante Estrangeiro
	@FindBy(xpath = "//h1[contains(.,'Disponível para saque nacional')]")
	public WebElement lblSaqueNacionalRE;
	// Gestor Verifica valor utilizado nacional Representante Estrangeiro
	@FindBy(xpath = "//span[contains(.,'R$ 0,00')]")
	public WebElement dataValorUtilizadonacionalRE;
	// Gestor lbl valor utilizado saque nacional Representante Estrangeiro
	@FindBy(xpath = "//span[contains(.,'Valor Utilizado')]")
	public WebElement lblValorUtilizadonacionalRE;
	// Gestor lbl Total disponível nacional Representante Estrangeiro
	@FindBy(xpath = "//p[contains(.,'Total Disponível: R$ 0,00')]")
	public WebElement dataTotalDisponivelnacionalRE;
	// Gestor lbl Limite Total nacional Representante Estrangeiro
	@FindBy(xpath = "//span[contains(.,'Limite Total')]")
	public WebElement lblLimiteTotalNacionalRE;
	// Gestor data Limite Compras total nacional Representante Estrangeiro
	@FindBy(xpath = "//span[contains(.,'R$ 0,00')]")
	public WebElement dataLimiteTotalNacionalRE;
	// Gestor lbl Saque exterior Representante Estrangeiro
	@FindBy(xpath = "//h1[contains(.,'Disponível para compra no exterior')]")
	public WebElement lblSaqueExteriorRE;
	// Gestor Verifica valor utilizado exterior Representante Estrangeiro
	@FindBy(xpath = "//div[normalize-space()='US$ 0,00']")
	public WebElement dataValorUtilizadoexteriorRE;
	// Gestor lbl valor utilizado saque exterior Representante Estrangeiro
	@FindBy(xpath = "//span[contains(.,'Valor Utilizado')]")
	public WebElement lblValorUtilizadoexteriorRE;
	// Gestor Valor utilizado Representante Estrangeiro
	@FindBy(xpath = "//span[normalize-space()='(R$ 0,00)']")
	public WebElement dataVUtilizadoRE;
	// Gestor lbl Total disponível exterior Representante Estrangeiro
	@FindBy(xpath = "//p[contains(.,'Total Disponível: R$ 0,00')]")
	public WebElement dataTotalDisponivelexteriorRE;
	// Gestor lbl Limite Compras exterior Representante Estrangeiro
	@FindBy(xpath = "//span[2][contains(.,'Limite Compras')]")
	public WebElement lblLimiteTotalexteriorRE;
	// Gestor data Limite Compras total exterior Representante Estrangeiro
	@FindBy(xpath = "//div[normalize-space()='US$ 0,00']")
	public WebElement dataLimiteTotalExteriorRE;
	// Gestor Valor utilizado Compras exterior Representante Estrangeiro
	@FindBy(xpath = "//span[normalize-space()='(R$ 0,00)']")
	public WebElement dataLComprasRE;
	// Gestor lbl Saque internacional Representante Estrangeiro
	@FindBy(xpath = "//h1[contains(.,'Disponível para saque internacional')]")
	public WebElement lblSaqueInternacionalRE;
	// Gestor Verifica valor utilizado internacional Representante Estrangeiro
	@FindBy(xpath = "//div[normalize-space()='US$ 0,00']")
	public WebElement dataValorUtilizadoInternacionalRE;
	// Gestor lbl valor utilizado saque internacional Representante Estrangeiro
	@FindBy(xpath = "//span[normalize-space()='Valor Utilizado']")
	public WebElement lblValorUtilizadoInternacionalRE;
	// Gestor Valor utilizado internacional Representante Estrangeiro
	@FindBy(xpath = "//span[normalize-space()='(R$ 0,00)']")
	public WebElement dataVInternacionalRE;
	// Gestor lbl Total disponível internacional Representante Estrangeiro
	@FindBy(xpath = "//p[contains(.,'Total Disponível: R$ 0,00')]")
	public WebElement dataTotalDisponivelInternacionalRE;
	// Gestor lbl Limite Total internacional Representante Estrangeiro
	@FindBy(xpath = "//span[normalize-space()='Limite Total']")
	public WebElement lblLimiteTotalinternacionalRE;
	// Gestor data Limite Compras total internacional Representante Estrangeiro
	@FindBy(xpath = "//div[normalize-space()='US$ 0,00']")
	public WebElement dataLimiteTotalInternacionalRE;
	// Gestor Valor utilizado Compras Internacional Representante Estrangeiro
	@FindBy(xpath = "//span[normalize-space()='(R$ 0,00)']")
	public WebElement dataLComprasInternacionalRE;
	// Gestor Valor Total Representante Estrangeiro
	@FindBy(xpath = "//p[contains(.,'Total Disponível: R$ 0,00')]")
	public WebElement lbltotaldisponivelRE;
	// Gestor Valor Total Utilizado Representante Estrangeiro
	@FindBy(xpath = "//span[normalize-space()='R$ 0,00']")
	public WebElement datatotalutilizadoRE;
	// Gestor Escrita Limite Total Representante Estrangeiro
	@FindBy(xpath = "//span[normalize-space()='Limite Total']")
	public WebElement lbllimiteTotalRE;
	// Gestor Limite Total Utilizado Representante Estrangeiro
	@FindBy(xpath = "//span[normalize-space()='R$ 0,00']")
	public WebElement datalimiteutilizadoRE;
	// Gestor Escrita Limite Total Representante Estrangeiro
	@FindBy(xpath = "//span[normalize-space()='Valor Utilizado']")
	public WebElement lblvalorutilizadoRE;
	// Campo verificação Nome Consulta de Limites Representante Estrangeiro em Inglês
	@FindBy(xpath = "//h1[contains(.,'Limit Consultation')]")
	public WebElement lblenconsutalimiteRE;
	// Campo verificação Subtitulo Detalhes do Portador Representante Estrangeiro em Inglês
	@FindBy(xpath = "//h1[contains(.,'Cardholder')]")
	public WebElement lblendetalhesportadorRE;
	// Campo verificação Nome do Cartão Representante Estrangeiro em Inglês
	@FindBy(xpath = "//h1[contains(.,'AMEX CONTA EBTA')]")
	public WebElement dataenNomedocartaoRE;
	// Campo verificação CompID Representante Estrangeiro em Inglês
	@FindBy(xpath = "//p[contains(.,'605313')]")
	public WebElement dataenCompidRE;
	// Campo verificação Titulo Status Representante Estrangeiro em Inglês
	@FindBy(xpath = "//div[@data-test='DataItem_Label' and contains(., 'Status')]")
	public WebElement lblenStatusRE;
	// Campo verificação Data Status Representante Estrangeiro Bloqueado em Inglês
	@FindBy(xpath = "//div[@data-test='DataItem_Data' and contains(., 'Block' )]")
	public WebElement dataenStatusRE;
	// Campo verificação Titulo Vencimento Representante Estrangeiro em Inglês
	@FindBy(xpath = "//div[@data-test='DataItem_Label' and contains(., 'Due date')]")
	public WebElement lblenVencimentoRE;
	// Campo verificação Data Vencimento Representante Estrangeiro em Inglês
	@FindBy(xpath = "//div[@data-test='DataItem_Data' and contains(., 'Day 25')]")
	public WebElement dataenVencimentoRE;
	// Campo verificação Titulo Nome do Portador Representante Estrangeiro em Inglês
	@FindBy(xpath = "//div[@data-test='DataItem_Label' and contains(., 'Cardholder')]")
	public WebElement lblenNomedoportadorRE;
	// Campo verificação Nome do Portador Representante Estrangeiro em Inglês
	@FindBy(xpath = "//div[@data-test='DataItem_Data' and contains(., 'PORT ARQ DOIS')]")
	public WebElement dataenNomedoportadorRE;
	// Campo verificação Titulo Empresa Representante Estrangeiro em Inglês
	@FindBy(xpath = "//div[@data-test='DataItem_Label' and contains(., 'Company')]")
	public WebElement lblenNomedaempresaRE;
	// Campo verificação Nome da Empresa Representante Estrangeiro em Inglês
	@FindBy(xpath = "//div[@data-test='DataItem_Data' and contains(., 'ARQUIVO DOIS')]")
	public WebElement dataenNomedaempresaRE;
	// Campo verificação CNPJ Representante Estrangeiro em Inglês
	@FindBy(xpath = "//div[@data-test='DataItem_Info' and contains(., '75.808.590/0001-82')]")
	public WebElement dataenCNPJRE;
	// Campo verificação Título Número do Cartão Representante Estrangeiro em Inglês
	@FindBy(xpath = "//div[@data-test='DataItem_Label' and contains(., 'Card number')]")
	public WebElement lblenNCartaoRE;
	// Campo verificação Número do Cartão Representante Estrangeiro em Inglês
	@FindBy(xpath = "//div[@data-test='DataItem_Data' and contains(., '**** **** **** 9341')]")
	public WebElement dataenNCartaoRE;
	// Campo verificação Título Contato Representante Estrangeiro em Inglês
	@FindBy(xpath = "//div[@data-test='DataItem_Label' and contains(., 'Contact')]")
	public WebElement lblenContatoRE;
	// Campo verificação Data Contato Representante Estrangeiro em Inglês
	@FindBy(xpath = "//div[@data-test='DataItem_Data' and contains(., '(011)  9 6633-4466')]")
	public WebElement dataenContatoRE;
	// Gestor titulo Disponivel para compra Representante Estrangeiro em Inglês
	@FindBy(xpath = "//h1[contains(.,'Available for purchase')]")
	public WebElement lblendisponivelcompraRE;
	// Gestor Verifica valor utilizado Representante Estrangeiro em Inglês
	@FindBy(xpath = "//span[normalize-space()='R$ 0,00']")
	public WebElement dataenValorUtilizadoRE;
	// Gestor lbl valor utilizado Representante Estrangeiro em Inglês
	@FindBy(xpath = "//span[contains(.,'Amount used')]")
	public WebElement lblenValorUtilizadoRE;
	// Gestor lbl Total disponível Representante Estrangeiro em Inglês
	@FindBy(xpath = "//p[contains(.,'Total available: R$ 0,00')]")
	public WebElement dataenTotalDisponivelRE;
	// Gestor lbl Limite Compras Representante Estrangeiro em Inglês
	@FindBy(xpath = "//span[contains(.,'Purchase Limit')]")
	public WebElement lblenLimiteComprasRE;
	// Gestor data Limite Compras Representante Estrangeiro em Inglês
	@FindBy(xpath = "//span[contains(.,'R$ 0,00')]")
	public WebElement dataenLimiteComprasRE;
	// Gestor lbl Saque Nacional Representante Estrangeiro em Inglês
	@FindBy(xpath = "//h1[contains(.,'Available for withdrawal')]")
	public WebElement lblenSaqueNacionalRE;
	// Gestor Verifica valor utilizado nacional Representante Estrangeiro em Inglês
	@FindBy(xpath = "//span[contains(.,'R$ 0,00')]")
	public WebElement dataenValorUtilizadonacionalRE;
	// Gestor lbl valor utilizado saque nacional Representante Estrangeiro em Inglês
	@FindBy(xpath = "//span[contains(.,'Used')]")
	public WebElement lblenValorUtilizadonacionalRE;
	// Gestor lbl Total disponível nacional Representante Estrangeiro em Inglês
	@FindBy(xpath = "//p[contains(.,'Total available: R$ 0,00')]")
	public WebElement dataenTotalDisponivelnacionalRE;
	// Gestor lbl Limite Total nacional Representante Estrangeiro em Inglês
	@FindBy(xpath = "//span[normalize-space()='Limit']")
	public WebElement lblenLimiteTotalNacionalRE;
	// Gestor data Limite Compras total nacional Representante Estrangeiro em Inglês
	@FindBy(xpath = "//span[contains(.,'R$ 0,00')]")
	public WebElement dataenLimiteTotalNacionalRE;
	// Gestor lbl Saque exterior Representante Estrangeiro em Inglês
	@FindBy(xpath = "//h1[contains(.,'Available for purchase abroad')]")
	public WebElement lblenSaqueExteriorRE;
	// Gestor Verifica valor utilizado exterior Representante Estrangeiro em Inglês
	@FindBy(xpath = "//div[normalize-space()='USD 0,00']")
	public WebElement dataenValorUtilizadoexteriorRE;
	// Gestor lbl valor utilizado saque exterior Representante Estrangeiro em Inglês
	@FindBy(xpath = "//span[contains(.,'Amount used')]")
	public WebElement lblenValorUtilizadoexteriorRE;
	// Gestor Valor utilizado Representante Estrangeiro em Inglês
	@FindBy(xpath = "//span[normalize-space()='(R$ 0,00)']")
	public WebElement dataenVUtilizadoRE;
	// Gestor lbl Total disponível exterior Representante Estrangeiro em Inglês
	@FindBy(xpath = "//p[contains(.,'Total available: R$ 0,00')]")
	public WebElement dataenTotalDisponivelexteriorRE;
	// Gestor lbl Limite Compras exterior Representante Estrangeiro em Inglês
	@FindBy(xpath = "//span[2][contains(.,'Purchase Limit')]")
	public WebElement lblenLimiteTotalexteriorRE;
	// Gestor data Limite Compras total exterior Representante Estrangeiro em Inglês
	@FindBy(xpath = "//div[normalize-space()='USD 0,00']")
	public WebElement dataenLimiteTotalExteriorRE;
	// Gestor Valor utilizado Compras exterior Representante Estrangeiro em Inglês
	@FindBy(xpath = "//span[normalize-space()='(R$ 0,00)']")
	public WebElement dataenLComprasRE;
	// Gestor lbl Saque internacional Representante Estrangeiro em Inglês
	@FindBy(xpath = "//h1[contains(.,'Available for withdrawal abroad')]")
	public WebElement lblenSaqueInternacionalRE;
	// Gestor Verifica valor utilizado internacional Representante Estrangeiro em Inglês
	@FindBy(xpath = "//div[normalize-space()='USD 0,00']")
	public WebElement dataenValorUtilizadoInternacionalRE;
	// Gestor lbl valor utilizado saque internacional Representante Estrangeiro em Inglês
	@FindBy(xpath = "//span[normalize-space()='Used']")
	public WebElement lblenValorUtilizadoInternacionalRE;
	// Gestor Valor utilizado internacional Representante Estrangeiro em Inglês
	@FindBy(xpath = "//span[normalize-space()='(R$ 0,00)']")
	public WebElement dataenVInternacionalRE;
	// Gestor lbl Total disponível internacional Representante Estrangeiro em Inglês
	@FindBy(xpath = "//p[contains(.,'Total available: R$ 0,00')]")
	public WebElement dataenTotalDisponivelInternacionalRE;
	// Gestor lbl Limite Total internacional Representante Estrangeiro em Inglês
	@FindBy(xpath = "//span[normalize-space()='Limit']")
	public WebElement lblenLimiteTotalinternacionalRE;
	// Gestor data Limite Compras total internacional Representante Estrangeiro em Inglês
	@FindBy(xpath = "//div[normalize-space()='USD 0,00']")
	public WebElement dataenLimiteTotalInternacionalRE;
	// Gestor Valor utilizado Compras Internacional Representante Estrangeiro em Inglês
	@FindBy(xpath = "//span[normalize-space()='(R$ 0,00)']")
	public WebElement dataenLComprasInternacionalRE;
	// Gestor Valor Total Representante Estrangeiro em Inglês
	@FindBy(xpath = "//p[contains(.,'Total available: R$ 0,00')]")
	public WebElement lblentotaldisponivelRE;
	// Gestor Valor Total Utilizado Representante Estrangeiro em Inglês
	@FindBy(xpath = "//span[normalize-space()='R$ 0,00']")
	public WebElement dataentotalutilizadoRE;
	// Gestor Escrita Limite Total Representante Estrangeiro em Inglês
	@FindBy(xpath = "//span[normalize-space()='Total limit']")
	public WebElement lblenlimiteTotalRE;
	// Gestor Limite Total Utilizado Representante Estrangeiro em Inglês
	@FindBy(xpath = "//span[normalize-space()='R$ 0,00']")
	public WebElement dataenlimiteutilizadoRE;
	// Gestor Escrita Limite Total Representante Estrangeiro em Inglês
	@FindBy(xpath = "//span[normalize-space()='Amount used']")
	public WebElement lblenvalorutilizadoRE;
	// Aba Portador mensagem de nega��o quando houver comna
	@FindBy(xpath = "//p[contains(text(), 'Please confirm that the cardholder') and (s name or CPF was typed correctly or check the search filtes.')]")
	public WebElement msgPortadorErroEng;

	public void clicarTxtPortador() {
		txtPortador.click();
	}

	public void clicarTxtCpfNome() {
		txtCpfNome.click();
	}

	public void informarTxtCnpjEmpresa(String nomeCpfPortador) {
		txtCpfNome.sendKeys(nomeCpfPortador);
	}

	public void informarTxtNomePortadorCentralizado(String nomeCpfPortadorCentralizado) {
		txtCpfNomeCentralizado.sendKeys(nomeCpfPortadorCentralizado);
	}

	public void informarTxtNomePortadorCentralizadoCFI(String nomeCpfPortadorCentralizadoCFI) {
		txtCpfNomeCentralizadoCFI.sendKeys(nomeCpfPortadorCentralizadoCFI);
	}

	public void informarTxtNomePortadorHibridoCFI(String nomeCpfPortadorHibridoCFI) {
		txtCpfNomeHibridoCFI.sendKeys(nomeCpfPortadorHibridoCFI);
	}

	public void reNomePortadorCentralizadoI(String escolherPortadorCentralizado) {
		txtCPFCentralizado.sendKeys(escolherPortadorCentralizado);
	}

	public void digitarCpfInexistente(String cpfBusca) {
		txtCpfNome.sendKeys(cpfBusca);
	}

	public void clicarMenuPortadores() {
		lblPortadores.click();
	}

	public void clicarMenuPortadoresIngles() {
		lblPortadoresIngles.click();
	}

	public void clicarEscolhaPortador() {
		lblEscolhaPortador.click();
	}

	public void clicarEscolhaPortadorHibridoCFIRE() {
		lblEscolhaPortadorHibridoCFI.click();
	}

	public void clicarEscolhaPortadorHibridoCFI() {
		lblEscolhaPortadorHibridoCFI.click();
	}

	public void clicarEscolhaPortadorCentralizado() {
		lblEscolhaPortadorCentralizado.click();
	}

	public void clicarEscolhaPortadorCentralizadoCFI() {
		lblEscolhaPortadorCentralizadoCFI.click();
	}

	public void clicarEscolhaPortadorElo() {
		lblEscolhaPortadorElo.click();
	}

	public void clicarEscolhaPortadorAmex() {
		lblEscolhaPortadorAmex.click();
	}

	public void clicarEscolhaPortadorMastercard() {
		lblEscolhaPortadorMastercard.click();
	}

	public void msgLblValorTotal() {
		lblValorTotal.click();
	}

}
