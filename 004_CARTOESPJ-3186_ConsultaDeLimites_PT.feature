#encoding: Cp1252
##################################################
########## CENARIOS limites PORTAL PJ ##################
##################################################
@004_CARTOESPJ-3186_ConsultarLimites
Feature: CARTOESPJ-3186 Consultar Limites portal PJ

  @ConsultarLimite_PortadorVerificarConsultaDeLimitePositivo
  Scenario Outline: CARTOESPJ - 3193 - Portador - Verificar Consulta De Limite Positivo
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico Consulta De Limite Positivo Portador
    And Verifico o Logout com sucesso

    Examples:
      | cpf          					 | senha     			 | ambiente | rede      |
      | "45292163228" | "Br@12345" | "TI"     | "empresa" |

  @ConsultarLimite_PortadorVerificarConsultaDeLimiteNegativo
  Scenario Outline: CARTOESPJ - 3234 - Portador - Verificar Consulta De Limite Negativo
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico Consulta De Limite Negativo Portador
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      |
      | "26689115755" | "Br@12345" | "TI"     | "empresa" |

  @ConsultarLimite_PortadorVerificarConsultaDeLimiteTotalPortadorHibrido
  Scenario Outline: CARTOESPJ - 3186 - Portador - Verificar Consulta De Limite Total Portador Hibrido
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico Consulta De Limite total Portador Hibrido
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      |
      | "34327744000" | "Br@12345" | "TI"     | "empresa" |

  @ConsultarLimite_PortadorVerificarConsultaDeLimiteTotalPortadorHibridoCFI
  Scenario Outline: CARTOESPJ - 3193 - Portador - Verificar Consulta De Limite Total do Portador Hibrido CFI
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico Consulta De Limite total Portador Hibrido CFI
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      |
      | "12446143350" | "Br@12345" | "TI"     | "empresa" |

  @ConsultarLimite_PortadorVerificarConsultaDeLimiteTotalPortadorCentralizado
  Scenario Outline: CARTOESPJ - 3186 - Portador - Verificar Consulta De Limite Total do Portador Centralizado
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico Consulta De Limite total disponivel Portador Centralizado
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      |
      | "59297120008" | "Br@12345" | "TI"     | "empresa" |

  @ConsultarLimite_PortadorVerificarConsultaDeLimiteTotalPortadorCentralizadoCFI
  Scenario Outline: CARTOESPJ - 3193 - Portador - Verificar Consulta De Limite Total do Portador Centralizado CFI
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico Consulta De Limite total Portador Centralizado CFI
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      |
      | "12446143350" | "Br@12345" | "TI"     | "empresa" |

  @ConsultarLimite_PortadorVerificarConsultaDeLimiteDisponivelPortadorHibrido
  Scenario Outline: CARTOESPJ - 3186 - Portador - Verificar Consulta De Limite Disponivel do Portador Hibrido
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico Consulta De Limite disponivel Portador Hibrido
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      |
      | "34327744000" | "Br@12345" | "TI"     | "empresa" |

  @ConsultarLimite_PortadorVerificarConsultaDeLimiteDisponivelPortadorHibridoCFI
  Scenario Outline: CARTOESPJ - 3186 - Portador - Verificar Consulta De Limite Disponivel do Portador Hibrido CFI
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico Consulta De Limite disponivel Portador Hibrido CFI
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      |
      | "12446143350" | "Br@12345" | "TI"     | "empresa" |

  @ConsultarLimite_PortadorVerificarConsultaDeLimiteDisponivelPortadorCentralizado
  Scenario Outline: CARTOESPJ - 3186 - Portador - Verificar Consulta De Limite Disponivel do Portador Centralizado
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico Consulta De Limite total disponivel Portador Centralizado
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      |
      | "59297120008" | "Br@12345" | "TI"     | "empresa" |

  @ConsultarLimite_PortadorVerificarConsultaDeLimiteDisponivelPortadorCentralizadoCFI
  Scenario Outline: CARTOESPJ - 3186 - Portador - Verificar Consulta De Limite Disponivel do Portador Centralizado CFI
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico Consulta De Limite Portador disponivel Centralizado CFI
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      |
      | "10782133134" | "Br@12345" | "TI"     | "empresa" |

  @ConsultarLimite_GestorVerificarConsultaLimitePositivodoPortador
  Scenario Outline: CARTOESPJ - 3240 - Gestor - Verificar Consulta Limite Positivo do Portador
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico Consulta De Limite Positivo
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      |
      | "25358287078" | "Br@12345" | "TI"     | "empresa" |

  @ConsultarLimite_GestorVerificarConsultaLimiteNegativodoPortador
  Scenario Outline: CARTOESPJ - 3241 - Gestor - Verificar Consulta Limite Negativo do Portador
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico Consulta De Limite Negativo
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      |
      | "00002397390" | "Br@12345" | "TI"     | "empresa" |

  @ConsultarLimite_GestorVerificaConsultaLimiteDisponiveldoPortadorCentralizado
  Scenario Outline: CARTOESPJ - 3186 - Gestor - Verificar Consulta Limite Disponivel do Portador Centralizado
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico Consulta De Limite Disponivel Portador Centralizado <nomeCpfPortadorCentralizado>
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      |nomeCpfPortadorCentralizado|
      | "06218670635" | "Br@12345" | "TH"     | "empresa" |"01461080606"|

  @ConsultarLimite_GestorVerificaConsultaLimiteDisponiveldoPortadorHibrido
  Scenario Outline: CARTOESPJ - 3186 - Gestor - Verificar Consulta Limite Disponivel do Portador Hibrido
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico Consulta De Limite Disponivel Portador Hibrido <nomePortadorHibrido>
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      | nomePortadorHibrido |
      | "06218670635" | "Br@12345" | "TI"     | "empresa" | "45291270446"       |


  @ConsultarLimite_GestorVerificaConsultaLimiteDisponiveldoPortadorHibridoCFI
  Scenario Outline: CARTOESPJ - 3186 - Gestor - Verificar Consulta Limite Disponivel do Portador Hibrido CFI
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico Consulta De Limite Disponivel Portador Hibrido CFI <nomeCpfPortadorHibridoCFI>
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      | nomeCpfPortadorHibridoCFI |
      | "06218670635" | "Br@12345" | "TI"     | "empresa" | "12446143350"             |

  @ConsultarLimite_GestorVerificaConsultaLimiteDisponiveldoPortadorCentralizadoCFI
  Scenario Outline: CARTOESPJ - 3186 - Gestor - Verificar Consulta Limite Disponivel do Portador Centralizado CFI
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico Consulta De Limite Disponivel Portador Centralizado CFI <nomeCpfPortadorCentralizadoCFI>
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      								| nomeCpfPortadorCentralizadoCFI | ambiente | rede      |
      | "34589936909" | "Br@12345" | "26690202402"                  									| "TI"     | "empresa" |

	@ConsultarLimite_GestorVerificaConsultaLimiteTotaldoPortadorCentralizado
  Scenario Outline: CARTOESPJ - 3186 - Gestor - Verificar Consulta Limite Disponivel do Portador Centralizado CFI
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico Consulta De Limite Total Portador Centralizado <nomeCpfPortadorCentralizado>
    And Verifico o Logout com sucesso

    Examples:
      | cpf          						 | senha     			 | ambiente | rede     				 |nomeCpfPortadorCentralizado|
      | "06218670635" | "Br@12345" | "TI"    				 | "empresa" |"01461080606"|
      
  @ConsultarLimite_GestorVerificaConsultaLimiteTotaldoPortadorHibrido
  Scenario Outline: CARTOESPJ - 3186 - Gestor - Verificar Consulta Limite Disponivel do Portador Centralizado CFI
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico Consulta De Limite Total Portador Hibrido <nomePortadorHibrido>
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      | nomePortadorHibrido |
      | "06218670635" | "Br@12345" | "TI"     | "empresa" | "45291270446"       |
      
  @ConsultarLimite_GestorVerificaConsultaLimiteTotaldoPortadorHibridoCFI
  Scenario Outline: CARTOESPJ - 3186 - Gestor - Verificar Consulta Limite Disponivel do Portador Hibrido CFI
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
   	And Verifico Consulta De Limite Total Portador Hibrido CFI <nomeCpfPortadorHibridoCFI>
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      | nomeCpfPortadorHibridoCFI |
      | "06218670635" | "Br@12345" | "TI"     | "empresa" | "12446143350"             |    
      
  @ConsultarLimite_GestorVerificaConsultaLimiteTotaldoPortadorCentralizadoCFI
  Scenario Outline: CARTOESPJ - 3186 - Gestor - Verificar Consulta Limite Disponivel do Portador Hibrido CFI
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico Consulta De Limite Total Portador Centralizado CFI <nomeCpfPortadorCentralizadoCFI>
    And Verifico o Logout com sucesso

    Examples:
      | cpf           					| senha     			  | nomeCpfPortadorCentralizadoCFI | ambiente | rede      |
      | "34589936909" | "Br@12345" | 							"26690202402"                 		 | 		"TI"    		| "empresa" |
      
   @ConsultarLimite_GestorVerificaDataeHoraCorretaConsultadeLimites
  	Scenario Outline: CARTOESPJ - 3186 - Gestor - Verificar data e hora correta da tela Consulta de Limites
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico a data e horario correto da ultima atualizacao como Gestor da tela Consulta de Limites
    And Verifico o Logout com sucesso

    Examples:
      | cpf           					| senha     			  |  ambiente | rede      			 |
      | "62755113030" | "Br@12345" | 		"TH"    		 | "empresa" |     
      
    @ConsultarLimite_GestorVerificaDataeHoraCorretaConsultadeLimitesdateladelimitesdoPortador
  	Scenario Outline: CARTOESPJ - 3186 - Gestor - Verificar data e hora correta da tela Consulta de Limites do Portador
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico a data e horario correto da ultima atualizacao como Gestor da tela Consulta de Limites
    And Verifico a data e horario correto da ultima atualizacao do Portador como Gestor
    And Verifico o Logout com sucesso

    Examples:
      | cpf           					| senha     			  |  ambiente | rede      |
      | "62755113030" | "Br@12345" | 		"TI"    		| "empresa" |   
  
  @ConsultarLimite_GestorVerificaLimitesDoPortador
  Scenario Outline: CARTOESPJ - 3186 - Gestor - Verifico Limites do Portador Selecionado
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico Limites do portador
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha       | ambiente | rede      |
      | "71741745829" | "Br@12345" 	| "TI"     | "empresa" |        

  @ConsultarLimite_GestorBuscarPortadoresPorNome
  Scenario Outline: CARTOESPJ - 3242 - Gestor - Buscar portadores por Nome
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico a busca de portadores por Nome <nomeCpfPortador>
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | nomeCpfPortador | ambiente | rede      |
      | "25358287078" | "Br@12345" | "PORTADOR PJ"   | "TI"     | "empresa" |

  @ConsultarLimite_GestorBuscarPortadoresPorNomeDoProduto
  Scenario Outline: CARTOESPJ - 3243 - Gestor - Buscar empresas por Nome do Produto
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico a busca de portadores por Nome do Produto <nomeProduto>
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | nomeProduto                 | ambiente | rede      |
      | "25358287078" | "Br@12345" | "EMPRESARIAL PLATINUM VISA" | "TI"     | "empresa" |

  @ConsultarLimite_GestorBuscarPortadoresPorNomeDaEmpresa
  Scenario Outline: CARTOESPJ - 3244 - Gestor - Buscar empresas por Nome da Empresa
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico a busca de portadores por Nome da Empresa <nomeEmpresa>
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | nomeEmpresa            | ambiente | rede      |
      | "25358287078" | "Br@12345" | "PARCE DE FATURA FASE" | "TI"     | "empresa" |

  @ConsultarLimite_GestorFiltrarPorBandeiraAConsultaDeLimiteDePortadoresElo
  Scenario Outline: CARTOESPJ - 3186 - Gestor - Filtrar Por Bandeira A Consulta De Limite De Portadores Elo
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico o Filtro Por Bandeira Elo
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      |
      | "25358287078" | "Br@12345" | "TI"     | "empresa" |
      
  @ConsultarLimite_GestorFiltrarPorBandeiraAConsultaDeLimiteDePortadoresVisa
  Scenario Outline: CARTOESPJ - 3186 - Gestor - Filtrar Por Bandeira A Consulta De Limite De Portadores Visa
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico o Filtro Por Bandeira Visa
    And Verifico o Logout com sucesso
    
    Examples:
      | cpf           | senha      | ambiente | rede      |
      | "25358287078" | "Br@12345" | "TI"     | "empresa" |
      
  @ConsultarLimite_GestorFiltrarPorBandeiraAConsultaDeLimiteDePortadoresAmex
  Scenario Outline: CARTOESPJ - 3186 - Gestor - Filtrar Por Bandeira A Consulta De Limite De Portadores Amex
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico o Filtro Por Bandeira Amex
    And Verifico o Logout com sucesso  

    Examples:
      | cpf           | senha      | ambiente | rede      |
      | "34589936909" | "Br@12345" | "TI"     | "empresa" |    
    
  @ConsultarLimite_GestorFiltrarPorBandeiraAConsultaDeLimiteDePortadoresMastercard
  Scenario Outline: CARTOESPJ - 3186 - Gestor - Filtrar Por Bandeira A Consulta De Limite De Portadores Mastercard
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico o Filtro Por Bandeira Mastercard
    And Verifico o Logout com sucesso  

    Examples:
      | cpf           | senha      | ambiente | rede      |
      | "34589936909" | "Br@12345" | "TI"     | "empresa" |
          
  @ConsultarLimite_GestorVerificarBuscaPortadoresPorCPF
  Scenario Outline: CARTOESPJ - 3247 - Gestor - Verificar busca portadores por CPF
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico a busca de portadores por CPF <nomeCpfPortador>
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | nomeCpfPortador | ambiente | rede      |
      | "25358287078" | "Br@12345" | "45292163228"   | "TI"     | "empresa" |

  @ConsultarLimite_GestorVerificarBuscaProdutosPorCompID
  Scenario Outline: CARTOESPJ - 3248 - Gestor - Verificar busca produtos por Comp ID
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico a busca produtos por Comp ID <compID>
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      | compID   |
      | "25358287078" | "Br@12345" | "TI"     | "empresa" | "606943" |

  @ConsultarLimite_GestorVerificarBuscaEmpresasPorCNPJ
  Scenario Outline: CARTOESPJ - 3293 - Gestor - Verificar busca Empresas por CNPJ
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico a busca Empresas por CNPJ <cnpj>
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      | cnpj             |
      | "25358287078" | "Br@12345" | "TI"     | "empresa" | "78139464000170" |

  @ConsultarLimite_GestorVerificarBuscaEmpresasComDadosInexistentes
  Scenario Outline: CARTOESPJ - 3296 - Gestor - Verificar empresas com dados inexistentes
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico empresas com dados inexistentes <cnpj>
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      | cnpj             |
      | "25358287078" | "Br@12345" | "TI"     | "empresa" | "67699844000143" |

  @ConsultarLimite_GestorVerificarBuscaProdutosComDadosInexistentes
  Scenario Outline: CARTOESPJ - 3297 - Gestor - Verificar produtos com dados inexistentes
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico produtos com dados inexistentes <produtos>
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      | produtos              |
      | "25358287078" | "Br@12345" | "TI"     | "empresa" | "PRODUTO INEXISTENTE" |

  @ConsultarLimite_GestorVerificarBuscaPortadoresComDadosInexistentes
  Scenario Outline: CARTOESPJ - 3298 - Gestor - Verificar portadores com dados inexistentes
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico portadores com dados inexistentes <cpfBusca>
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      | cpfBusca      |
      | "25358287078" | "Br@12345" | "TI"     | "empresa" | "78139464000" |

  @ConsultarLimite_GestorVerificarBuscaEmpresasComCNPJinvalido
  Scenario Outline: CARTOESPJ - 3308 - Gestor - Verificar busca empresas com CNPJ invalido
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico empresas com dados inexistentes <cnpj>
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      | cnpj             |
      | "25358287078" | "Br@12345" | "TI"     | "empresa" | "78139464000070" |

  @ConsultarLimite_GestorVerificarBuscaPortadorComCPFInvalido
  Scenario Outline: CARTOESPJ - 3309 - Gestor - Verificar busca portador com CPF invalido
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico portadores com dados inexistentes <cpfBusca>
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      | cpfBusca      |
      | "25358287078" | "Br@12345" | "TI"     | "empresa" | "26785125090" |

  @ConsultarLimite_GestorVerificarConsultaEValidarDetalhesDoPortador
  Scenario Outline: CARTOESPJ - 3186 - Gestor - Verificar consulta e validar detalhes do Portador como Gestor
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico limites e validar os  detalhes do Portador
    And Verifico o Logout com sucesso

    Examples:
      | cpf           					| senha      			| ambiente | rede      |
      | "25358287078" | "Br@12345" | "TI"    				| "empresa" |
      
  @ConsultarLimite_PortadorVerificaDetalhesdeLimies
  Scenario Outline: CARTOESPJ - 3186  - Portador - Verifica detalhes dos meus Limites como Portador
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Verifico detalhes dos meus limites como Portador
    And Verifico o Logout com sucesso

    Examples:
      | cpf           					| senha      			| ambiente | rede      |
      | "13042551822" | "Br@12345" | "TI"    				| "empresa" |

  @ConsultarLimite_GestorValidarBotaoVoltar
  Scenario Outline: CARTOESPJ - 3318 - Gestor - Validar bot�o Voltar
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Valido o bot�o Voltar
    And Verifico o Logout com sucesso

    Examples:
      | cpf          					  | senha     			 | ambiente | rede      |
      | "25358287078" | "Br@12345" |      "TI"    	 | "empresa" |
      
 @ConsultarLimite_GestorVerificarConsultaLimiteEmpresaRepresentanteEstrangeiro
  Scenario Outline: CARTOESPJ - 3186 - Gestor Estrangeiro - Verificar Consulta Limite Empresa Representante Estrangeiro
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <email> <senha>
    Then Fecho o Pop-up Trazer Dados Open Finance como Gestor
    Then Verifico o login efetuado com sucesso
    And Verifico Limite Total do Representante Estrangeiro
    And Verifico o Logout com sucesso

    Examples:
      | email           																			 | senha     			  | ambiente | rede      |
      | "PRIMACESSWEB@GMAIL.COM" |"Br@123456"	| "TH"     			| "empresa" |
  
   @ConsultarLimite_GestorVerificarConsultaLimiteComoRepresentanteEstrangeirodoPortadorHibrido
   Scenario Outline: CARTOESPJ - 3186 - Gestor Estrangeiro - Verificar Limite Como Representante Estrangeiro de um Portador Hibrido
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <email> <senha>
    Then Fecho o Pop-up Trazer Dados Open Finance como Gestor
    Then Verifico o login efetuado com sucesso
    And Verifico Total Disponivel como Representante Estrangeiro de Portador Hibrido
    And Verifico o Logout com sucesso

    Examples:
      | email           																			 	 | senha      			  | ambiente | rede      |
      | "PRIMACESSWEB@GMAIL.COM"   |"Br@123456"	| "TH"    			 | "empresa" |
      
   @ConsultarLimite_GestorVerificarConsultaLimiteComoRepresentanteEstrangeirodoPortadorHibridoCFI
   Scenario Outline: CARTOESPJ - 3186 - Gestor Estrangeiro - Verificar Limite Como Representante Estrangeiro de um Portador Hibrido CFI
     Given Acessar a pagina do Portal PJ <ambiente> <rede>
     When Efetuo o login <email> <senha>
     Then Fecho o Pop-up Trazer Dados Open Finance como Gestor
     Then Verifico o login efetuado com sucesso
     And Verifico Total Disponivel como Representante Estrangeiro de Portador Hibrido CFI <REportadorHibridoCFI>
     And Verifico o Logout com sucesso

     Examples:
       | email           				 															 | senha     		  | ambiente | rede      				| REportadorHibridoCFI|
       | "TESTEQA68@GMAIL.COM" 				 |"Br@12345"	|    "TH"    	  | "empresa"  |        "26690132609"      |
      
   @ConsultarLimite_GestorVerificarConsultaLimiteComoRepresentanteEstrangeirodoPortadorCentralizado
  	Scenario Outline: CARTOESPJ - 3186 - Gestor Estrangeiro - Verificar Limite Como Representante Estrangeiro de um Portador Centralizado
     Given Acessar a pagina do Portal PJ <ambiente> <rede>
     When Efetuo o login <email> <senha>
     Then Fecho o Pop-up Trazer Dados Open Finance como Gestor
     Then Verifico o login efetuado com sucesso
     And Verifico Total Disponivel como Representante Estrangeiro do Portador Centralizado <escolherPortadorCentralizado>
     And Verifico o Logout com sucesso

     Examples:
       | email           				 																				 | senha      			| ambiente | rede    			 		 | escolherPortadorCentralizado|
       | "TESTEQAPRIMACESS1@GMAIL.COM" |"Br@12345"	| "TH"    		 		| "empresa" | "45292797669"|
      
   @ConsultarLimite_GestorVerificarConsultaLimiteComoRepresentanteEstrangeirodoPortadorCentralizadoCFI
  	Scenario Outline: CARTOESPJ - 3240 - Gestor Estrangeiro - Verificar Limite Como Representante Estrangeiro de um Portador Centralizado CFI
     Given Acessar a pagina do Portal PJ <ambiente> <rede>
     When Efetuo o login <email> <senha>
     Then Fecho o Pop-up Trazer Dados Open Finance como Gestor
     Then Verifico o login efetuado com sucesso
     And Verifico Total Disponivel como Representante Estrangeiro de Portador Centralizado CFI <escolherPortadorCentralizadoCFI>
     And Verifico o Logout com sucesso

     Examples:
       | email           																			 | senha     			  | ambiente		  | 		rede    			 | escolherPortadorCentralizadoCFI|
       | "TESTEQA68@GMAIL.COM"					 |"Br@12345"	  |      "TH"    		  | "empresa" 	 |        "26690131475"                        |
       
   @ConsultarLimite_RepresentanteEstrangeiroVerificaLimitesDetalhesdoPortador
  	Scenario Outline: CARTOESPJ - 3186 - Gestor Estrangeiro - Verificar Limites e detalhes Como Representante Estrangeiro de um Portador
 		 Given Acessar a pagina do Portal PJ <ambiente> <rede>
     When Efetuo o login <email> <senha>
     Then Fecho o Pop-up Trazer Dados Open Finance como Gestor
     Then Verifico o login efetuado com sucesso
     And Verifico limites e valido os  detalhes do Portador como Representante Estrangeiro <escolherPortadorCentralizado>
     And Verifico o Logout com sucesso
    
     Examples:
       | email           														  | senha      			| ambiente | rede     			 | escolherPortadorCentralizado|
       | "TESTEQA68@GMAIL.COM" |"Br@12345"	  | "TH"     			| "empresa" | 		"93742250000"                        |    
          
  @ConsultarLimite_GestorValidarPaginacaoEmpresas
  Scenario Outline: CARTOESPJ - 3319 - Gestor - Validar pagina��o Empresas
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Valido pagina��o Empresas
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      |
      | "25358287078" | "Br@12345" | "TI"     | "empresa" |

  @ConsultarLimite_GestorValidarPaginacaoProdutos
  Scenario Outline: CARTOESPJ - 3320 - Gestor - Validar paginacao Produtos
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Valido pagina��o Produtos
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      |
      | "25358287078" | "Br@12345" | "TI"     | "empresa" |

  @ConsultarLimite_GestorValidarPaginacaoPortadores
  Scenario Outline: CARTOESPJ - 3321 - Gestor - Validar paginacao Portadores
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Valido pagina��o Portadores
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      |
      | "25358287078" | "Br@12345" | "TI"     | "empresa" |

  @ConsultarLimite_PortadorAtivarOptOut
  Scenario Outline: CARTOESPJ - 3235 - Portador - Ativar opt-out para opt-in do Bloqueio tempor�rio
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Efetuo bloqueio temporario
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      |
      | "45292163228" | "Br@12345" | "TI"     | "empresa" |

  @ConsultarLimite_PortadorDesativarOptOut
  Scenario Outline: CARTOESPJ - 3236 - Portador - Desativar opt-in para opt-out do Bloqueio tempor�rio
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And Efetuo desbloqueio temporario
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      |
      | "13042551822" | "Br@12345" | "TI"     | "empresa" |

  @ConsultarLimite_PortadorDesativarOptOutComBloqueioCentral
  Scenario Outline: CARTOESPJ - 3726 - Portador - Desativar opt-in para opt-out do Bloqueio tempor�rio com bloqueio na central
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Verifico o login efetuado com sucesso
    And efetuo desbloqueio tempor�rio com bloqueio na central
    And Verifico o Logout com sucesso

    Examples:
      | cpf           | senha      | ambiente | rede      |
      | "45292163228" | "Br@12345" | "TI"     | "empresa" |
