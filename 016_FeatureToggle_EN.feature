#encoding: iso-8859-1
################################################################
############## CENARIOS DE FEATURE TOGGLE INGLES ###############
################################################################

@016_FeatureToggle_EN
Feature: 016_FeatureToggle_EN

  #Aviso Viagem ativado
  @FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleAtivadoNacionalIngles
  Scenario Outline: CTWR 08-09 Validar funcionalidade Aviso Viagem com Feature Toggle ativado gestor nacional em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Aviso Viagem

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "aviso_viagem" | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleAtivadoREIngles
  Scenario Outline: CTWR 08-09 Validar funcionalidade Aviso Viagem com Feature Toggle ativado gestor estrangeiro em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Aviso Viagem

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "aviso_viagem" | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleAtivadoPortadorIngles
  Scenario Outline: CTWR 08-09 Validar funcionalidade Aviso Viagem com Feature Toggle ativado portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Aviso Viagem

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "aviso_viagem" | "portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleAtivadoGPIngles
  Scenario Outline: CTWR 08-09 Validar funcionalidade Aviso Viagem com Feature Toggle ativado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Aviso Viagem

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "aviso_viagem" | "gestor portador" | "web"      |

  #Aviso Viagem inativado
  @FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleInativadoNacionalIngles
  Scenario Outline: CTWR 08-11 Validar funcionalidade Aviso Viagem com Feature Toggle inativado gestor nacional em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Aviso Viagem
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "aviso_viagem" | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleInativadoREIngles
  Scenario Outline: CTWR 08-11 Validar funcionalidade Aviso Viagem com Feature Toggle inativado gestor estrangeiro em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Aviso Viagem
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "aviso_viagem" | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleInativadoPortadorIngles
  Scenario Outline: CTWR 08-11 Validar funcionalidade Aviso Viagem com Feature Toggle inativado portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Aviso Viagem
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "aviso_viagem" | "portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleInativadoGPIngles
  Scenario Outline: CTWR 08-11 Validar funcionalidade Aviso Viagem com Feature Toggle inativado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Aviso Viagem
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "aviso_viagem" | "gestor portador" | "web"      |

  #Aviso Viagem ocultado
  @FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleOcultadoNacionalIngles
  Scenario Outline: CTWR 08-10 Validar funcionalidade Aviso Viagem com Feature Toggle ocultado gestor nacional em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Aviso Viagem
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "aviso_viagem" | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleOcultadoREIngles
  Scenario Outline: CTWR 08-10 Validar funcionalidade Aviso Viagem com Feature Toggle ocultado gestor estrangeiro em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Aviso Viagem
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "aviso_viagem" | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleOcultadoPortadorIngles
  Scenario Outline: CTWR 08-10 Validar funcionalidade Aviso Viagem com Feature Toggle ocultado portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Aviso Viagem
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "aviso_viagem" | "portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleOcultadoGPIngles
  Scenario Outline: CTWR 08-10 Validar funcionalidade Aviso Viagem com Feature Toggle ocultado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Aviso Viagem
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "aviso_viagem" | "gestor portador" | "web"      |


  #Boletos ativado
  @FeatureToggle_ValidarFuncionalidadeBoletosComFeatureToggleAtivadoNacionalIngles
  Scenario Outline: CTWR 08-18 Validar funcionalidade Boletos com Feature Toggle ativado gestor nacional em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Boletos

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "boleto"       | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeBoletosComFeatureToggleAtivadoREIngles
  Scenario Outline: CTWR 08-18 Validar funcionalidade Boletos com Feature Toggle ativado gestor estrangeiro em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Boletos

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "boleto"       | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeBoletosComFeatureToggleAtivadoGPIngles
  Scenario Outline: CTWR 08-18 Validar funcionalidade Boletos com Feature Toggle ativado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Boletos

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "boleto"       | "gestor portador" | "web"      |

  #Boletos inativado
  @FeatureToggle_ValidarFuncionalidadeBoletosComFeatureToggleInativadoNacionalIngles
  Scenario Outline: CTWR 08-19 Validar funcionalidade Boletos com Feature Toggle inativado gestor nacional em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Boletos
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "boleto"       | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeBoletosComFeatureToggleInativadoREIngles
  Scenario Outline: CTWR 08-19 Validar funcionalidade Boletos com Feature Toggle inativado gestor estrangeiro em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Boletos
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "boleto"       | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeBoletosComFeatureToggleInativadoGPIngles
  Scenario Outline: CTWR 08-19 Validar funcionalidade Boletos com Feature Toggle inativado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Boletos
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "boleto"       | "gestor portador" | "web"      |

  #Boletos ocultado
  @FeatureToggle_ValidarFuncionalidadeBoletosComFeatureToggleOcultadoNacionalIngles
  Scenario Outline: CTWR 08-20 Validar funcionalidade Boletos com Feature Toggle ocultado gestor nacional em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Boletos
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "boleto"       | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeBoletosComFeatureToggleOcultadoREIngles
  Scenario Outline: CTWR 08-20 Validar funcionalidade Boletos com Feature Toggle ocultado gestor estrangeiro em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Boletos
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "boleto"       | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeBoletosComFeatureToggleOcultadoGPIngles
  Scenario Outline: CTWR 08-20 Validar funcionalidade Boletos com Feature Toggle ocultado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Boletos
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "boleto"       | "gestor portador" | "web"      |


  #LGPD ativado
  @FeatureToggle_ValidarFuncionalidadeLGPDComFeatureToggleAtivadoNacionalIngles
  Scenario Outline: CTWR 08-26 Validar funcionalidade LGPD com Feature Toggle ativado gestor nacional em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de LGPD

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "lgpd"         | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeLGPDComFeatureToggleAtivadoPortadorIngles
  Scenario Outline: CTWR 08-26 Validar funcionalidade LGPD com Feature Toggle ativado portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de LGPD

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "lgpd"         | "portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeLGPDComFeatureToggleAtivadoGPIngles
  Scenario Outline: CTWR 08-26 Validar funcionalidade LGPD com Feature Toggle ativado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de LGPD

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "lgpd"         | "gestor portador" | "web"      |

  #LGPD inativado
  @FeatureToggle_ValidarFuncionalidadeLGPDComFeatureToggleInativadoNacionalIngles
  Scenario Outline: CTWR 08-27 Validar funcionalidade LGPD com Feature Toggle inativado gestor nacional em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de LGPD
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "lgpd"         | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeLGPDComFeatureToggleInativadoPortadorIngles
  Scenario Outline: CTWR 08-27 Validar funcionalidade LGPD com Feature Toggle inativado portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de LGPD
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "lgpd"         | "portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeLGPDComFeatureToggleInativadoGPIngles
  Scenario Outline: CTWR 08-27 Validar funcionalidade LGPD com Feature Toggle inativado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de LGPD
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "lgpd"         | "gestor portador" | "web"      |

  #LGPD ocultado
  @FeatureToggle_ValidarFuncionalidadeLGPDComFeatureToggleOcultadoNacionalIngles
  Scenario Outline: CTWR 08-28 Validar funcionalidade LGPD com Feature Toggle ocultado gestor nacional em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de LGPD
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "lgpd"         | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeLGPDComFeatureToggleOcultadoPortadorIngles
  Scenario Outline: CTWR 08-28 Validar funcionalidade LGPD com Feature Toggle ocultado portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de LGPD
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "lgpd"         | "portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeLGPDComFeatureToggleOcultadoGPIngles
  Scenario Outline: CTWR 08-28 Validar funcionalidade LGPD com Feature Toggle ocultado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de LGPD
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "lgpd"         | "gestor portador" | "web"      |


  #Taxa de Cambio ativado
  @FeatureToggle_ValidarFuncionalidadeTaxaDeCambioComFeatureToggleAtivadoNacionalIngles
  Scenario Outline: CTWR 08-50 Validar funcionalidade Taxa de Cambio com Feature Toggle ativado gestor nacional em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Taxa de Cambio

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade         | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consulta_taxa_cambio" | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeTaxaDeCambioComFeatureToggleAtivadoREIngles
  Scenario Outline: CTWR 08-50 Validar funcionalidade Taxa de Cambio com Feature Toggle ativado gestor estrangeiro em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Taxa de Cambio

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade         | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "consulta_taxa_cambio" | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeTaxaDeCambioComFeatureToggleAtivadoPortadorIngles
  Scenario Outline: CTWR 08-50 Validar funcionalidade Taxa de Cambio com Feature Toggle ativado portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Taxa de Cambio

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade         | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "consulta_taxa_cambio" | "portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeTaxaDeCambioComFeatureToggleAtivadoGPIngles
  Scenario Outline: CTWR 08-50 Validar funcionalidade Taxa de Cambio com Feature Toggle ativado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Taxa de Cambio

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade         | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "consulta_taxa_cambio" | "gestor portador" | "web"      |

  #Taxa de Cambio inativado
  @FeatureToggle_ValidarFuncionalidadeTaxaDeCambioComFeatureToggleInativadoNacionalIngles
  Scenario Outline: CTWR 08-51 Validar funcionalidade Taxa de Cambio com Feature Toggle inativado gestor nacional em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Taxa de Cambio
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade         | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consulta_taxa_cambio" | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeTaxaDeCambioComFeatureToggleInativadoREIngles
  Scenario Outline: CTWR 08-51 Validar funcionalidade Taxa de Cambio com Feature Toggle inativado gestor estrangeiro em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Taxa de Cambio
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade         | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "consulta_taxa_cambio" | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeTaxaDeCambioComFeatureToggleInativadoPortadorIngles
  Scenario Outline: CTWR 08-51 Validar funcionalidade Taxa de Cambio com Feature Toggle inativado portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Taxa de Cambio
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade         | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "consulta_taxa_cambio" | "portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeTaxaDeCambioComFeatureToggleInativadoGPIngles
  Scenario Outline: CTWR 08-51 Validar funcionalidade Taxa de Cambio com Feature Toggle inativado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Taxa de Cambio
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade         | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "consulta_taxa_cambio" | "gestor portador" | "web"      |

  #Taxa de Cambio ocultado
  @FeatureToggle_ValidarFuncionalidadeTaxaDeCambioComFeatureToggleOcultadoNacionalIngles
  Scenario Outline: CTWR 08-52 Validar funcionalidade Taxa de Cambio com Feature Toggle ocultado gestor nacional em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Taxa de Cambio
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade         | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consulta_taxa_cambio" | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeTaxaDeCambioComFeatureToggleOcultadoREIngles
  Scenario Outline: CTWR 08-52 Validar funcionalidade Taxa de Cambio com Feature Toggle ocultado gestor estrangeiro em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Taxa de Cambio
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade         | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "consulta_taxa_cambio" | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeTaxaDeCambioComFeatureToggleOcultadoPortadorIngles
  Scenario Outline: CTWR 08-52 Validar funcionalidade Taxa de Cambio com Feature Toggle ocultado portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Taxa de Cambio
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade         | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "consulta_taxa_cambio" | "portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeTaxaDeCambioComFeatureToggleOcultadoGPIngles
  Scenario Outline: CTWR 08-52 Validar funcionalidade Taxa de Cambio com Feature Toggle ocultado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Taxa de Cambio
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade         | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "consulta_taxa_cambio" | "gestor portador" | "web"      |


  #Parcelamento de Fatura ativado
  @FeatureToggle_ValidarFuncionalidadeParcelamentoDeFaturaComFeatureToggleAtivadoNacionalIngles
  Scenario Outline: CTWR 08-29 Validar funcionalidade Parcelamento de Fatura com Feature Toggle ativado gestor nacional em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And Acesso o cabecalho de Parcelamento de Fatura

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade        | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "parcelamento_fatura" | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeParcelamentoDeFaturaComFeatureToggleAtivadoREIngles
  Scenario Outline: CTWR 08-29 Validar funcionalidade Parcelamento de Fatura com Feature Toggle ativado gestor estrangeiro em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And Acesso o cabecalho de Parcelamento de Fatura

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade        | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "parcelamento_fatura" | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeParcelamentoDeFaturaComFeatureToggleAtivadoGPIngles
  Scenario Outline: CTWR 08-29 Validar funcionalidade Parcelamento de Fatura com Feature Toggle ativado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And Acesso o cabecalho de Parcelamento de Fatura

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade        | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "parcelamento_fatura" | "gestor portador" | "web"      |

  #Parcelamento de Fatura inativado
  @FeatureToggle_ValidarFuncionalidadeParcelamentoDeFaturaComFeatureToggleInativadoNacionalIngles
  Scenario Outline: CTWR 08-30 Validar funcionalidade Parcelamento de Fatura com Feature Toggle inativado gestor nacional em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And Acesso o cabecalho de Parcelamento de Fatura
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade        | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "parcelamento_fatura" | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeParcelamentoDeFaturaComFeatureToggleInativadoREIngles
  Scenario Outline: CTWR 08-30 Validar funcionalidade Parcelamento de Fatura com Feature Toggle inativado gestor estrangeiro em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And Acesso o cabecalho de Parcelamento de Fatura
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade        | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "parcelamento_fatura" | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeParcelamentoDeFaturaComFeatureToggleInativadoGPIngles
  Scenario Outline: CTWR 08-30 Validar funcionalidade Parcelamento de Fatura com Feature Toggle inativado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And Acesso o cabecalho de Parcelamento de Fatura
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade        | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "parcelamento_fatura" | "gestor portador" | "web"      |

  #Parcelamento de Fatura ocultado
  @FeatureToggle_ValidarFuncionalidadeParcelamentoDeFaturaComFeatureToggleOcultadoNacionalIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Parcelamento de Fatura com Feature Toggle ocultado gestor nacional em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And Acesso o cabecalho de Parcelamento de Fatura
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade        | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "parcelamento_fatura" | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeParcelamentoDeFaturaComFeatureToggleOcultadoREIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Parcelamento de Fatura com Feature Toggle ocultado gestor estrangeiro em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And Acesso o cabecalho de Parcelamento de Fatura
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade        | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "parcelamento_fatura" | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeParcelamentoDeFaturaComFeatureToggleOcultadoGPIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Parcelamento de Fatura com Feature Toggle ocultado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And Acesso o cabecalho de Parcelamento de Fatura
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade        | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "parcelamento_fatura" | "gestor portador" | "web"      |


  #Consultas ativado
  @FeatureToggle_ValidarBotaoConsultasComFeatureToggleAtivadoNacionalIngles
  Scenario Outline: CTWR 08-32 Validar botao Consultas com Feature Toggle ativado gestor nacional em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Consultas

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consultas"    | "gestor" | "web"      |

  @FeatureToggle_ValidarBotaoConsultasComFeatureToggleAtivadoREIngles
  Scenario Outline: CTWR 08-32 Validar botao Consultas com Feature Toggle ativado gestor estrangeiro em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Consultas

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "consultas"    | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarBotaoConsultasComFeatureToggleAtivadoPortadorIngles
  Scenario Outline: CTWR 08-32 Validar botao Consultas com Feature Toggle ativado portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Consultas

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "consultas"    | "portador" | "web"      |

  @FeatureToggle_ValidarBotaoConsultasComFeatureToggleAtivadoGPIngles
  Scenario Outline: CTWR 08-32 Validar botao Consultas com Feature Toggle ativado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Consultas

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "consultas"    | "gestor portador" | "web"      |

  #Consultas inativado
  @FeatureToggle_ValidarBotaoConsultasComFeatureToggleInativadoNacionalIngles
  Scenario Outline: CTWR 08-33 Validar botao Consultas com Feature Toggle inativado gestor nacional em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Consultas
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consultas"    | "gestor" | "web"      |

  @FeatureToggle_ValidarBotaoConsultasComFeatureToggleInativadoREIngles
  Scenario Outline: CTWR 08-33 Validar botao Consultas com Feature Toggle inativado gestor estrangeiro em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Consultas
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "consultas"    | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarBotaoConsultasComFeatureToggleInativadoPortadorIngles
  Scenario Outline: CTWR 08-33 Validar botao Consultas com Feature Toggle inativado portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Consultas
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "consultas"    | "portador" | "web"      |

  @FeatureToggle_ValidarBotaoConsultasComFeatureToggleInativadoGPIngles
  Scenario Outline: CTWR 08-33 Validar botao Consultas com Feature Toggle inativado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Consultas
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "consultas"    | "gestor portador" | "web"      |

  #Consultas ocultado
  @FeatureToggle_ValidarBotaoConsultasComFeatureToggleOcultadoNacionalIngles
  Scenario Outline: CTWR 08-34 Validar botao Consultas com Feature Toggle ocultado gestor nacional em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Consultas
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consultas"    | "gestor" | "web"      |

  @FeatureToggle_ValidarBotaoConsultasComFeatureToggleOcultadoREIngles
  Scenario Outline: CTWR 08-34 Validar botao Consultas com Feature Toggle ocultado gestor estrangeiro em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Consultas
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "consultas"    | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarBotaoConsultasComFeatureToggleOcultadoPortadorIngles
  Scenario Outline: CTWR 08-34 Validar botao Consultas com Feature Toggle ocultado portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Consultas
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "consultas"    | "portador" | "web"      |

  @FeatureToggle_ValidarBotaoConsultasComFeatureToggleOcultadoGPIngles
  Scenario Outline: CTWR 08-34 Validar botao Consultas com Feature Toggle ocultado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Consultas
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "consultas"    | "gestor portador" | "web"      |


  #Solicitacoes Gestor ativado
  @FeatureToggle_ValidarBotaoSolicitacoesGestorComFeatureToggleAtivadoNacionalIngles
  Scenario Outline: CTWR 08-35 Validar botao Solicitacao com Feature Toggle ativado gestor nacional em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Solicitacoes Gestor

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade       | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "solicitacao_gestor" | "gestor" | "web"      |

  @FeatureToggle_ValidarBotaoSolicitacoesGestorComFeatureToggleAtivadoREIngles
  Scenario Outline: CTWR 08-35 Validar botao Solicitacoes Gestor com Feature Toggle ativado gestor estrangeiro em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Solicitacoes Gestor

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade       | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "solicitacao_gestor" | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarBotaoSolicitacoesGestorComFeatureToggleAtivadoGPIngles
  Scenario Outline: CTWR 08-35 Validar botao Solicitacoes Gestor com Feature Toggle ativado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Solicitacoes Gestor

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade       | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "solicitacao_gestor" | "gestor portador" | "web"      |

  #Solicitacoes Gestor inativado
  @FeatureToggle_ValidarBotaoSolicitacoesGestorComFeatureToggleInativadoNacionalIngles
  Scenario Outline: CTWR 08-36 Validar botao Solicitacoes Gestor com Feature Toggle inativado gestor nacional em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Solicitacoes Gestor
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade       | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "solicitacao_gestor" | "gestor" | "web"      |

  @FeatureToggle_ValidarBotaoSolicitacoesGestorComFeatureToggleInativadoREIngles
  Scenario Outline: CTWR 08-36 Validar botao Solicitacoes Gestor com Feature Toggle inativado gestor estrangeiro em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Solicitacoes Gestor
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade       | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "solicitacao_gestor" | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarBotaoSolicitacoesGestorComFeatureToggleInativadoGPIngles
  Scenario Outline: CTWR 08-36 Validar botao Solicitacoes Gestor com Feature Toggle inativado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Solicitacoes Gestor
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade       | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "solicitacao_gestor" | "gestor portador" | "web"      |

  #Solicitacoes Gestor ocultado
  @FeatureToggle_ValidarBotaoSolicitacoesGestorComFeatureToggleOcultadoNacionalIngles
  Scenario Outline: CTWR 08-37 Validar botao Solicitacoes Gestor com Feature Toggle ocultado gestor nacional em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Solicitacoes Gestor
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade       | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "solicitacao_gestor" | "gestor" | "web"      |

  @FeatureToggle_ValidarBotaoSolicitacoesGestorComFeatureToggleOcultadoREIngles
  Scenario Outline: CTWR 08-37 Validar botao Solicitacoes Gestor com Feature Toggle ocultado gestor estrangeiro em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Solicitacoes Gestor
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade       | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "solicitacao_gestor" | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarBotaoSolicitacoesGestorComFeatureToggleOcultadoGPIngles
  Scenario Outline: CTWR 08-37 Validar botao Solicitacoes Gestor com Feature Toggle ocultado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Solicitacoes Gestor
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade       | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "solicitacao_gestor" | "gestor portador" | "web"      |


  #Fatura ativado
  @FeatureToggle_ValidarFuncionalidadeFaturaComFeatureToggleAtivadoNacionalIngles
  Scenario Outline: CTWR 08-59 Validar funcionalidade Fatura com Feature Toggle ativado gestor nacional em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Faturas

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "fatura"       | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeFaturaComFeatureToggleAtivadoREIngles
  Scenario Outline: CTWR 08-59 Validar funcionalidade Fatura com Feature Toggle ativado gestor estrangeiro em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Faturas

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "fatura"       | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeFaturaComFeatureToggleAtivadoGPIngles
  Scenario Outline: CTWR 08-59 Validar funcionalidade Fatura com Feature Toggle ativado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Faturas

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "fatura"       | "gestor portador" | "web"      |

  #Fatura inativado
  @FeatureToggle_ValidarFuncionalidadeFaturaComFeatureToggleInativadoNacionalIngles
  Scenario Outline: CTWR 08-60 Validar funcionalidade Fatura com Feature Toggle inativado gestor nacional em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Faturas
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "fatura"       | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeFaturaComFeatureToggleInativadoREIngles
  Scenario Outline: CTWR 08-60 Validar funcionalidade Fatura com Feature Toggle inativado gestor estrangeiro em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Faturas
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "fatura"       | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeFaturaComFeatureToggleInativadoGPIngles
  Scenario Outline: CTWR 08-60 Validar funcionalidade Fatura com Feature Toggle inativado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Faturas
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "fatura"       | "gestor portador" | "web"      |

  #Fatura ocultado
  @FeatureToggle_ValidarFuncionalidadeFaturaComFeatureToggleOcultadoNacionalIngles
  Scenario Outline: CTWR 08-61 Validar funcionalidade Fatura com Feature Toggle ocultado gestor nacional em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Faturas
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "fatura"       | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeFaturaComFeatureToggleOcultadoREIngles
  Scenario Outline: CTWR 08-61 Validar funcionalidade Fatura com Feature Toggle ocultado gestor estrangeiro em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Faturas
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "fatura"       | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeFaturaComFeatureToggleOcultadoGPIngles
  Scenario Outline: CTWR 08-61 Validar funcionalidade Fatura com Feature Toggle ocultado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o cabecalho de Faturas
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "fatura"       | "gestor portador" | "web"      |


  #Consulta de Limites Portador
  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesPortadorComFeatureToggleInativaemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites Portador com Feature Toggle Inativa em Ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Portador que a tela Consulta de Limites esta Inativo em Ingles
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade     | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesPortadorComFeatureToggleOcultoemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites Portador com Feature Toggle Oculta em Ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Portador que a tela Consulta de Limites esta Oculto em Ingles
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade     | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesPortadorComFeatureToggleAtivaemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites Portador com Feature Toggle Ativa em Ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Portador que a tela Consulta de Limites esta Ativa em Ingles
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade     | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorComFeatureToggleInativaemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites Gestor com Feature Toggle Inativa em Ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor que a tela Consulta de Limites esta Inativo em Ingles
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade     | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorComFeatureToggleOcultoemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites Gestor com Feature Toggle Oculto em Ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor que a tela Consulta de Limites esta Oculto em Ingles
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade     | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorComFeatureToggleAtivaemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites Gestor com Feature Toggle Ativa em Ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor que a tela Consulta de Limites esta Ativa em Ingles
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade     | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorEstrangeiroComFeatureToggleInativaemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites como Gestor Estrangeiro com Feature Toggle Inativa em Ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor Estrangeiro que a tela Consulta de Limites esta Inativo em Ingles
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Gestor Estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorEstrangeiroComFeatureToggleOcultoemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites como Gestor Estrangeiro com Feature Toggle Oculto em Ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor Estrangeiro que a tela Consulta de Limites esta Oculto em Ingles
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Gestor Estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorEstrangeiroComFeatureToggleAtivaemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites como Gestor Estrangeiro com Feature Toggle Ativa em Ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor Estrangeiro que a tela Consulta de Limites esta Ativa em Ingles
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Gestor Estrangeiro" | "web"      |


  #Solicitacoes ativado
  @FeatureToggle_ValidarBotaoSolicitacoesComFeatureToggleAtivadoREIngles
  Scenario Outline: CTWR 08-74 Validar botao Solicitacoes com Feature Toggle ativado gestor estrangeiro em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Solicitacoes

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "solicitacao"  | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarBotaoSolicitacoesComFeatureToggleAtivadoPortadorIngles
  Scenario Outline: CTWR 08-74 Validar botao Solicitacoes com Feature Toggle ativado portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Solicitacoes

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "solicitacao"  | "portador" | "web"      |

  @FeatureToggle_ValidarBotaoSolicitacoesComFeatureToggleAtivadoGPIngles
  Scenario Outline: CTWR 08-74 Validar botao Solicitacoes com Feature Toggle ativado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Solicitacoes

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "solicitacao"  | "gestor portador" | "web"      |

  #Solicitacoes inativado
  @FeatureToggle_ValidarBotaoSolicitacoesComFeatureToggleInativadoREIngles
  Scenario Outline: CTWR 08-75 Validar botao Solicitacoes com Feature Toggle inativado gestor estrangeiro em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Solicitacoes
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "solicitacao"  | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarBotaoSolicitacoesComFeatureToggleInativadoPortadorIngles
  Scenario Outline: CTWR 08-75 Validar botao Solicitacoes com Feature Toggle inativado portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Solicitacoes
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "solicitacao"  | "portador" | "web"      |

  @FeatureToggle_ValidarBotaoSolicitacoesComFeatureToggleInativadoGPIngles
  Scenario Outline: CTWR 08-75 Validar botao Solicitacoes com Feature Toggle inativado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Solicitacoes
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "solicitacao"  | "gestor portador" | "web"      |

  #Solicitacoes ocultado
  @FeatureToggle_ValidarBotaoSolicitacoesComFeatureToggleOcultadoREIngles
  Scenario Outline: CTWR 08-76 Validar botao Solicitacoes com Feature Toggle ocultado gestor estrangeiro em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Solicitacoes
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "solicitacao"  | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarBotaoSolicitacoesComFeatureToggleOcultadoPortadorIngles
  Scenario Outline: CTWR 08-76 Validar botao Solicitacoes com Feature Toggle ocultado portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Solicitacoes
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "solicitacao"  | "portador" | "web"      |

  @FeatureToggle_ValidarBotaoSolicitacoesComFeatureToggleOcultadoGPIngles
  Scenario Outline: CTWR 08-76 Validar botao Solicitacoes com Feature Toggle ocultado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Solicitacoes
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "solicitacao"  | "gestor portador" | "web"      |


  #Boleto ativado
  @FeatureToggle_ValidarBotaoBoletoComFeatureToggleAtivadoPortadorIngles
  Scenario Outline: CTWR 08-77 Validar botao Boleto com Feature Toggle ativado portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Boleto

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade    | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "boleto_portador" | "portador" | "web"      |

  #Boleto inativado
  @FeatureToggle_ValidarBotaoBoletoComFeatureToggleInativadoPortadorIngles
  Scenario Outline: CTWR 08-78 Validar botao Boleto com Feature Toggle inativado portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Boleto
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade    | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "boleto_portador" | "portador" | "web"      |

  #Boleto ocultado
  @FeatureToggle_ValidarBotaoBoletoComFeatureToggleOcultadoPortadorIngles
  Scenario Outline: CTWR 08-79 Validar botao Boleto com Feature Toggle ocultado portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o botao de Boleto
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade    | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "boleto_portador" | "portador" | "web"      |


  #Extrato ativado
  @FeatureToggle_ValidarFuncionalidadeExtratoComFeatureToggleAtivadoNacionalIngles
  Scenario Outline: CTWR 08-62 Validar funcionalidade Extrato com Feature Toggle ativado gestor nacional em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o funcionalidade de Extrato

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "extrato"      | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeExtratoComFeatureToggleAtivadoREIngles
  Scenario Outline: CTWR 08-62 Validar funcionalidade Extrato com Feature Toggle ativado gestor estrangeiro em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o funcionalidade de Extrato

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "extrato"      | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeExtratoComFeatureToggleAtivadoPortadorIngles
  Scenario Outline: CTWR 08-62 Validar funcionalidade Extrato com Feature Toggle ativado portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o funcionalidade de Extrato

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "extrato"      | "portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeExtratoComFeatureToggleAtivadoGPIngles
  Scenario Outline: CTWR 08-62 Validar funcionalidade Extrato com Feature Toggle ativado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o funcionalidade de Extrato

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "extrato"      | "gestor portador" | "web"      |

  #Extrato inativado
  @FeatureToggle_ValidarFuncionalidadeExtratoComFeatureToggleInativadoNacionalIngles
  Scenario Outline: CTWR 08-63 Validar funcionalidade Extrato com Feature Toggle inativado gestor nacional em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o funcionalidade de Extrato
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "extrato"      | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeExtratoComFeatureToggleInativadoREIngles
  Scenario Outline: CTWR 08-63 Validar funcionalidade Extrato com Feature Toggle inativado gestor estrangeiro em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o funcionalidade de Extrato
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "extrato"      | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeExtratoComFeatureToggleInativadoPortadorIngles
  Scenario Outline: CTWR 08-63 Validar funcionalidade Extrato com Feature Toggle inativado portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o funcionalidade de Extrato
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "extrato"      | "portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeExtratoComFeatureToggleInativadoGPIngles
  Scenario Outline: CTWR 08-63 Validar funcionalidade Extrato com Feature Toggle inativado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o funcionalidade de Extrato
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "extrato"      | "gestor portador" | "web"      |

  #Extrato ocultado
  @FeatureToggle_ValidarFuncionalidadeExtratoComFeatureToggleOcultadoNacionalIngles
  Scenario Outline: CTWR 08-64 Validar funcionalidade Extrato com Feature Toggle ocultado gestor nacional em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o funcionalidade de Extrato
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "extrato"      | "gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeExtratoComFeatureToggleOcultadoREIngles
  Scenario Outline: CTWR 08-64 Validar funcionalidade Extrato com Feature Toggle ocultado gestor estrangeiro em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o funcionalidade de Extrato
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TH"     | "empresa" | "extrato"      | "gestor estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeExtratoComFeatureToggleOcultadoPortadorIngles
  Scenario Outline: CTWR 08-64 Validar funcionalidade Extrato com Feature Toggle ocultado portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o funcionalidade de Extrato
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TH"     | "empresa" | "extrato"      | "portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeExtratoComFeatureToggleOcultadoGPIngles
  Scenario Outline: CTWR 08-64 Validar funcionalidade Extrato com Feature Toggle ocultado gestor portador em ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    And acesso o funcionalidade de Extrato
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf | senha      | ambiente | rede      | funcionalidade | perfil            | plataforma |
      | "admin"  | "admin"  | ""  | "Br@12345" | "TH"     | "empresa" | "extrato"      | "gestor portador" | "web"      |
      
       @FeatureToggle_ValidarFuncionalidadeRelatoriosGestorComFeatureToggleInativaemIngles
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Relatorios Gestor com Feature Toggle Inativa em Ingles
	    Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login em ingles <cpf> <senha>
	    #And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor que a tela Relat�rios esta Inativo em Ingles
	    And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "relatorios_fixos" | "Gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeRelatoriosGestorComFeatureToggleOcultoemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Relatrios Gestor com Feature Toggle Oculto em Ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    #And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor que a tela Relat�rios esta Oculto em Ingles
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "relatorios_fixos" | "Gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeRelatoriosGestorComFeatureToggleAtivaemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Relatrios Gestor com Feature Toggle Ativa em Ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    #And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor que a tela Relat�rios esta Ativa em Ingles
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "relatorios_fixos" | "Gestor" | "web"      |  
      
      @FeatureToggle_ValidarFuncionalidadeRelatoriosGestorPortadorComFeatureToggleInativaemIngles
  		Scenario Outline: CTWR 08-31 Validar funcionalidade Relatorios Gestor Portador com Feature Toggle Inativa em Ingles
		    Given Acessar a pagina do Portal ADM <ambiente>
		    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
		    Then Acesso a opcao Gerenciar Funcionalidades
		    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
		    And Acessar a pagina do Portal PJ <ambiente> <rede>
		    And Efetuo o login em ingles <cpf> <senha>
		    #And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
		    And Verifico o login efetuado com sucesso
		    Then Verifico como Gestor Portador que a tela Relat�rios esta Inativo em Ingles
		    And Acessar a pagina do Portal ADM <ambiente>
		    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
		    And Acesso a opcao Gerenciar Funcionalidades
		    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "06218670635" | "Br@12345" | "TI"     | "empresa" | "relatorios_fixos" | "Gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeRelatoriosGestorPortadorComFeatureToggleOcultoemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Relatrios Gestor Portador com Feature Toggle Oculto em Ingles
	    Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login em ingles <cpf> <senha>
	    #And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor Portador que a tela Relat�rios esta Oculto em Ingles
	    And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "06218670635" | "Br@12345" | "TI"     | "empresa" | "relatorios_fixos" | "Gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeRelatoriosGestorPortadorComFeatureToggleAtivaemIngles
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Relatrios Gestor Portador com Feature Toggle Ativa em Ingles
	    Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login em ingles <cpf> <senha>
	    #And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor Portador que a tela Relat�rios esta Ativa em Ingles
	    And Verifico o Logout com sucesso
	    And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "06218670635" | "Br@12345" | "TI"     | "empresa" | "relatorios_fixos" | "Gestor" | "web"      | 
      
      @FeatureToggle_ValidarFuncionalidadeRelatoriosGestorEstrangeiroComFeatureToggleInativoemIngles
  		Scenario Outline: CTWR 08-31 Validar funcionalidade Relatorios Gestor Estrangeiro com Feature Toggle Inativo em Ingles
		    Given Acessar a pagina do Portal ADM <ambiente>
		    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
		    Then Acesso a opcao Gerenciar Funcionalidades
		    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
		    And Acessar a pagina do Portal PJ <ambiente> <rede>
		    And Efetuo o login em ingles <cpf> <senha>
		    #And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
		    And Verifico o login efetuado com sucesso
		    Then Verifico como Gestor Estrangeiro que a tela Relat�rios esta Inativo em Ingles
		    And Acessar a pagina do Portal ADM <ambiente>
		    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
		    And Acesso a opcao Gerenciar Funcionalidades
		    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     | "empresa" | "relatorios_fixos" | "Gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeRelatoriosGestorEstrangeiroComFeatureToggleOcultoemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Relatrios Gestor Estrangeiro com Feature Toggle Oculto em Ingles
	    Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login em ingles <cpf> <senha>
	    #And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor Estrangeiro que a tela Relat�rios esta Oculto em Ingles
	    And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     | "empresa" | "relatorios_fixos" | "Gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeRelatoriosGestorEstrangeiroComFeatureToggleAtivaemIngles
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Relatrios Gestor Estrangeiro com Feature Toggle Ativa em Ingles
	    Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login em ingles <cpf> <senha>
	    #And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor Estrangeiro que a tela Relat�rios esta Ativa em Ingles
	    And Verifico o Logout com sucesso
	    And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     | "empresa" | "relatorios_fixos" | "Gestor" | "web"      | 
      
      #Consulta de Limites Portador
  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesPortadorComFeatureToggleInativaemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites Portador com Feature Toggle Inativa em Ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Portador que a tela Consulta de Limites esta Inativo em Ingles
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade     | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesPortadorComFeatureToggleOcultoemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites Portador com Feature Toggle Oculta em Ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Portador que a tela Consulta de Limites esta Oculto em Ingles
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade     | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesPortadorComFeatureToggleAtivaemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites Portador com Feature Toggle Ativa em Ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Portador que a tela Consulta de Limites esta Ativa em Ingles
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade     | perfil     | plataforma |
      | "admin"  | "admin"  | "45291270446" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorComFeatureToggleInativaemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites Gestor com Feature Toggle Inativa em Ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor que a tela Consulta de Limites esta Inativo em Ingles
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade     | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorComFeatureToggleOcultoemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites Gestor com Feature Toggle Oculto em Ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor que a tela Consulta de Limites esta Oculto em Ingles
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade     | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorComFeatureToggleAtivaemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites Gestor com Feature Toggle Ativa em Ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor que a tela Consulta de Limites esta Ativa em Ingles
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf           | senha      | ambiente | rede      | funcionalidade     | perfil   | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Gestor" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorEstrangeiroComFeatureToggleInativoemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites como Gestor Estrangeiro com Feature Toggle Inativo em Ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor Estrangeiro que a tela Consulta de Limites esta Inativo em Ingles
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Gestor Estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorEstrangeiroComFeatureToggleOcultoemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites como Gestor Estrangeiro com Feature Toggle Oculto em Ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor Estrangeiro que a tela Consulta de Limites esta Oculto em Ingles
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Gestor Estrangeiro" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorEstrangeiroComFeatureToggleAtivaemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites como Gestor Estrangeiro com Feature Toggle Ativa em Ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor Estrangeiro que a tela Consulta de Limites esta Ativa em Ingles
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Gestor Estrangeiro" | "web"      |
      
      
  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorPortadorComFeatureToggleInativaemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites Gestor Estrangeiro com Feature Toggle Inativa em Ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
   	And Efetuo o login em ingles <cpf> <senha>
    And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor Portador que a tela Consulta de Limites esta Inativo em Ingles
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "06218670635" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Gestor Portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorPortadorComFeatureToggleOcultoemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites Gestor Portador com Feature Toggle Oculto em Ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor Portador que a tela Consulta de Limites esta Oculto em Ingles
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "06218670635" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Gestor Portador" | "web"      |

  @FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorPortadorComFeatureToggleAtivaemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites Gestor Portador com Feature Toggle Ativa em Ingles
    Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
    And Verifico o login efetuado com sucesso
    Then Verifico como Gestor Portador que a tela Consulta de Limites esta Ativa em Ingles
    And Verifico o Logout com sucesso
    And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     | perfil               | plataforma |
      | "admin"  | "admin"  | "06218670635" | "Br@12345" | "TI"     | "empresa" | "consulta_limites" | "Gestor Portador" | "web"      |    
     
   
   	@FeatureToggle_ValidarAbaProdutosemConsultadeLimitescomoGestorestaInativaemIngles
 		 Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites aba Produtos Inativa como Gestor Feature Toggle Inativa em Ingles
	    Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login em ingles <cpf> <senha>
	    And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor que a aba Produtos em Consulta de Limites esta Inativa em Ingles
	    And Verifico o Logout com sucesso
	    And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade    								 | perfil       | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consulta_limite_prod" | "Gestor" | "web"      |    
      
      @FeatureToggle_ValidarAbaPortadoresemConsultadeLimitescomoGestorestaInativaemIngles
 			 Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites aba Portador  Inativa como Gestor Feature Toggle Inativa em Ingles
		    Given Acessar a pagina do Portal ADM <ambiente>
		    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
		    Then Acesso a opcao Gerenciar Funcionalidades
		    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
		    And Acessar a pagina do Portal PJ <ambiente> <rede>
		    And Efetuo o login em ingles <cpf> <senha>
		    And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
		    And Verifico o login efetuado com sucesso
		    Then Verifico como Gestor que a aba Portadores em Consulta de Limites esta Inativa em Ingles
		    And Verifico o Logout com sucesso
		    And Acessar a pagina do Portal ADM <ambiente>
		    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
		    And Acesso a opcao Gerenciar Funcionalidades
		    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     									| perfil               | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consulta_limite_port" | "Gestor"				  | "web"      |    
		
		@FeatureToggle_ValidarAbaProdutosemConsultadeLimitescomoGestorestaOcultaemIngles
 		 Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites aba Produtos Oculta como Gestor Feature Toggle Oculta em Ingles
	    Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login em ingles <cpf> <senha>
	    And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor que a aba Produtos em Consulta de Limites esta Oculta em Ingles
	    And Verifico o Logout com sucesso
	    And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade    								 | perfil       | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consulta_limite_prod" | "Gestor" | "web"      |    
      
      @FeatureToggle_ValidarAbaPortadoresemConsultadeLimitescomoGestorestaOcultaemIngles
 			 Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites aba Portador Oculta como Gestor Feature Toggle Oculta em Ingles
		    Given Acessar a pagina do Portal ADM <ambiente>
		    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
		    Then Acesso a opcao Gerenciar Funcionalidades
		    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
		    And Acessar a pagina do Portal PJ <ambiente> <rede>
		    And Efetuo o login em ingles <cpf> <senha>
		    And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
		    And Verifico o login efetuado com sucesso
		    Then Verifico como Gestor que a aba Portadores em Consulta de Limites esta Oculta em Ingles
		    And Verifico o Logout com sucesso
		    And Acessar a pagina do Portal ADM <ambiente>
		    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
		    And Acesso a opcao Gerenciar Funcionalidades
		    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     									| perfil               | plataforma |
      | "admin"  | "admin"  | "25358287078" | "Br@12345" | "TI"     | "empresa" | "consulta_limite_port" | "Gestor"				  | "web"      |   
      
      
      @FeatureToggle_ValidarAbaProdutosemConsultadeLimitescomoGestorPortadorestaInativaemIngles
 		 Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites aba Produtos Inativa como Gestor Portador Feature Toggle Inativa em Ingles
	    Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login em ingles <cpf> <senha>
	    And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Gestor Portador que a aba Produtos em Consulta de Limites esta Inativa em Ingles
	    And Verifico o Logout com sucesso
	    And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade    								 | perfil       | plataforma |
      | "admin"  | "admin"  | "06218670635" | "Br@12345" | "TI"     | "empresa" | "consulta_limite_prod" | "Gestor" | "web"      |    
      
      @FeatureToggle_ValidarAbaPortadoresemConsultadeLimitescomoGestorPortadorestaInativaemIngles
 			 Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites aba Portador Inativa como Gestor Portador Feature Toggle Inativa em Ingles
		    Given Acessar a pagina do Portal ADM <ambiente>
		    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
		    Then Acesso a opcao Gerenciar Funcionalidades
		    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
		    And Acessar a pagina do Portal PJ <ambiente> <rede>
		    And Efetuo o login em ingles <cpf> <senha>
		    #And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
		    And Verifico o login efetuado com sucesso
		    Then Verifico como Gestor Portador que a aba Portadores em Consulta de Limites esta Inativa em Ingles
		    And Verifico o Logout com sucesso
		    And Acessar a pagina do Portal ADM <ambiente>
		    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
		    And Acesso a opcao Gerenciar Funcionalidades
		    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     									| perfil               | plataforma |
      | "admin"  | "admin"  | "06218670635" | "Br@12345" | "TI"     | "empresa" | "consulta_limite_port" | "Gestor"				  | "web"      |    
		
		@FeatureToggle_ValidarAbaProdutosemConsultadeLimitescomoGestorPortadorestaOcultaemIngles
 		 Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites aba Produtos Oculta como Gestor Portador Feature Toggle Oculta em Ingles
	    Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login em ingles <cpf> <senha>
	    And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
	    And Verifico o login efetuado com sucesso no idioma escolhido
	    Then Verifico como Gestor Portador que a aba Produtos em Consulta de Limites esta Oculta em Ingles
	    And Verifico o Logout com sucesso
	    And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade    								 | perfil       | plataforma |
      | "admin"  | "admin"  | "06218670635" | "Br@12345" | "TI"     | "empresa" | "consulta_limite_prod" | "Gestor" | "web"      |    
      
      @FeatureToggle_ValidarAbaPortadoresemConsultadeLimitescomoGestorPortadorestaOcultaemIngles
 			 Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites aba Portador Oculta como Gestor Portador Feature Toggle Oculta em Ingles
		    Given Acessar a pagina do Portal ADM <ambiente>
		    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
		    Then Acesso a opcao Gerenciar Funcionalidades
		    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
		    And Acessar a pagina do Portal PJ <ambiente> <rede>
		    And Efetuo o login em ingles <cpf> <senha>
		   # And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
		    And Verifico o login efetuado com sucesso no idioma escolhido
		    Then Verifico como Gestor Portador que a aba Portadores em Consulta de Limites esta Oculta em Ingles
		    And Verifico o Logout com sucesso
		    And Acessar a pagina do Portal ADM <ambiente>
		    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
		    And Acesso a opcao Gerenciar Funcionalidades
		    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                   | senha      | ambiente | rede      | funcionalidade     									| perfil               | plataforma |
      | "admin"  | "admin"  | "06218670635" | "Br@12345" | "TI"     | "empresa" | "consulta_limite_port" | "Gestor"				  | "web"      |    
      
      
    @FeatureToggle_ValidarAbaProdutosemConsultadeLimitescomoGestorEstrangeiroestaInativaemIngles
 		 Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites aba Produtos Inativa como Gestor Estrangeiro Feature Toggle Inativa em Ingles
	    Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login em ingles <cpf> <senha>
	    And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
	    And Verifico o login efetuado com sucesso no idioma escolhido
	    Then Verifico como Gestor Estrangeiro que a aba Produtos em Consulta de Limites esta Inativa em Ingles
	    And Verifico o Logout com sucesso
	    And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                											   | senha      | ambiente | rede      | funcionalidade    								 | perfil       | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     | "empresa" | "consulta_limite_prod" | "Gestor" | "web"      |    
      
      @FeatureToggle_ValidarAbaPortadoresemConsultadeLimitescomoGestorEstrangeiroestaInativaemIngles
 			 Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites aba Portador Inativa como Gestor Estrangeiro Feature Toggle InativaemIngles
		    Given Acessar a pagina do Portal ADM <ambiente>
		    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
		    Then Acesso a opcao Gerenciar Funcionalidades
		    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
		    And Acessar a pagina do Portal PJ <ambiente> <rede>
		    And Efetuo o login em ingles <cpf> <senha>
		    And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
		    And Verifico o login efetuado com sucesso no idioma escolhido
		    Then Verifico como Gestor Estrangeiro que a aba Portadores em Consulta de Limites esta Inativa em Ingles
		    And Verifico o Logout com sucesso
		    And Acessar a pagina do Portal ADM <ambiente>
		    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
		    And Acesso a opcao Gerenciar Funcionalidades
		    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                  										 | senha      | ambiente | rede      | funcionalidade     									| perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     | "empresa" | "consulta_limite_port" | "Gestor"				  | "web"      |    
		
		@FeatureToggle_ValidarAbaProdutosemConsultadeLimitescomoGestorEstrangeiroestaOcultaemIngles
 		 Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites aba Produtos Oculta como Gestor Estrangeiro Feature Toggle Oculta em Ingles
	    Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login em ingles <cpf> <senha>
	    And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
	    And Verifico o login efetuado com sucesso no idioma escolhido
	    Then Verifico como Gestor Estrangeiro que a aba Produtos em Consulta de Limites esta Oculta em Ingles
	    And Verifico o Logout com sucesso
	    And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                  										 | senha      | ambiente | rede      | funcionalidade    								 | perfil       | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     | "empresa" | "consulta_limite_prod" | "Gestor" | "web"      |    
      
      @FeatureToggle_ValidarAbaPortadoresemConsultadeLimitescomoGestorEstrangeiroestaOcultaemIngles
 			 Scenario Outline: CTWR 08-31 Validar funcionalidade Consulta de Limites aba Portador Oculta como Gestor Estrangeiro Feature Toggle Oculta em Ingles
		    Given Acessar a pagina do Portal ADM <ambiente>
		    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
		    Then Acesso a opcao Gerenciar Funcionalidades
		    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
		    And Acessar a pagina do Portal PJ <ambiente> <rede>
		    And Efetuo o login em ingles <cpf> <senha>
		    And Fecho o Pop-up Trazer Dados Open Finance em Ingles como Gestor
		    And Verifico o login efetuado com sucesso no idioma escolhido
		    Then Verifico como Gestor Estrangeiro que a aba Portadores em Consulta de Limites esta Oculta em Ingles
		    And Verifico o Logout com sucesso
		    And Acessar a pagina do Portal ADM <ambiente>
		    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
		    And Acesso a opcao Gerenciar Funcionalidades
		    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>

    Examples:
      | loginADM | senhaADM | cpf                  										 | senha      | ambiente | rede      | funcionalidade     									| perfil               | plataforma |
      | "admin"  | "admin"  | "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     | "empresa" | "consulta_limite_port" | "Gestor"				  | "web"      |       

  @FeatureToggle_PortadorVerificaFuncionalidadeRelatoriosComFeatureToggleAtivoemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Portador com Feature Toggle Ativa em Ingles
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    Then Verifico como Portador que a tela Relat�rios esta Ativa em Ingles
    And Verifico o Logout com sucesso
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "45291270446" | "Br@12345" | "TI"     				| "empresa"										  | "relatorios_fixos" | "Portador"			 | "web"      | 
      
      
   @FeatureToggle_PortadorVerificaFuncionalidadeRelatoriosComFeatureToggleInativoemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Portador com Feature Toggle Inativo em Ingles
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Portador que a tela Relat�rios esta Inativo
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "45291270446" | "Br@12345" | "TI"     				| "empresa"										  | "relatorios_fixos" | "Portador"			 | "web"      | 
      
  @FeatureToggle_PortadorVerificaFuncionalidadeRelatoriosComFeatureToggleOcultoemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Portador com Feature Toggle Oculto em Ingles
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso
    Then Verifico como Portador que a tela Relat�rios esta Oculto
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "45291270446" | "Br@12345" | "TI"     				| "empresa"										  | "relatorios_fixos" | "Portador"			 | "web"      | 
       
   @FeatureToggle_PortadorVerificaFuncionalidadeBloqueioComFeatureToggleAtivo
  Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Portador com Feature Toggle Ativo
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade Bloqueio <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
    And Verifico o login efetuado com sucesso no idioma escolhido
    Then Verifico como Portador que a tela de Bloqueio esta Ativo
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "45291270446" | "Br@12345" | "TI"     				| "empresa"										  | "bloqueio" | "Portador"			 | "web"      | 
       
    @FeatureToggle_PortadorVerificaFuncionalidadeBloqueioComFeatureToggleInativo
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Portador com Feature Toggle Inativo
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade Bloqueio <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login em ingles <cpf> <senha>
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Portador que a tela de Bloqueio esta Inativo
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "45291270446" | "Br@12345" | "TI"     				| "empresa"										  | "bloqueio"						 | "Portador"			 | "web"      |     
       
   @FeatureToggle_PortadorVerificaFuncionalidadeBloqueioComFeatureToggleOculto
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Portador com Feature Toggle Oculto
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Ocultar funcionalidade Bloqueio <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login em ingles <cpf> <senha>
	    And Verifico o login efetuado com sucesso
	    Then Verifico como Portador que a tela de Bloqueio esta Oculto
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "45291270446" | "Br@12345" | "TI"     				| "empresa"										  | "bloqueio" | "Portador"			 | "web"      |     
       
   @FeatureToggle_GestorVerificaFuncionalidadeBloqueioComFeatureToggleAtivoemIngles
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Gestor com Feature Toggle Ativo em Ingles
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade Bloqueio <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login em ingles <cpf> <senha>
	    And Verifico o login efetuado com sucesso no idioma escolhido
	    Then Verifico como Gestor que a tela de Bloqueio esta Ativo em Ingles
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "25358287078" | "Br@12345" | "TI"     				| "empresa"										  | "bloqueio" | "Gestor"			 | "web"      | 
       
    @FeatureToggle_GestorVerificaFuncionalidadeBloqueioComFeatureToggleInativoemIngles
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Gestor com Feature Toggle Inativo em Ingles
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade Bloqueio <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login em ingles <cpf> <senha>
	    And Verifico o login efetuado com sucesso no idioma escolhido
	    Then Verifico como Gestor que a tela de Bloqueio esta Inativo em Ingles
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "25358287078" | "Br@12345" | "TI"     				| "empresa"										  | "bloqueio"						 | "Gestor"			 | "web"      |     
       
   @FeatureToggle_GestorVerificaFuncionalidadeBloqueioComFeatureToggleOcultoemIngles
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Gestor com Feature Toggle Oculto em Ingles
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Ocultar funcionalidade Bloqueio <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login em ingles <cpf> <senha>
	    And Verifico o login efetuado com sucesso no idioma escolhido
	    Then Verifico como Gestor que a tela de Bloqueio esta Oculto em Ingles
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "25358287078" | "Br@12345" | "TI"     				| "empresa"										  | "bloqueio" | "Gestor"			 | "web"      |     
       
       
    @FeatureToggle_GestorPortadorVerificaFuncionalidadeBloqueioComFeatureToggleAtivoemIngles
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Gestor Portador com Feature Toggle Ativo em Ingles
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade Bloqueio <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login em ingles <cpf> <senha>
	    And Verifico o login efetuado com sucesso no idioma escolhido
	    Then Verifico como Gestor que a tela de Bloqueio esta Ativo em Ingles
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "06218670635" | "Br@12345" | "TI"     				| "empresa"										  | "bloqueio" | "Gestor"			 | "web"      | 
       
    @FeatureToggle_GestorPortadorVerificaFuncionalidadeBloqueioComFeatureToggleInativoemIngles
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Gestor Portador com Feature Toggle Inativo em Ingles
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade Bloqueio <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login em ingles <cpf> <senha>
	    And Verifico o login efetuado com sucesso no idioma escolhido
	    Then Verifico como Gestor que a tela de Bloqueio esta Inativo em Ingles
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "m231510"    | "Bra@dio1"  		| "06218670635" | "Br@12345" | "TH"     				| "empresa"										  | "bloqueio"						 | "Gestor"			 | "web"      |     
       
   @FeatureToggle_GestorPortadorVerificaFuncionalidadeBloqueioComFeatureToggleOcultoemIngles
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Gestor Portador com Feature Toggle Oculto em Ingles
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Ocultar funcionalidade Bloqueio <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login em ingles <cpf> <senha>
	    And Verifico o login efetuado com sucesso no idioma escolhido
	    Then Verifico como Gestor que a tela de Bloqueio esta Oculto em Ingles
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "06218670635" | "Br@12345" | "TI"     				| "empresa"										  | "bloqueio" | "Gestor"			 | "web"      |     
       
       
    @FeatureToggle_GestorEstrangeiroVerificaFuncionalidadeBloqueioComFeatureToggleAtivoemIngles
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Gestor Estrangeiro com Feature Toggle Ativo em Ingles
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade Bloqueio <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login em ingles <cpf> <senha>
	    And Verifico o login efetuado com sucesso no idioma escolhido
	    Then Verifico como Gestor que a tela de Bloqueio esta Ativo em Ingles
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     				| "empresa"										  | "bloqueio" | "Gestor"			 | "web"      | 
       
    @FeatureToggle_GestorEstrangeiroVerificaFuncionalidadeBloqueioComFeatureToggleInativoemIngles
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Gestor Estrangeiro com Feature Toggle Inativo em Ingles
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade Bloqueio <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login em ingles <cpf> <senha>
	    And Verifico o login efetuado com sucesso no idioma escolhido
	    Then Verifico como Gestor que a tela de Bloqueio esta Inativo em Ingles
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     				| "empresa"										  | "bloqueio"						 | "Gestor"			 | "web"      |     
       
   @FeatureToggle_GestorEstrangeiroVerificaFuncionalidadeBloqueioComFeatureToggleOcultoemIngles
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Relat�rios Gestor Estrangeiro com Feature Toggle Oculto em Ingles
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Ocultar funcionalidade Bloqueio <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login em ingles <cpf> <senha>
	    And Verifico o login efetuado com sucesso no idioma escolhido
	    Then Verifico como Gestor que a tela de Bloqueio esta Oculto em Ingles
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     													| "empresa"										  | "bloqueio" | "Gestor"			 | "web"      |  
       
       @FeatureToggle_PortadorVerificaFuncionalidadeServicosComFeatureToggleAtivaemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Servicos como Portador com Feature Toggle Ativa em Ingles
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
	  And Verifico o login efetuado com sucesso no idioma escolhido
    Then Verifico como Portador que a aba Servicos esta Ativa
    And Verifico o Logout com sucesso
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "45291270446" | "Br@12345" | "TI"     				| "empresa"										  | "servicos" | "Portador"			 | "web"      | 
      
      
   @FeatureToggle_PortadorVerificaFuncionalidadeServicosComFeatureToggleInativoemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Servi�os como Portador com Feature Toggle Inativo em Ingles
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
	  And Verifico o login efetuado com sucesso no idioma escolhido
    Then Verifico como Portador que a aba Servicos esta Inativo
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "45291270446" | "Br@12345" | "TI"     				| "empresa"										  | "servicos" | "Portador"			 | "web"      | 
      
  @FeatureToggle_PortadorVerificaFuncionalidadeServicosComFeatureToggleOcultoemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Servi�os como Portador com Feature Toggle Oculto em Ingles
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
   And Efetuo o login em ingles <cpf> <senha>
	 And Verifico o login efetuado com sucesso no idioma escolhido
    Then Verifico como Portador que a tela Servi�os esta Oculto
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "45291270446" | "Br@12345" | "TI"     				| "empresa"										  | "servicos" | "Portador"			 | "web"      |  
       
    
    @FeatureToggle_GestorVerificaFuncionalidadeServicosComFeatureToggleAtivaemIngles
 	 Scenario Outline: CTWR 08-31 Validar funcionalidade Servicos como Gestor com Feature Toggle Ativa em Ingles
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
	  And Verifico o login efetuado com sucesso no idioma escolhido
    Then Verifico como Gestor que a aba Servicos esta Ativa
    And Verifico o Logout com sucesso
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "25358287078" | "Br@12345" | "TI"     				| "empresa"										  | "servicos" | "Gestor"			 | "web"      | 
      
      
   @FeatureToggle_GestorVerificaFuncionalidadeServicosComFeatureToggleInativoemIngles
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Servi�os como Gestor com Feature Toggle Inativo em Ingles
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login em ingles <cpf> <senha>
	    And Verifico o login efetuado com sucesso no idioma escolhido
	    Then Verifico como Gestor que a aba de Servicos esta Inativo
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "25358287078" | "Br@12345" | "TI"     				| "empresa"										  | "servicos" | "Gestor"			 | "web"      | 
      
  @FeatureToggle_GestorVerificaFuncionalidadeServicosComFeatureToggleOcultoemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Servi�os como Gestor com Feature Toggle Oculto em Ingles
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
	  And Verifico o login efetuado com sucesso no idioma escolhido
    Then Verifico como Gestor que a aba de Servicos esta Oculto
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "25358287078" | "Br@12345" | "TI"     				| "empresa"										  | "servicos" | "Gestor"			 | "web"      | 
       
  @FeatureToggle_GestorPortadorVerificaFuncionalidadeServicosComFeatureToggleAtivaemIngles
 	 Scenario Outline: CTWR 08-31 Validar funcionalidade Servicos como Gestor Portador com Feature Toggle Ativa em Ingles
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
	  And Verifico o login efetuado com sucesso no idioma escolhido
    Then Verifico como Gestor que a aba de Servicos esta Ativa
    And Verifico o Logout com sucesso
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "06218670635" | "Br@12345" | "TI"     				| "empresa"										  | "servicos" | "Gestor"			 | "web"      | 
      
      
   @FeatureToggle_GestorPortadorVerificaFuncionalidadeServicosComFeatureToggleInativoemIngles
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Servi�os como Gestor Portador com Feature Toggle Inativo em Ingles
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login em ingles <cpf> <senha>
	    And Verifico o login efetuado com sucesso no idioma escolhido
	    Then Verifico como Gestor que a aba de Servicos esta Inativo
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "06218670635" | "Br@12345" | "TI"     				| "empresa"										  | "servicos" | "Gestor"			 | "web"      | 
      
  @FeatureToggle_GestorPortadorVerificaFuncionalidadeServicosComFeatureToggleOcultoemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Servi�os como Gestor Portador com Feature Toggle Oculto em Ingles
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
   	And Efetuo o login em ingles <cpf> <senha>
	  And Verifico o login efetuado com sucesso no idioma escolhido
    Then Verifico como Gestor que a aba de Servicos esta Oculto
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "06218670635" | "Br@12345" | "TI"     				| "empresa"										  | "servicos" | "Gestor"			 | "web"      |   
       
  @FeatureToggle_GestorEstrangeiroVerificaFuncionalidadeServicosComFeatureToggleAtivaemIngles
 	 Scenario Outline: CTWR 08-31 Validar funcionalidade Servicos como Gestor Estrangeiro com Feature Toggle Ativa em Ingles
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
	  And Verifico o login efetuado com sucesso no idioma escolhido
    Then Verifico como Gestor que a aba de Servicos esta Ativa
    And Verifico o Logout com sucesso
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     				| "empresa"										  | "servicos"							 | "Gestor Estrangeiro"			 | "web"      | 
      
      
   @FeatureToggle_GestorEstrangeiroVerificaFuncionalidadeServicosComFeatureToggleInativoemIngles
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Servi�os como Gestor Estrangeiro com Feature Toggle Inativo em Ingles
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login em ingles <cpf> <senha>
	    And Verifico o login efetuado com sucesso no idioma escolhido
	    Then Verifico como Gestor que a aba de Servicos esta Inativo
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     				| "empresa"										  | "servicos" | "Gestor Estrangeiro"			 | "web"      | 
      
  @FeatureToggle_GestorEstrangeiroPortadorVerificaFuncionalidadeServicosComFeatureToggleOcultoemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Servi�os como Gestor Portador com Feature Toggle Oculto Estrangeiro em Ingles
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
   And Efetuo o login em ingles <cpf> <senha>
	   And Verifico o login efetuado com sucesso no idioma escolhido
    Then Verifico como Gestor que a aba de Servicos esta Oculto
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     				| "empresa"										  | "servicos"							 | "Gestor Estrangeiro"			 | "web"      |    
       
       
   @FeatureToggle_PortadorVerificaFuncionalidadeFormadePagamentoComFeatureToggleAtivaemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Forma de Pagamento como Portador com Feature Toggle Ativa em Ingles
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
	  And Verifico o login efetuado com sucesso no idioma escolhido
    Then Verifico como Portador que a funcionalidade Forma de Pagamento esta Ativa em Ingles
    And Verifico o Logout com sucesso
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "45291270446" | "Br@12345" | "TI"     				| "empresa"										  | "consulta_forma_pgto" | "Portador"			 | "web"      | 
      
      
   @FeatureToggle_PortadorVerificaFuncionalidadeFormadePagamentoComFeatureToggleInativoemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Forma de Pagamento como Portador com Feature Toggle InativoemIngles
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
	  And Verifico o login efetuado com sucesso no idioma escolhido em Ingles
    Then Verifico como Portador que a funcionalidade Forma de Pagamento esta Inativo
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "45291270446" | "Br@12345" | "TI"     				| "empresa"										  | "consulta_forma_pgto" | "Portador"			 | "web"      | 
      
  @FeatureToggle_PortadorVerificaFuncionalidadeFormadePagamentoComFeatureToggleOcultoemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Forma de Pagamento como Portador com Feature Toggle Oculto em Ingles
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
   And Efetuo o login em ingles <cpf> <senha>
	 And Verifico o login efetuado com sucesso no idioma escolhido
	  Then Verifico como Gestor que a funcionalidade Forma de Pagamento esta Oculto em Ingles
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "45291270446" | "Br@12345" | "TI"     				| "empresa"										  | "consulta_forma_pgto" | "Portador"			 | "web"      |  
       
    
    @FeatureToggle_GestorVerificaFuncionalidadeFormadePagamentoComFeatureToggleAtivaemIngles
 	 Scenario Outline: CTWR 08-31 Validar funcionalidade Forma de Pagamento como Gestor com Feature Toggle Ativa em Ingles
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
	  And Verifico o login efetuado com sucesso no idioma escolhido
    Then Verifico como Gestor que a funcionalidade Forma de Pagamento esta Ativa em Ingles
    And Verifico o Logout com sucesso
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "25358287078" | "Br@12345" | "TI"     				| "empresa"										  | "consulta_forma_pgto" | "Gestor"			 | "web"      | 
      
      
   @FeatureToggle_GestorVerificaFuncionalidadeFormadePagamentoComFeatureToggleInativoemIngles
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Forma de Pagamento como Gestor com Feature Toggle Inativo em Ingles
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login em ingles <cpf> <senha>
	    And Verifico o login efetuado com sucesso no idioma escolhido
	    Then Verifico como Gestor que a funcionalidade Forma de Pagamento esta Inativo em Ingles
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "25358287078" | "Br@12345" | "TI"     				| "empresa"										  | "consulta_forma_pgto" | "Gestor"			 | "web"      | 
      
  @FeatureToggle_GestorVerificaFuncionalidadeFormadePagamentoComFeatureToggleOcultoemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Forma de Pagamento como Gestor com Feature Toggle Oculto em Ingles
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
   	And Efetuo o login em ingles <cpf> <senha>
	  And Verifico o login efetuado com sucesso no idioma escolhido
    Then Verifico como Gestor que a funcionalidade Forma de Pagamento esta Oculto em Ingles
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "25358287078" | "Br@12345" | "TI"     				| "empresa"										  | "consulta_forma_pgto" | "Gestor"			 | "web"      | 
       
  @FeatureToggle_GestorPortadorVerificaFuncionalidadeFormadePagamentoComFeatureToggleAtivaemIngles
 	 Scenario Outline: CTWR 08-31 Validar funcionalidade Forma de Pagamento como Gestor Portador com Feature Toggle Ativa em Ingles
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
	  And Verifico o login efetuado com sucesso no idioma escolhido
    Then Verifico como Gestor que a funcionalidade Forma de Pagamento esta Ativa em Ingles
    And Verifico o Logout com sucesso
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "06218670635" | "Br@12345" | "TI"     				| "empresa"										  | "consulta_forma_pgto" | "Gestor"			 | "web"      | 
      
      
   @FeatureToggle_GestorPortadorVerificaFuncionalidadeFormadePagamentoComFeatureToggleInativoemIngles
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Forma de Pagamento como Gestor Portador com Feature Toggle Inativo em Ingles
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login em ingles <cpf> <senha>
	    And Verifico o login efetuado com sucesso no idioma escolhido
	    Then Verifico como Gestor que a funcionalidade Forma de Pagamento esta Inativo em Ingles
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "06218670635" | "Br@12345" | "TI"     				| "empresa"										  | "consulta_forma_pgto" | "Gestor"			 | "web"      | 
      
  @FeatureToggle_GestorPortadorVerificaFuncionalidadeFormadePagamentoComFeatureToggleOcultoemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Forma de Pagamento como Gestor Portador com Feature Toggle Oculto em Ingles
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
 	 	And Efetuo o login em ingles <cpf> <senha>
	  And Verifico o login efetuado com sucesso no idioma escolhido
    Then Verifico como Gestor que a funcionalidade Forma de Pagamento esta Oculto em Ingles
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "06218670635" | "Br@12345" | "TI"     				| "empresa"										  | "consulta_forma_pgto" | "Gestor"			 | "web"      |   
       
  @FeatureToggle_GestorEstrangeiroVerificaFuncionalidadeFormadePagamentoComFeatureToggleAtivaemIngles
 	 Scenario Outline: CTWR 08-31 Validar funcionalidade Forma de Pagamento como Gestor Estrangeiro com Feature Toggle Ativa em Ingles
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
    And Efetuo o login em ingles <cpf> <senha>
	  And Verifico o login efetuado com sucesso no idioma escolhido
    Then Verifico como Gestor que a funcionalidade Forma de Pagamento esta Ativa em Ingles
    And Verifico o Logout com sucesso
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente													 | rede     														| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     				| "empresa"										  | "consulta_forma_pgto"							 | "Gestor Estrangeiro"			 | "web"      | 
      
      
   @FeatureToggle_GestorEstrangeiroVerificaFuncionalidadeFormadePagamentoComFeatureToggleInativoemIngles
  	Scenario Outline: CTWR 08-31 Validar funcionalidade Forma de Pagamento como Gestor Estrangeiro com Feature Toggle Inativo em Ingles
	   	Given Acessar a pagina do Portal ADM <ambiente>
	    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    Then Acesso a opcao Gerenciar Funcionalidades
	    And Inativar funcionalidade <funcionalidade> <perfil> <plataforma>
	    And Acessar a pagina do Portal PJ <ambiente> <rede>
	    And Efetuo o login em ingles <cpf> <senha>
	    And Verifico o login efetuado com sucesso no idioma escolhido
	    Then Verifico como Gestor que a funcionalidade Forma de Pagamento esta Inativo em Ingles
			And Acessar a pagina do Portal ADM <ambiente>
	    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
	    And Acesso a opcao Gerenciar Funcionalidades
	    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      				| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     				| "empresa"										  | "consulta_forma_pgto" | "Gestor Estrangeiro"			 | "web"      | 
      
  @FeatureToggle_GestorEstrangeiroPortadorVerificaFuncionalidadeFormadePagamentoComFeatureToggleOcultoemIngles
  Scenario Outline: CTWR 08-31 Validar funcionalidade Forma de Pagamento como Gestor Portador com Feature Toggle Oculto Estrangeiro em Ingles
   	Given Acessar a pagina do Portal ADM <ambiente>
    When Efetuo o login no Portal ADM <loginADM> <senhaADM>
    Then Acesso a opcao Gerenciar Funcionalidades
    And Ocultar funcionalidade <funcionalidade> <perfil> <plataforma>
    And Acessar a pagina do Portal PJ <ambiente> <rede>
   	And Efetuo o login em ingles <cpf> <senha>
	  And Verifico o login efetuado com sucesso no idioma escolhido
    Then Verifico como Gestor que a funcionalidade Forma de Pagamento esta Oculto em Ingles
		And Acessar a pagina do Portal ADM <ambiente>
    And Efetuo o login no Portal ADM <loginADM> <senhaADM>
    And Acesso a opcao Gerenciar Funcionalidades
    And Ativar funcionalidade <funcionalidade> <perfil> <plataforma>
    
    Examples:
       | loginADM | senhaADM | cpf                   | senha     				 | ambiente | rede      													| funcionalidade    | perfil               | plataforma |
       | "admin"    | "admin"  		| "TESTEQA70@GMAIL.COM" | "Br@12345" | "TI"     				| "consulta_forma_pgto"										  | "servicos"							 | "Gestor Estrangeiro"			 | "web"      |     