package br.com.bradesco.pupj.runners;

import br.com.bradesco.automacaocartoes.core.ExtendedCucumberRunner;
import br.com.bradesco.pupj.utilitarios.SetUpTearDown;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberOptions.SnippetType;
import org.junit.runner.RunWith;

/*===========================================================================/*
/* RUNNER UTILIZADO PARA EXECU��O LOCAL NO ECLIPSE,
/* CADA FUNCIONALIDADE TEM O SEU RESPECTIVO RUNNER
/*  					* LOGIN PORTAL ADM *
/*===========================================================================*/
@RunWith(ExtendedCucumberRunner.class)
@CucumberOptions(

        plugin = {"pretty",
                "html:target/cucumber-report",
                "junit:target/cucumber-report/junitResult.xml",
                "json:target/cucumber-report/jsonResult.json"},
        snippets = SnippetType.CAMELCASE,
        monochrome = true,
        stepNotifications = true,
        useFileNameCompatibleName = true,
        strict = true,
        features = {"src/test/resources/features/EN/016_FeatureToggle_EN.feature",
                "src/test/resources/features/PT/016_FeatureToggle_PT.feature"},
        glue = {"br.com.bradesco.pupj.steps", "br.com.bradesco.pupj.utilitarios"},


       // tags = {"@FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleAtivado"}
        // tags = {"@FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleAtivadoIngles"}
        // tags = {"@FeatureToggle_ValidarFuncionalidadeFaturasComFeatureToggleAtivadoNacional"}
        // tags = {"@FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleAtivado"}
        // tags = {"@FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleAtivadoIngles"}
        // tags = {"@FeatureToggle_ValidarFuncionalidadeConsultadeLimitesPortadorComFeatureToggleInativa"}
        // tags = {"@FeatureToggle_ValidarFuncionalidadeConsultadeLimitesPortadorComFeatureToggleOculto"}
        // tags = {"@FeatureToggle_ValidarFuncionalidadeConsultadeLimitesPortadorComFeatureToggleAtiva"}
        // tags = {"@FeatureToggle_ValidarFuncionalidadeConsultadeLimitesPortadorComFeatureToggleInativaemIngles"}
        // tags = {"@FeatureToggle_ValidarFuncionalidadeConsultadeLimitesPortadorComFeatureToggleOcultoemIngles"}
        // tags = {"@FeatureToggle_ValidarFuncionalidadeConsultadeLimitesPortadorComFeatureToggleAtivaemIngles"}
        // tags = {"@FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorComFeatureToggleAtiva"}
        // tags = {"@FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorComFeatureToggleInativa"}
        // tags=  {"@FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorComFeatureToggleOculto"}
        // tags = {"@FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorComFeatureToggleAtivaemIngles"}
        // tags = {"@FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorComFeatureToggleInativaemIngles"}
        // tags = {"@FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorComFeatureToggleOcultoemIngles"}
        // tags = {"@FeatureToggle_ValidarFuncionalidadeAvisoViagemComFeatureToggleAtivadoIngles"}
        // tags = {"@FeatureToggle_ValidarFuncionalidadeConsultadeLimitesPortadorComFeatureToggleInativaemIngles"}
        // tags = {"@FeatureToggle_ValidarFuncionalidadeConsultadeLimitesPortadorComFeatureToggleOcultoemIngles"}
        // tags = {"@FeatureToggle_ValidarFuncionalidadeConsultadeLimitesPortadorComFeatureToggleAtivaemIngles"}
        // tags = {"@FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorComFeatureToggleAtivaemIngles"}
        // tags = {"@FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorComFeatureToggleInativaemIngles"}
        // tags = {"@FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorComFeatureToggleOcultoemIngles"}
        // tags = {"@FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorEstrangeiroComFeatureToggleAtivaemIngles"}
        // tags = {"@FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorEstrangeiroComFeatureToggleInativaemIngles"}
        // tags = {"@FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorEstrangeiroComFeatureToggleOcultoemIngles"}  
        // tags = {"@FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorEstrangeiroComFeatureToggleAtiva"}
        // tags = {"@FeatureToggle_ValidarFuncionalidadeConsultadeLimitesGestorEstrangeiroComFeatureToggleInativa"}
		// tags={"@FeatureToggle_ValidarFuncionalidadeRelatoriosGestorComFeatureToggleInativaemIngles"}
		//tags={"@FeatureToggle_ValidarFuncionalidadeRelatoriosGestorComFeatureToggleOcultoemIngles"}
		//tags={"@FeatureToggle_ValidarFuncionalidadeRelatoriosGestorComFeatureToggleAtivaemIngles"}
		//tags={"@FeatureToggle_ValidarFuncionalidadeRelatoriosGestorComFeatureToggleAtiva"}
		//tags={"@FeatureToggle_ValidarFuncionalidadeRelatoriosGestorComFeatureToggleInativo"}
		//tags={"@FeatureToggle_ValidarFuncionalidadeRelatoriosGestorComFeatureToggleOculto"}
		 // tags={"@FeatureToggle_ValidarFuncionalidadeRelatoriosGestorPortadorComFeatureToggleInativa"}
		 //tags={"@FeatureToggle_ValidarFuncionalidadeRelatoriosGestorPortadorComFeatureToggleOculto"}
		//tags={"@FeatureToggle_ValidarFuncionalidadeRelatoriosGestorPortadorComFeatureToggleAtiva"}
		//tags={"@FeatureToggle_ValidarFuncionalidadeRelatoriosGestorEstrangeiroComFeatureToggleAtiva"}
		 //tags={"@FeatureToggle_ValidarFuncionalidadeRelatoriosGestorEstrangeiroComFeatureToggleInativo"}
		 //tags={"@FeatureToggle_ValidarFuncionalidadeRelatoriosGestorEstrangeiroComFeatureToggleOculto"} 
		//tags={"@FeatureToggle_ValidarFuncionalidadeRelatoriosGestorPortadorComFeatureToggleInativaemIngles"}
		 //tags={"@FeatureToggle_ValidarFuncionalidadeRelatoriosGestorPortadorComFeatureToggleOcultoemIngles"}
		//tags={"@FeatureToggle_ValidarFuncionalidadeRelatoriosGestorPortadorComFeatureToggleAtivaemIngles"}
		//tags={"@FeatureToggle_ValidarFuncionalidadeRelatoriosGestorEstrangeiroComFeatureToggleAtivaemIngles"}
		 //tags={"@FeatureToggle_ValidarFuncionalidadeRelatoriosGestorEstrangeiroComFeatureToggleInativoemIngles"}
		 //tags={"@FeatureToggle_ValidarFuncionalidadeRelatoriosGestorEstrangeiroComFeatureToggleOcultoemIngles"}
		// tags = {"@FeatureToggle_ValidarBotaoSolicitacoesGestorComFeatureToggleAtivadoNacionalIngles"}
        //tags = {"@ConsultarLimite_PortadorVerificaFuncionalidadeRelatoriosComFeatureToggleAtiva"}
      //tags = {"@FeatureToggle_PortadorVerificaFuncionalidadeRelatoriosComFeatureToggleInativo"}
        //tags = {"@FeatureToggle_PortadorVerificaFuncionalidadeRelatoriosComFeatureToggleOculto"}
       // tags = {"@FeatureToggle_PortadorVerificaFuncionalidadeRelatoriosComFeatureToggleAtivoemIngles"}
        // tags = {"@FeatureToggle_PortadorVerificaFuncionalidadeRelatoriosComFeatureToggleInativoemIngles"}
       //' tags = {"@FeatureToggle_PortadorVerificaFuncionalidadeRelatoriosComFeatureToggleOcultoemIngles"}
       // tags = {" @FeatureToggle_PortadorVerificaFuncionalidadeBloqueioComFeatureToggleAtivo"}
      // tags = {" @FeatureToggle_PortadorVerificaFuncionalidadeBloqueioComFeatureToggleInativo"}
      // tags = {" @FeatureToggle_PortadorVerificaFuncionalidadeBloqueioComFeatureToggleOculto"}
        //tags = {" @FeatureToggle_GestorVerificaFuncionalidadeBloqueioComFeatureToggleAtivoemIngles"}
        //tags = {" @FeatureToggle_GestorVerificaFuncionalidadeBloqueioComFeatureToggleInativoemIngles"}
        //tags = {" @FeatureToggle_GestorVerificaFuncionalidadeBloqueioComFeatureToggleOcultoemIngles"}
        //tags = {" @FeatureToggle_GestorPortadorVerificaFuncionalidadeBloqueioComFeatureToggleAtivoemIngles"}
       // tags = {" @FeatureToggle_GestorPortadorVerificaFuncionalidadeBloqueioComFeatureToggleInativoemIngles"}
        //tags = {" @FeatureToggle_GestorPortadorVerificaFuncionalidadeBloqueioComFeatureToggleOcultoemIngles"}
        //tags = {" @FeatureToggle_GestorEstrangeiroVerificaFuncionalidadeBloqueioComFeatureToggleAtivoemIngles"}
        //tags = {" @FeatureToggle_GestorEstrangeiroVerificaFuncionalidadeBloqueioComFeatureToggleInativoemIngles"}
        //tags = {" @FeatureToggle_GestorEstrangeiroVerificaFuncionalidadeBloqueioComFeatureToggleInativoemIngles"}
        //tags ={"@FeatureToggle_GestorEstrangeiroVerificaFuncionalidadeServicosComFeatureToggleInativo"}
        //tags ={" @FeatureToggle_PortadorVerificaFuncionalidadeServicosComFeatureToggleInativo"}
       //tags = {"@FeatureToggle_GestorVerificaFuncionalidadeServicosComFeatureToggleInativo"}
       // tags = {"@FeatureToggle_GestorVerificaFuncionalidadeServicosComFeatureToggleOculto"}
       // tags = {"@FeatureToggle_GestorVerificaFuncionalidadeServicosComFeatureToggleOcultoemIngles"}
      // tags = {"@FeatureToggle_GestorVerificaFuncionalidadeFormadePagamentoComFeatureToggleAtiva"}
      // tags = {"@FeatureToggle_GestorVerificaFuncionalidadeFormadePagamentoComFeatureToggleInativo"}
        tags = {"@FeatureToggle_GestorVerificaFuncionalidadeFormadePagamentoComFeatureToggleInativoemIngles"}
        		
)

public class FeatureToggleTest extends SetUpTearDown {

}