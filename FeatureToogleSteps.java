package br.com.bradesco.pupj.steps;

import br.com.bradesco.automacaocartoes.core.DriverFactory;
import br.com.bradesco.pupj.pagesweb.*;
import br.com.bradesco.pupj.utilitarios.UtilsWeb;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class FeatureToogleSteps extends UtilsWeb {

    public static WebDriver driverWeb = DriverFactory.getDriverWeb();
    bloqueioDesbloqueioCancelamento_Servicos servicos = new bloqueioDesbloqueioCancelamento_Servicos(driverWeb);
    CabecalhoRodape_CabecalhoPage cabecalhoLogado = new CabecalhoRodape_CabecalhoPage(driverWeb);
    FeatureToogle_Page Feature_Toogle = new FeatureToogle_Page(driverWeb);
    FormaDePagamento_Portador_FormaDePagamentoPage formadepagamentoPortador = new FormaDePagamento_Portador_FormaDePagamentoPage(driverWeb);
	FormaDePagamento_Gestor_FormaDePagamentoPage formadepagamentoGestor = new FormaDePagamento_Gestor_FormaDePagamentoPage(driverWeb);
    Limites_ConsultaDeLimite_PortadoresPage pageLimitePortador = new Limites_ConsultaDeLimite_PortadoresPage(driverWeb);
    Login_LoginPage login = new Login_LoginPage(driverWeb);
    Limites_ConsultaDeLimite_DetalhesPages limiteGestor = new Limites_ConsultaDeLimite_DetalhesPages(driverWeb);

	Actions mouseOver = new Actions(driverWeb);


    @Given("Acessar a pagina do Portal ADM {string}")
    public void acessarAPaginaDoPortalPJ(String ambiente) throws InterruptedException {
        switch (ambiente) {
            case "TU":
                abrirUrl("https://192.168.245.197:9443/pupj-portal-adm/login.xhtml");
                //System.out.println("Ambiente informado incorretamente");
                System.out.println("Portal administrativo de TU");
                break;
            case "TI":
                deletarCookies();
                abrirUrl("https://192.168.245.198/pupj-portal-adm/login.jsp");
                System.out.println("Portal administrativo de TI");
                break;
            case "TH":
                abrirUrl("https://192.168.254.12/pupj-portal-adm/login.jsp");
                //System.out.println("Ambiente informado incorretamente");
                break;
            default:
                System.out.println("Ambiente informado incorretamente, valores v�lidos TH, TI, TU.");
        }

        maximizarPaginaWeb();
        espera(4);
        if (!elementoExiste(Feature_Toogle.txtLogin)) {
            tirarScreenshot("Erro Certificado SSL");
            if (elementoExiste(login.btnAvancadoChrome)) {
                login.clicarAvancadoChrome();
                espera(1);
                tirarScreenshot("Erro Certificado SSL - Concordando com o Risco");
                login.clicarLinkCertificadoOkChrome();
                espera(3);
            }
            if (elementoExiste(login.btnMaisInformacoesIe)) {
                espera(1);
                login.clicarMaisInformacoesIe();
                espera(1);
                tirarScreenshot("Erro Certificado SSL - Concordando com o Risco");
                login.clicarContinuarIe();
                espera(3);
            }
        }
        tirarScreenshot("Tela inicial de Login Portal ADM");
    }

    @When("Efetuo o login no Portal ADM {string} {string}")
    public void efetuoOLogin(String login, String senha) {
        Feature_Toogle.informarLogin(login);
        Feature_Toogle.informarSenha(senha);
        esperaElemento(Feature_Toogle.btnAcessar, 50);
        tirarScreenshot("Realizando o Login");
        Feature_Toogle.focarBotaoAcessar();
        Feature_Toogle.clicarAcessar();
        scrollToTop();
    }

    @Then("Acesso a opcao Gerenciar Funcionalidades")
    public void acessoAOpcaoGerenciarFuncionalidades() {
        esperaElemento(Feature_Toogle.menuPlataformaPJ, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.menuPlataformaPJ));
        Feature_Toogle.clicarMenuPlataformaPJ();
        esperaElemento(Feature_Toogle.menuGerenciarFuncionalidades, 80);
        Assert.assertTrue(elementoExiste(Feature_Toogle.menuGerenciarFuncionalidades));
        Feature_Toogle.clicarMenuGerenciarFuncionalidades();
        scrollToTop();
        tirarScreenshot("Tela Gerenciar Funcionalidades");
    }

    @And("Ocultar funcionalidade {string} {string} {string}")
    public void ocultarFuncionalidade(String funcionalidade, String perfil, String plataforma) throws InterruptedException {
        buscarFuncionalidade(funcionalidade, perfil, plataforma);

        esperaElemento(Feature_Toogle.cbStatusManutencao, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.cbStatusManutencao));
        Feature_Toogle.clicarCbStatusManutencao();
        esperaElemento(Feature_Toogle.menuStatus, 80);
        Assert.assertTrue(elementoExiste(Feature_Toogle.menuStatus));
        Feature_Toogle.clicarOptOcultarMenuCbStatus();
        tirarScreenshot("Ocultando Funcionalidade");
        esperaElemento(Feature_Toogle.btnSalvar, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.btnSalvar));
        Feature_Toogle.clicarBtnSalvar();
        esperaElemento(Feature_Toogle.frmMensagens, 80);
        Assert.assertTrue(elementoExiste(Feature_Toogle.frmMensagens));
        tirarScreenshot("Funcionalidade Ocultada");
    }

    @And("Ativar funcionalidade {string} {string} {string}")
    public void ativarFuncionalidade(String funcionalidade, String perfil, String plataforma) throws InterruptedException {
        buscarFuncionalidade(funcionalidade, perfil, plataforma);

        esperaElemento(Feature_Toogle.cbStatusManutencao, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.cbStatusManutencao));
        Feature_Toogle.clicarCbStatusManutencao();
        esperaElemento(Feature_Toogle.menuStatus, 80);
        Assert.assertTrue(elementoExiste(Feature_Toogle.menuStatus));
        Feature_Toogle.clicarOptAtivoCbStatus();
        tirarScreenshot("Ativar Funcionalidade");
        esperaElemento(Feature_Toogle.btnSalvar, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.btnSalvar));
        Feature_Toogle.clicarBtnSalvar();
        esperaElemento(Feature_Toogle.frmMensagens, 80);
        Assert.assertTrue(elementoExiste(Feature_Toogle.frmMensagens));
        tirarScreenshot("Funcionalidade Ativada");
    }

    @And("Inativar funcionalidade {string} {string} {string}")
    public void inativarFuncionalidade(String funcionalidade, String perfil, String plataforma) throws InterruptedException {
        buscarFuncionalidade(funcionalidade, perfil, plataforma);

        esperaElemento(Feature_Toogle.cbStatusManutencao, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.cbStatusManutencao));
        Feature_Toogle.clicarCbStatusManutencao();
        esperaElemento(Feature_Toogle.menuStatus, 80);
        Assert.assertTrue(elementoExiste(Feature_Toogle.menuStatus));
        Feature_Toogle.clicarOptInativoCbStats();
        tirarScreenshot("Inativar Funcionalidade");
        esperaElemento(Feature_Toogle.btnSalvar, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.btnSalvar));
        Feature_Toogle.clicarBtnSalvar();
        esperaElemento(Feature_Toogle.frmMensagens, 80);
        Assert.assertTrue(elementoExiste(Feature_Toogle.frmMensagens));
        tirarScreenshot("Funcionalidade Inativada");
    }
    @And("Ocultar funcionalidade Bloqueio {string} {string} {string}")
    public void ocultarFuncionalidadeBloqueio(String funcionalidade, String perfil, String plataforma) throws InterruptedException {
        buscarFuncionalidadeBloqueio(funcionalidade, perfil, plataforma);

        esperaElemento(Feature_Toogle.cbStatusManutencao, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.cbStatusManutencao));
        Feature_Toogle.clicarCbStatusManutencao();
        esperaElemento(Feature_Toogle.menuStatus, 80);
        Assert.assertTrue(elementoExiste(Feature_Toogle.menuStatus));
        Feature_Toogle.clicarOptOcultarMenuCbStatus();
        tirarScreenshot("Ocultando Funcionalidade");
        esperaElemento(Feature_Toogle.btnSalvar, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.btnSalvar));
        Feature_Toogle.clicarBtnSalvar();
        esperaElemento(Feature_Toogle.frmMensagens, 80);
        Assert.assertTrue(elementoExiste(Feature_Toogle.frmMensagens));
        tirarScreenshot("Funcionalidade Ocultada");
    }

    @And("Ativar funcionalidade Bloqueio {string} {string} {string}")
    public void ativarFuncionalidadeBloqueio(String funcionalidade, String perfil, String plataforma) throws InterruptedException {
    	buscarFuncionalidadeBloqueio(funcionalidade, perfil, plataforma);

        esperaElemento(Feature_Toogle.cbStatusManutencao, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.cbStatusManutencao));
        Feature_Toogle.clicarCbStatusManutencao();
        esperaElemento(Feature_Toogle.menuStatus, 80);
        Assert.assertTrue(elementoExiste(Feature_Toogle.menuStatus));
        Feature_Toogle.clicarOptAtivoCbStatus();
        tirarScreenshot("Ativar Funcionalidade");
        esperaElemento(Feature_Toogle.btnSalvar, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.btnSalvar));
        Feature_Toogle.clicarBtnSalvar();
        esperaElemento(Feature_Toogle.frmMensagens, 80);
        Assert.assertTrue(elementoExiste(Feature_Toogle.frmMensagens));
        tirarScreenshot("Funcionalidade Ativada");
    }

    @And("Inativar funcionalidade Bloqueio {string} {string} {string}")
    public void inativarFuncionalidadeBloqueio(String funcionalidade, String perfil, String plataforma) throws InterruptedException {
    	buscarFuncionalidadeBloqueio(funcionalidade, perfil, plataforma);

        esperaElemento(Feature_Toogle.cbStatusManutencao, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.cbStatusManutencao));
        Feature_Toogle.clicarCbStatusManutencao();
        esperaElemento(Feature_Toogle.menuStatus, 80);
        Assert.assertTrue(elementoExiste(Feature_Toogle.menuStatus));
        Feature_Toogle.clicarOptInativoCbStats();
        tirarScreenshot("Inativar Funcionalidade");
        esperaElemento(Feature_Toogle.btnSalvar, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.btnSalvar));
        Feature_Toogle.clicarBtnSalvar();
        esperaElemento(Feature_Toogle.frmMensagens, 80);
        Assert.assertTrue(elementoExiste(Feature_Toogle.frmMensagens));
        tirarScreenshot("Funcionalidade Inativada");
    }

    public void buscarFuncionalidade(String funcionalidade, String perfil, String plataforma) {
        funcionalidade = funcionalidade.toLowerCase();
        perfil = perfil.toLowerCase();
        plataforma = plataforma.toLowerCase();

        esperaElemento(Feature_Toogle.txtNomeFuncionalidade, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.txtNomeFuncionalidade));
        Feature_Toogle.informarFuncionalidade(funcionalidade);
        esperaElemento(Feature_Toogle.cbPerfil, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.cbPerfil));
        Feature_Toogle.clicarComboPerfil();
        esperaElemento(Feature_Toogle.menuPerfil, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.menuPerfil));

        switch (perfil) {
            case "todos":
                Feature_Toogle.clicarOptTodosCbPerfil();
                break;
            case "gestor":
                Feature_Toogle.clicarOptGestorCbPerfil();
                break;
            case "portador":
                Feature_Toogle.clicarOptPortadorCbPerfil();
                break;
            case "gestor portador":
                Feature_Toogle.clicarOptGestorPortadorCbPerfil();
                break;
            case "gestor estrangeiro":
                Feature_Toogle.clicarOptGestorEstrangeiroCbPerfil();
                break;
            case "sem perfil":
                Feature_Toogle.clicarOptSemPerfilCbPerfil();
                break;
        }
        esperaElemento(Feature_Toogle.cbPlataforma, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.cbPlataforma));
        Feature_Toogle.clicarComboPlataforma();
        esperaElemento(Feature_Toogle.menuPlataforma, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.menuPlataforma));

        switch (plataforma) {
            case "todos":
                Feature_Toogle.clicarOptTodosCbPlataforma();
                break;
            case "web":
                Feature_Toogle.clicarOptWebCbPlataforma();
                break;
            case "mobile":
                Feature_Toogle.clicarOptMobileCbPlataforma();
                break;
        }
        esperaElemento(Feature_Toogle.btnPesquisar, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.btnPesquisar));
        Feature_Toogle.clicarPesquisar();
        esperaElemento(Feature_Toogle.tblResultadoConsulta, 80);
        Assert.assertTrue(elementoExiste(Feature_Toogle.tblResultadoConsulta));
        esperaElemento(Feature_Toogle.btnEditarFuncionalidade1Consulta, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.btnEditarFuncionalidade1Consulta));
        tirarScreenshot("Busca por Funcionalidade");
        Feature_Toogle.clicarEditarFuncionalidade1Consulta();
        esperaElemento(Feature_Toogle.lblManutencaoFuncionalidade, 0);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblManutencaoFuncionalidade));
        tirarScreenshot("Manuten��o de Funcionalidades");
    }
    
    public void buscarFuncionalidadeBloqueio(String funcionalidade, String perfil, String plataforma) {
        funcionalidade = funcionalidade.toLowerCase();
        perfil = perfil.toLowerCase();
        plataforma = plataforma.toLowerCase();

        esperaElemento(Feature_Toogle.txtNomeFuncionalidade, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.txtNomeFuncionalidade));
        Feature_Toogle.informarFuncionalidade(funcionalidade);
        esperaElemento(Feature_Toogle.cbPerfil, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.cbPerfil));
        Feature_Toogle.clicarComboPerfil();
        esperaElemento(Feature_Toogle.menuPerfil, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.menuPerfil));

        switch (perfil) {
            case "todos":
                Feature_Toogle.clicarOptTodosCbPerfil();
                break;
            case "gestor":
                Feature_Toogle.clicarOptGestorCbPerfil();
                break;
            case "portador":
                Feature_Toogle.clicarOptPortadorCbPerfil();
                break;
            case "gestor portador":
                Feature_Toogle.clicarOptGestorPortadorCbPerfil();
                break;
            case "gestor estrangeiro":
                Feature_Toogle.clicarOptGestorEstrangeiroCbPerfil();
                break;
            case "sem perfil":
                Feature_Toogle.clicarOptSemPerfilCbPerfil();
                break;
        }
        
        esperaElemento(Feature_Toogle.cbPlataforma, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.cbPlataforma));
        Feature_Toogle.clicarComboPlataforma();
        esperaElemento(Feature_Toogle.menuPlataforma, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.menuPlataforma));

        switch (plataforma) {
            case "todos":
                Feature_Toogle.clicarOptTodosCbPlataforma();
                break;
            case "web":
                Feature_Toogle.clicarOptWebCbPlataforma();
                break;
            case "mobile":
                Feature_Toogle.clicarOptMobileCbPlataforma();
                break;
        }
        
        esperaElemento(Feature_Toogle.btnPesquisar, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.btnPesquisar));
        Feature_Toogle.clicarPesquisar();
        esperaElemento(Feature_Toogle.tblResultadoConsulta, 80);
        Assert.assertTrue(elementoExiste(Feature_Toogle.tblResultadoConsulta));
        esperaElemento(Feature_Toogle.btnEditarFuncionalidade3Consulta, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.btnEditarFuncionalidade3Consulta));
        tirarScreenshot("Busca por Funcionalidade");
        Feature_Toogle.clicarEditarFuncionalidade3Consulta();
        esperaElemento(Feature_Toogle.lblManutencaoFuncionalidade, 0);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblManutencaoFuncionalidade));
        tirarScreenshot("Manuten��o de Funcionalidades");
    }

    @And("acesso o cabecalho de Boletos")
    public void acessoOCabecalhoDeBoletos() {
        String color = cabecalhoLogado.lblMenuConsultasBoletos.getCssValue("color");

        if (!elementoExiste(cabecalhoLogado.lblMenuConsultasBoletos)) {
            tirarScreenshot("Menu Boletos Oculto");
        } else if (color.equalsIgnoreCase("#290a3b") || color.equalsIgnoreCase("rgba(41, 10, 59, 1)")) {
            tirarScreenshot("Menu Boletos Inativo");
        } else {
            esperaElemento(cabecalhoLogado.lblMenuConsultasBoletos, 60);
            Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultasBoletos));
            cabecalhoLogado.clicarMenuConsultasBoletos();
            tirarScreenshot("Cabe�alho - Boletos");
        }
    }

    @And("acesso o cabecalho de LGPD")
    public void acessoOCabecalhoDeLGPD() {
        esperaElemento(cabecalhoLogado.lblMenuServicos, 60);
        scrollAteOElemento(cabecalhoLogado.lblMenuServicos);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicos));
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuServicos).perform();
        String color = cabecalhoLogado.lblMenuServicosLgpd.getCssValue("color");

        if (!elementoExiste(cabecalhoLogado.lblMenuServicosLgpd)) {
            tirarScreenshot("Menu LGPD Oculto");
        } else if (color.equalsIgnoreCase("#290a3b") || color.equalsIgnoreCase("rgba(41, 10, 59, 1)")) {
            tirarScreenshot("Menu LGPD Inativo");
        } else {
            esperaElemento(cabecalhoLogado.lblMenuServicosLgpd, 60);
            Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicosLgpd));
            cabecalhoLogado.clicarMenuServicosLgpd();
            tirarScreenshot("Cabe�alho - LGPD");
        }
    }

    @And("acesso o cabecalho de Taxa de Cambio")
    public void acessoOCabecalhoDeTaxaDeCambio() {
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        scrollAteOElemento(cabecalhoLogado.lblMenuConsultas);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        String color = cabecalhoLogado.lblMenuConsultasTaxaDeCambio.getCssValue("color");

        if (!elementoExiste(cabecalhoLogado.lblMenuConsultasTaxaDeCambio)) {
            tirarScreenshot("Menu Taxa de C�mbio Oculto");
        } else if (color.equalsIgnoreCase("#290a3b") || color.equalsIgnoreCase("rgba(41, 10, 59, 1)")) {
            tirarScreenshot("Menu Taxa de C�mbio Inativo");
        } else {
            esperaElemento(cabecalhoLogado.lblMenuConsultasTaxaDeCambio, 60);
            Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultasTaxaDeCambio));
            cabecalhoLogado.clicarMenuConsultasTaxaDeCambio();
            tirarScreenshot("Cabe�alho - Taxa de C�mbio");
        }
    }

    @And("acesso o cabecalho de Aviso Viagem")
    public void acessoOCabecalhoDeAvisoViagem() {
        String color = cabecalhoLogado.lblMenuAvisoDeViagem.getCssValue("color");

        if (!elementoExiste(cabecalhoLogado.lblMenuAvisoDeViagem)) {
            tirarScreenshot("Menu Aviso Viagem Oculto");
        } else if (color.equalsIgnoreCase("#290a3b") || color.equalsIgnoreCase("rgba(41, 10, 59, 1)")) {
            tirarScreenshot("Menu Aviso Viagem Inativo");
        } else {
            esperaElemento(cabecalhoLogado.lblMenuAvisoDeViagem, 60);
            Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuAvisoDeViagem));
            cabecalhoLogado.clicarMenuAvisoDeViagem();
            tirarScreenshot("Cabe�alho - Aviso Viagem");
        }
    }

    @And("Acesso o cabecalho de Parcelamento de Fatura")
    public void acessoOCabecalhoDeParcelamentoDeFatura() {
        esperaElemento(cabecalhoLogado.lblMenuServicos, 60);
        scrollAteOElemento(cabecalhoLogado.lblMenuServicos);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicos));
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuServicos).perform();
        String color = cabecalhoLogado.lblMenuServicosParcelamentoDeFatura.getCssValue("color");

        if (!elementoExiste(cabecalhoLogado.lblMenuServicosParcelamentoDeFatura)) {
            tirarScreenshot("Menu Parcelamento de Fatura Oculto");
        }  else if (color.equalsIgnoreCase("#290a3b") || color.equalsIgnoreCase("rgba(41, 10, 59, 1)")) {
            tirarScreenshot("Menu Parcelamento de Fatura Inativo");
        } else {
            esperaElemento(cabecalhoLogado.lblMenuServicosParcelamentoDeFatura, 60);
            Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicosParcelamentoDeFatura));
            cabecalhoLogado.clicarMenuServicosParcelamentoDeFatura();
            tirarScreenshot("Cabe�alho - Parcelamento de Fatura");
        }
    }

    @Then("Verifico como Portador que a tela Consulta de Limites esta Inativo")
    public void verifico_Tela_Consulta_De_Limite_Inativa_Portador() throws InterruptedException {
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        cabecalhoLogado.clicarMenuConsultas();
        tirarScreenshot("Menu consultas Inativo");
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        espera(2);
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites Portador Inativo");
    }

    @Then("Verifico como Portador que a tela Consulta de Limites esta Oculto")
    public void verifico_Tela_Consulta_De_Limite_Oculta_Portador() throws InterruptedException {
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        cabecalhoLogado.clicarMenuConsultas();
        tirarScreenshot("Menu consultas Oculto");
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        espera(2);
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites Portador Oculto");
    }

    @Then("Verifico como Portador que a tela Consulta de Limites esta Ativa")
    public void verifico_Tela_Consulta_De_Limite_Ativa_Portador() throws InterruptedException {
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        cabecalhoLogado.clicarMenuConsultas();
        tirarScreenshot("Menu consultas Oculto");
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblConsultaLimites);
        esperaElemento(Feature_Toogle.lblConsultaLimites, 60);
        esperaElemento(Feature_Toogle.lblLimiteTotal, 60);
        esperaElemento(Feature_Toogle.dataCartaoPortador, 60);
        esperaElemento(Feature_Toogle.dataNomedocartaoPortador, 60);
        esperaElemento(Feature_Toogle.lblStatusPortador, 60);
        esperaElemento(Feature_Toogle.dataStatusPortador, 60);
        esperaElemento(Feature_Toogle.bloqueiotemporarioportador, 60);
        scrollAteOElemento(Feature_Toogle.lblConsultaLimites);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblConsultaLimites));
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblLimiteTotal));
        Assert.assertTrue(elementoExiste(Feature_Toogle.dataCartaoPortador));
        Assert.assertTrue(elementoExiste(Feature_Toogle.dataNomedocartaoPortador));
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblStatusPortador));
        Assert.assertTrue(elementoExiste(Feature_Toogle.dataStatusPortador));
        Assert.assertTrue(elementoExiste(Feature_Toogle.bloqueiotemporarioportador));
        Assert.assertEquals("Consulta de limites", Feature_Toogle.lblConsultaLimites.getText());
        Assert.assertEquals("Limite total R$ 12.000,00", Feature_Toogle.lblLimiteTotal.getText());
        Assert.assertEquals("Final 1186", Feature_Toogle.dataCartaoPortador.getText());
        Assert.assertEquals("AMEX CORPORATE GOLD", Feature_Toogle.dataNomedocartaoPortador.getText());
        Assert.assertEquals("Status", Feature_Toogle.lblStatusPortador.getText());
        Assert.assertEquals("Bloqueio tempor�rio", Feature_Toogle.bloqueiotemporarioportador.getText());
        espera(2);
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites Portador Ativo");
    }

    @Then("Verifico como Portador que a tela Consulta de Limites esta Inativo em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Inativa_Portador_Ingles() throws InterruptedException {
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        cabecalhoLogado.clicarMenuConsultas();
        tirarScreenshot("Menu consultas Inativo em Portugues");
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        espera(2);
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites Portador Inativo");
    }

    @Then("Verifico como Portador que a tela Consulta de Limites esta Oculto em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Oculta_Portador_Ingles() throws InterruptedException {
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        cabecalhoLogado.clicarMenuConsultas();
        tirarScreenshot("Menu consultas Oculto em Portugues");
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        espera(2);
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites Portador Oculto");
    }

    @Then("Verifico como Portador que a tela Consulta de Limites esta Ativa em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Ativa_Portador_Ingles() throws InterruptedException {
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        cabecalhoLogado.clicarMenuConsultas();
        tirarScreenshot("Menu consultas Ativo em Portugues");
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblConsultaLimites);
        esperaElemento(Feature_Toogle.lblConsultaLimites, 60);
        esperaElemento(Feature_Toogle.lblLimiteTotal, 60);
        esperaElemento(Feature_Toogle.dataCartaoPortador, 60);
        esperaElemento(Feature_Toogle.dataNomedocartaoPortador, 60);
        esperaElemento(Feature_Toogle.lblStatusPortador, 60);
        esperaElemento(Feature_Toogle.dataStatusPortador, 60);
        esperaElemento(Feature_Toogle.bloqueiotemporarioportador, 60);
        scrollAteOElemento(Feature_Toogle.lblConsultaLimites);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblConsultaLimites));
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblLimiteTotal));
        Assert.assertTrue(elementoExiste(Feature_Toogle.dataCartaoPortador));
        Assert.assertTrue(elementoExiste(Feature_Toogle.dataNomedocartaoPortador));
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblStatusPortador));
        Assert.assertTrue(elementoExiste(Feature_Toogle.dataStatusPortador));
        Assert.assertTrue(elementoExiste(Feature_Toogle.bloqueiotemporarioportador));
        Assert.assertEquals("Consulta de limites", Feature_Toogle.lblConsultaLimites.getText());
        Assert.assertEquals("Limite total R$ 12.000,00", Feature_Toogle.lblLimiteTotal.getText());
        Assert.assertEquals("Final 1186", Feature_Toogle.dataCartaoPortador.getText());
        Assert.assertEquals("AMEX CORPORATE GOLD", Feature_Toogle.dataNomedocartaoPortador.getText());
        Assert.assertEquals("Status", Feature_Toogle.lblStatusPortador.getText());
        Assert.assertEquals("Bloqueio tempor�rio", Feature_Toogle.bloqueiotemporarioportador.getText());
        espera(2);
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites Portador Ativo");
    }

    @Then("Verifico como Gestor que a tela Consulta de Limites esta Inativo")
    public void verifico_Tela_Consulta_De_Limite_Inativa_Gestor() throws InterruptedException {
        scrollAteOElemento(Feature_Toogle.lblSolicitacoes);
        esperaElemento(Feature_Toogle.lblSolicitacoes, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblSolicitacoes));
        Assert.assertEquals("Solicita��es", Feature_Toogle.lblSolicitacoes.getText());
        espera(2);
        scrollToTop();
        espera(1);
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        cabecalhoLogado.clicarMenuConsultas();
        tirarScreenshot("Menu consultas Inativo");
        espera(1);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        espera(2);
        tirarScreenshot("Tela Consulta de Limites Gestor Inativo");
    }

    @Then("Verifico como Gestor que a tela Consulta de Limites esta Oculto")
    public void verifico_Tela_Consulta_De_Limite_Oculta_Gestor() throws InterruptedException {
        scrollAteOElemento(Feature_Toogle.lblSolicitacoes);
        esperaElemento(Feature_Toogle.lblSolicitacoes, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblSolicitacoes));
        Assert.assertEquals("Solicita��es", Feature_Toogle.lblSolicitacoes.getText());
        espera(2);
        scrollToTop();
        espera(1);
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        cabecalhoLogado.clicarMenuConsultas();
        tirarScreenshot("Menu consultas Inativo");
        espera(1);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        espera(2);
        tirarScreenshot("Tela Consulta de Limites Gestor Inativo");
    }

    @Then("Verifico como Gestor que a tela Consulta de Limites esta Ativa")
    public void verifico_Tela_Consulta_De_Limite_Ativa_Gestor() throws InterruptedException {
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        cabecalhoLogado.clicarMenuConsultas();
        tirarScreenshot("Menu consultas Ativa");
        espera(2);
        esperaElemento(limiteGestor.msgTitulo, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        limiteGestor.clicarTitulo();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitulo);
        esperaElemento(limiteGestor.lblPortadores, 60);
        esperaElemento(limiteGestor.lblEmpresas, 60);
        esperaElemento(limiteGestor.lblProdutos, 60);
        scrollAteOElemento(limiteGestor.msgTitulo);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadores));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresas));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutos));
        Assert.assertEquals("Consulta de Limite", limiteGestor.msgTitulo.getText());
        Assert.assertEquals("Portadores", limiteGestor.lblPortadores.getText());
        Assert.assertEquals("Empresas", limiteGestor.lblEmpresas.getText());
        Assert.assertEquals("Produtos", limiteGestor.lblProdutos.getText());
        espera(2);
        tirarScreenshot("Tela Consulta de Limites Gestor");
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites Gestor");
    }

    @Then("Verifico como Gestor que a tela Consulta de Limites esta Inativo em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Inativa_Gestor_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        scrollAteOElemento(cabecalhoLogado.lblMainConsultations);
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu consultas Inativo em Ingl�s");
        espera(3);
        cabecalhoLogado.clickMainConsultations();
        tirarScreenshot("Menu consultas Inativo em Ingl�s");
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        espera(2);
        scrollToTop();
        tirarScreenshot("Tela Consulta de Limites Gestor Inativo");
    }

    @Then("Verifico como Gestor que a tela Consulta de Limites esta Oculto em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Oculta_Gestor_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        scrollAteOElemento(cabecalhoLogado.lblMainConsultations);
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu consultas Oculto em Ingl�s");
        espera(3);
        cabecalhoLogado.clickMainConsultations();
        tirarScreenshot("Menu consultas Oculto em Ingl�s");
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        espera(2);
        scrollToTop();
        tirarScreenshot("Tela Consulta de Limites Gestor Oculto");
    }

    @Then("Verifico como Gestor que a tela Consulta de Limites esta Ativa em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Ativa_Gestor_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu consultas Ativa em Ingl�s");
        espera(2);
        cabecalhoLogado.clickMainConsultations();
        esperaElemento(limiteGestor.msgTitleEn, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        limiteGestor.clickTitle();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitleEn);
        esperaElemento(limiteGestor.lblPortadoresIngles, 60);
        esperaElemento(limiteGestor.lblEmpresasIngles, 60);
        esperaElemento(limiteGestor.lblProdutosIngles, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadoresIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresasIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutosIngles));
        Assert.assertEquals("Limit Consultation", limiteGestor.msgTitleEn.getText());
        Assert.assertEquals("Carholders", limiteGestor.lblPortadoresIngles.getText());
        Assert.assertEquals("Companies", limiteGestor.lblEmpresasIngles.getText());
        Assert.assertEquals("Products", limiteGestor.lblProdutosIngles.getText());
        espera(2);
        tirarScreenshot("Tela Consulta de Limites Gestor em Ingles");
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites Gestor em Ingles");
    }

    @Then("Verifico como Gestor Estrangeiro que a tela Consulta de Limites esta Inativo em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Inativa_Gestor_Estrangeiro_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        scrollAteOElemento(cabecalhoLogado.lblMainConsultations);
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu consultas Inativo em Ingl�s");
        espera(3);
        cabecalhoLogado.clickMainConsultations();
        tirarScreenshot("Menu consultas Inativo em Ingl�s");
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        espera(2);
        scrollToTop();
        tirarScreenshot("Tela Consulta de Limites Gestor Estrangeiro Inativo");
    }

    @Then("Verifico como Gestor Estrangeiro que a tela Consulta de Limites esta Oculto em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Oculta_Gestor_Estrangeiro_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        scrollAteOElemento(cabecalhoLogado.lblMainConsultations);
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu consultas Oculto em Ingl�s");
        espera(3);
        cabecalhoLogado.clickMainConsultations();
        tirarScreenshot("Menu consultas Oculto em Ingl�s");
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        espera(2);
        scrollToTop();
        tirarScreenshot("Tela Consulta de Limites Gestor Estrangeiro Oculto");
    }

    @Then("Verifico como Gestor Estrangeiro que a tela Consulta de Limites esta Ativa em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Ativa_Gestor_Estrangeiro_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu consultas Ativa em Ingl�s");
        espera(2);
        cabecalhoLogado.clickMainConsultations();
        esperaElemento(limiteGestor.msgTitleEn, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        limiteGestor.clickTitle();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitleEn);
        esperaElemento(limiteGestor.lblPortadoresIngles, 60);
        esperaElemento(limiteGestor.lblEmpresasIngles, 60);
        esperaElemento(limiteGestor.lblProdutosIngles, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadoresIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresasIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutosIngles));
        Assert.assertEquals("Limit Consultation", limiteGestor.msgTitleEn.getText());
        Assert.assertEquals("Carholders", limiteGestor.lblPortadoresIngles.getText());
        Assert.assertEquals("Companies", limiteGestor.lblEmpresasIngles.getText());
        Assert.assertEquals("Products", limiteGestor.lblProdutosIngles.getText());
        espera(2);
        tirarScreenshot("Tela Consulta de Limites Gestor Estrangeiro em Ingles");
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites Gestor Estrangeiro em Ingles");
    }

    @Then("Verifico como Gestor Estrangeiro que a tela Consulta de Limites esta Inativo")
    public void verifico_Tela_Consulta_De_Limite_Inativa_Gestor_Estrangeiro() throws InterruptedException {
        scrollAteOElemento(Feature_Toogle.lblSolicitacoes);
        esperaElemento(Feature_Toogle.lblSolicitacoes, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblSolicitacoes));
        Assert.assertEquals("Solicita��es", Feature_Toogle.lblSolicitacoes.getText());
        espera(2);
        scrollToTop();
        espera(1);
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        cabecalhoLogado.clicarMenuConsultas();
        tirarScreenshot("Menu consultas Inativo");
        espera(1);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        espera(2);
        tirarScreenshot("Tela Consulta de Limites Gestor Estrangeiro Inativo");
    }

    @Then("Verifico como Gestor Estrangeiro que a tela Consulta de Limites esta Oculto")
    public void verifico_Tela_Consulta_De_Limite_Oculta_Gestor_Estrangeiro() throws InterruptedException {
        scrollAteOElemento(Feature_Toogle.lblSolicitacoes);
        esperaElemento(Feature_Toogle.lblSolicitacoes, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblSolicitacoes));
        Assert.assertEquals("Solicita��es", Feature_Toogle.lblSolicitacoes.getText());
        espera(2);
        scrollToTop();
        espera(1);
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        cabecalhoLogado.clicarMenuConsultas();
        tirarScreenshot("Menu consultas Inativo");
        espera(1);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        espera(2);
        tirarScreenshot("Tela Consulta de Limites Gestor Estrangeiro Inativo");
    }

    @Then("Verifico como Gestor Estrangeiro que a tela Consulta de Limites esta Ativa")
    public void verifico_Tela_Consulta_De_Limite_Ativa_Gestor_Estrangeiro() throws InterruptedException {
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        cabecalhoLogado.clicarMenuConsultas();
        tirarScreenshot("Menu consultas Ativa");
        espera(2);
        esperaElemento(limiteGestor.msgTitulo, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        limiteGestor.clicarTitulo();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitulo);
        esperaElemento(limiteGestor.lblPortadores, 60);
        esperaElemento(limiteGestor.lblEmpresas, 60);
        esperaElemento(limiteGestor.lblProdutos, 60);
        scrollAteOElemento(limiteGestor.msgTitulo);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadores));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresas));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutos));
        Assert.assertEquals("Consulta de Limite", limiteGestor.msgTitulo.getText());
        Assert.assertEquals("Portadores", limiteGestor.lblPortadores.getText());
        Assert.assertEquals("Empresas", limiteGestor.lblEmpresas.getText());
        Assert.assertEquals("Produtos", limiteGestor.lblProdutos.getText());
        espera(2);
        tirarScreenshot("Tela Consulta de Limites Gestor Estrangeiro");
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites Gestor Estrangeiro");
    }

    @And("acesso o botao de Consultas")
    public void acessoOBotaoDeConsultas() {
        String color = cabecalhoLogado.lblMenuConsultas.getCssValue("color");

        if (!elementoExiste(cabecalhoLogado.lblMenuConsultas)) {
            tirarScreenshot("Bot�o Consultas Oculto");
        } else if (color.equalsIgnoreCase("#290a3b") || color.equalsIgnoreCase("rgba(41, 10, 59, 1)")) {
            tirarScreenshot("Bot�o Consultas Inativo");
        } else {
            esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
            Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
            cabecalhoLogado.clicarMenuConsultas();
            tirarScreenshot("Cabe�alho - Consultas");
        }
    }

    @And("acesso o botao de Solicitacoes Gestor")
    public void acessoOBotaoDeSolicitacoesGestor() {
        esperaElemento(cabecalhoLogado.lblMenuSolicitacoes, 60);
        scrollAteOElemento(cabecalhoLogado.lblMenuSolicitacoes);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuSolicitacoes));
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuSolicitacoes).perform();
        String color = cabecalhoLogado.lblMenuSolicitacoesSolicitacoesPortadores.getCssValue("color");

        if (!elementoExiste(cabecalhoLogado.lblMenuSolicitacoesSolicitacoesPortadores)) {
            tirarScreenshot("Bot�o Solicita��es Oculto");
        } else if (color.equalsIgnoreCase("#290a3b") || color.equalsIgnoreCase("rgba(41, 10, 59, 1)")) {
            tirarScreenshot("Bot�o Solicita��es Inativo");
        } else {
            esperaElemento(cabecalhoLogado.lblMenuSolicitacoes, 60);
            Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuSolicitacoes));
            cabecalhoLogado.clicarMenuSolicitacoesSolicitacoesPortadores();
            tirarScreenshot("Cabe�alho - Solicita��es");
        }
    }

    @And("acesso o cabecalho de Faturas")
    public void acessoOCabecalhoDeFaturas() {
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        scrollAteOElemento(cabecalhoLogado.lblMenuConsultas);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        String color = cabecalhoLogado.lblMenuConsultasFaturas.getCssValue("color");

        if (!elementoExiste(cabecalhoLogado.lblMenuConsultasFaturas)) {
            tirarScreenshot("Menu Faturas Oculto");
        } else if (color.equalsIgnoreCase("#290a3b") || color.equalsIgnoreCase("rgba(41, 10, 59, 1)")) {
            tirarScreenshot("Menu Faturas Inativo");
        } else {
            esperaElemento(cabecalhoLogado.lblMenuConsultasFaturas, 60);
            Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultasFaturas));
            cabecalhoLogado.clicarMenuConsultasFaturas();
            tirarScreenshot("Cabe�alho - Faturas");
        }
    }
    
    @Then("Verifico como Gestor Portador que a tela Consulta de Limites esta Inativo em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Inativa_Gestor_Portador_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        scrollAteOElemento(cabecalhoLogado.lblMainConsultations);
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu consultas Inativo em Ingl�s");
        espera(3);
        cabecalhoLogado.clickMainConsultations();
        tirarScreenshot("Menu consultas Inativo em Ingl�s");
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        espera(2);
        scrollToTop();
        tirarScreenshot("Tela Consulta de Limites Gestor Portador Inativo");
    }

    @Then("Verifico como Gestor Portador que a tela Consulta de Limites esta Oculto em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Oculta_Gestor_Portador_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        scrollAteOElemento(cabecalhoLogado.lblMainConsultations);
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu consultas Oculto em Ingl�s");
        espera(3);
        cabecalhoLogado.clickMainConsultations();
        tirarScreenshot("Menu consultas Oculto em Ingl�s");
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        espera(2);
        scrollToTop();
        tirarScreenshot("Tela Consulta de Limites Gestor Portador Oculto");
    }

    @Then("Verifico como Gestor Portador que a tela Consulta de Limites esta Ativa em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Ativa_Gestor_Portador_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu consultas Ativa em Ingl�s");
        espera(2);
        cabecalhoLogado.clickMainConsultations();
        esperaElemento(limiteGestor.msgTitleEn, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        limiteGestor.clickTitle();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitleEn);
        esperaElemento(limiteGestor.lblPortadoresIngles, 60);
        esperaElemento(limiteGestor.lblEmpresasIngles, 60);
        esperaElemento(limiteGestor.lblProdutosIngles, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadoresIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresasIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutosIngles));
        Assert.assertEquals("Limit Consultation", limiteGestor.msgTitleEn.getText());
        Assert.assertEquals("Carholders", limiteGestor.lblPortadoresIngles.getText());
        Assert.assertEquals("Companies", limiteGestor.lblEmpresasIngles.getText());
        Assert.assertEquals("Products", limiteGestor.lblProdutosIngles.getText());
        espera(2);
        tirarScreenshot("Tela Consulta de Limites Gestor Portador em Ingles");
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites Gestor Portador em Ingles");
    }

    @Then("Verifico como Gestor Portador que a tela Consulta de Limites esta Inativo")
    public void verifico_Tela_Consulta_De_Limite_Inativa_Gestor_Portador() throws InterruptedException {
        scrollAteOElemento(Feature_Toogle.lblSolicitacoes);
        esperaElemento(Feature_Toogle.lblSolicitacoes, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblSolicitacoes));
        Assert.assertEquals("Solicita��es", Feature_Toogle.lblSolicitacoes.getText());
        espera(2);
        scrollToTop();
        espera(1);
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        cabecalhoLogado.clicarMenuConsultas();
        tirarScreenshot("Menu consultas Inativo");
        espera(1);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        espera(2);
        tirarScreenshot("Tela Consulta de Limites Gestor Portador Inativo");
    }

    @Then("Verifico como Gestor Portador que a tela Consulta de Limites esta Oculto")
    public void verifico_Tela_Consulta_De_Limite_Portador_Gestor_Estrangeiro() throws InterruptedException {
        scrollAteOElemento(Feature_Toogle.lblSolicitacoes);
        esperaElemento(Feature_Toogle.lblSolicitacoes, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblSolicitacoes));
        Assert.assertEquals("Solicita��es", Feature_Toogle.lblSolicitacoes.getText());
        espera(2);
        scrollToTop();
        espera(1);
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        cabecalhoLogado.clicarMenuConsultas();
        tirarScreenshot("Menu consultas Inativo");
        espera(1);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        espera(2);
        tirarScreenshot("Tela Consulta de Limites Gestor Portador Inativo");
    }

    @Then("Verifico como Gestor Portador que a tela Consulta de Limites esta Ativa")
    public void verifico_Tela_Consulta_De_Limite_Ativa_Gestor_Portador() throws InterruptedException {
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        cabecalhoLogado.clicarMenuConsultas();
        tirarScreenshot("Menu consultas Ativa");
        espera(2);
        esperaElemento(limiteGestor.msgTitulo, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        limiteGestor.clicarTitulo();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitulo);
        esperaElemento(limiteGestor.lblPortadores, 60);
        esperaElemento(limiteGestor.lblEmpresas, 60);
        esperaElemento(limiteGestor.lblProdutos, 60);
        scrollAteOElemento(limiteGestor.msgTitulo);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadores));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresas));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutos));
        Assert.assertEquals("Consulta de Limite", limiteGestor.msgTitulo.getText());
        Assert.assertEquals("Portadores", limiteGestor.lblPortadores.getText());
        Assert.assertEquals("Empresas", limiteGestor.lblEmpresas.getText());
        Assert.assertEquals("Produtos", limiteGestor.lblProdutos.getText());
        espera(2);
        tirarScreenshot("Tela Consulta de Limites Gestor Portador");
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites Gestor Portador");
    }
    
    @Then("Verifico como Gestor que a aba Produtos em Consulta de Limites esta Inativa em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Aba_Produtos_Inativa_Gestor_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu consultas Ativa em Ingl�s");
        espera(2);
        cabecalhoLogado.clickMainConsultations();
        esperaElemento(limiteGestor.msgTitleEn, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        limiteGestor.clickTitle();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitleEn);
        esperaElemento(limiteGestor.lblPortadoresIngles, 60);
        esperaElemento(limiteGestor.lblEmpresasIngles, 60);
        esperaElemento(limiteGestor.lblProdutosIngles, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadoresIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresasIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutosIngles));
        Assert.assertEquals("Limit Consultation", limiteGestor.msgTitleEn.getText());
        Assert.assertEquals("Carholders", limiteGestor.lblPortadoresIngles.getText());
        Assert.assertEquals("Companies", limiteGestor.lblEmpresasIngles.getText());
        Assert.assertEquals("Products", limiteGestor.lblProdutosIngles.getText());
        espera(2);
        tirarScreenshot("Tela Consulta de Limites Gestor Portador em Ingles");
        limiteGestor.clicarMenuProdutosIngles();
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba produtos Inativa Gestor em Ingles");
    }
    
    @Then("Verifico como Gestor que a aba Portadores em Consulta de Limites esta Inativa em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Aba_Portadores_Inativa_Gestor_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu consultas Ativa em Ingl�s");
        espera(2);
        cabecalhoLogado.clickMainConsultations();
        esperaElemento(limiteGestor.msgTitleEn, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        limiteGestor.clickTitle();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitleEn);
        esperaElemento(limiteGestor.lblPortadoresIngles, 60);
        esperaElemento(limiteGestor.lblEmpresasIngles, 60);
        esperaElemento(limiteGestor.lblProdutosIngles, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadoresIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresasIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutosIngles));
        Assert.assertEquals("Limit Consultation", limiteGestor.msgTitleEn.getText());
        Assert.assertEquals("Carholders", limiteGestor.lblPortadoresIngles.getText());
        Assert.assertEquals("Companies", limiteGestor.lblEmpresasIngles.getText());
        Assert.assertEquals("Products", limiteGestor.lblProdutosIngles.getText());
        espera(2);
        tirarScreenshot("Tela Consulta de Limites Gestor Portador em Ingles");
        limiteGestor.clicarMenuPortadoresIngles();
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba Produtos Inativa Gestor em Ingles");
    }
    
    @Then("Verifico como Gestor que a aba Empresas em Consulta de Limites esta Inativa em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Aba_Empresas_Inativa_Gestor_IIngles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu consultas Ativa em Ingl�s");
        espera(2);
        cabecalhoLogado.clickMainConsultations();
        esperaElemento(limiteGestor.msgTitleEn, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        limiteGestor.clickTitle();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitleEn);
        esperaElemento(limiteGestor.lblPortadoresIngles, 60);
        esperaElemento(limiteGestor.lblEmpresasIngles, 60);
        esperaElemento(limiteGestor.lblProdutosIngles, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadoresIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresasIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutosIngles));
        Assert.assertEquals("Limit Consultation", limiteGestor.msgTitleEn.getText());
        Assert.assertEquals("Carholders", limiteGestor.lblPortadoresIngles.getText());
        Assert.assertEquals("Companies", limiteGestor.lblEmpresasIngles.getText());
        Assert.assertEquals("Products", limiteGestor.lblProdutosIngles.getText());
        espera(2);
        tirarScreenshot("Tela Consulta de Limites Gestor Portador em Ingles");
        limiteGestor.clicarMenuEmpresasIngles();
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba Empresas Inativa Gestor em Ingles");
    }
    
    @Then("Verifico como Gestor que a aba Produtos em Consulta de Limites esta Inativa")
    public void verifico_Tela_Consulta_De_Limite_Aba_Produtos_Inativa_Gestor() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        tirarScreenshot("Menu consultas Ativa em Portugues");
        espera(2);
        cabecalhoLogado.clicarMenuConsultas();
        esperaElemento(limiteGestor.msgTitulo, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        limiteGestor.clicarTitulo();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitulo);
        esperaElemento(limiteGestor.lblPortadores, 60);
        esperaElemento(limiteGestor.lblEmpresas, 60);
        esperaElemento(limiteGestor.lblProdutos, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadores));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresas));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutos));
        Assert.assertEquals("Consulta de Limite", limiteGestor.msgTitulo.getText());
        Assert.assertEquals("Portadores", limiteGestor.lblPortadores.getText());
        Assert.assertEquals("Empresas", limiteGestor.lblEmpresas.getText());
        Assert.assertEquals("Produtos", limiteGestor.lblProdutos.getText());
        espera(2);
        tirarScreenshot("Tela Consulta de Limites Gestor Portador");
        limiteGestor.clicarMenuProdutos();
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba produtos Inativa Gestor em Portugues");
    }
    
    @Then("Verifico como Gestor que a aba Portadores em Consulta de Limites esta Inativa")
    public void verifico_Tela_Consulta_De_Limite_Aba_Portadores_Inativa_Gestor() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        tirarScreenshot("Menu consultas Ativa em Portugues");
        espera(2);
        cabecalhoLogado.clicarMenuConsultas();
        esperaElemento(limiteGestor.msgTitulo, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        limiteGestor.clicarTitulo();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitulo);
        esperaElemento(limiteGestor.lblPortadores, 60);
        esperaElemento(limiteGestor.lblEmpresas, 60);
        esperaElemento(limiteGestor.lblProdutos, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadores));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresas));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutos));
        Assert.assertEquals("Consulta de Limite", limiteGestor.msgTitulo.getText());
        Assert.assertEquals("Portadores", limiteGestor.lblPortadores.getText());
        Assert.assertEquals("Empresas", limiteGestor.lblEmpresas.getText());
        Assert.assertEquals("Produtos", limiteGestor.lblProdutos.getText());
        espera(2);
        tirarScreenshot("Tela Consulta de Limites Gestor Portador");
        limiteGestor.clicarMenuPortadores();
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba Portadores Inativa Gestor em Portugues");
    }
    
    @Then("Verifico como Gestor que a aba Empresas em Consulta de Limites esta Inativa")
    public void verifico_Tela_Consulta_De_Limite_Aba_Empresas_Inativa_Gestor() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        tirarScreenshot("Menu consultas Ativa em Portugues");
        espera(2);
        cabecalhoLogado.clicarMenuConsultas();
        esperaElemento(limiteGestor.msgTitulo, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        limiteGestor.clicarTitulo();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitulo);
        esperaElemento(limiteGestor.lblPortadores, 60);
        esperaElemento(limiteGestor.lblEmpresas, 60);
        esperaElemento(limiteGestor.lblProdutos, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadores));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresas));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutos));
        Assert.assertEquals("Consulta de Limite", limiteGestor.msgTitulo.getText());
        Assert.assertEquals("Portadores", limiteGestor.lblPortadores.getText());
        Assert.assertEquals("Empresas", limiteGestor.lblEmpresas.getText());
        Assert.assertEquals("Produtos", limiteGestor.lblProdutos.getText());
        espera(2);
        tirarScreenshot("Tela Consulta de Limites Gestor Portador");
        limiteGestor.clicarMenuEmpresas();
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba Empresas Inativa Gestor em Portugues");
    }
    
    @Then("Verifico como Gestor que a aba Produtos em Consulta de Limites esta Oculta em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Aba_Produtos_Oculta_Gestor_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu consultas Ativa em Ingl�s");
        espera(2);
        cabecalhoLogado.clickMainConsultations();
        esperaElemento(limiteGestor.msgTitleEn, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        limiteGestor.clickTitle();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitleEn);
        esperaElemento(limiteGestor.lblPortadoresIngles, 60);
        esperaElemento(limiteGestor.lblEmpresasIngles, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadoresIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresasIngles));
        Assert.assertEquals("Limit Consultation", limiteGestor.msgTitleEn.getText());
        Assert.assertEquals("Carholders", limiteGestor.lblPortadoresIngles.getText());
        Assert.assertEquals("Companies", limiteGestor.lblEmpresasIngles.getText());
        espera(4);
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba produtos Oculta Gestor em Ingles");
    }
    
    @Then("Verifico como Gestor que a aba Portadores em Consulta de Limites esta Oculta em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Aba_Portadores_Oculta_Gestor_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu consultas Ativa em Ingl�s");
        espera(2);
        cabecalhoLogado.clickMainConsultations();
        esperaElemento(limiteGestor.msgTitleEn, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        limiteGestor.clickTitle();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitleEn);
        esperaElemento(limiteGestor.lblEmpresasIngles, 60);
        esperaElemento(limiteGestor.lblProdutosIngles, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresasIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutosIngles));
        Assert.assertEquals("Limit Consultation", limiteGestor.msgTitleEn.getText());
        Assert.assertEquals("Companies", limiteGestor.lblEmpresasIngles.getText());
        Assert.assertEquals("Products", limiteGestor.lblProdutosIngles.getText());
        espera(4);
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba Ocultas Inativa Gestor em Ingles");
    }
    
    @Then("Verifico como Gestor que a aba Empresas em Consulta de Limites esta Oculta em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Aba_Empresas_Oculta_Gestor_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu consultas Ativa em Ingl�s");
        espera(2);
        cabecalhoLogado.clickMainConsultations();
        esperaElemento(limiteGestor.msgTitleEn, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        limiteGestor.clickTitle();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitleEn);
        esperaElemento(limiteGestor.lblPortadoresIngles, 60);
        esperaElemento(limiteGestor.lblEmpresasIngles, 60);
        esperaElemento(limiteGestor.lblProdutosIngles, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadoresIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutosIngles));
        Assert.assertEquals("Limit Consultation", limiteGestor.msgTitleEn.getText());
        Assert.assertEquals("Carholders", limiteGestor.lblPortadoresIngles.getText());
        Assert.assertEquals("Products", limiteGestor.lblProdutosIngles.getText());
        espera(4);
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba Empresas Oculta Gestor em Ingles");
    }
    
    @Then("Verifico como Gestor que a aba Produtos em Consulta de Limites esta Oculta")
    public void verifico_Tela_Consulta_De_Limite_Aba_Produtos_Oculta_Gestor() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        tirarScreenshot("Menu consultas Ativa em Portugues");
        espera(2);
        cabecalhoLogado.clicarMenuConsultas();
        esperaElemento(limiteGestor.msgTitulo, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        limiteGestor.clicarTitulo();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitulo);
        esperaElemento(limiteGestor.lblPortadores, 60);
        esperaElemento(limiteGestor.lblEmpresas, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadores));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresas));
        Assert.assertEquals("Consulta de Limite", limiteGestor.msgTitulo.getText());
        Assert.assertEquals("Portadores", limiteGestor.lblPortadores.getText());
        Assert.assertEquals("Empresas", limiteGestor.lblEmpresas.getText());
        espera(4);
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba produtos Oculta Gestor em Portugues");
    }
    
    @Then("Verifico como Gestor que a aba Portadores em Consulta de Limites esta Oculta")
    public void verifico_Tela_Consulta_De_Limite_Aba_Portadores_Oculta_Gestor() throws InterruptedException {
    	Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        tirarScreenshot("Menu consultas Ativa em Portugues");
        espera(2);
        cabecalhoLogado.clicarMenuConsultas();
        esperaElemento(limiteGestor.msgTitulo, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        limiteGestor.clicarTitulo();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitulo);
        esperaElemento(limiteGestor.lblPortadores, 60);
        esperaElemento(limiteGestor.lblEmpresas, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadores));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresas));
        Assert.assertEquals("Consulta de Limite", limiteGestor.msgTitulo.getText());
        Assert.assertEquals("Produtos", limiteGestor.lblProdutos.getText());
        Assert.assertEquals("Empresas", limiteGestor.lblEmpresas.getText());
        espera(4);
        tirarScreenshot("Tela Consulta de Limites Gestor sem a aba Portadores");
        limiteGestor.clicarPortadores();
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba Portadores Oculta Gestor em Portugues");
    }
    
    @Then("Verifico como Gestor  que a aba Empresas em Consulta de Limites esta Oculta")
    public void verifico_Tela_Consulta_De_Limite_Aba_Empresas_Oculta_Gestor() throws InterruptedException {
    	Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        tirarScreenshot("Menu consultas Ativa em Portugues");
        espera(2);
        cabecalhoLogado.clicarMenuConsultas();
        esperaElemento(limiteGestor.msgTitulo, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        limiteGestor.clicarTitulo();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitulo);
        esperaElemento(limiteGestor.lblPortadores, 60);
        esperaElemento(limiteGestor.lblProdutos, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadores));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresas));
        Assert.assertEquals("Consulta de Limite", limiteGestor.msgTitulo.getText());
        Assert.assertEquals("Portadores", limiteGestor.lblPortadores.getText());
        Assert.assertEquals("Produtos", limiteGestor.lblProdutos.getText());
        espera(4);
        tirarScreenshot("Tela Consulta de Limites Gestor  sem a aba Produtos");
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba Empresas Oculta Gestor em Ingles");
    }
    
    @Then("Verifico como Gestor Portador que a aba Produtos em Consulta de Limites esta Inativa em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Aba_Produtos_Inativa_Gestor_Portador_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu consultas Ativa em Ingl�s");
        espera(2);
        cabecalhoLogado.clickMainConsultations();
        esperaElemento(limiteGestor.msgTitleEn, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        limiteGestor.clickTitle();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitleEn);
        esperaElemento(limiteGestor.lblPortadoresIngles, 60);
        esperaElemento(limiteGestor.lblEmpresasIngles, 60);
        esperaElemento(limiteGestor.lblProdutosIngles, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadoresIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresasIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutosIngles));
        Assert.assertEquals("Limit Consultation", limiteGestor.msgTitleEn.getText());
        Assert.assertEquals("Carholders", limiteGestor.lblPortadoresIngles.getText());
        Assert.assertEquals("Companies", limiteGestor.lblEmpresasIngles.getText());
        Assert.assertEquals("Products", limiteGestor.lblProdutosIngles.getText());
        espera(2);
        tirarScreenshot("Tela Consulta de Limites Gestor Portador aba Produtos Inativa em Ingles");
        limiteGestor.clicarMenuProdutosIngles();
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba produtos Inativa Gestor em Ingles");
    }
    
    @Then("Verifico como Gestor Portador que a aba Portadores em Consulta de Limites esta Inativa em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Aba_Portadores_Inativa_Gestor_Portador_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu consultas Ativa em Ingl�s");
        espera(2);
        cabecalhoLogado.clickMainConsultations();
        esperaElemento(limiteGestor.msgTitleEn, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        limiteGestor.clickTitle();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitleEn);
        esperaElemento(limiteGestor.lblPortadoresIngles, 60);
        esperaElemento(limiteGestor.lblEmpresasIngles, 60);
        esperaElemento(limiteGestor.lblProdutosIngles, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadoresIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresasIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutosIngles));
        Assert.assertEquals("Limit Consultation", limiteGestor.msgTitleEn.getText());
        Assert.assertEquals("Carholders", limiteGestor.lblPortadoresIngles.getText());
        Assert.assertEquals("Companies", limiteGestor.lblEmpresasIngles.getText());
        Assert.assertEquals("Products", limiteGestor.lblProdutosIngles.getText());
        espera(2);
        tirarScreenshot("Tela Consulta de Limites Gestor Portador aba Portadores Inativa em Ingles");
        limiteGestor.clicarMenuPortadoresIngles();
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba Produtos Inativa Gestor em Ingles");
    }
    
    @Then("Verifico como Gestor Portador que a aba Empresas em Consulta de Limites esta Inativa em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Aba_Empresas_Inativa_Gestor_Portador_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu consultas Ativa em Ingl�s");
        espera(2);
        cabecalhoLogado.clickMainConsultations();
        esperaElemento(limiteGestor.msgTitleEn, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        limiteGestor.clickTitle();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitleEn);
        esperaElemento(limiteGestor.lblPortadoresIngles, 60);
        esperaElemento(limiteGestor.lblEmpresasIngles, 60);
        esperaElemento(limiteGestor.lblProdutosIngles, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadoresIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresasIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutosIngles));
        Assert.assertEquals("Limit Consultation", limiteGestor.msgTitleEn.getText());
        Assert.assertEquals("Carholders", limiteGestor.lblPortadoresIngles.getText());
        Assert.assertEquals("Companies", limiteGestor.lblEmpresasIngles.getText());
        Assert.assertEquals("Products", limiteGestor.lblProdutosIngles.getText());
        espera(2);
        tirarScreenshot("Tela Consulta de Limites Gestor Portador aba Empresas Inativa em Ingles");
        limiteGestor.clicarMenuEmpresasIngles();
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba Empresas Inativa Gestor em Ingles");
    }
    
    @Then("Verifico como Gestor Portador que a aba Produtos em Consulta de Limites esta Inativa")
    public void verifico_Tela_Consulta_De_Limite_Aba_Produtos_Inativa_Gestor_Portador() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        tirarScreenshot("Menu consultas Ativa em Portugues");
        espera(2);
        cabecalhoLogado.clicarMenuConsultas();
        esperaElemento(limiteGestor.msgTitulo, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        limiteGestor.clicarTitulo();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitulo);
        esperaElemento(limiteGestor.lblPortadores, 60);
        esperaElemento(limiteGestor.lblEmpresas, 60);
        esperaElemento(limiteGestor.lblProdutos, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadores));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresas));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutos));
        Assert.assertEquals("Consulta de Limite", limiteGestor.msgTitulo.getText());
        Assert.assertEquals("Portadores", limiteGestor.lblPortadores.getText());
        Assert.assertEquals("Empresas", limiteGestor.lblEmpresas.getText());
        Assert.assertEquals("Produtos", limiteGestor.lblProdutos.getText());
        espera(2);
        tirarScreenshot("Tela Consulta de Limites Gestor Portador aba Produtos Inativa");
        limiteGestor.clicarMenuProdutos();
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba produtos Inativa Gestor em Portugues");
    }
    
    @Then("Verifico como Gestor Portador que a aba Portadores em Consulta de Limites esta Inativa")
    public void verifico_Tela_Consulta_De_Limite_Aba_Portadores_Inativa_Gestor_Portador() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        tirarScreenshot("Menu consultas Ativa em Portugues");
        espera(2);
        cabecalhoLogado.clicarMenuConsultas();
        esperaElemento(limiteGestor.msgTitulo, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        limiteGestor.clicarTitulo();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitulo);
        esperaElemento(limiteGestor.lblPortadores, 60);
        esperaElemento(limiteGestor.lblEmpresas, 60);
        esperaElemento(limiteGestor.lblProdutos, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadores));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresas));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutos));
        Assert.assertEquals("Consulta de Limite", limiteGestor.msgTitulo.getText());
        Assert.assertEquals("Portadores", limiteGestor.lblPortadores.getText());
        Assert.assertEquals("Empresas", limiteGestor.lblEmpresas.getText());
        Assert.assertEquals("Produtos", limiteGestor.lblProdutos.getText());
        espera(2);
        tirarScreenshot("Tela Consulta de Limites Gestor Portador aba Portadores Inativa");
        limiteGestor.clicarMenuPortadores();
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba Portadores Inativa Gestor em Portugues");
    }
    
    @Then("Verifico como Gestor Portador que a aba Empresas em Consulta de Limites esta Inativa")
    public void verifico_Tela_Consulta_De_Limite_Aba_Empresas_Inativa_Gestor_Portador() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        tirarScreenshot("Menu consultas Ativa em Portugues");
        espera(2);
        cabecalhoLogado.clicarMenuConsultas();
        esperaElemento(limiteGestor.msgTitulo, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        limiteGestor.clicarTitulo();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitulo);
        esperaElemento(limiteGestor.lblPortadores, 60);
        esperaElemento(limiteGestor.lblEmpresas, 60);
        esperaElemento(limiteGestor.lblProdutos, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadores));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresas));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutos));
        Assert.assertEquals("Consulta de Limite", limiteGestor.msgTitulo.getText());
        Assert.assertEquals("Portadores", limiteGestor.lblPortadores.getText());
        Assert.assertEquals("Empresas", limiteGestor.lblEmpresas.getText());
        Assert.assertEquals("Produtos", limiteGestor.lblProdutos.getText());
        espera(2);
        tirarScreenshot("Tela Consulta de Limites Gestor Portador aba Empresas Inativa");
        limiteGestor.clicarMenuEmpresas();
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba Empresas Inativa Gestor em Portugues");
    }
    
    @Then("Verifico como Gestor Portador que a aba Produtos em Consulta de Limites esta Oculta em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Aba_Produtos_Oculta_Gestor_Portador_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu consultas Ativa em Ingl�s");
        espera(2);
        cabecalhoLogado.clickMainConsultations();
        esperaElemento(limiteGestor.msgTitleEn, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        limiteGestor.clickTitle();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitleEn);
        esperaElemento(limiteGestor.lblPortadoresIngles, 60);
        esperaElemento(limiteGestor.lblEmpresasIngles, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadoresIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresasIngles));
        Assert.assertTrue(!elementoExiste(limiteGestor.lblProdutosIngles));
        Assert.assertEquals("Limit Consultation", limiteGestor.msgTitleEn.getText());
        Assert.assertEquals("Carholders", limiteGestor.lblPortadoresIngles.getText());
        Assert.assertEquals("Companies", limiteGestor.lblEmpresasIngles.getText());
        espera(4);
        tirarScreenshot("Tela Consulta de Limites Gestor Portador em Ingles sem a aba Produtos");
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba produtos Oculta Gestor em Ingles");
    }
    
    @Then("Verifico como Gestor Portador que a aba Portadores em Consulta de Limites esta Oculta em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Aba_Portadores_Oculta_Gestor_Portador_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu consultas Ativa em Ingl�s");
        espera(2);
        cabecalhoLogado.clickMainConsultations();
        esperaElemento(limiteGestor.msgTitleEn, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        limiteGestor.clickTitle();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitleEn);
        esperaElemento(limiteGestor.lblEmpresasIngles, 60);
        esperaElemento(limiteGestor.lblProdutosIngles, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresasIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutosIngles));
        Assert.assertTrue(!elementoExiste(limiteGestor.lblPortadoresIngles));
        Assert.assertEquals("Limit Consultation", limiteGestor.msgTitleEn.getText());
        Assert.assertEquals("Companies", limiteGestor.lblEmpresasIngles.getText());
        Assert.assertEquals("Products", limiteGestor.lblProdutosIngles.getText());
        espera(4);
        tirarScreenshot("Tela Consulta de Limites Gestor Portador em Ingles sem a aba Portadores");
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba Ocultas Inativa Gestor em Ingles");
    }
    
    @Then("Verifico como Gestor Portador que a aba Empresas em Consulta de Limites esta Oculta em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Aba_Empresas_Oculta_Gestor_Portador_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu consultas Ativa em Ingl�s");
        espera(2);
        cabecalhoLogado.clickMainConsultations();
        esperaElemento(limiteGestor.msgTitleEn, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        limiteGestor.clickTitle();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitleEn);
        esperaElemento(limiteGestor.lblPortadoresIngles, 60);
        esperaElemento(limiteGestor.lblEmpresasIngles, 60);
        esperaElemento(limiteGestor.lblProdutosIngles, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadoresIngles));
        Assert.assertTrue(!elementoExiste(limiteGestor.lblEmpresasIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutosIngles));
        Assert.assertEquals("Limit Consultation", limiteGestor.msgTitleEn.getText());
        Assert.assertEquals("Carholders", limiteGestor.lblPortadoresIngles.getText());
        Assert.assertEquals("Products", limiteGestor.lblProdutosIngles.getText());
        espera(4);
        tirarScreenshot("Tela Consulta de Limites Gestor Portador em Ingles sem a aba Produtos");
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba Empresas Oculta Gestor em Ingles");
    }
    
    @Then("Verifico como Gestor Portador que a aba Produtos em Consulta de Limites esta Oculta")
    public void verifico_Tela_Consulta_De_Limite_Aba_Produtos_Oculta_Gestor_Portador() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        tirarScreenshot("Menu consultas Ativa em Portugues");
        espera(2);
        cabecalhoLogado.clicarMenuConsultas();
        esperaElemento(limiteGestor.msgTitulo, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        limiteGestor.clicarTitulo();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitulo);
        esperaElemento(limiteGestor.lblPortadores, 60);
        esperaElemento(limiteGestor.lblEmpresas, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadores));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresas));
        Assert.assertTrue(!elementoExiste(limiteGestor.lblProdutos));
        Assert.assertEquals("Consulta de Limite", limiteGestor.msgTitulo.getText());
        Assert.assertEquals("Portadores", limiteGestor.lblPortadores.getText());
        Assert.assertEquals("Empresas", limiteGestor.lblEmpresas.getText());
        espera(4);
        tirarScreenshot("Tela Consulta de Limites Gestor Portador sem a aba Produtos");
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba produtos Oculta Gestor em Portugues");
    }
    
    @Then("Verifico como Gestor Portador que a aba Portadores em Consulta de Limites esta Oculta")
    public void verifico_Tela_Consulta_De_Limite_Aba_Portadores_Oculta_Gestor_Portador() throws InterruptedException {
    	Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        tirarScreenshot("Menu consultas Ativa em Portugues");
        espera(2);
        cabecalhoLogado.clicarMenuConsultas();
        esperaElemento(limiteGestor.msgTitulo, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        limiteGestor.clicarTitulo();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitulo);
        esperaElemento(limiteGestor.lblProdutos, 60);
        esperaElemento(limiteGestor.lblEmpresas, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutos));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresas));
        Assert.assertTrue(!elementoExiste(limiteGestor.lblPortadores));
        Assert.assertEquals("Consulta de Limite", limiteGestor.msgTitulo.getText());
        Assert.assertEquals("Produtos", limiteGestor.lblProdutos.getText());
        Assert.assertEquals("Empresas", limiteGestor.lblEmpresas.getText());
        espera(4);
        tirarScreenshot("Tela Consulta de Limites Gestor Portador sem a aba Portadores");
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba Portadores Oculta Gestor em Portugues");
    }
    
    @Then("Verifico como Gestor Portador que a aba Empresas em Consulta de Limites esta Oculta")
    public void verifico_Tela_Consulta_De_Limite_Aba_Empresas_Oculta_Gestor_Portador() throws InterruptedException {
    	Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        tirarScreenshot("Menu consultas Ativa em Portugues");
        espera(2);
        cabecalhoLogado.clicarMenuConsultas();
        esperaElemento(limiteGestor.msgTitulo, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        limiteGestor.clicarTitulo();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitulo);
        esperaElemento(limiteGestor.lblPortadores, 60);
        esperaElemento(limiteGestor.lblProdutos, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadores));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutos));
        Assert.assertTrue(!elementoExiste(limiteGestor.lblEmpresas));
        Assert.assertEquals("Consulta de Limite", limiteGestor.msgTitulo.getText());
        Assert.assertEquals("Portadores", limiteGestor.lblPortadores.getText());
        Assert.assertEquals("Produtos", limiteGestor.lblProdutos.getText());
        espera(4);
        tirarScreenshot("Tela Consulta de Limites Gestor Portador sem a aba Produtos");
        limiteGestor.clicarEmpresas();
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba Empresas Oculta Gestor em Ingles");
    }
    @Then("Verifico como Gestor Estrangeiro que a aba Produtos em Consulta de Limites esta Inativa em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Aba_Produtos_Inativa_Gestor_Estrangeiro_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu consultas Ativa em Ingl�s");
        espera(2);
        cabecalhoLogado.clickMainConsultations();
        esperaElemento(limiteGestor.msgTitleEn, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        limiteGestor.clickTitle();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitleEn);
        esperaElemento(limiteGestor.lblPortadoresIngles, 60);
        esperaElemento(limiteGestor.lblEmpresasIngles, 60);
        esperaElemento(limiteGestor.lblProdutosIngles, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadoresIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresasIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutosIngles));
        Assert.assertEquals("Limit Consultation", limiteGestor.msgTitleEn.getText());
        Assert.assertEquals("Carholders", limiteGestor.lblPortadoresIngles.getText());
        Assert.assertEquals("Companies", limiteGestor.lblEmpresasIngles.getText());
        Assert.assertEquals("Products", limiteGestor.lblProdutosIngles.getText());
        espera(4);
        tirarScreenshot("Tela Consulta de Limites Gestor Portador em Ingles sem a aba Produtos");
        limiteGestor.clicarProdutos();
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        espera(1);
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba produtos Oculta Gestor em Ingles");
    }
    
    @Then("Verifico como Gestor Estrangeiro que a aba Portadores em Consulta de Limites esta Inativa em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Aba_Portadores_Inativa_Gestor_Estrangeiro_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu consultas Ativa em Ingl�s");
        espera(2);
        cabecalhoLogado.clickMainConsultations();
        esperaElemento(limiteGestor.msgTitleEn, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        limiteGestor.clickTitle();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitleEn);
        esperaElemento(limiteGestor.lblPortadoresIngles, 60);
        esperaElemento(limiteGestor.lblEmpresasIngles, 60);
        esperaElemento(limiteGestor.lblProdutosIngles, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadoresIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresasIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutosIngles));
        Assert.assertEquals("Limit Consultation", limiteGestor.msgTitleEn.getText());
        Assert.assertEquals("Carholders", limiteGestor.lblPortadoresIngles.getText());
        Assert.assertEquals("Companies", limiteGestor.lblEmpresasIngles.getText());
        Assert.assertEquals("Products", limiteGestor.lblProdutosIngles.getText());
        espera(4);
        tirarScreenshot("Tela Consulta de Limites Gestor Estrangeiro em Ingles sem a aba Portadores");
        limiteGestor.clicarPortadores();
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        espera(1);
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba Ocultas Inativa Gestor em Ingles");
    }
    
    @Then("Verifico como Gestor Estrangeiro que a aba Empresas em Consulta de Limites esta Inativa em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Aba_Empresas_Inativa_Gestor_Estrangeiro_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu consultas Ativa em Ingl�s");
        espera(2);
        cabecalhoLogado.clickMainConsultations();
        esperaElemento(limiteGestor.msgTitleEn, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        limiteGestor.clickTitle();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitleEn);
        esperaElemento(limiteGestor.lblPortadoresIngles, 60);
        esperaElemento(limiteGestor.lblEmpresasIngles, 60);
        esperaElemento(limiteGestor.lblProdutosIngles, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadoresIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresasIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutosIngles));
        Assert.assertEquals("Limit Consultation", limiteGestor.msgTitleEn.getText());
        Assert.assertEquals("Carholders", limiteGestor.lblPortadoresIngles.getText());
        Assert.assertEquals("Companies", limiteGestor.lblEmpresasIngles.getText());
        Assert.assertEquals("Products", limiteGestor.lblProdutosIngles.getText());
        espera(4);
        tirarScreenshot("Tela Consulta de Limites Gestor Estrangeiro em Ingles sem a aba Produtos");
        limiteGestor.clicarEmpresas();
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba Empresas Oculta Gestor em Ingles");
    }
    
    @Then("Verifico como Gestor Estrangeiro que a aba Produtos em Consulta de Limites esta Inativa")
    public void verifico_Tela_Consulta_De_Limite_Aba_Produtos_Inativa_Gestor_Estrangeiro() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        tirarScreenshot("Menu consultas Ativa em Portugues");
        espera(2);
        cabecalhoLogado.clicarMenuConsultas();
        esperaElemento(limiteGestor.msgTitulo, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        limiteGestor.clicarTitulo();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitulo);
        esperaElemento(limiteGestor.lblPortadores, 60);
        esperaElemento(limiteGestor.lblEmpresas, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadores));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresas));
        Assert.assertEquals("Consulta de Limite", limiteGestor.msgTitulo.getText());
        Assert.assertEquals("Portadores", limiteGestor.lblPortadores.getText());
        Assert.assertEquals("Empresas", limiteGestor.lblEmpresas.getText());
        espera(4);
        tirarScreenshot("Tela Consulta de Limites Gestor Estrangeiro sem a aba Produtos");
        limiteGestor.clicarProdutos();
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba produtos Oculta Gestor em Portugues");
    }
    
    @Then("Verifico como Gestor Estrangeiro que a aba Portadores em Consulta de Limites esta Inativa")
    public void verifico_Tela_Consulta_De_Limite_Aba_Portadores_Inativa_Gestor_Estrangeiro() throws InterruptedException {
    	Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        tirarScreenshot("Menu consultas Ativa em Portugues");
        espera(2);
        cabecalhoLogado.clicarMenuConsultas();
        esperaElemento(limiteGestor.msgTitulo, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        limiteGestor.clicarTitulo();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitulo);
        esperaElemento(limiteGestor.lblPortadores, 60);
        esperaElemento(limiteGestor.lblEmpresas, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadores));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresas));
        Assert.assertEquals("Consulta de Limite", limiteGestor.msgTitulo.getText());
        Assert.assertEquals("Produtos", limiteGestor.lblProdutos.getText());
        Assert.assertEquals("Empresas", limiteGestor.lblEmpresas.getText());
        espera(4);
        tirarScreenshot("Tela Consulta de Limites Gestor Estrangeiro sem a aba Portadores");
        limiteGestor.clicarPortadores();
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba Portadores Oculta Gestor em Portugues");
    }
    
    @Then("Verifico como Gestor Estrangeiro que a aba Empresas em Consulta de Limites esta Inativa")
    public void verifico_Tela_Consulta_De_Limite_Aba_Empresas_Inativa_Gestor_Estrangeiro() throws InterruptedException {
    	Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        tirarScreenshot("Menu consultas Ativa em Portugues");
        espera(2);
        cabecalhoLogado.clicarMenuConsultas();
        esperaElemento(limiteGestor.msgTitulo, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        limiteGestor.clicarTitulo();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitulo);
        esperaElemento(limiteGestor.lblPortadores, 60);
        esperaElemento(limiteGestor.lblEmpresas, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadores));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresas));
        Assert.assertEquals("Consulta de Limite", limiteGestor.msgTitulo.getText());
        Assert.assertEquals("Portadores", limiteGestor.lblPortadores.getText());
        Assert.assertEquals("Produtos", limiteGestor.lblProdutos.getText());
        espera(4);
        tirarScreenshot("Tela Consulta de Limites Gestor Estrangeiro sem a aba Produtos");
        limiteGestor.clicarEmpresas();
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba Empresas Oculta Gestor em Ingles");
    }
    
    @Then("Verifico como Gestor Estrangeiro que a aba Produtos em Consulta de Limites esta Oculta em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Aba_Produtos_Oculta_Gestor_Estrangeiro_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu consultas Ativa em Ingl�s");
        espera(2);
        cabecalhoLogado.clickMainConsultations();
        esperaElemento(limiteGestor.msgTitleEn, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        limiteGestor.clickTitle();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitleEn);
        esperaElemento(limiteGestor.lblPortadoresIngles, 60);
        esperaElemento(limiteGestor.lblEmpresasIngles, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadoresIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresasIngles));
        Assert.assertTrue(!elementoExiste(limiteGestor.lblProdutosIngles));
        Assert.assertEquals("Limit Consultation", limiteGestor.msgTitleEn.getText());
        Assert.assertEquals("Carholders", limiteGestor.lblPortadoresIngles.getText());
        Assert.assertEquals("Companies", limiteGestor.lblEmpresasIngles.getText());
        espera(4);
        tirarScreenshot("Tela Consulta de Limites Gestor Portador em Ingles sem a aba Produtos");
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba produtos Oculta Gestor em Ingles");
    }
    
    @Then("Verifico como Gestor Estrangeiro que a aba Portadores em Consulta de Limites esta Oculta em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Aba_Portadores_Oculta_Gestor_Estrangeiro_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu consultas Ativa em Ingl�s");
        espera(2);
        cabecalhoLogado.clickMainConsultations();
        esperaElemento(limiteGestor.msgTitleEn, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        limiteGestor.clickTitle();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitleEn);
        esperaElemento(limiteGestor.lblPortadoresIngles, 60);
        esperaElemento(limiteGestor.lblEmpresasIngles, 60);
        esperaElemento(limiteGestor.lblProdutosIngles, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        Assert.assertTrue(!elementoExiste(limiteGestor.lblPortadoresIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresasIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutosIngles));
        Assert.assertEquals("Limit Consultation", limiteGestor.msgTitleEn.getText());
        Assert.assertEquals("Companies", limiteGestor.lblEmpresasIngles.getText());
        Assert.assertEquals("Products", limiteGestor.lblProdutosIngles.getText());
        espera(4);
        tirarScreenshot("Tela Consulta de Limites Gestor Estrangeiro em Ingles sem a aba Portadores");
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba Ocultas Inativa Gestor em Ingles");
    }
    
    @Then("Verifico como Gestor Estrangeiro que a aba Empresas em Consulta de Limites esta Oculta em Ingles")
    public void verifico_Tela_Consulta_De_Limite_Aba_Empresas_Oculta_Gestor_Estrangeiro_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu consultas Ativa em Ingl�s");
        espera(2);
        cabecalhoLogado.clickMainConsultations();
        esperaElemento(limiteGestor.msgTitleEn, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        limiteGestor.clickTitle();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitleEn);
        esperaElemento(limiteGestor.lblPortadoresIngles, 60);
        esperaElemento(limiteGestor.lblEmpresasIngles, 60);
        esperaElemento(limiteGestor.lblProdutosIngles, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitleEn));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadoresIngles));
        Assert.assertTrue(!elementoExiste(limiteGestor.lblEmpresasIngles));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutosIngles));
        Assert.assertEquals("Limit Consultation", limiteGestor.msgTitleEn.getText());
        Assert.assertEquals("Carholders", limiteGestor.lblPortadoresIngles.getText());
        Assert.assertEquals("Products", limiteGestor.lblProdutosIngles.getText());
        espera(4);
        tirarScreenshot("Tela Consulta de Limites Gestor Estrangeiro em Ingles sem a aba Produtos");
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba Empresas Oculta Gestor em Ingles");
    }
    
    @Then("Verifico como Gestor Estrangeiro que a aba Produtos em Consulta de Limites esta Oculta")
    public void verifico_Tela_Consulta_De_Limite_Aba_Produtos_Oculta_Gestor_Estrangeiro() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        tirarScreenshot("Menu consultas Ativa em Portugues");
        espera(2);
        cabecalhoLogado.clicarMenuConsultas();
        esperaElemento(limiteGestor.msgTitulo, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        limiteGestor.clicarTitulo();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitulo);
        esperaElemento(limiteGestor.lblPortadores, 60);
        esperaElemento(limiteGestor.lblEmpresas, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadores));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresas));
        Assert.assertTrue(!elementoExiste(limiteGestor.lblProdutos));
        Assert.assertEquals("Consulta de Limite", limiteGestor.msgTitulo.getText());
        Assert.assertEquals("Portadores", limiteGestor.lblPortadores.getText());
        Assert.assertEquals("Empresas", limiteGestor.lblEmpresas.getText());
        espera(4);
        tirarScreenshot("Tela Consulta de Limites Gestor Estrangeiro sem a aba Produtos");
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba produtos Oculta Gestor em Portugues");
    }
    
    @Then("Verifico como Gestor Estrangeiro que a aba Portadores em Consulta de Limites esta Oculta")
    public void verifico_Tela_Consulta_De_Limite_Aba_Portadores_Oculta_Gestor_Estrangeiro() throws InterruptedException {
    	Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        tirarScreenshot("Menu consultas Ativa em Portugues");
        espera(2);
        cabecalhoLogado.clicarMenuConsultas();
        esperaElemento(limiteGestor.msgTitulo, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        limiteGestor.clicarTitulo();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitulo);
        esperaElemento(limiteGestor.lblPortadores, 60);
        esperaElemento(limiteGestor.lblEmpresas, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutos));
        Assert.assertTrue(elementoExiste(limiteGestor.lblEmpresas));
        Assert.assertTrue(!elementoExiste(limiteGestor.lblPortadores));
        Assert.assertEquals("Consulta de Limite", limiteGestor.msgTitulo.getText());
        Assert.assertEquals("Produtos", limiteGestor.lblProdutos.getText());
        Assert.assertEquals("Empresas", limiteGestor.lblEmpresas.getText());
        espera(4);
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba Portadores Oculta Gestor em Portugues");
    }
    
    @Then("Verifico como Gestor Estrangeiro que a aba Empresas em Consulta de Limites esta Oculta")
    public void verifico_Tela_Consulta_De_Limite_Aba_Empresas_Oculta_Gestor_Estrangeiro() throws InterruptedException {
    	Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        tirarScreenshot("Menu consultas Ativa em Portugues");
        espera(2);
        cabecalhoLogado.clicarMenuConsultas();
        esperaElemento(limiteGestor.msgTitulo, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        limiteGestor.clicarTitulo();
        espera(1);
        scrollAteOElemento(limiteGestor.msgTitulo);
        esperaElemento(limiteGestor.lblPortadores, 60);
        esperaElemento(limiteGestor.lblProdutos, 60);
        Assert.assertTrue(elementoExiste(limiteGestor.msgTitulo));
        Assert.assertTrue(elementoExiste(limiteGestor.lblPortadores));
        Assert.assertTrue(elementoExiste(limiteGestor.lblProdutos));
        Assert.assertTrue(!elementoExiste(limiteGestor.lblEmpresas));
        Assert.assertEquals("Consulta de Limite", limiteGestor.msgTitulo.getText());
        Assert.assertEquals("Portadores", limiteGestor.lblPortadores.getText());
        Assert.assertEquals("Produtos", limiteGestor.lblProdutos.getText());
        espera(4);
        tirarScreenshot("Tela Consulta de Limites Gestor Estrangeiro sem a aba Produtos");
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Consulta de Limites aba Empresas Oculta Gestor em Ingles");
    }
    
    @Then("Verifico como Gestor que a tela Relat�rios esta Inativo em Ingles")
    public void verifico_Tela_Relatorios_Inativa_Gestor_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        scrollAteOElemento(cabecalhoLogado.lblMainConsultations);
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu Relat�rios Inativo em Ingl�s");
        espera(3);
        tirarScreenshot("Submenu Relatorios Inativo em Ingl�s");
        esperaElemento(cabecalhoLogado.hrefRelatorio, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.hrefRelatorio));
        espera(2);
        abrirUrl("https://192.168.245.198/consulta/relatorio/");
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(cabecalhoLogado.lblRelatoriosEn, 60);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblRelatoriosEn));
        Assert.assertEquals("Reports", cabecalhoLogado.lblRelatoriosEn.getText());
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        tirarScreenshot("Tela Consulta de Limites Gestor Inativa e mensagem de Servi�o Indispon�vel");
        espera(1);
        scrollToTop();
        tirarScreenshot("Funcionalidade Relat�rios Inativo");
    }

    @Then("Verifico como Gestor que a tela Relat�rios esta Oculto em Ingles")
    public void verifico_Tela_Relatorio_Oculta_Gestor_Ingles() throws InterruptedException {
    	Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        scrollAteOElemento(cabecalhoLogado.lblMainConsultations);
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu Relat�rios Oculto em Ingl�s");
        espera(3);
        tirarScreenshot("Submenu Relatorios Oculto  em Ingl�s");
        Assert.assertTrue(!elementoExiste(cabecalhoLogado.hrefRelatorio));
        espera(2);
        abrirUrl("https://192.168.245.198/consulta/relatorio/");
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(cabecalhoLogado.lblRelatoriosEn, 60);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblRelatoriosEn));
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Reports", cabecalhoLogado.lblRelatoriosEn.getText());
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        tirarScreenshot("Tela Consulta de Limites Gestor Oculto e mensagem de Servi�o Indispon�vel");
        espera(1);
        scrollToTop();
        tirarScreenshot("Tela Consulta de Limites Gestor  Oculto");
    }

    @Then("Verifico como Gestor que a tela Relat�rios esta Ativa em Ingles")
    public void verifico_Tela_Relatorios_Ativa_Gestor_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.hrefRelatorio));
        tirarScreenshot("Menu Relat�rios Ativa em Ingl�s");
        espera(2);
        cabecalhoLogado.clickRelatorios();
        esperaElemento(cabecalhoLogado.msgRelatoriosEn, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.msgRelatoriosEn));
        cabecalhoLogado.clickRelatoriosEn();
        espera(1);
        scrollAteOElemento(cabecalhoLogado.msgRelatoriosEn);
        esperaElemento(cabecalhoLogado.msgRelatoriosEn, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.msgRelatoriosEn));
        Assert.assertEquals("Reports", cabecalhoLogado.msgRelatoriosEn.getText());
        espera(2);
        tirarScreenshot("Tela Relat�rios Gestor Portador Ativo em Ingles");
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Relat�rios Gestor Portador Ativo em Ingles");
    }
    
    @Then("Verifico como Gestor que a tela Relat�rios esta Inativo")
    public void verifico_Tela_Relatorios_Inativa_Gestor() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        scrollAteOElemento(cabecalhoLogado.lblMenuConsultas);
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        tirarScreenshot("Menu Relat�rios Inativo");
        espera(3);
        tirarScreenshot("Submenu Relatorios Inativo");
        esperaElemento(cabecalhoLogado.hrefRelatorio, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.hrefRelatorio));
        espera(2);
        abrirUrl("https://192.168.245.198/consulta/relatorio/");
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(cabecalhoLogado.lblRelatorios, 60);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblRelatorios));
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Relat�rios", cabecalhoLogado.lblRelatorios.getText());
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        tirarScreenshot("Tela Consulta de Limites Gestor Inativa e mensagem de Servi�o Indispon�vel");
        espera(1);
        scrollToTop();
        tirarScreenshot("Funcionalidade Relat�rios Inativo");
    }

    @Then("Verifico como Gestor que a tela Relat�rios esta Oculto")
    public void verifico_Tela_Relatorio_Oculta_Gestor() throws InterruptedException {
    	Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        scrollAteOElemento(cabecalhoLogado.lblMenuConsultas);
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        tirarScreenshot("Menu Relat�rios Inativo");
        espera(3);
        tirarScreenshot("Submenu Relatorios Oculto");
        Assert.assertTrue(!elementoExiste(cabecalhoLogado.hrefRelatorio));
        espera(2);
        abrirUrl("https://192.168.245.198/consulta/relatorio/");
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(cabecalhoLogado.lblRelatorios, 60);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblRelatorios));
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Relat�rios", cabecalhoLogado.lblRelatorios.getText());
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        tirarScreenshot("Tela Consulta de Limites Gestor Oculto e mensagem de Servi�o Indispon�vel");
        espera(1);
        scrollToTop();
        tirarScreenshot("Tela Consulta de Limites Gestor Oculto");
    }

    @Then("Verifico como Gestor que a tela Relat�rios esta Ativa")
    public void verifico_Tela_Relatorios_Ativa_Gestor() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.hrefRelatorio));
        tirarScreenshot("Menu Relat�rios Ativa");
        espera(2);
        cabecalhoLogado.clickRelatorios();
        esperaElemento(cabecalhoLogado.msgRelatorios, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.msgRelatorios));
        cabecalhoLogado.clickRelatoriosPt();
        espera(1);
        scrollAteOElemento(cabecalhoLogado.msgRelatorios);
        esperaElemento(cabecalhoLogado.msgRelatorios, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.msgRelatorios));
        Assert.assertEquals("Relat�rios", cabecalhoLogado.msgRelatorios.getText());
        espera(2);
        tirarScreenshot("Tela Relat�rios Gestor Portador Ativo");
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Relat�rios Gestor Portador Ativo");
    }
    
    @Then("Verifico como Gestor Portador que a tela Relat�rios esta Inativo")
    public void verifico_Tela_Relatorios_Inativa_Gestor_Portador() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        scrollAteOElemento(cabecalhoLogado.lblMenuConsultas);
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        tirarScreenshot("Menu Relat�rios Inativo");
        espera(3);
        tirarScreenshot("Submenu Relatorios Inativo");
        esperaElemento(cabecalhoLogado.hrefRelatorio, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.hrefRelatorio));
        espera(2);
        abrirUrl("https://192.168.245.198/consulta/relatorio/");
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(cabecalhoLogado.lblRelatorios, 60);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblRelatorios));
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Relat�rios", cabecalhoLogado.lblRelatorios.getText());
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        tirarScreenshot("Tela Consulta de Limites Gestor Inativa e mensagem de Servi�o Indispon�vel");
        espera(1);
        scrollToTop();
        tirarScreenshot("Funcionalidade Relat�rios Inativo");
    }

    @Then("Verifico como Gestor Portador que a tela Relat�rios esta Oculto")
    public void verifico_Tela_Relatorio_Oculta_Gestor_Portador() throws InterruptedException {
    	Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        scrollAteOElemento(cabecalhoLogado.lblMenuConsultas);
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        tirarScreenshot("Menu Relat�rios Inativo");
        espera(3);
        tirarScreenshot("Submenu Relatorios Oculto");
        Assert.assertTrue(!elementoExiste(cabecalhoLogado.hrefRelatorio));
        espera(2);
        abrirUrl("https://192.168.245.198/consulta/relatorio/");
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(cabecalhoLogado.lblRelatorios, 60);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblRelatorios));
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Relat�rios", cabecalhoLogado.lblRelatorios.getText());
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        tirarScreenshot("Tela Consulta de Limites Gestor Oculto e mensagem de Servi�o Indispon�vel");
        espera(1);
        scrollToTop();
        tirarScreenshot("Tela Consulta de Limites Gestor Oculto");
    }

    @Then("Verifico como Gestor Portador que a tela Relat�rios esta Ativa")
    public void verifico_Tela_Relatorios_Ativa_Gestor_Portador() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.hrefRelatorio));
        tirarScreenshot("Menu Relat�rios Ativa");
        espera(2);
        cabecalhoLogado.clickRelatorios();
        esperaElemento(cabecalhoLogado.msgRelatorios, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.msgRelatorios));
        cabecalhoLogado.clickRelatoriosPt();
        espera(1);
        scrollAteOElemento(cabecalhoLogado.msgRelatorios);
        esperaElemento(cabecalhoLogado.msgRelatorios, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.msgRelatorios));
        Assert.assertEquals("Relat�rios", cabecalhoLogado.msgRelatorios.getText());
        espera(2);
        tirarScreenshot("Tela Relat�rios Gestor Estrangeiro Ativo");
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Relat�rios Gestor Estrangeiro Ativo");
    }
    
    @Then("Verifico como Gestor Estrangeiro que a tela Relat�rios esta Inativo")
    public void verifico_Tela_Relatorios_Inativa_Gestor_Estrangeiro() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        scrollAteOElemento(cabecalhoLogado.lblMenuConsultas);
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        tirarScreenshot("Menu Relat�rios Inativo");
        espera(3);
        tirarScreenshot("Submenu Relatorios Inativo");
        esperaElemento(cabecalhoLogado.hrefRelatorio, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.hrefRelatorio));
        espera(2);
        abrirUrl("https://192.168.245.198/consulta/relatorio/");
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(cabecalhoLogado.lblRelatorios, 60);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblRelatorios));
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Relat�rios", cabecalhoLogado.lblRelatorios.getText());
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        tirarScreenshot("Tela Consulta de Limites Gestor Inativa e mensagem de Servi�o Indispon�vel");
        espera(1);
        scrollToTop();
        tirarScreenshot("Funcionalidade Relat�rios Inativo");
    }

    @Then("Verifico como Gestor Estrangeiro que a tela Relat�rios esta Oculto")
    public void verifico_Tela_Relatorio_Oculta_Gestor_Estrangeiro() throws InterruptedException {
    	Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        scrollAteOElemento(cabecalhoLogado.lblMenuConsultas);
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        tirarScreenshot("Menu Relat�rios Inativo");
        espera(3);
        tirarScreenshot("Submenu Relatorios Oculto");
        Assert.assertTrue(!elementoExiste(cabecalhoLogado.hrefRelatorio));
        espera(2);
        abrirUrl("https://192.168.245.198/consulta/relatorio/");
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(cabecalhoLogado.lblRelatorios, 60);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblRelatorios));
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Relat�rios", cabecalhoLogado.lblRelatorios.getText());
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        tirarScreenshot("Tela Consulta de Limites Gestor Oculto e mensagem de Servi�o Indispon�vel");
        espera(1);
        scrollToTop();
        tirarScreenshot("Tela Consulta de Limites Gestor Portador Oculto");
    }

    @Then("Verifico como Gestor Estrangeiro que a tela Relat�rios esta Ativa")
    public void verifico_Tela_Relatorios_Ativa_Gestor_Estrangeiro() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.hrefRelatorio));
        tirarScreenshot("Menu Relat�rios Ativa");
        espera(2);
        cabecalhoLogado.clickRelatorios();
        esperaElemento(cabecalhoLogado.msgRelatorios, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.msgRelatorios));
        cabecalhoLogado.clickRelatoriosPt();
        espera(1);
        scrollAteOElemento(cabecalhoLogado.msgRelatorios);
        esperaElemento(cabecalhoLogado.msgRelatorios, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.msgRelatorios));
        Assert.assertEquals("Relat�rios", cabecalhoLogado.msgRelatorios.getText());
        espera(2);
        tirarScreenshot("Tela Relat�rios Gestor Estrangeiro Ativo");
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Relat�rios Gestor Estrangeiro Ativo");
    }
    
    @Then("Verifico como Gestor Portador que a tela Relat�rios esta Inativo em Ingles")
    public void verifico_Tela_Relatorios_Inativa_Gestor_Portador_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        scrollAteOElemento(cabecalhoLogado.lblMainConsultations);
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu Relat�rios Inativo em Ingl�s");
        espera(3);
        tirarScreenshot("Submenu Relatorios Inativo em Ingl�s");
        esperaElemento(cabecalhoLogado.hrefRelatorio, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.hrefRelatorio));
        espera(2);
        abrirUrl("https://192.168.245.198/consulta/relatorio/");
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(cabecalhoLogado.lblRelatoriosEn, 60);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblRelatoriosEn));
        Assert.assertEquals("Reports", cabecalhoLogado.lblRelatoriosEn.getText());
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        tirarScreenshot("Tela Consulta de Limites Gestor Inativa e mensagem de Servi�o Indispon�vel");
        espera(1);
        scrollToTop();
        tirarScreenshot("Funcionalidade Relat�rios Inativo");
    }

    @Then("Verifico como Gestor Portador que a tela Relat�rios esta Oculto em Ingles")
    public void verifico_Tela_Relatorio_Oculta_Gestor_Portador_Ingles() throws InterruptedException {
    	Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        scrollAteOElemento(cabecalhoLogado.lblMainConsultations);
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu Relat�rios Inativo em Ingl�s");
        espera(3);
        tirarScreenshot("Submenu Relatorios Oculto  em Ingl�s");
        Assert.assertTrue(!elementoExiste(cabecalhoLogado.hrefRelatorio));
        espera(2);
        abrirUrl("https://192.168.245.198/consulta/relatorio/");
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(cabecalhoLogado.lblRelatoriosEn, 60);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblRelatoriosEn));
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Reports", cabecalhoLogado.lblRelatoriosEn.getText());
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        tirarScreenshot("Tela Consulta de Limites Gestor Oculto e mensagem de Servi�o Indispon�vel");
        espera(1);
        scrollToTop();
        tirarScreenshot("Tela Consulta de Limites Gestor  Oculto");
    }

    @Then("Verifico como Gestor Portador que a tela Relat�rios esta Ativa em Ingles")
    public void verifico_Tela_Relatorios_Ativa_Gestor_Portador_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.hrefRelatorio));
        tirarScreenshot("Menu Relat�rios Ativa em Ingl�s");
        espera(2);
        cabecalhoLogado.clickRelatorios();
        esperaElemento(cabecalhoLogado.msgRelatoriosEn, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.msgRelatoriosEn));
        cabecalhoLogado.clickRelatoriosEn();
        espera(1);
        scrollAteOElemento(cabecalhoLogado.msgRelatoriosEn);
        esperaElemento(cabecalhoLogado.msgRelatoriosEn, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.msgRelatoriosEn));
        Assert.assertEquals("Reports", cabecalhoLogado.msgRelatoriosEn.getText());
        espera(2);
        tirarScreenshot("Tela Relat�rios Gestor Portador Ativo em Ingles");
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Relat�rios Gestor Portador Ativo em Ingles");
    }
    
    @Then("Verifico como Gestor Estrangeiro que a tela Relat�rios esta Inativo em Ingles")
    public void verifico_Tela_Relatorios_Inativa_Gestor_Estrangeiro_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        scrollAteOElemento(cabecalhoLogado.lblMainConsultations);
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu Relat�rios Inativo em Ingl�s");
        espera(3);
        tirarScreenshot("Submenu Relatorios Inativo em Ingl�s");
        esperaElemento(cabecalhoLogado.hrefRelatorio, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.hrefRelatorio));
        espera(2);
        abrirUrl("https://192.168.245.198/consulta/relatorio/");
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(cabecalhoLogado.lblRelatoriosEn, 60);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblRelatoriosEn));
        Assert.assertEquals("Reports", cabecalhoLogado.lblRelatoriosEn.getText());
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        tirarScreenshot("Tela Consulta de Limites Gestor Inativa e mensagem de Servi�o Indispon�vel");
        espera(1);
        scrollToTop();
        tirarScreenshot("Funcionalidade Relat�rios Inativo");
    }

    @Then("Verifico como Gestor Estrangeiro que a tela Relat�rios esta Oculto em Ingles")
    public void verifico_Tela_Relatorio_Oculta_Gestor_Estrangeiro_Ingles() throws InterruptedException {
    	Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        scrollAteOElemento(cabecalhoLogado.lblMainConsultations);
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMainConsultations));
        tirarScreenshot("Menu Relat�rios Inativo em Ingl�s");
        espera(3);
        tirarScreenshot("Submenu Relatorios Oculto  em Ingl�s");
        Assert.assertTrue(!elementoExiste(cabecalhoLogado.hrefRelatorio));
        espera(2);
        abrirUrl("https://192.168.245.198/consulta/relatorio/");
        espera(2);
        scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
        esperaElemento(cabecalhoLogado.lblRelatoriosEn, 60);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblRelatoriosEn));
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Reports", cabecalhoLogado.lblRelatoriosEn.getText());
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        tirarScreenshot("Tela Consulta de Limites Gestor Oculto e mensagem de Servi�o Indispon�vel");
        espera(1);
        scrollToTop();
        tirarScreenshot("Tela Consulta de Limites Gestor  Oculto");
    }

    @Then("Verifico como Gestor Estrangeiro que a tela Relat�rios esta Ativa em Ingles")
    public void verifico_Tela_Relatorios_Ativa_Gestor_Estrangeiro_Ingles() throws InterruptedException {
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMainConsultations).perform();
        esperaElemento(cabecalhoLogado.lblMainConsultations, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.hrefRelatorio));
        tirarScreenshot("Menu Relat�rios Ativa em Ingl�s");
        espera(2);
        cabecalhoLogado.clickRelatorios();
        esperaElemento(cabecalhoLogado.msgRelatoriosEn, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.msgRelatoriosEn));
        cabecalhoLogado.clickRelatoriosEn();
        espera(1);
        scrollAteOElemento(cabecalhoLogado.msgRelatoriosEn);
        esperaElemento(cabecalhoLogado.msgRelatoriosEn, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.msgRelatoriosEn));
        Assert.assertEquals("Reports", cabecalhoLogado.msgRelatoriosEn.getText());
        espera(2);
        tirarScreenshot("Tela Relat�rios Gestor Portador Ativo em Ingles");
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Relat�rios Gestor Portador Ativo em Ingles");
    }
    
    @And("acesso o botao de Solicitacoes")
    public void acessoOBotaoDeSolicitacoes() {
        String color = cabecalhoLogado.lblMenuSolicitacoes.getCssValue("color");

        if (!elementoExiste(cabecalhoLogado.lblMenuSolicitacoes)) {
            tirarScreenshot("Bot�o Solicita��es Oculto");
        } else if (color.equalsIgnoreCase("#290a3b") || color.equalsIgnoreCase("rgba(41, 10, 59, 1)")) {
            tirarScreenshot("Bot�o Solicita��es Inativo");
        } else {
            esperaElemento(cabecalhoLogado.lblMenuSolicitacoes, 60);
            Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuSolicitacoes));
            cabecalhoLogado.clicarMenuSolicitacoes();
            tirarScreenshot("Cabe�alho - Solicita��es");
        }
    }

    @And("acesso o botao de Boleto")
    public void acessoOBotaoDeBoleto() {
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        scrollAteOElemento(cabecalhoLogado.lblMenuConsultas);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        String color = cabecalhoLogado.lblMenuConsultasBoletos.getCssValue("color");

        if (!elementoExiste(cabecalhoLogado.lblMenuConsultasBoletos)) {
            tirarScreenshot("Menu Boletos Oculto");
        } else if (color.equalsIgnoreCase("#290a3b") || color.equalsIgnoreCase("rgba(41, 10, 59, 1)")) {
            tirarScreenshot("Menu Boletos Inativo");
        } else {
            esperaElemento(cabecalhoLogado.lblMenuConsultasBoletos, 60);
            Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultasBoletos));
            cabecalhoLogado.clicarMenuConsultasBoletos();
            tirarScreenshot("Cabe�alho - Boletos");
        }
    }

    @And("acesso o funcionalidade de Extrato")
    public void acessoOFuncionalidadeDeExtrato() {
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        scrollAteOElemento(cabecalhoLogado.lblMenuConsultas);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        String color = cabecalhoLogado.lblMenuConsultasExtratos.getCssValue("color");

        if (!elementoExiste(cabecalhoLogado.lblMenuConsultasExtratos)) {
            tirarScreenshot("Menu Extrato Oculto");
        } else if (color.equalsIgnoreCase("#290a3b") || color.equalsIgnoreCase("rgba(41, 10, 59, 1)")) {
            tirarScreenshot("Menu Extrato Inativo");
        } else {
            esperaElemento(cabecalhoLogado.lblMenuConsultasExtratos, 60);
            Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultasExtratos));
            cabecalhoLogado.clicarMenuConsultasExtratos();
            tirarScreenshot("Cabe�alho - Extrato");
        }
    }
    
    @Then("Verifico como Portador que a tela Relat�rios esta Inativo")
    public void verifico_Tela_Relat�rios_Inativa_Portador_Inativo() throws InterruptedException {
    	Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        scrollAteOElemento(cabecalhoLogado.lblMenuConsultas);
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        tirarScreenshot("Menu Relat�rios Inativo");
        espera(3);
        tirarScreenshot("Submenu Relatorios Inativo");
        esperaElemento(cabecalhoLogado.hrefRelatorio, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.hrefRelatorio));
        espera(2);
        abrirUrl("https://192.168.245.198/consulta/relatorio/");
        espera(2);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        espera(2);
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Relat�rios Portador Inativo");
    }

    @Then("Verifico como Portador que a tela Relat�rios esta Oculto")
    public void verifico_Tela_Relat�rios_Oculta_Portador() throws InterruptedException {
    	Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        scrollAteOElemento(cabecalhoLogado.lblMenuConsultas);
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        tirarScreenshot("Menu Relat�rios Oculto");
        espera(3);
        tirarScreenshot("Submenu Relatorios Oculto");
        Assert.assertTrue(!elementoExiste(cabecalhoLogado.hrefRelatorio));
        espera(2);
        abrirUrl("https://192.168.245.198/consulta/relatorio/");
        espera(2);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        espera(2);
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Relat�rios Portador Oculto");
    }

    @Then("Verifico como Portador que a tela Relat�rios esta Ativa")
    public void verifico_Tela_Relat�rios_Ativa_Portador() throws InterruptedException {
    	Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.hrefRelatorio));
        cabecalhoLogado.clickRelatorios();
        tirarScreenshot("Menu Relat�rios Ativo");
        espera(2);
        scrollAteOElemento(cabecalhoLogado.msgRelatorios);
        esperaElemento(cabecalhoLogado.msgRelatorios, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.msgRelatorios));
        Assert.assertEquals("Relat�rios", cabecalhoLogado.msgRelatorios.getText());
        espera(2);
        tirarScreenshot("Tela Relat�rios Gestor Estrangeiro Ativo");
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Relat�rios Gestor Estrangeiro Ativo");
    }
    
    @Then("Verifico como Portador que a tela Relat�rios esta Oculto em Ingles")
    public void verifico_Tela_Relat�rios_Oculta_Portador_Ingles() throws InterruptedException {
    	Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        scrollAteOElemento(cabecalhoLogado.lblMenuConsultas);
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
        tirarScreenshot("Menu Relat�rios Oculto em Ingles");
        espera(3);
        tirarScreenshot("Submenu Relatorios Oculto em Ingles");
        Assert.assertTrue(!elementoExiste(cabecalhoLogado.hrefRelatorio));
        espera(2);
        abrirUrl("https://192.168.245.198/consulta/relatorio/");
        espera(2);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        espera(2);
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Relat�rios Portador Oculto em Ingles");
    }

    @Then("Verifico como Portador que a tela Relat�rios esta Ativa em Ingles")
    public void verifico_Tela_Relat�rios_Ativa_Portador_Ingles() throws InterruptedException {
    	Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        esperaElemento(cabecalhoLogado.hrefRelatorio, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.hrefRelatorio));
        cabecalhoLogado.clickRelatorios();
        tirarScreenshot("Menu Relat�rios Ativo");
        espera(2);
        scrollAteOElemento(cabecalhoLogado.msgRelatorios);
        esperaElemento(cabecalhoLogado.msgRelatorios, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.msgRelatorios));
        Assert.assertEquals("Relat�rios", cabecalhoLogado.msgRelatorios.getText());
        espera(2);
        tirarScreenshot("Tela Relat�rios Gestor Estrangeiro Ativo");
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Relat�rios Gestor Estrangeiro Ativo");
    }
    
    @Then("Verifico como Portador que a tela Relat�rios esta Inativo em Ingles")
    public void verifico_Tela_Relat�rios_Inativa_Portador_Inativo_Ingles() throws InterruptedException {
    	Actions mouseOver = new Actions(driverWeb);
        mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
        scrollAteOElemento(cabecalhoLogado.lblMenuConsultas);
        esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
        esperaElemento(cabecalhoLogado.hrefRelatorio, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.hrefRelatorio));
        tirarScreenshot("Menu Relat�rios Inativo");
        espera(3);
        tirarScreenshot("Submenu Relatorios Inativo em Ingles");
        esperaElemento(cabecalhoLogado.hrefRelatorio, 60);
        Assert.assertTrue(elementoExiste(cabecalhoLogado.hrefRelatorio));
        espera(2);
        abrirUrl("https://192.168.245.198/consulta/relatorio/");
        espera(2);
        esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
        espera(2);
        scrollToTop();
        espera(1);
        tirarScreenshot("Tela Relat�rios Portador Inativo em Ingles");
    }
    
    @Then("Verifico como Portador que a tela de Bloqueio esta Ativo")
    public void verifico_Tela_Bloqueio_ativa_Portador() throws InterruptedException {
    esperaElemento(cabecalhoLogado.lblMenuServicos, 60);
	scrollAteOElemento(cabecalhoLogado.lblMenuServicos);
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicos));
	mouseOver.moveToElement(cabecalhoLogado.lblMenuServicos).perform();
	tirarScreenshot("Cabe�alho - Menu Servi�os");
	espera(2);
	esperaElemento(cabecalhoLogado.lblMenuServicosBloqueio, 60);
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicosBloqueio));
	mouseOver.moveToElement(cabecalhoLogado.lblMenuServicosBloqueio).perform();
	cabecalhoLogado.clicarMenuServicosBloqueio();
	espera(3);
	scrollAteOElemento(servicos.lblServicos);
	esperaElemento(servicos.lblServicos, 60);
	Assert.assertTrue(elementoExiste(servicos.lblServicos));
	Assert.assertEquals("Servi�os", servicos.lblServicos.getText());
	tirarScreenshot("Menu de bloqueio Feature Toogle Ativo");
	espera(2);
	scrollToTop();
	tirarScreenshot("Menu de bloqueio Feature Toogle Ativo");
    }
    
    @Then("Verifico como Portador que a tela de Bloqueio esta Inativo")
    public void verifico_Tela_Bloqueio_Inativo_Portador() throws InterruptedException {
    esperaElemento(cabecalhoLogado.lblMenuServicos, 60);
	scrollAteOElemento(cabecalhoLogado.lblMenuServicos);
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicos));
	mouseOver.moveToElement(cabecalhoLogado.lblMenuServicos).perform();
	tirarScreenshot("Cabe�alho - Menu Servi�os");
	espera(2);
	esperaElemento(cabecalhoLogado.lblMenuServicosBloqueio, 60);
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicosBloqueio));
	espera(3);
	tirarScreenshot("Menu de bloqueio Inativo");
	espera(1);
	abrirUrl("https://192.168.245.198/servicos/");
	scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
	esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
    esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
    Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
    Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
    Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
    Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
	scrollToTop();
	tirarScreenshot("Menu de bloqueio Inativo");
    }
    
    @Then("Verifico como Portador que a tela de Bloqueio esta Oculto")
    public void verifico_Tela_Bloqueio_Oculto_Portador() throws InterruptedException {
        esperaElemento(cabecalhoLogado.lblMenuServicos, 60);
    	scrollAteOElemento(cabecalhoLogado.lblMenuServicos);
    	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicos));
    	mouseOver.moveToElement(cabecalhoLogado.lblMenuServicos).perform();
    	tirarScreenshot("Cabe�alho - Menu Servi�os");
    	espera(2);
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosBloqueio));
    	espera(3);
    	tirarScreenshot("Menu de bloqueio Oculto");
    	espera(1);
    	abrirUrl("https://192.168.245.198/servicos/");
    	scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
    	esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
    	scrollToTop();
    	tirarScreenshot("Menu de bloqueio Inativo");
    }
    
    @Then("Verifico como Gestor que a tela de Bloqueio esta Ativo")
    public void verifico_Tela_Bloqueio_ativa_Gestor() throws InterruptedException {
    esperaElemento(cabecalhoLogado.lblMenuServicos, 60);
	scrollAteOElemento(cabecalhoLogado.lblMenuServicos);
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicos));
	mouseOver.moveToElement(cabecalhoLogado.lblMenuServicos).perform();
	tirarScreenshot("Cabe�alho - Menu Servi�os");
	espera(2);
	esperaElemento(cabecalhoLogado.lblMenuServicosBloqueio, 60);
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicosBloqueio));
	mouseOver.moveToElement(cabecalhoLogado.lblMenuServicosBloqueio).perform();
	cabecalhoLogado.clicarMenuServicosBloqueio();
	espera(3);
	scrollAteOElemento(servicos.lblServicos);
	esperaElemento(servicos.lblServicos, 60);
	Assert.assertTrue(elementoExiste(servicos.lblServicos));
	Assert.assertEquals("Servi�os", servicos.lblServicos.getText());
	tirarScreenshot("Menu de bloqueio Feature Toogle Ativo");
	espera(2);
	scrollToTop();
	tirarScreenshot("Menu de bloqueio Feature Toogle Ativo");
    }
    
    @Then("Verifico como Gestor que a tela de Bloqueio esta Inativo")
    public void verifico_Tela_Bloqueio_Inativo_Gestor() throws InterruptedException {
    esperaElemento(cabecalhoLogado.lblMenuServicos, 60);
	scrollAteOElemento(cabecalhoLogado.lblMenuServicos);
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicos));
	mouseOver.moveToElement(cabecalhoLogado.lblMenuServicos).perform();
	tirarScreenshot("Cabe�alho - Menu Servi�os");
	espera(2);
	esperaElemento(cabecalhoLogado.lblMenuServicosBloqueio, 60);
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicosBloqueio));
	espera(3);
	tirarScreenshot("Menu de bloqueio Inativo");
	espera(1);
	abrirUrl("https://192.168.245.198/servicos/");
	scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
	esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
    esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
    Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
    Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
    Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
    Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
	scrollToTop();
	tirarScreenshot("Menu de bloqueio Inativo");
    }
    
    @Then("Verifico como Gestor que a tela de Bloqueio esta Oculto")
    public void verifico_Tela_Bloqueio_Oculto_Gestor() throws InterruptedException {
        esperaElemento(cabecalhoLogado.lblMenuServicos, 60);
    	scrollAteOElemento(cabecalhoLogado.lblMenuServicos);
    	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicos));
    	mouseOver.moveToElement(cabecalhoLogado.lblMenuServicos).perform();
    	tirarScreenshot("Cabe�alho - Menu Servi�os");
    	espera(2);
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosBloqueio));
    	espera(3);
    	tirarScreenshot("Menu de bloqueio Oculto");
    	espera(1);
    	abrirUrl("https://192.168.245.198/servicos/");
    	scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
    	esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
    	scrollToTop();
    	tirarScreenshot("Menu de bloqueio Inativo");
    }
    
    @Then("Verifico como Gestor que a tela de Bloqueio esta Ativo em Ingles")
    public void verifico_tela_bloqueio_ativa_gestor_ingles() throws InterruptedException {
    esperaElemento(cabecalhoLogado.lblMenuServicos, 60);
	scrollAteOElemento(cabecalhoLogado.lblMenuServicos);
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicos));
	mouseOver.moveToElement(cabecalhoLogado.lblMenuServicos).perform();
	tirarScreenshot("Cabe�alho - Menu Servi�os");
	espera(2);
	esperaElemento(cabecalhoLogado.lblMenuServicosBloqueio, 60);
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicosBloqueio));
	mouseOver.moveToElement(cabecalhoLogado.lblMenuServicosBloqueio).perform();
	cabecalhoLogado.clicarMenuServicosBloqueio();
	espera(3);
	scrollAteOElemento(servicos.lblServicosEn);
	esperaElemento(servicos.lblServicosEn, 60);
	Assert.assertTrue(elementoExiste(servicos.lblServicosEn));
	Assert.assertEquals("Services", servicos.lblServicosEn.getText());
	tirarScreenshot("Menu de bloqueio Feature Toogle Ativo em Ingles");
	espera(2);
	scrollToTop();
	tirarScreenshot("Menu de bloqueio Feature Toogle Ativo");
    }
    
    @Then("Verifico como Gestor que a tela de Bloqueio esta Inativo em Ingles")
    public void verifico_tela_bloqueio_inativo_gestor_ingles() throws InterruptedException {
    esperaElemento(cabecalhoLogado.lblMenuServicos, 60);
	scrollAteOElemento(cabecalhoLogado.lblMenuServicos);
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicos));
	mouseOver.moveToElement(cabecalhoLogado.lblMenuServicos).perform();
	tirarScreenshot("Cabe�alho - Menu Servi�os");
	espera(2);
	esperaElemento(cabecalhoLogado.lblMenuServicosBloqueio, 60);
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicosBloqueio));
	espera(3);
	tirarScreenshot("Menu de bloqueio Inativo");
	espera(1);
	abrirUrl("https://192.168.254.12/servicos/");
	scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
	esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
    esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
    Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
    Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
    Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
    Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
	scrollToTop();
	tirarScreenshot("Menu de bloqueio Inativo");
    }
    
    @Then("Verifico como Gestor que a tela de Bloqueio esta Oculto em Ingles")
    public void verifico_tela_bloqueio_oculto_gestor() throws InterruptedException {
        esperaElemento(cabecalhoLogado.lblMenuServicos, 60);
    	scrollAteOElemento(cabecalhoLogado.lblMenuServicos);
    	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicos));
    	mouseOver.moveToElement(cabecalhoLogado.lblMenuServicos).perform();
    	tirarScreenshot("Cabe�alho - Menu Servi�os");
    	espera(2);
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosBloqueio));
    	espera(3);
    	tirarScreenshot("Menu de bloqueio Oculto");
    	espera(1);
    	abrirUrl("https://192.168.254.198/servicos/");
    	scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
    	esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
        esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
        Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
        Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
        Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
        Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
    	scrollToTop();
    	tirarScreenshot("Menu de bloqueio Inativo");
    }
    
    @Then("Verifico como Gestor que a aba de Servicos esta Ativo")
    public void verifico_aba_servicos_ativa_gestor() throws InterruptedException {
    esperaElemento(cabecalhoLogado.lblMenuServicos, 60);
	scrollAteOElemento(cabecalhoLogado.lblMenuServicos);
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicos));
	mouseOver.moveToElement(cabecalhoLogado.lblMenuServicos).perform();
	tirarScreenshot("Cabe�alho - Menu Servi�os");
	espera(2);
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicosBloqueio));
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicosDesbloqueio));
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicosCancelamento));
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicosParcelamentoDeFatura));
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicosLgpd));
	scrollAteOElemento(cabecalhoLogado.lblMenuServicosBloqueio);
	tirarScreenshot("Menu de bloqueio Feature Toogle Ativo");
	espera(2);
	scrollToTop();
	tirarScreenshot("Menu de bloqueio Feature Toogle Ativo");
    }
    
    @Then("Verifico como Gestor que a aba de Servicos esta Inativo")
    public void verifico_tela_servicos_Inativo_gestor() throws InterruptedException {
    	esperaElemento(cabecalhoLogado.lblMenuServicos, 60);
    	scrollAteOElemento(cabecalhoLogado.lblMenuServicos);
    	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicos));
    	mouseOver.moveToElement(cabecalhoLogado.lblMenuServicos).perform();
    	tirarScreenshot("Cabe�alho - Menu Servi�os Inativo");
    	espera(2);
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosBloqueio));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosDesbloqueio));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosCancelamento));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosParcelamentoDeFatura));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosLgpd));
    	scrollAteOElemento(cabecalhoLogado.lblMenuServicos);
    	tirarScreenshot("Menu de bloqueio Feature Toogle Inativo");
    	espera(2);
    	scrollToTop();
    	tirarScreenshot("Menu de bloqueio Feature Toogle Inativo");
    }
    
    @Then("Verifico como Gestor que a aba de Servicos esta Oculto")
    public void verifico_Tela_servicos_oculto_gestor() throws InterruptedException {
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicos));
    	tirarScreenshot("Cabe�alho - Menu Servi�os Inativo");
    	espera(2);
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosBloqueio));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosDesbloqueio));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosCancelamento));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosParcelamentoDeFatura));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosLgpd));
    	tirarScreenshot("Menu de bloqueio Feature Toogle Oculto");
    	espera(2);
    	tirarScreenshot("Menu de bloqueio Feature Toogle Oculto");
    }
    
    @Then("Verifico como Portador que a aba de Servicos esta Ativo")
    public void verifico_aba_servicos_ativa_portador() throws InterruptedException {
    esperaElemento(cabecalhoLogado.lblMenuServicos, 60);
	scrollAteOElemento(cabecalhoLogado.lblMenuServicos);
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicos));
	mouseOver.moveToElement(cabecalhoLogado.lblMenuServicos).perform();
	tirarScreenshot("Cabe�alho - Menu Servi�os");
	espera(2);
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicosBloqueio));
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicosDesbloqueio));
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicosCancelamento));
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicosParcelamentoDeFatura));
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicosLgpd));
	scrollAteOElemento(cabecalhoLogado.lblMenuServicosBloqueio);
	tirarScreenshot("Menu de bloqueio Feature Toogle Ativo");
	espera(2);
	scrollToTop();
	tirarScreenshot("Menu de bloqueio Feature Toogle Ativo");
    }
    
    @Then("Verifico como Portador que a aba de Servicos esta Inativo")
    public void verifico_tela_servicos_Inativo_portador() throws InterruptedException {
    	esperaElemento(cabecalhoLogado.lblMenuServicos, 60);
    	scrollAteOElemento(cabecalhoLogado.lblMenuServicos);
    	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicos));
    	mouseOver.moveToElement(cabecalhoLogado.lblMenuServicos).perform();
    	tirarScreenshot("Cabe�alho - Menu Servi�os Inativo");
    	espera(2);
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosBloqueio));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosDesbloqueio));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosCancelamento));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosParcelamentoDeFatura));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosLgpd));
    	scrollAteOElemento(cabecalhoLogado.lblMenuServicos);
    	tirarScreenshot("Menu de bloqueio Feature Toogle Inativo");
    	espera(2);
    	scrollToTop();
    	tirarScreenshot("Menu de bloqueio Feature Toogle Inativo");
    }
    
    @Then("Verifico como Portador que a aba de Servicos esta Oculto")
    public void verifico_Tela_servicos_oculto_portador() throws InterruptedException {
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicos));
    	tirarScreenshot("Cabe�alho - Menu Servi�os Inativo");
    	espera(2);
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosBloqueio));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosDesbloqueio));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosCancelamento));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosParcelamentoDeFatura));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosLgpd));
    	tirarScreenshot("Menu de bloqueio Feature Toogle Oculto");
    	espera(2);
    	tirarScreenshot("Menu de bloqueio Feature Toogle Oculto");
    }
    
    @Then("Verifico como Gestor que a aba de Servicos esta Ativo em Ingles")
    public void verifico_aba_servicos_ativa_gestor_ingles() throws InterruptedException {
    esperaElemento(cabecalhoLogado.lblMenuServicos, 60);
	scrollAteOElemento(cabecalhoLogado.lblMenuServicos);
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicos));
	mouseOver.moveToElement(cabecalhoLogado.lblMenuServicos).perform();
	tirarScreenshot("Cabe�alho - Menu Servi�os");
	espera(2);
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicosBloqueio));
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicosDesbloqueio));
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicosCancelamento));
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicosParcelamentoDeFatura));
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicosLgpd));
	scrollAteOElemento(cabecalhoLogado.lblMenuServicosBloqueio);
	tirarScreenshot("Menu de bloqueio Feature Toogle Ativo");
	espera(2);
	scrollToTop();
	tirarScreenshot("Menu de bloqueio Feature Toogle Ativo");
    }
    
    @Then("Verifico como Gestor que a aba de Servicos esta Inativo em Ingles")
    public void verifico_tela_servicos_Inativo_gestor_ingles() throws InterruptedException {
    	esperaElemento(cabecalhoLogado.lblMenuServicos, 60);
    	scrollAteOElemento(cabecalhoLogado.lblMenuServicos);
    	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicos));
    	mouseOver.moveToElement(cabecalhoLogado.lblMenuServicos).perform();
    	tirarScreenshot("Cabe�alho - Menu Servi�os Inativo");
    	espera(2);
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosBloqueio));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosDesbloqueio));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosCancelamento));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosParcelamentoDeFatura));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosLgpd));
    	scrollAteOElemento(cabecalhoLogado.lblMenuServicos);
    	tirarScreenshot("Menu de bloqueio Feature Toogle Inativo");
    	espera(2);
    	scrollToTop();
    	tirarScreenshot("Menu de bloqueio Feature Toogle Inativo");
    }
    
    @Then("Verifico como Gestor que a aba de Servicos esta Oculto em Ingles")
    public void verifico_Tela_servicos_oculto_gestor_ingles() throws InterruptedException {
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicos));
    	tirarScreenshot("Cabe�alho - Menu Servi�os Inativo");
    	espera(2);
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosBloqueio));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosDesbloqueio));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosCancelamento));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosParcelamentoDeFatura));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosLgpd));
    	tirarScreenshot("Menu de bloqueio Feature Toogle Oculto");
    	espera(2);
    	tirarScreenshot("Menu de bloqueio Feature Toogle Oculto");
    }
    
    @Then("Verifico como Portador que a aba de Servicos esta Ativo em Ingles")
    public void verifico_aba_servicos_ativa_portador_ingles() throws InterruptedException {
    esperaElemento(cabecalhoLogado.lblMenuServicos, 60);
	scrollAteOElemento(cabecalhoLogado.lblMenuServicos);
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicos));
	mouseOver.moveToElement(cabecalhoLogado.lblMenuServicos).perform();
	tirarScreenshot("Cabe�alho - Menu Servi�os");
	espera(2);
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicosBloqueio));
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicosDesbloqueio));
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicosCancelamento));
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicosParcelamentoDeFatura));
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicosLgpd));
	scrollAteOElemento(cabecalhoLogado.lblMenuServicosBloqueio);
	tirarScreenshot("Menu de bloqueio Feature Toogle Ativo");
	espera(2);
	scrollToTop();
	tirarScreenshot("Menu de bloqueio Feature Toogle Ativo");
    }
    
    @Then("Verifico como Portador que a aba de Servicos esta Inativo em Ingles")
    public void verifico_tela_servicos_Inativo_portador_ingles() throws InterruptedException {
    	esperaElemento(cabecalhoLogado.lblMenuServicos, 60);
    	scrollAteOElemento(cabecalhoLogado.lblMenuServicos);
    	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuServicos));
    	mouseOver.moveToElement(cabecalhoLogado.lblMenuServicos).perform();
    	tirarScreenshot("Cabe�alho - Menu Servi�os Inativo");
    	espera(2);
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosBloqueio));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosDesbloqueio));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosCancelamento));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosParcelamentoDeFatura));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosLgpd));
    	scrollAteOElemento(cabecalhoLogado.lblMenuServicos);
    	tirarScreenshot("Menu de bloqueio Feature Toogle Inativo");
    	espera(2);
    	scrollToTop();
    	tirarScreenshot("Menu de bloqueio Feature Toogle Inativo");
    }
    
    @Then("Verifico como Portador que a aba de Servicos esta Oculto em Ingles")
    public void verifico_Tela_servicos_oculto_portador_ingles() throws InterruptedException {
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicos));
    	tirarScreenshot("Cabe�alho - Menu Servi�os Inativo");
    	espera(2);
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosBloqueio));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosDesbloqueio));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosCancelamento));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosParcelamentoDeFatura));
    	Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuServicosLgpd));
    	tirarScreenshot("Menu de bloqueio Feature Toogle Oculto");
    	espera(2);
    	tirarScreenshot("Menu de bloqueio Feature Toogle Oculto");
    }
    
    @Then("Verifico como Gestor que a funcionalidade Forma de Pagamento esta Ativo")
    public void verifico_aba_forma_pagamento_ativa_gestor() throws InterruptedException {
    esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
	scrollAteOElemento(cabecalhoLogado.lblMenuConsultas);
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
	mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
	tirarScreenshot("Cabe�alho - Menu Consultas");
	espera(2);
	esperaElemento(cabecalhoLogado.lblMenuConsultasFormaDePagamento, 60);
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultasFormaDePagamento));
	espera(1);
	scrollToTop();
	cabecalhoLogado.clicarMenuConsultasFormaDePagamento();
	espera(2);
	scrollAteOElemento(formadepagamentoGestor.lblFormadePagamento);
	Assert.assertTrue(elementoExiste(formadepagamentoGestor.lblFormadePagamento));
    Assert.assertEquals("Forma de pagamento", formadepagamentoGestor.lblFormadePagamento.getText());
	tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Ativo");
	espera(2);
	scrollToTop();
	tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Ativo");
    }
    
    @Then("Verifico como Gestor que a funcionalidade Forma de Pagamento esta Inativo")
    public void verifico_tela_forma_pagamento_Inativo_gestor() throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		scrollAteOElemento(cabecalhoLogado.lblMenuConsultas);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
		tirarScreenshot("Cabe�alho - Menu Consultas");
		espera(2);
		scrollAteOElemento(cabecalhoLogado.lblMenuConsultasFormaDePagamento);
		esperaElemento(cabecalhoLogado.lblMenuConsultasFormaDePagamento, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultasFormaDePagamento));
		espera(1);
		scrollToTop();
		espera(2);
		abrirUrl("https://192.168.245.198/consulta/forma-de-pagamento/");
		espera(2);
		scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
		esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
		esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
		Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
		Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
		Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
		Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
		tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Inativo");
		espera(2);
		scrollToTop();
		tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Inativo");
	}

	@Then("Verifico como Gestor que a funcionalidade Forma de Pagamento esta Oculto")
    public void verifico_tela_forma_pagamento_oculto_gestor() throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		scrollAteOElemento(cabecalhoLogado.lblMenuConsultas);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
		tirarScreenshot("Cabe�alho - Menu Consultas");
		espera(2);
		Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuConsultasFormaDePagamento));
		espera(2);
		abrirUrl("https://192.168.245.198/consulta/forma-de-pagamento/");
		espera(2);
		scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
		esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
		esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
		Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
		Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
		Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
		Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
		tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Oculto");
		espera(2);
		scrollToTop();
		tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Oculto");
    }
    
    @Then("Verifico como Portador que a funcionalidade Forma de Pagamento esta Ativo")
    public void verifico_tela_forma_pagamento_ativa_portador() throws InterruptedException {
    	esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
    	scrollAteOElemento(cabecalhoLogado.lblMenuConsultas);
    	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
    	mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
    	tirarScreenshot("Cabe�alho - Menu Consultas");
    	espera(2);
    	scrollAteOElemento(cabecalhoLogado.lblMenuConsultasFormaDePagamento);
    	esperaElemento(cabecalhoLogado.lblMenuConsultasFormaDePagamento, 60);
    	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultasFormaDePagamento));
    	espera(1);
    	tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Ativo");
		scrollToTop();
		tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Ativo");
		cabecalhoLogado.clicarMenuConsultasFormaDePagamento();
    	espera(2);
    	scrollAteOElemento(formadepagamentoGestor.lblFormadePagamento);
    	Assert.assertTrue(elementoExiste(formadepagamentoGestor.lblFormadePagamento));
        Assert.assertEquals("Forma de pagamento", formadepagamentoGestor.lblFormadePagamento.getText());
    	tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Ativo");
    	espera(2);
    	scrollToTop();
    	tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Ativo");
    }
    
    @Then("Verifico como Portador que a funcionalidade Forma de Pagamento esta Inativo")
    public void verifico_tela_forma_pagamento_Inativo_portador() throws InterruptedException {
    	esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		scrollAteOElemento(cabecalhoLogado.lblMenuConsultas);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
		tirarScreenshot("Cabe�alho - Menu Consultas");
		espera(2);
		scrollAteOElemento(cabecalhoLogado.lblMenuConsultasFormaDePagamento);
		esperaElemento(cabecalhoLogado.lblMenuConsultasFormaDePagamento, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultasFormaDePagamento));
		espera(1);
		tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Inativo");
		scrollToTop();
		espera(2);
		tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Inativo");
		abrirUrl("https://192.168.245.198/consulta/forma-de-pagamento/");
		espera(2);
		scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
		esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
		esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
		Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
		Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
		Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
		Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
		tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Inativo");
		espera(2);
		scrollToTop();
		tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Inativo");
    }
    
    @Then("Verifico como Portador que a funcionalidade Forma de Pagamento esta Oculto")
    public void verifico_tela_forma_pagamento_oculto_portador() throws InterruptedException {
    	esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		scrollAteOElemento(cabecalhoLogado.lblMenuConsultas);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
		tirarScreenshot("Cabe�alho - Menu Consultas");
		espera(2);
		Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuConsultasFormaDePagamento));
		tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Oculto");
		espera(2);
		abrirUrl("https://192.168.245.198/consulta/forma-de-pagamento/");
		espera(2);
		scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
		esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
		esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
		Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
		Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
		Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
		Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
		tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Oculto");
		espera(2);
		scrollToTop();
		tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Oculto");
    }
    @Then("Verifico como Gestor que a funcionalidade Forma de Pagamento esta Ativo em Ingles")
    public void verifico_aba_forma_pagamento_ativa_gestor_ingles() throws InterruptedException {
    esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
	scrollAteOElemento(cabecalhoLogado.lblMenuConsultas);
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
	mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
	tirarScreenshot("Cabe�alho - Menu Consultas");
	espera(2);
	scrollAteOElemento(cabecalhoLogado.lblMenuConsultasFormaDePagamento);
	esperaElemento(cabecalhoLogado.lblMenuConsultasFormaDePagamento, 60);
	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultasFormaDePagamento));
	tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Ativo em Ingles");
	espera(2);
	cabecalhoLogado.clicarMenuConsultasFormaDePagamento();
	espera(2);
	scrollAteOElemento(formadepagamentoGestor.lblFormadePagamentoEn);
	Assert.assertTrue(elementoExiste(formadepagamentoGestor.lblFormadePagamentoEn));
    Assert.assertEquals("Payment method", formadepagamentoGestor.lblFormadePagamentoEn.getText());
	tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Ativo em Ingles");
	espera(2);
	scrollToTop();
	tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Ativo em Ingles");
    }
    
    @Then("Verifico como Gestor que a funcionalidade Forma de Pagamento esta Inativo em Ingles")
    public void verifico_tela_forma_pagamento_Inativo_gestor_ingles() throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		scrollAteOElemento(cabecalhoLogado.lblMenuConsultas);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
		tirarScreenshot("Cabe�alho - Menu Consultas");
		espera(2);
		scrollAteOElemento(cabecalhoLogado.lblMenuConsultasFormaDePagamento);
		esperaElemento(cabecalhoLogado.lblMenuConsultasFormaDePagamento, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultasFormaDePagamento));
		tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Inativo em Ingles");
		espera(2);
		abrirUrl("https://192.168.245.198/consulta/forma-de-pagamento/");
		espera(2);
		scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
		esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
		esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
		Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
		Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
		Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
		Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
		tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Inativo em Ingles");
		espera(2);
		scrollToTop();
		tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Inativo em Ingles");
	}

	@Then("Verifico como Gestor que a funcionalidade Forma de Pagamento esta Oculto em Ingles")
    public void verifico_tela_forma_pagamento_oculto_gestor_ingles() throws InterruptedException {
		esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		scrollAteOElemento(cabecalhoLogado.lblMenuConsultas);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
		tirarScreenshot("Cabe�alho - Menu Consultas");
		espera(2);
		Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuConsultasFormaDePagamento));
		tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Inativo em Oculto");
		espera(2);
		abrirUrl("https://192.168.245.198/consulta/forma-de-pagamento/");
		espera(2);
		scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
		esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
		esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
		Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
		Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
		Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
		Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
		tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Oculto em Ingles");
		espera(2);
		scrollToTop();
		tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Oculto em Ingles");
    }
    
    @Then("Verifico como Portador que a funcionalidade Forma de Pagamento esta Ativo em Ingles")
    public void verifico_tela_forma_pagamento_ativa_portador_ingles() throws InterruptedException {
    	esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
    	scrollAteOElemento(cabecalhoLogado.lblMenuConsultas);
    	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
    	mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
    	tirarScreenshot("Cabe�alho - Menu Consultas");
    	espera(2);
    	scrollAteOElemento(cabecalhoLogado.lblMenuConsultasFormaDePagamento);
    	esperaElemento(cabecalhoLogado.lblMenuConsultasFormaDePagamento, 60);
    	Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultasFormaDePagamento));
    	tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Inativo em Ativo em Ingles");
		espera(2);
    	cabecalhoLogado.clicarMenuConsultasFormaDePagamento();
    	espera(2);
    	scrollAteOElemento(formadepagamentoGestor.lblFormadePagamentoEn);
    	Assert.assertTrue(elementoExiste(formadepagamentoGestor.lblFormadePagamentoEn));
        Assert.assertEquals("Payment method", formadepagamentoGestor.lblFormadePagamentoEn.getText());
    	tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Ativo");
    	espera(2);
    	scrollToTop();
    	tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Ativo");
    }
    
    @Then("Verifico como Portador que a funcionalidade Forma de Pagamento esta Inativo em Ingles")
    public void verifico_tela_forma_pagamento_Inativo_portador_ingles() throws InterruptedException {
    	esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		scrollAteOElemento(cabecalhoLogado.lblMenuConsultas);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
		tirarScreenshot("Cabe�alho - Menu Consultas");
		espera(2);
		scrollAteOElemento(cabecalhoLogado.lblMenuConsultasFormaDePagamento);
		esperaElemento(cabecalhoLogado.lblMenuConsultasFormaDePagamento, 60);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultasFormaDePagamento));
		tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Inativo em Inativo em Ingles");
		espera(2);
		abrirUrl("https://192.168.245.198/consulta/forma-de-pagamento/");
		espera(2);
		scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
		esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
		esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
		Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
		Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
		Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
		Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
		tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Inativo em Ingles");
		espera(2);
		scrollToTop();
		tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Inativo em Ingles");
    }
    
    @Then("Verifico como Portador que a funcionalidade Forma de Pagamento esta Oculto em Ingles")
    public void verifico_tela_forma_pagamento_oculto_portador_ingles() throws InterruptedException {
    	esperaElemento(cabecalhoLogado.lblMenuConsultas, 60);
		scrollAteOElemento(cabecalhoLogado.lblMenuConsultas);
		Assert.assertTrue(elementoExiste(cabecalhoLogado.lblMenuConsultas));
		mouseOver.moveToElement(cabecalhoLogado.lblMenuConsultas).perform();
		tirarScreenshot("Cabe�alho - Menu Consultas");
		espera(2);
		Assert.assertTrue(!elementoExiste(cabecalhoLogado.lblMenuConsultasFormaDePagamento));
		tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Inativo em Oculto em Ingles");
		espera(2);
		abrirUrl("https://192.168.245.198/consulta/forma-de-pagamento/");
		espera(2);
		scrollAteOElemento(Feature_Toogle.lblServicoIndisponivel);
		esperaElemento(Feature_Toogle.lblServicoIndisponivel, 60);
		esperaElemento(Feature_Toogle.msgServicoIndisponivel, 60);
		Assert.assertTrue(elementoExiste(Feature_Toogle.lblServicoIndisponivel));
		Assert.assertTrue(elementoExiste(Feature_Toogle.msgServicoIndisponivel));
		Assert.assertEquals("Servi�o indispon�vel", Feature_Toogle.lblServicoIndisponivel.getText());
		Assert.assertEquals("Servi�o Indispon�vel Temporariamente.", Feature_Toogle.msgServicoIndisponivel.getText());
		tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Oculto em Ingles");
		espera(2);
		scrollToTop();
		tirarScreenshot("Funcionalidade Forma de pagamento Feature Toogle Oculto em Ingles");
    }
}