package br.com.bradesco.pupj.pagesweb;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import br.com.bradesco.pupj.utilitarios.UtilsWeb;

public class OpenFinance_TrazerDados_Page extends UtilsWeb {

	@SuppressWarnings("unused")

	private WebDriver driverWeb;

	public OpenFinance_TrazerDados_Page (WebDriver driverWeb) {
		this.driverWeb = driverWeb;
		PageFactory.initElements(driverWeb, this);
	}

	// Bot�o Fechar do Popup Trazer Dados Open Finance
	@FindBy(xpath = "//button[contains(@class, 'closeButton')]")
	public WebElement fecharpopupOpenFinance;
	

	public void fecharpopupTrazerDados() {
		fecharpopupOpenFinance.click();
	}

}