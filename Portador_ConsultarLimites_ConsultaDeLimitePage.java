package br.com.bradesco.pupj.pagesweb;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import br.com.bradesco.pupj.utilitarios.UtilsWeb;

public class Portador_ConsultarLimites_ConsultaDeLimitePage extends UtilsWeb {

	@SuppressWarnings("unused")
	
	private WebDriver driverWeb;

	public Portador_ConsultarLimites_ConsultaDeLimitePage(WebDriver driverWeb) {
		this.driverWeb = driverWeb;
		PageFactory.initElements(driverWeb, this);
	}

	// Texto "Consultas"
	@FindBy(xpath = "//span[text()='Consultas']")
	public WebElement lblConsultas;

	// Texto "Consulta de limites"
	@FindBy(xpath = "//h1[text()='Consulta de limites']")
	public WebElement lblConsultaDeLimites;

	// Escolha cart�o
	@FindBy(xpath = "//section")
	public WebElement lblCartao;
	// Escolha cart�o
	@FindBy(xpath = "//span[contains(.,'Final ')]")
	public WebElement lblMsg;
	// Escolha cart�o
	@FindBy(xpath = "//span[contains(.,'Final 2695')]")
	public WebElement lblMsg2;
	
	public void clicarCartao() {
		lblCartao.click();
	}
}