package br.com.bradesco.pupj.pagesweb;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import br.com.bradesco.pupj.utilitarios.UtilsWeb;

public class Limites_ConsultaDeLimite_EmpresasPage extends UtilsWeb {

	@SuppressWarnings("unused")
	
	private WebDriver driverWeb;

	public Limites_ConsultaDeLimite_EmpresasPage(WebDriver driverWeb) {
		this.driverWeb = driverWeb;
		PageFactory.initElements(driverWeb, this);
	}

	// Campo nome do cart�o
	@FindBy(xpath = "//span[contains(.,'EMPRESARIAL PLATINUM VISA')]")
	public WebElement msgNomeCartao;	
	// Campo valor de limite do cart�o
	@FindBy(xpath = "//span[contains(.,'Limite total R$ 30.000,00')]")
	public WebElement msgValorDeLimite;
	// Campo nome do cart�o/CNPJ
	@FindBy(id = "nomePortador")
	public WebElement txtCnpj;
	// Campo nome da Empresa
	@FindBy(id = "nomePortador")
	public WebElement msgNomeEmpresa;
	
}