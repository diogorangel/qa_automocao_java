package br.com.bradesco.pupj.pagesweb;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import br.com.bradesco.pupj.utilitarios.UtilsWeb;

public class Limites_ConsultaDeLimite_ProdutosPage extends UtilsWeb {

	@SuppressWarnings("unused")
	
	private WebDriver driverWeb;

	public Limites_ConsultaDeLimite_ProdutosPage(WebDriver driverWeb) {
		this.driverWeb = driverWeb;
		PageFactory.initElements(driverWeb, this);
	}

	// Campo nome do cart�o
	@FindBy(id = "nomePortador")
	public WebElement txtPortador;
	
	// Campo nome do cart�o Produto
	@FindBy(id = "nomePortador")
	public WebElement txtProduto;
	
	// Campo Compid
	@FindBy(id = "nomePortador")
	public WebElement txtCompid;

	
}