#encoding: Cp1252
####################################################
########## CENARIOS LOGIN PORTAL PJ ################
####################################################
@000_CARTOESPJ-4873TrazerDadosOpenFinance
Feature: CARTOESPJ-3338 Open Finance- Consentimento da Recep��o de Dados

  @CARTOESPJ-4873FecharPopupTrazerDadosOpenFinanceComoGestor
  Scenario Outline: CARTOESPJ-4873 - Fechar Pop Up Trazer Dados do Open Finance
    Given Acessar a pagina do Portal PJ <ambiente> <rede>
    When Efetuo o login <cpf> <senha>
    Then Fecho o Pop-up Trazer Dados Open Finance como Gestor
    And Verifico o Logout com sucesso

    Examples: 
      | cpf          						 | senha      			| ambiente | rede      |
      | "06218670635" | "Br@12345" | "TI"    			 | "empresa" |