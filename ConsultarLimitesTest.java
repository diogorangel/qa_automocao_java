package br.com.bradesco.pupj.runners;

import br.com.bradesco.automacaocartoes.core.ExtendedCucumberRunner;
import br.com.bradesco.pupj.utilitarios.SetUpTearDown;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberOptions.SnippetType;
import org.junit.runner.RunWith;

@RunWith(ExtendedCucumberRunner.class)
@CucumberOptions(
		plugin = { "pretty", 
				"html:target/cucumber-report", 
				"junit:target/cucumber-report/junitResult.xml",
				"json:target/cucumber-report/jsonResult.json" },
		snippets = SnippetType.CAMELCASE, 	
		monochrome = true,
		stepNotifications = true, 
		useFileNameCompatibleName = true,
		features = {"src/test/resources/features/PT/000_CARTOESPJ-3172_Login_PT.feature",
					"src/test/resources/features/PT/004_CARTOESPJ-3186_ConsultaDeLimites_PT.feature" },
		glue = {"br.com.bradesco.pupj.steps", "br.com.bradesco.pupj.utilitarios" }, 
		//tags = {"@004_CARTOESPJ-3186_ConsultarLimites" }
		//tags = {"@ConsultarLimite_PortadorVerificarConsultaDeLimiteDisponivelPortadorHibrido"}
		//tags = {"@ConsultarLimite_GestorVerificarConsultaLimitePositivodoPortador"}
		//tags = {"@ConsultarLimite_GestorVerificaConsultaLimiteDisponiveldoPortadorHibrido"}
		//tags = {"@ConsultarLimite_GestorVerificaConsultaLimiteDisponiveldoPortadorHibridoCFI"}
		//tags = {"@ConsultarLimite_GestorBuscarPortadoresPorNome"}
		//tags = {"@ConsultarLimite_GestorVerificaConsultaLimiteDisponiveldoPortadorCentralizado"}
		//tags = {"@ConsultarLimite_GestorVerificaConsultaLimiteDisponiveldoPortadorCentralizadoCFI"}
		//tags = {"@ConsultarLimite_GestorBuscarPortadoresPorNome"}
		//tags = {"@ConsultarLimite_GestorValidarBotaoVoltar"}
		//tags = {"@ConsultarLimite_GestorBuscarPortadoresPorNomeDoProduto"}
		//tags = {"@ConsultarLimite_GestorVerificarBuscaEmpresasPorCNPJ"}
		//tags = {"@ConsultarLimite_GestorBuscarPortadoresPorNomeDaEmpresa"}
		//tags = {"@ConsultarLimite_GestorVerificarBuscaProdutosPorCompID"}
		//tags = {"@ConsultarLimite_PortadorVerificarConsultaDeLimitePortadorHibrido"}
		//tags = {"@ConsultarLimite_PortadorVerificarConsultaDeLimiteDisponivelPortadorCentralizadoCFI"}
		//tags = {"@ConsultarLimite_PortadorVerificarConsultaDeLimiteTotalPortadorHibridoCFI"}
		//tags = {"@ConsultarLimite_GestorVerificaConsultaLimiteTotaldoPortadorCentralizado"}
		//tags = {"@ConsultarLimite_GestorVerificaLimitesDoPortador"}
		//tags = {"@ConsultarLimite_GestorVerificaConsultaLimiteDisponiveldoPortadorCentralizado"}
		//tags = {"@ConsultarLimite_GestorVerificaConsultaLimiteTotaldoPortadorCentralizadoCFI"}
		//tags = {"@ConsultarLimite_GestorVerificaConsultaLimiteTotaldoPortadorHibrido"}
		//tags = {"@ConsultarLimite_GestorVerificaConsultaLimiteTotaldoPortadorHibridoCFI"}
		//tags = {"@ConsultarLimite_PortadorVerificarConsultaDeLimiteNegativo"}
		//tags = {"@ConsultarLimite_PortadorAtivarOptOut"}
		//tags = {"@ConsultarLimite_PortadorDesativarOptOut"}
		//tags = {"@ConsultarLimite_GestorVerificarBuscaEmpresasComCNPJinvalido"}
		//tags ={"@ConsultarLimite_GestorVerificarBuscaPortadorComCPFInvalido"}
		//tags = {"@ConsultarLimite_GestorVerificaDataeHoraCorretaConsultadeLimites"}
		//tags ={"@ConsultarLimite_GestorVerificaDataeHoraCorretaConsultadeLimitesdateladelimitesdoPortador"}
		//tags = {"@ConsultarLimite_GestorVerificarConsultaEValidarDetalhesDoPortador"}
		//tags = {"@ConsultarLimite_GestorVerificarConsultaLimiteEmpresaRepresentanteEstrangeiro"}
		//tags = {"@ConsultarLimite_PortadorVerificaDetalhesdeLimies"}
		//tags = {"@ConsultarLimite_GestorVerificarConsultaLimiteComoRepresentanteEstrangeirodoPortadorHibrido"}
		//tags = {"@ConsultarLimite_GestorVerificarConsultaLimiteComoRepresentanteEstrangeirodoPortadorHibridoCFI"}
		//tags = {"@ConsultarLimite_GestorVerificarConsultaLimiteComoRepresentanteEstrangeirodoPortadorCentralizado"}
		//tags = {"@ConsultarLimite_GestorVerificarConsultaLimiteComoRepresentanteEstrangeirodoPortadorCentralizadoCFI"}
		tags = {"@ConsultarLimite_RepresentanteEstrangeiroVerificaLimitesDetalhesdoPortador"}
		//tags = {"@ConsultarLimite_GestorVerificarBuscaProdutosComDadosInexistentes"}		
		//tags = {"@ConsultarLimite_GestorVerificarBuscaEmpresasComDadosInexistentes"}
		//tags = {"@ConsultarLimite_GestorVerificarBuscaEmpresasPorCNPJ"}
		//tags = {"@ConsultarLimite_GestorFiltrarPorBandeiraAConsultaDeLimiteDePortadoresElo"}
		//tags = {"@ConsultarLimite_GestorFiltrarPorBandeiraAConsultaDeLimiteDePortadoresVisa"}
		//tags = {"@ConsultarLimite_GestorFiltrarPorBandeiraAConsultaDeLimiteDePortadoresAmex"}
		//tags = {"@ConsultarLimite_GestorFiltrarPorBandeiraAConsultaDeLimiteDePortadoresMastercard"}
		//tags = {"@ConsultarLimite_PortadorVerificarConsultaDeLimitePositivo"}
		//tags = {" @ConsultarLimite_GestorVerificarConsultaEValidarDetalhesDoPortador"}
		//tags = {"@ConsultarLimite_GestorFiltrarPorBandeiraAConsultaDeLimiteDePortadores" }
		//tags = { "@ConsultarLimite_GestorVerificarConsultaLimiteNegativodoPortador" }		
	    //tags = {"@ConsultarLimite_PortadorVerificarConsultaDeLimitePositivo,@ConsultarLimite_PortadorVerificarConsultaDeLimiteNegativo,@ConsultarLimite_GestorVerificarConsultaLimitePositivodoPortador,@ConsultarLimite_GestorVerificarConsultaLimiteNegativodoPortador"}
)

public class ConsultarLimitesTest extends SetUpTearDown {

}